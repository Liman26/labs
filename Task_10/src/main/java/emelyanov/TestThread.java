package emelyanov;

import static java.lang.Thread.sleep;

/**
 * Тестовый класс в котором методы для демонстрации каждого из возможных состояний потока
 */
public class TestThread {
    private Thread thread;
    private final Object block = new Object();

    /**
     * Получить текущее состояние потока
     * @return состояние потока или Undefined, если поток не создан
     */
    public String getStateThread() {
        if (thread == null) {
            return "Undefined";
        } else {
            return thread.getState().toString();
        }
    }

    /**
     * Создать поток, состояние NEW
     */
    public void newThread() {
        thread = new Thread(() -> System.out.println("Thread run and work"));
    }

    /**
     * Создать поток, состояние Runnable
     */
    public void runnableThread() {
        thread = new Thread(() -> {
            System.out.println("Thread run and work");
            while (true) {}
        });
        thread.start();
    }

    /**
     * Создать поток, состояние Waiting
     */
    public void waitingThread() {
        thread = new Thread(() -> {
            System.out.println("Thread run and work");
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    /**
     * Создать поток, состояние TimeWaiting
     */
    public void timeWaitingThread() {
        thread = new Thread(() -> {
            try {
                System.out.println("Thread run and work");
                sleep(Long.MAX_VALUE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }

    /**
     * Создать поток, состояние Terminated
     */
    public void terminatedThread() {
        thread = new Thread(() -> System.out.println("Thread run and work"));
        thread.start();
    }

    /**
     * Создать поток, состояние Blocked
     */
    public void blockedThread() {
        thread = new Thread(() -> {
            System.out.println("Thread run and work 1");
            synchronized (block) {
                while (true) {}
            }
        });
        thread.start();
        thread = new Thread(() -> {
            System.out.println("Thread run and work 2");
            synchronized (block) {
                while (true) {}
            }
        });
        thread.start();
    }
}
