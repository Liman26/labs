package emelyanov;

public class Task10 {
    public static void main(String[] args) throws InterruptedException {
        TestThread testThread = new TestThread();
        System.out.println(testThread.getStateThread());
        testThread.newThread();
        System.out.println(testThread.getStateThread());
        testThread.runnableThread();
        Thread.sleep(100);
        System.out.println(testThread.getStateThread());
        testThread.waitingThread();
        Thread.sleep(100);
        System.out.println(testThread.getStateThread());
        testThread.timeWaitingThread();
        Thread.sleep(100);
        System.out.println(testThread.getStateThread());
        testThread.terminatedThread();
        Thread.sleep(100);
        System.out.println(testThread.getStateThread());
        testThread.blockedThread();
        Thread.sleep(100);
        System.out.println(testThread.getStateThread());
        Thread.sleep(100);
        System.exit(0);
    }
}
