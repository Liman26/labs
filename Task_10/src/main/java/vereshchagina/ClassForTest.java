package vereshchagina;

/**
 * Тестовый класс, содержащий методы для демонстрации каждого из возможных состояний потока
 */
public class ClassForTest {

    /**
     * вспомогательный метод, переводит поток в состояние сна на 3 секунды
     */
    public synchronized void methodWithSleep() {

        try {
            Thread.sleep(3000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("finish");
    }

    /**
     * Перевод потока в состояние NEW
     * Поток находится в состоянии NEW сразу после его создания
     *
     * @return состояние в формате строки
     */
    public static String testStateNew() {
        Thread thread = new Thread();
        return thread.getState().toString();
    }

    /**
     * Перевод потока в состояние RUNNABLE
     * (запуск start())
     *
     * @return состояние в формате строки
     */
    public static String testStateRunnable() {
        Thread thread = new Thread();
        thread.start();
        return thread.getState().toString();
    }


    /**
     * Перевод потока в состояние WAITING способ 1
     * перевожу поток в состояние ожидания с помощью метода wait()
     *
     * @return состояние в формате строки
     */
    public String testStateWaiting1() throws InterruptedException {
        Thread thread = new Thread(() -> {
            synchronized (this) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        Thread.sleep(10);

        return thread.getState().toString();
    }

    /**
     * Перевод потока в состояние  WAITING способ 2
     * перевожу поток в состояние ожидания с помощью метода join()
     * thread2 находится в состоянии WAITING, пока не выполнится thread1,
     *
     * @return состояние в формате строки
     */
    public String testStateWaiting2() throws InterruptedException {
        Thread thread1 = new Thread(this::methodWithSleep);
        Thread thread2 = new Thread(() -> {
            thread1.start();
            try {
                thread1.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("finish!!");
        });
        thread2.start();
        Thread.sleep(10);
        return thread2.getState().toString();
    }

    /**
     * Перевод потока в состояние  TIMED WAITING способ 1
     * просто усыпляю с помощью sleep
     *
     * @return состояние в формате строки
     */
    public String testStateTimedWaiting1() throws InterruptedException {
        Thread thread = new Thread(this::methodWithSleep);
        thread.start();
        Thread.sleep(10);
        return thread.getState().toString();
    }

    /**
     * Перевод потока в состояние  TIMED WAITING способ 2
     * использую метод join(t)
     * thread2 находится в состоянии TIMED WAITING в течение t/1000 секунд, пока выполняется thread1
     *
     * @return состояние в формате строки
     */
    public String testStateTimedWaiting2() throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            while (true) {
            }
        });
        Thread thread2 = new Thread(() -> {
            thread1.start();
            try {
                thread1.join(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("finish!!");
        });
        thread2.start();
        Thread.sleep(10);
        return thread2.getState().toString();
    }

    /**
     * Перевод потока в состояние TERMINATED
     * это состояние завершенного потока
     * вызваю join(), чтобы созданный потом выполнился в первую очередь
     *
     * @return состояние в формате строки
     */
    public String testStateTerminated() throws InterruptedException {
        Thread thread = new Thread();
        thread.start();
        thread.join();
        Thread.sleep(10);
        return thread.getState().toString();
    }

    /**
     * Перевод потока в состояние  BLOCKED
     * thread2 пытается получить доступ к синхронизированному методу, когда его занимает thread1
     *
     * @return состояние в формате строки
     */
    public String testStateBlocked() throws InterruptedException {

        Thread thread1 = new Thread(this::methodWithSleep);
        Thread thread2 = new Thread(this::methodWithSleep);
        thread1.start();
        thread2.start();
        Thread.sleep(10);
        return thread2.getState().toString();
    }
}
