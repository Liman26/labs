package gushchin;

import gushchin.custom_threads.*;

/**
 * Фабрика для генерации кастомных потоков для демонстрации
 */
public class CustomThreadFactory {

    /**
     * Метод, возвращающий NEW поток
     *
     * @return - поток в состоянии NEW
     */
    public static Thread getNewThread() {
        return new NewThread();
    }

    /**
     * Метод, возвращающий TERMINATED поток
     *
     * @return - поток в состоянии TERMINATED
     */
    public static Thread getTerminatedThread() {
        Thread thread = new TerminatedThread();
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return thread;
    }

    /**
     * Метод, возвращающий RUNNABLE поток
     *
     * @return - поток в состоянии RUNNABLE
     */
    public static Thread getRunnableThread() {
        Thread thread = new RunnableThread();
        thread.start();
        return thread;
    }

    /**
     * Метод, возвращающий WAITING поток
     *
     * @return - поток в состоянии WAITING
     */
    public static Thread getWaiting() {
        Thread thread = new WaitingThread();
        thread.start();

        while (thread.getState() != Thread.State.WAITING) {
            trySleepThread();
        }

        return thread;
    }

    /**
     * Метод, возвращающий TIMED_WAITING поток
     *
     * @return - поток в состоянии TIMED_WAITING
     */
    public static Thread getTimedWaitingThread() {
        Thread thread = new TimedWaitingThread();
        while (thread.getState() != Thread.State.TIMED_WAITING) {
            trySleepThread();
        }
        return thread;
    }

    /**
     * Метод, возвращающий BLOCKED поток
     *
     * @return - поток в состоянии BLOCKED
     */
    public static Thread getBlockedThread() {
        Thread thread1 = new BlockedThread();
        Thread thread2 = new BlockedThread();

        thread1.start();
        thread2.start();

        while ((thread1.getState() != Thread.State.BLOCKED) && (thread2.getState() != Thread.State.BLOCKED)) {
            trySleepThread();
        }

        if (thread1.getState() == Thread.State.BLOCKED) {
            return thread1;
        } else {
            return thread2;
        }
    }

    /**
     * Приватный утилитный метод для ввода потока с состояние сна на 1000 мс.
     * В случае неудачи в стек трейс выбрасывается исключение InterruptedException
     */
    private static void trySleepThread() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
