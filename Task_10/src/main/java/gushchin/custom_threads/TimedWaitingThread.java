package gushchin.custom_threads;

public class TimedWaitingThread extends Thread{
    @Override
    public void run() {
        System.out.println("TIMED_WAITING");
        while (true) {
            try {
                Thread.sleep(100_000_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
