package mescheryakov;

/**
 * Вспомогательный класс, включающий методы synchronized
 */
public class SynchronizedMethods {

    /**
     * Метод, выполняющий wait() ля потока.
     * Будет использован для получения состояния потока "waiting"
     */
    public synchronized void waitThread() {
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод, выполняющий sleep() на 3000 мс, т.е. 3 секунды.
     * Будет использован для получения состояния потока "timed waiting" и "blocked"
     */
    public synchronized void sleepThread() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
