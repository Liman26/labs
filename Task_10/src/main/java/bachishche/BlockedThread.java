package bachishche;

/**
 * Класс потока, экземпляры которого используют в методе run
 * статический метод, находящийся внутри блока synchronize.
 * Таким образом, мьютексом является сам класс и все его потоки
 * получат статус BLOCKED, если другой объект класса
 * выполняется параллельно
 */
public class BlockedThread extends Thread {
    @Override
    public void run() {
        synchMethod();
    }

    public static synchronized void synchMethod() {
        try {
            sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

