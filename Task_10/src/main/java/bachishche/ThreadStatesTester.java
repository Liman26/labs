package bachishche;

import static java.lang.Thread.sleep;

/**
 * Тестовый класс, содержащий методы для демонстрации
 * каждого из возможных состояний потока
 */
public class ThreadStatesTester {
    /**
     * Метод, позволяющий получить поток с состоянием
     * NEW - объект класса Thread создан, но еще не запущен
     *
     * @return теукщее состояние потока
     */
    public static Thread.State getNewState() {
        return new Thread(() -> {
        }).getState();
    }

    /**
     * Метод, позволяющий получить поток с состоянием
     * RUNNABLE - объект класса Thread, готовый к запуску
     * или запустившийся
     *
     * @return теукщее состояние потока
     */
    public static Thread.State getRunnableState() {
        Thread tst = new Thread(() -> {
        });
        tst.start();
        return tst.getState();
    }

    /**
     * Метод, позволяющий получить поток с состоянием
     * BLOCKED - объект класса Thread, выполнение которого приостановлено
     * при попытке выполнить задачу, которая не может быть
     * завершена в данный момент времени
     *
     * @return теукщее состояние потока
     */
    public static Thread.State getBlockedState() throws InterruptedException {
        BlockedThread tst1 = new BlockedThread();
        BlockedThread tst2 = new BlockedThread();
        tst1.start();
        tst1.join(10);
        tst2.start();
        tst2.join(10);
        return tst2.getState();
    }

    /**
     * Метод, позволяющий получить поток с состоянием
     * WAITING - объект класса Thread, ожидающий выполнений другого
     * потока, связанного условием
     * Метод wait(), приводящий поток t в состояние waiting, вызывается через
     * реализованный как synchronized run
     * Для того, чтобы это метод отработал до return главного потока -
     * вызывается метод join(millis)
     *
     * @return теукщее состояние потока
     */
    public static Thread.State getWaitingState() throws InterruptedException {
        Runnable thread = new Runnable() {
            @Override
            public synchronized void run() {
                try {
                    wait();
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread tst1 = new Thread(thread);
        tst1.start();
        tst1.join(100);
        return tst1.getState();
    }

    /**
     * Метод, позволяющий получить поток с состоянием
     * TIMED_WAITING - объект класса Thread, ожидающий
     * завершение метода со счетчиком времени
     * С помощью join ожидаем, пока поток t начнет работу и в
     * методе start вызовет sleep(timeout). Пока не истечет
     * timeout, поток t будет иметь статус TimedWaiting
     *
     * @return теукщее состояние потока
     */
    public static Thread.State getTimedWaitingState() throws InterruptedException {
        Runnable thread = () -> {
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        Thread tst1 = new Thread(thread);
        tst1.start();
        tst1.join(100);
        return tst1.getState();
    }

    /**
     * Метод, позволяющий получить поток с состоянием
     * TERMINATE - объект класса Thread, завершивший свою работу
     * Команда t.join() присотанавливает выполнение текущего
     * потока, пока не будет завершён поток t
     *
     * @return теукщее состояние потока
     */
    public static Thread.State getTerminatedState() throws InterruptedException {
        Thread tst1 = new Thread(() -> {
        });
        tst1.start();
        tst1.join();
        return tst1.getState();
    }
}
