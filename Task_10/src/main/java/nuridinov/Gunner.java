package nuridinov;

/**
 * Класс канонира, который перезаряжает орудие
 */
public class Gunner extends Thread {
    Tank tank;

    Gunner(Tank tank) {
        this.tank = tank;
    }

    @Override
    public void run() {
        tank.reload();
    }
}
