package nuridinov;

/**
 * Класс наводчика, который произваодит выстрел
 */
public class Aimer extends Thread {
    Tank tank;

    Aimer(Tank tank) {
        this.tank = tank;
    }

    @Override
    public void run() {
        tank.shot();
    }
}
