package nuridinov;

/**
 * класс танк с двумя синхронизированными методами, они синхронизированны, потому что
 * нельзя одновременно стрелять и заряжать орудие
 */
public class Tank {
    private int ammo = 0;

    /**
     * метод выстрел из орудия
     */
    public synchronized void shot() {
        if (ammo == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        ammo--;
        notify();
    }

    /**
     * метод перезарядка орудия
     */
    public synchronized void reload() {
        if (ammo > 3) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        ammo++;
        notify();
    }
}
