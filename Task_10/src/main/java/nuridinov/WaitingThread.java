package nuridinov;

/**
 * класс необходимый для демонстации состояния TIMED_WAITING
 */
public class WaitingThread extends Thread{
    @Override
    public void run() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
