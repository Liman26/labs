package nuridinov;

/**
 * Класс необходимый для демонстрации состояния Потока BLOCKED
 */
public class BlockedThread extends Thread {
    @Override
    public void run() {
        blockDemonstration();
    }

    /**
     * используя этот метод можно ввести поток в состояние BLOCKED
     */
    private static synchronized void blockDemonstration() {
        System.out.println("Один из потоков будет заблокирован");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

}
