package koleda;

public class TestThread {

    /**
     * Ставим потом в состояние сна на 3 секунды
     * Будет использован для получения состояния потока "timed waiting" и "blocked"
     */
    public synchronized void sleepThread() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для получение состояния потока Wait
     * будет использован для получения состояния потока waiting
     */
    public synchronized void waitingThread() {
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
