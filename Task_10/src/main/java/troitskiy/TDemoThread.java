package troitskiy;

/**
 * Задача на использование:
 * 1. new Thread()
 * 2. thread.start()
 * 3. thread.join()
 * 4. Object.notify()
 * 5. Object.wait()
 * 6. Object.notifyAll()
 * 7. synchronized
 *
 * 8. thread.interrupt()
 * 9. thread.sleep()
 * 10. thread.yield()
 * ..... (тут остальные методы класса Thread)
 *
 *
 * Ваша задача сделать тестовый класс в котором будут методы для демонстрации каждого из возможных состояний потока
 * То есть: задача - добиться перевода потока в одно из состояний на картинке и получить состояние через метод thread.getState() и проверить ассертом
 * спользуйте выше перечисленные методы/операторы. По сути это все что есть в классе Thread и в Object для работы с потоками + synchronized
 *
 *
 * Всего будет 6 тестов на 6 состояний
 */
public class TDemoThread
{
    /**
     * метод для демонстрации  состояния  потока NEW
     * @return - поток в состоянии NEW
     */
    public static Thread GetNew() {
        return new TThread(TThread.TScenariy.PUSTOY);
    }

    /**
     * метод для демонстрации  состояния  потока TERMINATED
     * @return - поток в состоянии TERMINATED
     */
    public static Thread GetTerminated() {
        Thread thread =  new TThread(TThread.TScenariy.PUSTOY);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return thread;
    }

    /**
     * метод для демонстрации  состояния  потока RUNNABLE
     * @return - поток в состоянии RUNNABLE
     */
    public static Thread GetRunnable() {
        Thread thread = new TThread(TThread.TScenariy.BESKONECHNIY);
        thread.start();
        return thread;
    }

    /**
     * метод для демонстрации  состояния  потока TIMED_WAITING
     * @return - поток в состоянии TIMED_WAITING
     */
    public static Thread GetTimedWaiting()  {
        Thread thread =  new TThread(TThread.TScenariy.SLEEPING);
        thread.start();

        while (thread.getState() != Thread.State.TIMED_WAITING) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return thread;
    }

    /**
     * метод для демонстрации  состояния  потока WAITING
     * @return - поток в состоянии WAITING
     */
    public static Thread GetWaiting()  {
        Thread thread =  new TThread(TThread.TScenariy.WAITING);
        thread.start();

        while (thread.getState() != Thread.State.WAITING) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return thread;
    }



    /**
     * метод для демонстрации  состояния  потока BLOCKED
     * @return - поток в состоянии BLOCKED
     */
    public static Thread GetBlocked()  {
        Thread thread1 =  new TThread(TThread.TScenariy.SINCHRINISING);
        thread1.start();

        Thread thread2 =  new TThread(TThread.TScenariy.SINCHRINISING);
        thread2.start();

        while ((thread1.getState() != Thread.State.BLOCKED)  && (thread2.getState() != Thread.State.BLOCKED)) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        if (thread1.getState() == Thread.State.BLOCKED)  return thread1;
        else                                             return thread2;
    }
}
