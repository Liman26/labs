package troitskiy;

/**
 * Вспомогательный класс для демонстрации
 * каждого из возможных состояний потока
 *
 */
public class TThread extends Thread
{
    public enum TScenariy {PUSTOY, BESKONECHNIY, SLEEPING, WAITING, SINCHRINISING}

    private final TScenariy scenariy;

    /**
     * Конструктор с выбором сценария для этого потока
     * @param scenariy - номер сценария
     */
    public TThread(TScenariy scenariy) {
        this.scenariy = scenariy;
    }

    /**
     * Функция потока, реализует заданый сценарий работы
     */
    @Override
    public void run() {
        super.run();

        switch (scenariy){
            case BESKONECHNIY:
                while (true) {  }
            case SLEEPING:
                while (true) {
                    try {
                        Thread.sleep(100000000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            case WAITING:
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case SINCHRINISING:
                synchronized ( this.getClass() )
                {
                    while (true)
                    {
                    }
                }
        }

    }
}

