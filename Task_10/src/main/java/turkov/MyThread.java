package turkov;

/**
 * Класс, который используется для создания потоков и демонстрации всех 6 состояний их жизненного цикла
 * (NEW, RUNNABLE, WAITING, TIME_WAITING, BLOCKED, TERMINATED).
 * Для случаев паралллельной работы нескольких потоков указан явный монитор "lock".
 */
public class MyThread implements Runnable {

    private final Object lock = new Object();

    @Override
    public void run() {
        synchronized (lock) {
            try {
                Thread.sleep(1000);
                lock.wait();
            } catch (InterruptedException e) {
                System.err.println("My thread interrupted");
            }
        }
    }
}
