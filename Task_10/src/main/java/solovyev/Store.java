package solovyev;

/**
 * Класс реализующий магазин на складе которого хранится не больше четырех товаров
 */
public class Store {

    /**
     * Приватное поле характеризует количество товара на складе
     */
    private int product = 2;

    /**
     * Сихронизированный метод get(), заставляет покупателя ждать, если на складе 0 товаров
     */
    public synchronized void get() {
        while (product < 1) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        product--;
        System.out.println("one piece was purchased");
        System.out.println("on the warehouse: " + product);
        notify();
    }

    /**
     * Синхронизированный метод put(), заставляет поставщика ждать, если на складе 4 товара
     */
    public synchronized void put() {
        while (product > 3) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        product++;
        System.out.println("one piece arrived");
        System.out.println("on the warehouse: " + product);
        notify();
    }
}
