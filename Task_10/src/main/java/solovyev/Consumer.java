package solovyev;

/**
 * Класс потребитель
 * Покупает 2 товара
 */
public class Consumer extends Thread {
    Store store;

    Consumer(Store store) {
        this.store = store;
    }

    @Override
    public void run() {
        System.out.println("consumer " + getName());
        store.get();
        store.get();
    }
}
