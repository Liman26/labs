package solovyev;

/**
 * Класс производитель,
 * 8 раз поставляет товар
 */
public class Producer extends Thread {
    Store store;

    Producer(Store store) {
        this.store = store;
    }

    @Override
    public void run() {
        System.out.println("producer " + getName());
        for (int i = 0; i < 8; i++) {
            store.put();
        }
    }
}
