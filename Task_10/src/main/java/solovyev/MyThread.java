package solovyev;

/**
 * Класс реализующий простой поток
 */
public class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println(getName() + " started.");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(getName() + " ended.");
    }
}
