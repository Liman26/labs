package solovyev;

/**
 * Класс реализующий поток используемый для демонстрации состояния BLOCKED
 * Потоки этого клсса запускают синхронизированный метод LetsWait(),
 * поэтому они блокируются пока стартовавший поток не завершит свою работу.
 */
public class BlockedThread extends Thread {
    @Override
    public void run() {
        LetsWait();
    }

    /**
     * Синхронизированный метод, заставляет поток заснуть на 10 секунд,
     * тем самым заставляя остальные потоки класса BlockedThread перейти
     * в состояние BLOCKED
     */
    public static synchronized void LetsWait() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
