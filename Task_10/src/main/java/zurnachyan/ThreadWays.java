package zurnachyan;

/**
 * Перечисление возможных действий потока, для демонстрации различных его состояний
 */
public enum ThreadWays {
    TO_RUN, TO_WAIT, TO_NOTIFY, TO_SLEEP, TO_BLOCK
}
