package zurnachyan;

/**
 * Класс-поток, который имеет возможность попасть во все возможные состояния
 */
public class MyThread extends Thread{
    private static final Object objectForBlock = new Object();

    private ThreadWays way;

    public MyThread(){}

    public MyThread(ThreadWays way){
        this.way = way;
    }

    /**
     * Переопределенный метод, выполняется при запуске потока, имеет несколько путей выполнения
     * для того, чтоб попадать в различные состояния
     */
    @Override
    public void run(){
        System.out.println(this.getName() + " is running");

        switch (way) {
            case TO_RUN -> toRun();
            case TO_WAIT -> toWait();
            case TO_NOTIFY -> notify();
            case TO_SLEEP -> toSleep();
            case TO_BLOCK -> toBlock();
        }
    }


    /**
     * Метод для демонстрации состояния RUNNABLE и TERMINATED
     */
    private void toRun(){
        for (int i = 0; i < 100_000_000; i++){}
    }

    /**
     * Метод для демонстрации состояния WAITING
     */
    private void toWait(){
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для демонстрации состояния TIMED_WAITING
     */
    private void toSleep(){
        try {
            sleep(3_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для демонстрации состояния BLOCKED
     */
    private void toBlock() {
        synchronized (objectForBlock) {
            try {
                sleep(3_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
