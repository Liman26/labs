package zurnachyan;

import static java.lang.Thread.*;
import static zurnachyan.ThreadWays.*;

/**
 * Тестовый класс, в котором содержатся методы для введения потока в кадое из возможных состояний
 */
public class ThreadStates {
    private static MyThread thread;
    private static MyThread secondThread;

    /**
     * Первый метод вводит поток в состояние NEW
     * @return состояние потока
     */
    public static State stateNew(){
        thread = new MyThread();
        return thread.getState();
    }

    /**
     * Второй метод вводит поток в состояние RUNNABLE
     * @return состояние потока
     */
    public static State stateRunnable(){
        thread = new MyThread(TO_RUN);
        thread.start();
        return thread.getState();
    }

    /**
     * Третий метод вводит поток в состояние WAITING
     * @return состояние потока
     */
    public static State state_Waiting(){
        State state = State.NEW;
        thread = new MyThread(TO_WAIT);
        secondThread = new MyThread(TO_NOTIFY);
        thread.start();
        while(state != State.WAITING)
            state = thread.getState();
        secondThread.start();
        return state;
    }

    /**
     * Четвертый метод вводит поток в состояние TIMED_WAITING
     * @return состояние потока
     */
    public static State state_TimedWaiting(){
        State state = State.NEW;
        thread = new MyThread(TO_SLEEP);
        thread.start();
        while(state != State.TIMED_WAITING)
            state = thread.getState();
        return state;
    }

    /**
     * Пятый метод вводит поток в состояние BLOCKED
     * @return состояние потока
     */
    public static State stateBlocked(){
        State state = State.NEW;
        thread = new MyThread(TO_BLOCK);
        secondThread = new MyThread(TO_BLOCK);
        secondThread.start();
        thread.yield();
        thread.start();
        while(state != State.BLOCKED){
            state = thread.getState();
        }
        return state;
    }

    /**
     * Шестой метод вводит поток в состояние TERMINATED
     * @return состояние потока
     */
    public static State stateTerminated(){
        thread = new MyThread(TO_RUN);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return thread.getState();
    }
}



