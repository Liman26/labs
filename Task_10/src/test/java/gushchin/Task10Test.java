package gushchin;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Task10Test {
    @Test
    public void getNewTest() {
        assertEquals(CustomThreadFactory.getNewThread().getState(), Thread.State.NEW);
    }

    @Test
    public void getTerminatedTest() {
        assertEquals(CustomThreadFactory.getTerminatedThread().getState(), Thread.State.TERMINATED);
    }

    @Test
    public void getRunnableTest() {
        assertEquals(CustomThreadFactory.getRunnableThread().getState(), Thread.State.RUNNABLE);
    }

    @Test
    public void getWaitingTest() {
        assertEquals(CustomThreadFactory.getWaiting().getState(), Thread.State.WAITING);
    }

    @Test
    public void getTimedWaitingTest() {
        assertEquals(CustomThreadFactory.getTimedWaitingThread().getState(), Thread.State.TIMED_WAITING);
    }

    @Test
    public void getBlockedTest() {
        assertEquals(CustomThreadFactory.getBlockedThread().getState(), Thread.State.BLOCKED);
    }
}
