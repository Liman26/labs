package zurnachyan;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс в котором тестируются все методы, созданные в данной лабораторной
 */
public class TestStates {
    /**
     * Тест на состояние NEW
     */
    @Test
    public void test1(){
        assertEquals(Thread.State.NEW, ThreadStates.stateNew());
    }
    /**
     * Тест на состояние RUNNABLE
     */
    @Test
    public void test2(){
        assertEquals(Thread.State.RUNNABLE, ThreadStates.stateRunnable());
    }
    /**
     * Тест на состояние WAITING
     */
    @Test
    public void test3(){
        assertEquals(Thread.State.WAITING, ThreadStates.state_Waiting());
    }
    /**
     * Тест на состояние TIMED_WAITING
     */
    @Test
    public void test4(){
        assertEquals(Thread.State.TIMED_WAITING, ThreadStates.state_TimedWaiting());
    }
    /**
     * Тест на состояние BLOCKED
     */
    @Test
    public void test5(){
        assertEquals(Thread.State.BLOCKED, ThreadStates.stateBlocked());
    }
    /**
     * Тест на состояние TERMINATED
     */
    @Test
    public void test6(){
        assertEquals(Thread.State.TERMINATED, ThreadStates.stateTerminated());
    }
}
