package mescheryakov;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Класс, содержащий тесты каждого состояния потока
 */
public class ThreadStates {

    /**
     * Метод, показывающий состояние потока "new"
     */
    @Test
    public void newTest() {
        Thread thread = new Thread();
        assertEquals(Thread.State.NEW, thread.getState());
    }

    /**
     * Метод, показывающий состояние потока "runnable".
     * Поток переходит в это состояние после использования start()
     */
    @Test
    public void runnableTest() {
        Thread thread = new Thread();
        thread.start();
        assertEquals(Thread.State.RUNNABLE, thread.getState());
    }

    /**
     * Метод, показывающий состояние потока "waiting".
     * Для получения этого состояния мы отправляем в "сон" главный поток,
     * тогда вспомогательный поток, созданный с использованием метода waitThread(),
     * перейдет в состояние "waiting"
     */
    @Test
    public void waitingTest() {
        SynchronizedMethods object = new SynchronizedMethods();
        Thread thread = new Thread(object::waitThread);

        thread.start();
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(Thread.State.WAITING, thread.getState());

    }

    /**
     * Метод, показывающий состояние потока "timed waiting".
     * Для получения этого состояния мы отправляем в "сон" главный поток,
     * тогда вспомогательный поток, созданный с использованием метода sleepThread(),
     * перейдет в состояние "timed waiting" на 3 секунды
     */
    @Test
    public void timedWaitingTest() {
        SynchronizedMethods object = new SynchronizedMethods();
        Thread thread = new Thread(object::sleepThread);

        thread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(Thread.State.TIMED_WAITING, thread.getState());
    }

    /**
     * Метод, показывающий состояние потока "terminated".
     * Поток переходит в это состояние после использования join()
     */
    @Test
    public void terminatedTest() {
        Thread thread = new Thread();
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(Thread.State.TERMINATED, thread.getState());
    }

    /**
     * Метод, показывающий состояние потока "blocked".
     * Для этого создаем 2 потока с использованием метода sleepThread(), данный метод
     * synchronized, поэтому первый поток заблокирует mutex класса SynchronizedMethods.
     * Главный поток отправляем в "сон", чтобы отследить состояние "blocked" второго потока
     */
    @Test
    public void blockedTest() {
        SynchronizedMethods object = new SynchronizedMethods();
        Runnable runnable = object::sleepThread;
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);

        thread1.start();
        thread2.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(Thread.State.BLOCKED, thread2.getState());
    }
}
