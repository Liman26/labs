package bachishche;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс-тестировщик методов, каждый из которых приводит потоки в одно из
 * состояний  NEW, RUNNABLE, BLOCKED, WAITING, TIMED_WAITING и TERMINATED.
 */
public class ThreadStatesTesterTest {
    /**
     * Тест, фиксирующий поток в состоянии NEW
     */
    @Test
    void stateNewTest() {
        assertEquals(Thread.State.NEW, ThreadStatesTester.getNewState());
    }

    /**
     * Тест, фиксирующий поток в состоянии RUNNABLE
     */
    @Test
    void stateRunnableTest() {
        assertEquals(Thread.State.RUNNABLE, ThreadStatesTester.getRunnableState());
    }

    /**
     * Тест, фиксирующий поток в состоянии BLOCKED
     */
    @Test
    void stateBlockedTest() throws InterruptedException {
        assertEquals(Thread.State.BLOCKED, ThreadStatesTester.getBlockedState());
    }

    /**
     * Тест, фиксирующий поток в состоянии WAITING
     */
    @Test
    void stateWaitingTest() throws InterruptedException {
        assertEquals(Thread.State.WAITING, ThreadStatesTester.getWaitingState());
    }

    /**
     * Тест, фиксирующий поток в состоянии TIMED_WAITING
     */
    @Test
    void stateTimedWaitingTest() throws InterruptedException {
        assertEquals(Thread.State.TIMED_WAITING, ThreadStatesTester.getTimedWaitingState());
    }

    /**
     * Тест, фиксирующий поток в состоянии TERMINATED
     */
    @Test
    void stateTerminatedTest() throws InterruptedException {
        assertEquals(Thread.State.TERMINATED, ThreadStatesTester.getTerminatedState());
    }
}
