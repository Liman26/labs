package koleda;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Task10Test {

    /**
     * Метод, показывающий, что поток находится в состоянии NEW
     */
    @Test
    void newTest() {
        Thread thread = new Thread();
        assertEquals(Thread.State.NEW, thread.getState());
    }

    /**
     * Метод, показывающий, что поток находится в состоянии RUNNABLE
     */
    @Test
    void runnableTest() {
        Thread thread = new Thread();
        thread.start();
        assertEquals(Thread.State.RUNNABLE, thread.getState());
    }

    /**
     * Метод, показывающий, что поток находится в состоянии WAITING
     */
    @Test
    void waitingTest() throws InterruptedException {
        TestThread testThread = new TestThread();
        Thread thread = new Thread(testThread::waitingThread);
        thread.start();
        thread.sleep(50);
        assertEquals(Thread.State.WAITING, thread.getState());
    }

    /**
     * Метод, показывающий, что поток находится в состоянии TIMED_WAITING
     */
    @Test
    void timedWaitingTest() throws InterruptedException {
        TestThread testThread = new TestThread();
        Thread thread = new Thread(testThread::sleepThread);
        thread.start();
        thread.sleep(1000);
        assertEquals(Thread.State.TIMED_WAITING, thread.getState());
    }

    /**
     * Метод, показывающий, что поток находится в состоянии TERMINATED
     */
    @Test
    void terminatedTest() throws InterruptedException {
        Thread thread = new Thread();
        thread.start();
        thread.join();
        assertEquals(Thread.State.TERMINATED, thread.getState());
    }

    /**
     * Метод, показывающий, что поток находится в состоянии BLOCKED
     */
    @Test
    void blockedTest() throws InterruptedException {
        TestThread testThread = new TestThread();
        Runnable runnable = testThread::sleepThread;
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
        Thread.sleep(1000);
        assertEquals(Thread.State.BLOCKED, thread2.getState());
    }

}
