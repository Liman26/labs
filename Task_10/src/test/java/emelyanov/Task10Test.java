package emelyanov;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Task10Test {

    private static TestThread testThread;

    @BeforeAll
    public static void beforeAll() {
        testThread = new TestThread();
    }

    @Test
    @DisplayName("Проверка на состояние NEW")
    public void test1() {
        testThread.newThread();
        assertEquals("NEW", testThread.getStateThread());
    }

    @Test
    @DisplayName("Проверка на состояние RUNNABLE")
    public void test2() throws InterruptedException {
        testThread.runnableThread();
        Thread.sleep(100);
        assertEquals("RUNNABLE", testThread.getStateThread());
    }

    @Test
    @DisplayName("Проверка на состояние WAITING")
    public void test3() throws InterruptedException {
        testThread.waitingThread();
        Thread.sleep(100);
        assertEquals("WAITING", testThread.getStateThread());
    }

    @Test
    @DisplayName("Проверка на состояние TIMED_WAITING")
    public void test4() throws InterruptedException {
        testThread.timeWaitingThread();
        Thread.sleep(100);
        assertEquals("TIMED_WAITING", testThread.getStateThread());
    }

    @Test
    @DisplayName("Проверка на состояние TERMINATED")
    public void test5() throws InterruptedException {
        testThread.terminatedThread();
        Thread.sleep(100);
        assertEquals("TERMINATED", testThread.getStateThread());
    }

    @Test
    @DisplayName("Проверка на состояние BLOCKED")
    public void test6() throws InterruptedException {
        testThread.blockedThread();
        Thread.sleep(100);
        assertEquals("BLOCKED", testThread.getStateThread());
    }

}
