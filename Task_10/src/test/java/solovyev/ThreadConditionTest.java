package solovyev;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс демонстрирующий все состояния потока
 * NEW, RUNNABLE, TERMINATED, TIMED_WAITING, WAITING, BLOCKED
 */
public class ThreadConditionTest {

    /**
     * Тест для демонстрации состояния NEW
     * Поток находится в этом состоянии при создании нового потока,
     * до вызова метода start()
     */
    @Test
    public void NewThreadTest() {
        MyThread thread = new MyThread();
        assertEquals(Thread.State.NEW, thread.getState());
    }


    /**
     * Тест для демонстрации состояния RUNNABLE
     * Поток переходит в это состояние после вызова метода start()
     */
    @Test
    public void RunnableThreadTest() {
        MyThread thread1 = new MyThread();
        thread1.start();
        assertEquals(Thread.State.RUNNABLE, thread1.getState());
    }

    /**
     * Тест для демонстрации состояния TERMINATED
     * Поток переходит в это состоние после того как задача выполнена
     */
    @Test
    public void TerminatedThreadTest() {
        MyThread thread2 = new MyThread();
        thread2.start();
        try {
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(Thread.State.TERMINATED, thread2.getState());
    }

    /**
     * Тест для демонстарции состояния TIMED_WAITING
     * Поток находится в этом состоянии пока не истечет его время сна.
     */
    @Test
    public void TimedWaitingThreadTest() {
        MyThread thread = new MyThread();
        thread.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(Thread.State.TIMED_WAITING, thread.getState());
    }


    /**
     * Тест демонстрирующий состояние WAITING
     * Демонстрируется через работу магазина, на складе которого хранится
     * не более 4-х товаров. Поток consumer покупает всего 2 товара,
     * поэтому для того чтобы поток producer смог поставить все 8 товаров,
     * он застрянет в состоянии WAITING.
     */
    @Test
    public void WaitingThreadTest() {

        Store store1 = new Store();
        Consumer consumer = new Consumer(store1);
        Producer producer = new Producer(store1);
        consumer.start();
        producer.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(Thread.State.WAITING, producer.getState());
    }

    /**
     * Тест демонстрирующий состояние BLOCKED
     * Поток попадает в это состояние когда пытается выполнить что либо
     * засинхронизированное
     */
    @Test
    public void BlockedThreadTest() {

        BlockedThread thread1 = new BlockedThread();
        BlockedThread thread2 = new BlockedThread();
        thread1.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread2.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(Thread.State.BLOCKED, thread2.getState());
    }
}
