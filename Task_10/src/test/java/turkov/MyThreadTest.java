package turkov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MyThreadTest {

    /**
     * Метод для демонстрации состояния потока NEW.
     */
    @Test
    public void myThreadNew() {
        Thread thread = new Thread(new MyThread());
        Assertions.assertEquals(Thread.State.NEW, thread.getState());
    }

    /**
     * Метод для демонстрации состояния потока RUNNABLE.
     */
    @Test
    public void myThreadRunnable() {
        Thread thread = new Thread(new MyThread());
        thread.start();
        Assertions.assertEquals(Thread.State.RUNNABLE, thread.getState());
    }

    /**
     * Метод для демонстрации состояния потока TIMED_WAITING.
     */
    @Test
    public void myThreadTimedWaiting() throws InterruptedException {
        Thread thread = new Thread(new MyThread());
        thread.start();
        Thread.sleep(10);
        Assertions.assertEquals(Thread.State.TIMED_WAITING, thread.getState());
    }

    /**
     * Метод для демонстрации состояния потока BLOCKED (запускается 2 потока на одном мониторе c интервалом 10 мс).
     */
    @Test
    public void myThreadBlocked() throws InterruptedException {
        MyThread task = new MyThread();
        Thread th1 = new Thread(task);
        Thread th2 = new Thread(task);
        th1.start();
        Thread.sleep(10);
        th2.start();
        Thread.sleep(10);
        Assertions.assertEquals(Thread.State.BLOCKED, th2.getState());
    }

    /**
     * Метод для демонстрации состояния потока WAITING (запускается 2 потока на одном мониторе c интервалом 10 мс).
     */
    @Test
    public void myThreadWaiting() throws InterruptedException {
        MyThread task = new MyThread();
        Thread th1 = new Thread(task);
        Thread th2 = new Thread(task);
        th1.start();
        Thread.sleep(10);
        th2.start();
        Thread.sleep(1050);
        Assertions.assertEquals(Thread.State.WAITING, th1.getState());
    }

    /**
     * Метод для демонстрации состояния потока TERMINATED.
     * исключение "My thread interrupted" будет выброшено методом run() потока thread в результате его прерывания
     * ч/з interrupt().
     */
    @Test
    public void myThreadTerminated() throws InterruptedException {
        Thread thread = new Thread(new MyThread());
        thread.start();
        thread.interrupt();
        Thread.sleep(10);
        Assertions.assertEquals(Thread.State.TERMINATED, thread.getState());
    }
}
