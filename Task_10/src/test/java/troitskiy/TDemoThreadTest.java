package troitskiy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Метод для тестирования методов для демонстрации каждого из возможных состояний потока
 *
 * спользуя методы класса TDemoThread для получения потоа в заданном стостоянии и
 * получить состояние через метод thread.getState() и проверить ассертом
 *
 * для каждого состояния(метода) свой тест
 */
class TDemoThreadTest {
    /**
     * Проверка, что возвращенный нам поток находится в состоянии NEW
     */
    @Test
    void getNew() {
        assertEquals(TDemoThread.GetNew().getState(), Thread.State.NEW);
    }

    /**
     * Проверка, что возвращенный нам поток находится в состоянии TERMINATED
     */
    @Test
    void getTerminated() {
        assertEquals(TDemoThread.GetTerminated().getState(), Thread.State.TERMINATED);
    }

    /**
     * Проверка, что возвращенный нам поток находится в состоянии RUNNABLE
     */
    @Test
    void getRunnable() {
        assertEquals(TDemoThread.GetRunnable().getState(), Thread.State.RUNNABLE);
    }

    /**
     * Проверка, что возвращенный нам поток находится в состоянии TIMED_WAITING
     */
    @Test
    void getTimedWaiting() {
        assertEquals(TDemoThread.GetTimedWaiting().getState(), Thread.State.TIMED_WAITING);
    }

    /**
     * Проверка, что возвращенный нам поток находится в состоянии WAITING
     */
    @Test
    void getWaiting() {
        assertEquals(TDemoThread.GetWaiting().getState(), Thread.State.WAITING);
    }

    /**
     * Проверка, что возвращенный нам поток находится в состоянии BLOCKED
     */
    @Test
    void getBlocked() {
        assertEquals(TDemoThread.GetBlocked().getState(), Thread.State.BLOCKED);
    }
}