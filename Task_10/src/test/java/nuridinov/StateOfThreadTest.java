package nuridinov;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс-тест для демонстрации состояний потока
 */
public class StateOfThreadTest {
    /**
     * демонстрация состояния NEW, поток находиться в данном состоянии до вызова метода start()
     */
    @Test
    public void stateNEWofThread() {
        Thread thread = new Thread();
        assertEquals(Thread.State.NEW, thread.getState());
    }

    /**
     * демонстрация состояния RUNNABLE, поток переходит в это состояние сразу после вызова метода старт
     */
    @Test
    public void stateRUNNABLEofThread() {
        Thread thread = new Thread();
        thread.start();
        assertEquals(Thread.State.RUNNABLE, thread.getState());
    }

    /**
     * демонстрация состояния WAITING, поток переходит в это состояние после вызова метода wait()
     * и находится в нем до вызова метода start(), в текущем примере экземпляр класса наводчик
     * переходит в состояние wait() и остается в нем до перезарядки орудия(то есть вечно
     * т.к нет канонира, который может перезядить орудие)
     */
    @Test
    public void stateWAITINGofThread() {
        Tank tank = new Tank();
        Aimer aimer = new Aimer(tank);
        aimer.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(Thread.State.WAITING, aimer.getState());
    }

    /**
     * демонстрация состояния TERMINATED, поток переходит в это состояние после завершения своей работы
     */
    @Test
    public void stateTERMINATEDofThread() {
        Tank tank = new Tank();
        Aimer aimer = new Aimer(tank);
        Gunner gunner = new Gunner(tank);
        gunner.start();
        aimer.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(Thread.State.TERMINATED, aimer.getState());
    }

    /**
     * демонстрация состояния TIMED_WAITING, поток входит в это состояние после вызова метода sleep()
     */
    @Test
    public void stateTIMED_WAITINGofThread() {
        WaitingThread thread = new WaitingThread();
        thread.start();

        try {
            Thread.sleep(1_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(Thread.State.TIMED_WAITING, thread.getState());
    }

    /**
     * демонстрация состояния BLOCKED, поток переходит в данное состояние, когда для выполнение
     * ему требуется объект который в данный момент в другом потоке
     */
    @Test
    public void stateBLOCKEDofThread() {
        BlockedThread aThread = new BlockedThread();
        BlockedThread thread2 = new BlockedThread();

        aThread.start();
        try {
            Thread.sleep(1_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread2.start();

        try {
            Thread.sleep(1_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(Thread.State.BLOCKED, thread2.getState());
    }
}
