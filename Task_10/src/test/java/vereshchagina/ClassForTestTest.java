package vereshchagina;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Проверка тестового класса
 */
public class ClassForTestTest {
    ClassForTest test = new ClassForTest();

    /**
     * Перевод потока в состояние NEW
     */
    @Test
    public void testStateNew() {
        assertEquals("NEW", test.testStateNew());
    }

    /**
     * Перевод потока в состояние RUNNABLE
     */
    @Test
    public void testStateRunnable() {

        assertEquals("RUNNABLE", test.testStateRunnable());
    }

    /**
     * Перевод потока в состояние WAITING тест1
     */
    @Test
    public void testStateWaiting1() throws InterruptedException {
        assertEquals("WAITING", test.testStateWaiting1());
    }

    /**
     * Перевод потока в состояние  WAITING тест2
     */
    @Test
    public void testStateWaiting2() throws InterruptedException {

        assertEquals("WAITING", test.testStateWaiting2());
    }

    /**
     * Перевод потока в состояние  TIMED WAITING тест1
     */
    @Test
    public void testStateTimedWaiting1() throws InterruptedException {

        assertEquals("TIMED_WAITING", test.testStateTimedWaiting1());
    }

    /**
     * Перевод потока в состояние  TIMED WAITING тест2
     */
    @Test
    public void testStateTimedWaiting2() throws InterruptedException {

        assertEquals("TIMED_WAITING", test.testStateTimedWaiting2());
    }

    /**
     * Перевод потока в состояние TERMINATED
     */
    @Test
    public void testStateTerminated() throws InterruptedException {

        assertEquals("TERMINATED", test.testStateTerminated());
    }

    /**
     * Перевод потока в состояние  BLOCKED
     */
    @Test
    public void testStateBlocked() throws InterruptedException {

        assertEquals("BLOCKED", test.testStateBlocked());
    }
}
