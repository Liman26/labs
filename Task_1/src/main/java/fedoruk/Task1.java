package fedoruk;

import java.util.Scanner;

public class Task1 {

    static int minNumber(int a, int b) {

        if (a > b)
            return b;
        else
            return a;
    }

    public static void main(String[] args) {
        int a;
        int b;
        Scanner input = new Scanner(System.in);
        System.out.println("Введите число:");
        a = input.nextInt();
        b = input.nextInt();

        System.out.println("Наименьшее число: " + minNumber(a, b));
    }
}

