package kokhan;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Для определения минимального значения введи 2 числа: ");
        float number1 = scan.nextFloat();
        float number2 = scan.nextFloat();

        if (number1 == number2) {
            System.out.println("Введенные числа равны!");
        } else {
            System.out.println("Минимальное значение = " + Math.min(number1, number2));
        }

    }
}

