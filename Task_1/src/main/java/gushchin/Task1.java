package gushchin;

public class Task1 {
    public static void main(String[] args) {
        String startMessage = "Минимальное значение: ";
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);

        if (a < b)
            System.out.println(startMessage + a);
        else if (a > b)
            System.out.println(startMessage + b);
        else
            System.out.println("Введенные значения равны");
    }
}