package turkov;

import java.util.*;

public class Task1 {
    public static void main(String[] args) {

        double number1, number2, minimal;
        final double ACCURACY = 1E-20;
        Scanner input = new Scanner(System.in).useLocale(Locale.ROOT);

        System.out.print("Введите 2 числа ч/з пробел: ");
        number1 = input.nextDouble();
        number2 = input.nextDouble();
        if (Math.abs(Math.abs(number1) - Math.abs(number2)) < ACCURACY & number1 * number2 > 0) {
            System.out.println("Введенные числа равны");
        } else {
            minimal = Math.min(number1, number2);
            System.out.println("Минимальное из введенных чисел: " + minimal);
        }
    }
}