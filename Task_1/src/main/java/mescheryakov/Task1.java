package mescheryakov;

public class Task1 {
    public static void main(String[] args) {
        Integer a = Integer.valueOf(args[0]);
        Integer b = Integer.valueOf(args[1]);
        if (a < b) {
            System.out.println(a);
        }
        else if (a.equals(b)) {
            System.out.println("Введенные данные некорректны, одно число должно быть меньше другого");
        }
        else {
            System.out.println(b);
        }
    }
}
