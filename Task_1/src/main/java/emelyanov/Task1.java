package emelyanov;

import java.util.Scanner;

public class Task1 {
  public static void main(String[] args) {
    int numberOne;
    int numberTwo;

    Scanner input = new Scanner(System.in);

    System.out.print("Введите целое число 1: ");
    numberOne = input.nextInt();
    System.out.print("Введите целое число 2: ");
    numberTwo = input.nextInt();

    if (numberOne > numberTwo) {
      System.out.println("Минимальное число " + numberTwo);
    } else if (numberOne < numberTwo) {
      System.out.println("Минимальное число " + numberOne);
    } else {
      System.out.println("Числа равны");
    }
  }
}
