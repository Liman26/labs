package zurnachyan;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        int a, b;

        System.out.println("Enter two numbers... ");
        a = scan.nextInt();
        b = scan.nextInt();

        System.out.println("Min is " + Math.min(a, b));
    }
}
