package troitskiy;

import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        try {
            int a = Integer.parseInt(args[0]);
            int b = Integer.parseInt(args[1]);

            if (a > b)
            {
                a = b;
            }

            System.out.printf("Min: %d \n", a);
        }
        catch (Exception e)
        {
            System.out.println("Error");
        }
    }
}
