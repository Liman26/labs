package solovyev;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a, b;
        System.out.println("Введите число a:");
        a = input.nextInt();
        System.out.println("Введите число b:");
        b = input.nextInt();

        if (a < b) {
            System.out.println(a + " - наименьшее число.");
        } else if (a > b) {
            System.out.println(b + " - наименьшее число.");
        } else
            System.out.println("Они равны.");
    }
}
