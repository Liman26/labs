package nuridinov;

import java.util.Scanner;

public class MInOfTwo {
    public static void main(String[] args) {
        int firstNum, secondNum;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первое число: ");
        firstNum = scanner.nextInt();
        System.out.println("Введите второе число: ");
        secondNum = scanner.nextInt();
        if (firstNum > secondNum) {
            System.out.println("Первое число больше");
        } else if (secondNum > firstNum) {
            System.out.println("Второе число больше");
        } else {
            System.out.println("Введенные числа равны!");
        }
    }
}
