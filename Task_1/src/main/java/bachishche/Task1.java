package bachishche;

public class Task1 {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        System.out.println("Entered numbers: " + a + ", " + b);
        System.out.println("Minimum number: " + Math.min(a, b));
    }
}
