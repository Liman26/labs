package zhirnov;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        System.out.println("Введите два числа через enter");
        Scanner scanner = new Scanner(System.in);
        double number1 = scanner.nextDouble();
        double number2 = scanner.nextDouble();
        compare(number1, number2);
    }

    public static void compare(double number1, double number2){
        if (number1 > number2){
            System.out.println(number1 + " > " + number2);
        }else if (number1 < number2){
            System.out.println(number1 + " < " + number2);
        }else{
            System.out.println(number1 + " = " + number2);
        }
    }
}
