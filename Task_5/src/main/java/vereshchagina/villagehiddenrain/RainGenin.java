package vereshchagina.villagehiddenrain;

import vereshchagina.ranks.Genin;

/**
 * Класс, описывающий генина из деревни Скрытого Дождя
 */

public class RainGenin implements Genin {
    /**
     * конструктор
     */
    public RainGenin() {
        System.out.println("Генин из Скрытого Дождя");
    }
}
