package vereshchagina.villagehiddenrain;

import vereshchagina.RegularArmy;
import vereshchagina.ranks.Chuunin;
import vereshchagina.ranks.Genin;
import vereshchagina.ranks.Jounin;


/**
 * класс, обеспечивающий создание регулярной армии деревни Скрытого Дождя,
 * содержащий методы создания воинов трех рангов
 */
public class RainRegArmy implements RegularArmy {


    /**
     * метод, создающий представителя регулярной армии ранга чуунин
     *
     * @return чуунин деревни Скрытого Дождя
     */
    @Override
    public Chuunin createChuunin() {
        return new RainChuunin();
    }

    /**
     * метод, создающий представителя регулярной армии ранга джоунин
     *
     * @return джоунин деревни Скрытого Дождя
     */
    @Override
    public Jounin createJounin() {
        return new RainJounin();
    }


    /**
     * метод, создающий представителя регулярной армии ранга генин
     *
     * @return генин деревни Скрытого Дождя
     */
    @Override
    public Genin createGenin() {
        return new RainGenin();
    }
}
