package vereshchagina.villagehiddenrain;

import vereshchagina.ranks.Jounin;

/**
 * Класс, описывающий джоунина из деревни Скрытого Дождя
 */
public class RainJounin implements Jounin {
    /**
     * конструктор
     */
    public RainJounin() {
        System.out.println("Джоунин из Скрытого Дождя");
    }
}
