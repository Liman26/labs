package vereshchagina.villagehiddenrain;

import vereshchagina.ranks.Chuunin;

/**
 * Класс, описывающий чуунина из деревни Скрытого Дождя
 */

public class RainChuunin implements Chuunin {
    /**
     * конструктор
     */
    public RainChuunin() {
        System.out.println("Чуунин из Скрытого Дождя");
    }
}
