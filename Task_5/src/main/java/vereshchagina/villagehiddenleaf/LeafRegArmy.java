package vereshchagina.villagehiddenleaf;

import vereshchagina.RegularArmy;
import vereshchagina.ranks.Chuunin;
import vereshchagina.ranks.Genin;
import vereshchagina.ranks.Jounin;

/**
 * класс, обеспечивающий создание регулярной армии деревни Скрытого Листа,
 * содержащий методы создания воинов трех рангов
 */
public class LeafRegArmy implements RegularArmy {

    /**
     * метод, создающий представителя регулярной армии ранга чуунин
     *
     * @return чуунин деревни Скрытого Листа
     */
    @Override
    public Chuunin createChuunin() {
        return new LeafChuunin();
    }

    /**
     * метод, создающий представителя регулярной армии ранга джоунин
     *
     * @return джоунин деревни Скрытого Листа
     */
    @Override
    public Jounin createJounin() {
        return new LeafJounin();
    }


    /**
     * метод, создающий представителя регулярной армии ранга генин
     *
     * @return генин деревни Скрытого Листа
     */
    @Override
    public Genin createGenin() {
        return new LeafGenin();
    }
}
