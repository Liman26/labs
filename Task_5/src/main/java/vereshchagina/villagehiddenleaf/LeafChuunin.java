package vereshchagina.villagehiddenleaf;

import vereshchagina.ranks.Chuunin;

/**
 * Класс, описывающий чуунина из деревни Скрытого Листа
 */
public class LeafChuunin implements Chuunin {
    /**
     * конструктор
     */
    public LeafChuunin() {
        System.out.println("Чуунин из Скрытого Листа");
    }
}
