package vereshchagina.villagehiddenleaf;

import vereshchagina.ranks.Jounin;

/**
 * Класс, описывающий Джоунина из деревни Скрытого Листа
 */
public class LeafJounin implements Jounin {
    /**
     * конструктор
     */
    public LeafJounin() {
        System.out.println("Джоунин из Скрытого Листа");
    }
}
