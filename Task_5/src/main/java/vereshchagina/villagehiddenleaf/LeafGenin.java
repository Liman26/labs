package vereshchagina.villagehiddenleaf;

import vereshchagina.ranks.Genin;

/**
 * Класс, описывающий генина из деревни Скрытого Листа
 */
public class LeafGenin implements Genin {
    /**
     * конструктор
     */
    public LeafGenin() {
        System.out.println("Генин из Скрытого Листа");
    }
}
