package vereshchagina.villagehiddensand;

import vereshchagina.RegularArmy;
import vereshchagina.ranks.Chuunin;
import vereshchagina.ranks.Genin;
import vereshchagina.ranks.Jounin;

/**
 * класс, обеспечивающий создание регулярной армии деревни Скрытого Песка,
 * содержащий методы создания воинов трех рангов
 */
public class SandRegArmy implements RegularArmy {


    /**
     * метод, создающий представителя регулярной армии ранга чуунин
     *
     * @return чуунин деревни Скрытого Песка
     */
    @Override
    public Chuunin createChuunin() {
        return new SandChuunin();
    }

    /**
     * метод, создающий представителя регулярной армии ранга джоунин
     *
     * @return джоунин деревни Скрытого Песка
     */
    @Override
    public Jounin createJounin() {
        return new SandJounin();
    }


    /**
     * метод, создающий представителя регулярной армии ранга генин
     *
     * @return генин деревни Скрытого Песка
     */
    @Override
    public Genin createGenin() {
        return new SandGenin();
    }
}
