package vereshchagina.villagehiddensand;

import vereshchagina.ranks.Jounin;

/**
 * Класс, описывающий джоунина из деревни Скрытого Песка
 */
public class SandJounin implements Jounin {
    /**
     * конструктор
     */
    public SandJounin() {
        System.out.println("Джоунин из Скрытого Песка");
    }
}
