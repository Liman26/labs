package vereshchagina.villagehiddensand;

import vereshchagina.ranks.Chuunin;

/**
 * Класс, описывающий чуунина из деревни Скрытого Песка
 */
public class SandChuunin implements Chuunin {
    /**
     * конструктор
     */
    public SandChuunin() {
        System.out.println("Чуунин из Скрытого Песка");
    }
}
