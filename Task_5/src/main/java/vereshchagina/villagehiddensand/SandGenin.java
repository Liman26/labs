package vereshchagina.villagehiddensand;

import vereshchagina.ranks.Genin;

/**
 * Класс, описывающий генина из деревни Скрытого Песка
 */
public class SandGenin implements Genin {
    /**
     * конструктор
     */
    public SandGenin() {
        System.out.println("Генин из Скрытого Песка");
    }
}
