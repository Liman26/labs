package vereshchagina;

import vereshchagina.ranks.Chuunin;
import vereshchagina.ranks.Genin;
import vereshchagina.ranks.Jounin;

/**
 * интерфейс, описывающий иерархию регулярной армии деревни шиноби
 */
public interface RegularArmy {
    /**
     * метод, создающий представителя регулярной армии ранга чуунин
     *
     * @return чуунин
     */
    Chuunin createChuunin();

    /**
     * метод, создающий представителя регулярной армии ранга джоунин
     *
     * @return джоунин
     */
    Jounin createJounin();

    /**
     * метод, создающий представителя регулярной армии ранга генин
     *
     * @return генин
     */
    Genin createGenin();
}
