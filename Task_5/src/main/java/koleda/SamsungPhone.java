package koleda;

/**
 * Объект данного класса является конечным продуктом (Телефон Samsung)
 */
public class SamsungPhone implements Phone{
    public SamsungPhone(){
        System.out.println("Телефон Samsung...");
    }
}
