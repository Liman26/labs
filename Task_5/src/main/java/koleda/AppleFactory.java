package koleda;

/**
 * Реализация абстрактной фабрики AppleFactory по производству телефонов и ноутбуков от компании Apple
 */
public class AppleFactory implements TechFactory{
    /**
     * Метод для производства Телефона
     * @return объект класса ApplePhone
     */
    @Override
    public Phone createPhone() {
        return new ApplePhone();
    }
    /**
     * Метод для производства Ноутбука
     * @return объект класса AppleLaptop
     */
    @Override
    public Laptop createLaptop() {
        return new AppleLaptop();
    }
}
