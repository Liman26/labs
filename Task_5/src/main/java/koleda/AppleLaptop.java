package koleda;

/**
 * Объект данного класса является конечным продуктом (Ноутбук Apple)
 */
public class AppleLaptop implements Laptop{
    public AppleLaptop() {
        System.out.println("Ноутбук Apple...");
    }
}
