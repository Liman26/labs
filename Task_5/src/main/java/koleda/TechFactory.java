package koleda;

/**
 * Абстрактная фабрика TechFactory реализует производство телефонов и ноутбукров
 */
public interface TechFactory {
    Phone createPhone();
    Laptop createLaptop();
}
