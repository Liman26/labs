package koleda;

/**
 * Объект данного класса является конечным продуктом (Ноутбук Samsung)
 */
public class SamsungLaptop implements Laptop{
    public SamsungLaptop() {
        System.out.println("Ноутбук Samsung...");
    }
}
