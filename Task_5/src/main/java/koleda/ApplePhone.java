package koleda;

/**
 * Объект данного класса является конечным продуктом (Телефон Apple)
 */
public class ApplePhone implements Phone{
    public ApplePhone(){
        System.out.println("Телефон Apple...");
    }
}
