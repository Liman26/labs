package koleda;

/**
 * Реализация абстрактной фабрики SamsungFactory по производству телефонов и ноутбуков от компании Samsung
 */
public class SamsungFactory implements TechFactory{
    /**
     * Метод для производства Телефона
     * @return объект класса SamsungPhone
     */
    @Override
    public Phone createPhone() {
        return new SamsungPhone();
    }
    /**
     * Метод для производства Ноутбука
     * @return объект класса SamsungLaptop
     */
    @Override
    public Laptop createLaptop() {
        return new SamsungLaptop();
    }
}
