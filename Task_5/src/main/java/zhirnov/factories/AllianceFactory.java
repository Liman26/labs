package zhirnov.factories;

import zhirnov.archers.AllianceArcher;
import zhirnov.archers.Archer;
import zhirnov.warriors.AllianceWarrior;
import zhirnov.warriors.Warrior;

/**
 * Класс, который позволяет создавать warrior и archer для фракции alliance
 */
public class AllianceFactory implements SoldierFactory {

    /**
     * Метод, создающий warrior для фракции alliance
     * @return нового warrior
     */
    @Override
    public Warrior createWarrior() {
        return new AllianceWarrior();
    }

    /**
     * Метод, создающий archer для фракции alliance
     * @return нового warrior
     */
    @Override
    public Archer createArcher() {
        return new AllianceArcher();
    }
}
