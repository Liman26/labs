package zhirnov.factories;

import zhirnov.archers.Archer;
import zhirnov.archers.HordeArcher;
import zhirnov.warriors.HordeWarrior;
import zhirnov.warriors.Warrior;

/**
 * Класс, который позволяет создавать warrior и archer для фракции horde
 */
public class HordeFactory implements SoldierFactory {

    /**
     * Метод, создающий warrior для фракции horde
     * @return нового warrior
     */
    @Override
    public Warrior createWarrior() {
        return new HordeWarrior();
    }

    /**
     * Метод, создающий archer для фракции horde
     * @return нового warrior
     */
    @Override
    public Archer createArcher() {
        return new HordeArcher();
    }
}
