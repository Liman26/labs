package zhirnov.factories;

import zhirnov.archers.Archer;
import zhirnov.warriors.Warrior;

/**
 * Интерфейс, который позволяет создовать warrior и archer
 */
public interface SoldierFactory {
    /**
     * Метод, создающий warrior
     * @return нового warrior
     */
    Warrior createWarrior();

    /**
     * Метод, создающий archer
     * @return нового archer
     */
    Archer createArcher();
}
