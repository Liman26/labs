package bachishche;

import bachishche.engineers.Engineer;
import bachishche.engineers.YaleEngineer;
import bachishche.teachers.Teacher;
import bachishche.teachers.YaleTeacher;

/**
 * Реализация университета ("фабрики") Йель,
 * выпускающего преподавателей и инженеров
 */
public class YaleUniversity implements University {
    @Override
    public Engineer getEngineer() {
        return new YaleEngineer();
    }

    @Override
    public Teacher getTeacher() {
        return new YaleTeacher();
    }
}
