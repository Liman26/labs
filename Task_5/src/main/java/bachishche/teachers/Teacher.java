package bachishche.teachers;
/**
 * Интерфейс, характеризующий выпускника как преподавателя
 * и реализующий метод проведения урока
 */
public interface Teacher {
    default void conductLesson(){
        System.out.println("The lesson was conducted");
    }
}
