package bachishche.teachers;

/**
 * Сущность преподавателя, выпущенного из Йеля
 */
public class YaleTeacher implements Teacher {
    public YaleTeacher() {
        System.out.println("Teacher graduated from Yale!");
    }
}
