package bachishche.teachers;

/**
 * Сущность преподавателя, выпущенного из Оксфорда
 */
public class OxfordTeacher implements Teacher {
    public OxfordTeacher() {
        System.out.println("Teacher graduated from Oxford!");
    }
}
