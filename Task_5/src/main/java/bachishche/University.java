package bachishche;

import bachishche.engineers.Engineer;
import bachishche.teachers.Teacher;

/**
 * Реализация абстрактной фабрики University по выпуску
 * инженеров и преподавателей
 */
public interface University {
    Engineer getEngineer();

    Teacher getTeacher();
}
