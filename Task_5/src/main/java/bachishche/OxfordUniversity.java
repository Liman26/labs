package bachishche;

import bachishche.engineers.Engineer;
import bachishche.engineers.OxfordEngineer;
import bachishche.teachers.OxfordTeacher;
import bachishche.teachers.Teacher;

/**
 * Реализация университета ("фабрики") Оксфорд,
 * выпускающего преподавателей и инженеров
 */
public class OxfordUniversity implements University {
    @Override
    public Engineer getEngineer() {
        return new OxfordEngineer();
    }

    @Override
    public Teacher getTeacher() {
        return new OxfordTeacher();
    }
}
