package bachishche.engineers;

/**
 * Сущность инженера, выпущенного из Оксфорда
 */
public class OxfordEngineer implements Engineer {
    public OxfordEngineer() {
        System.out.println("Engineer graduated from Oxford!");
    }
}
