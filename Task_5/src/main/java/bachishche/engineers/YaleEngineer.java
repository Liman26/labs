package bachishche.engineers;
/**
 * Сущность инженера, выпущенного из Йеля
 */
public class YaleEngineer implements Engineer{
    public YaleEngineer(){
        System.out.println("Engineer graduated from Yale!");
    }
}
