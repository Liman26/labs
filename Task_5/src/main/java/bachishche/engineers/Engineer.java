package bachishche.engineers;

/**
 * Интерфейс, характеризующий выпускника как инженера и реализующий метод
 * ремонта принтера
 */
public interface Engineer {
    default void fixPrinter(){
        System.out.println("The printer is fixed!\n");
    }
}
