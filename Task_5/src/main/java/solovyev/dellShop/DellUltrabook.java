package solovyev.dellShop;

import solovyev.typeOfLaptops.Ultrabook;

/**
 * Класс DellUltrabook, представляющий ультрабук от компании Dell
 */
public class DellUltrabook implements Ultrabook {
    /**
     * Конструктор, продающий ультрабук Dell
     */
    public DellUltrabook() {
        System.out.println("Продан ультрабук Dell");
    }
}
