package solovyev.dellShop;

import solovyev.typeOfLaptops.OfficeLaptop;

/**
 * Класс DellOfficeLaptop, представляющий офисный ноутбук от компании Dell
 */
public class DellOfficeLaptop implements OfficeLaptop {
    /**
     * Конструктор, продающий офисный ноутбук Dell
     */
    public DellOfficeLaptop() {
        System.out.println("Продан офисный ноутбук Dell");
    }

}
