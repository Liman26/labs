package solovyev.dellShop;

import solovyev.typeOfLaptops.GamingLaptop;

/**
 * Класс DellGamingLaptop, представляющий игровой ноутбук от компании Dell
 */
public class DellGamingLaptop implements GamingLaptop {
    /**
     * Конструктор, продающий игровой ноутбук Dell
     */
    public DellGamingLaptop() {
        System.out.println("Продан игровой ноутбук Dell");
    }
}
