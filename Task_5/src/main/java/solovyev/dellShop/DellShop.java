package solovyev.dellShop;

import solovyev.NotebookShop;
import solovyev.typeOfLaptops.*;

/**
 * Класс, представляющий собой магазин ноутбуков Dell
 */
public class DellShop implements NotebookShop {

    /**
     * Метод для продажи ноутбука типа GamingLaptop, в магазине Dell
     *
     * @return объект класса DellGamingLaptop
     */
    @Override
    public GamingLaptop sellGamingLaptop() {
        return new DellGamingLaptop();
    }

    /**
     * Метод для продажи ноутбука типа OfficeLaptop, в магазине Dell
     *
     * @return объект класса DellOfficeLaptop
     */
    @Override
    public OfficeLaptop sellOfficeLaptop() {
        return new DellOfficeLaptop();
    }

    /**
     * Метод для продажи ноутбука типа Ultrabook, в магазине Dell
     *
     * @return объект класса DellUltrabook
     */
    @Override
    public Ultrabook sellUltrabook() {
        return new DellUltrabook();
    }

}

