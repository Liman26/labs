package solovyev.lenovoShop;

import solovyev.typeOfLaptops.GamingLaptop;

/**
 * Класс LenovoGamingLaptop, представляющий игровой ноутбук от компании Lenovo
 */
public class LenovoGamingLaptop implements GamingLaptop {
    /**
     * Конструктор, продающий игровой ноутбук Lenovo
     */
    public LenovoGamingLaptop() {
        System.out.println("Продан игровой ноутбук Lenovo");
    }
}
