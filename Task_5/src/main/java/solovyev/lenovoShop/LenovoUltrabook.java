package solovyev.lenovoShop;

import solovyev.typeOfLaptops.Ultrabook;

/**
 * Класс LenovoUltrabook, представляющий ультрабук от компании Lenovo
 */
public class LenovoUltrabook implements Ultrabook {
    /**
     * Конструктор, продающий ультрабук Lenovo
     */
    public LenovoUltrabook() {
        System.out.println("Продан ультрабук Lenovo");
    }
}
