package solovyev.lenovoShop;

import solovyev.typeOfLaptops.OfficeLaptop;

/**
 * Класс LenovoOfficeLaptop, представляющий офисный ноутбук от компании Lenovo
 */
public class LenovoOfficeLaptop implements OfficeLaptop {
    /**
     * Конструктор, продающий офисный ноутбук Lenovo
     */
    public LenovoOfficeLaptop() {
        System.out.println("Продан офисный ноутбук Lenovo");
    }
}
