package solovyev.lenovoShop;

import solovyev.NotebookShop;
import solovyev.typeOfLaptops.*;

/**
 * Класс, представляющий собой магазин ноутбуков Lenovo
 */
public class LenovoShop implements NotebookShop {
    /**
     * Метод для продажи ноутбука типа GamingLaptop, в магазине Lenovo
     *
     * @return объект класса LenovoGamingLaptop
     */
    @Override
    public GamingLaptop sellGamingLaptop() {
        return new LenovoGamingLaptop();
    }

    /**
     * Метод для продажи ноутбука типа OfficeLaptop, в магазине Lenovo
     *
     * @return объект класса LenovoOfficeLaptop
     */
    @Override
    public OfficeLaptop sellOfficeLaptop() {
        return new LenovoOfficeLaptop();
    }

    /**
     * Метод для продажи ноутбука типа Ultrabook, в магазине Lenovo
     *
     * @return объект класса LenovoUltrabook
     */
    @Override
    public Ultrabook sellUltrabook() {
        return new LenovoUltrabook();
    }

}
