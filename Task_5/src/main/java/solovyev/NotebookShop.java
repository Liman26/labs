package solovyev;

import solovyev.typeOfLaptops.GamingLaptop;
import solovyev.typeOfLaptops.OfficeLaptop;
import solovyev.typeOfLaptops.Ultrabook;

/**
 * Интерфейс, позволяющий продавать игровые ноутбуки, офисные и ультрабуки
 */
public interface NotebookShop {
    /**
     * Абстрактный метод sellGamingLaptop,
     * используемый для продажи ноутбуков типа: GamingLaptop
     * @return в зависимости от выбранного магазина,
     * возвращает объект продающий игровой ноутбук этого магазина
     */
    GamingLaptop sellGamingLaptop();

    /**
     * Абстрактный метод sellUltrabook,
     * используемый для продажи ноутбуков типа: Ultrabook
     * @return в зависимости от выбранного магазина,
     * возвращает объект продающий ультрабук этого магазина
     */
    Ultrabook sellUltrabook();

    /**
     * Абстрактный метод sellOfficeLaptop,
     * используемый для продажи ноутбуков типа: OfficeLaptop
     * @return в зависимости от выбранного магазина,
     * возвращает объект продающий офисный ноутбук этого магазина
     */
    OfficeLaptop sellOfficeLaptop();
}
