package solovyev.asusShop;

import solovyev.typeOfLaptops.GamingLaptop;

/**
 * Класс AsusGamingLaptop, представляющий игровой ноутбук от компании Asus
 */
public class AsusGamingLaptop implements GamingLaptop {
    /**
     * Конструктор, продающий игровой ноутбук Asus
     */
    public AsusGamingLaptop() {
        System.out.println("Продан игровой ноутбук Asus");
    }
}
