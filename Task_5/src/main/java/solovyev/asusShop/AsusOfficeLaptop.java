package solovyev.asusShop;

import solovyev.typeOfLaptops.OfficeLaptop;

/**
 * Класс AsusOfficeLaptop, представляющий офисный ноутбук от компании Asus
 */
public class AsusOfficeLaptop implements OfficeLaptop {
    /**
     * Конструктор, продающий офисный ноутбук Asus
     */
    public AsusOfficeLaptop() {
        System.out.println("Продан офисный ноутбук Asus");
    }
}
