package solovyev.asusShop;

import solovyev.typeOfLaptops.Ultrabook;

/**
 * Класс AsusUltrabook, представляющий ультрабук от компании Asus
 */
public class AsusUltrabook implements Ultrabook {
    /**
     * Конструктор, продающий ультрабук Asus
     */
    public AsusUltrabook() {
        System.out.println("Продан ультрабук Asus");
    }
}
