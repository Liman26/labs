package solovyev.asusShop;

import solovyev.NotebookShop;
import solovyev.typeOfLaptops.*;

/**
 * Класс, представляющий собой магазин ноутбуков Asus
 */
public class AsusShop implements NotebookShop {
    /**
     * Метод для продажи ноутбука типа GamingLaptop, в магазине Asus
     *
     * @return объект класса AsusGamingLaptop
     */
    @Override
    public GamingLaptop sellGamingLaptop() {
        return new AsusGamingLaptop();
    }

    /**
     * Метод для продажи ноутбука типа OfficeLaptop, в магазине Asus
     *
     * @return объект класса AsusOfficeLaptop
     */
    @Override
    public OfficeLaptop sellOfficeLaptop() {
        return new AsusOfficeLaptop();
    }

    /**
     * Метод для продажи ноутбука типа Ultrabook, в магазине Asus
     *
     * @return объект класса AsusUltrabook
     */
    @Override
    public Ultrabook sellUltrabook() {
        return new AsusUltrabook();
    }
}
