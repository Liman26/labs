package gushchin;

/**
 * Интерфейс MemesFactory является абстрактной фабрикой, содержащей в себе 3 метода
 * для создания текстовых, аудио- и видеомемов
 */
public interface MemesFactory {
    TextMeme createTextMeme();
    AudioMeme createAudioMeme();
    VideoMeme createVideoMeme();
}
