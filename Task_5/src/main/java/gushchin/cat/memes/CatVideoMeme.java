package gushchin.cat.memes;

import gushchin.VideoMeme;

/**
 * Класс CatVideoMeme - видео мем с котом
 */
public class CatVideoMeme implements VideoMeme {
    public CatVideoMeme() {
        System.out.println("Создание видео мема с котом");
    }
}
