package gushchin.cat.memes;

import gushchin.AudioMeme;

/**
 * Класс CatAudioMeme - аудио мем с котом
 */
public class CatAudioMeme implements AudioMeme {
    public CatAudioMeme() {
        System.out.println("Создание аудио мема с котом");
    }
}
