package gushchin.cat.memes;

import gushchin.AudioMeme;
import gushchin.MemesFactory;
import gushchin.TextMeme;
import gushchin.VideoMeme;

/**
 * Класс CatMemeFactory реализует абстрактную фабрику MemesFactory, предоставляя
 * возможность создать текстовый, аудио- или видеомем с котом
 */
public class CatMemeFactory implements MemesFactory {
    /**
     * Функция создания текстового мема с котом
     * @return объект класса CatTextMeme
     */
    @Override
    public TextMeme createTextMeme() {
        return new CatTextMeme();
    }

    /**
     * Функция создания аудиомема с котом
     * @return объект класса CatAudioMeme
     */
    @Override
    public AudioMeme createAudioMeme() {
        return new CatAudioMeme();
    }

    /**
     * Функция создания видеомема с котом
     * @return объект класса CatVideoMeme
     */
    @Override
    public VideoMeme createVideoMeme() {
        return new CatVideoMeme();
    }
}
