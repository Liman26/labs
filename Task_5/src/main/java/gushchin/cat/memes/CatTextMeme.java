package gushchin.cat.memes;

import gushchin.TextMeme;

/**
 * Класс CatTextMeme - текстовый мем с котом
 * Абсолютно не смешной. Так как текстовые мемы с котами обычно не заходят
 */
public class CatTextMeme implements TextMeme {
    public CatTextMeme() {
        System.out.println("Создание текстового мема с котом");
    }
}
