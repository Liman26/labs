package gushchin.parrot.memes;

import gushchin.VideoMeme;

/**
 * Класс ParrotVideoMeme - видеомем с попугаем
 */
public class ParrotVideoMeme implements VideoMeme {
    public ParrotVideoMeme() {
        System.out.println("Создание видеомема с попугаем");
    }
}
