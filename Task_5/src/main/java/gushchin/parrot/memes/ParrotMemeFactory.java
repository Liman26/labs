package gushchin.parrot.memes;

import gushchin.AudioMeme;
import gushchin.MemesFactory;
import gushchin.TextMeme;
import gushchin.VideoMeme;

/**
 * Класс ParrotMemeFactory реализует абстрактную фабрику MemesFactory, предоставляя
 * возможность создать текстовый, аудио- и видеомем с попугаем
 */
public class ParrotMemeFactory implements MemesFactory {
    /**
     * Функция создания текстового мема с попугаем
     * @return объект класса ParrotTextMeme
     */
    @Override
    public TextMeme createTextMeme() {
        return new ParrotTextMeme();
    }

    /**
     * Функция создания аудиомема с попугаем
     * @return объект класса ParrotAudioMeme
     */
    @Override
    public AudioMeme createAudioMeme() {
        return new ParrotAudioMeme();
    }

    /**
     * Функция создания видеомема с попугаем
     * @return объект класса ParrotVideoMeme
     */
    @Override
    public VideoMeme createVideoMeme() {
        return new ParrotVideoMeme();
    }
}
