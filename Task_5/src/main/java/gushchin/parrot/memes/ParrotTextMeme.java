package gushchin.parrot.memes;

import gushchin.TextMeme;

/**
 * Класс ParrotTextMeme - текстовый мем с попугаем
 */
public class ParrotTextMeme implements TextMeme {
    public ParrotTextMeme() {
        System.out.println("Создание текстового мема с попугаем");
    }
}
