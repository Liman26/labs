package gushchin.parrot.memes;

import gushchin.AudioMeme;

/**
 * Класс ParrotAudioMeme - аудиомем с попугаем
 */
public class ParrotAudioMeme implements AudioMeme {
    public ParrotAudioMeme() {
        System.out.println("Создание аудиомема с попугаем");
    }
}
