package emelyanov;

import emelyanov.base.AircraftsFactory;
import emelyanov.boeing.BoeingFactory;
import emelyanov.sukhoi.SukhoiFactory;

public class Task5 {
  public static void main(String[] args) {
    AircraftsFactory sukhoiFactory = new SukhoiFactory();
    AircraftsFactory boeingFactory = new BoeingFactory();
    System.out.println(sukhoiFactory.createBomber());
    System.out.println(sukhoiFactory.createFighter());
    System.out.println(sukhoiFactory.createPassenger());
    System.out.println(boeingFactory.createBomber());
    System.out.println(boeingFactory.createFighter());
    System.out.println(boeingFactory.createPassenger());
  }
}
