package emelyanov.sukhoi;

import emelyanov.base.Passenger;

/**
 * Самолет типа Пассажирский корпорации СУХОЙ
 */
public class SukhoiPassenger extends Passenger {
  private final String name = "СУ-174";

  public SukhoiPassenger() {
    super(400, 4000, 300);
  }

  @Override
  public String toString() {
    return "Пассажирский корпорации Сухой {" +
        "Название='" + name + '\'' +
        "Скорость='" + getSpeed() + '\'' +
        "Длина полета='" + getFlightLength() + '\'' +
        "Пассажиров='" + getPassengers() + '\'' +
        '}';
  }
}
