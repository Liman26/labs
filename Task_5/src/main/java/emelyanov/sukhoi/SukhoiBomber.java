package emelyanov.sukhoi;

import emelyanov.base.Bomber;

/**
 * Самолет типа Бомбардировщик корпорации СУХОЙ
 */
public class SukhoiBomber extends Bomber {
  private final String name="Су-17";

  public SukhoiBomber() {
    super(100, 1000, 4000);
  }

  @Override
  public String toString() {
    return "Бомбардировщик корпорации СУХОЙ {" +
        "Название='" + name + '\'' +
        "Скорость='" + getSpeed() + '\'' +
        "Длина полета='" + getFlightLength() + '\'' +
        "Вес боеприпасов='" + getWeightAmmo() + '\'' +
        '}';
  }
}
