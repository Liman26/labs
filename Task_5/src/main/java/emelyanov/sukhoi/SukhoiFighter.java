package emelyanov.sukhoi;

import emelyanov.base.Fighter;

/**
 * Самолет типа Fighter корпорации СУХОЙ
 */
public class SukhoiFighter extends Fighter {
  private final String name = "СУ-1";

  public SukhoiFighter() {
    super(200, 2000, 10, 20);
  }

  @Override
  public String toString() {
    return "Fighter корпорации Сухой {" +
        "Название='" + name + '\'' +
        "Скорость='" + getSpeed() + '\'' +
        "Длина полета='" + getFlightLength() + '\'' +
        "Ракет='" + getRockets() + '\'' +
        "Пулеметов='" + getGuns() + '\'' +
        '}';
  }
}
