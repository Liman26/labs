package emelyanov.base;

/**
 * базовое описание самолетов типа Бомбардировщик
 */
public abstract class Bomber extends Plane {
  private final int weightAmmo;

  /**
   * конструктор инициализирующий параметры самолета
   *
   * @param speed        - скорость
   * @param flightLength - длина палета
   * @param weightAmmo   - вес боеприпасов
   */
  public Bomber(int speed, int flightLength, int weightAmmo) {
    super(speed, flightLength);
    this.weightAmmo = weightAmmo;
  }

  /**
   * Получить максимальный вес боеприпасов
   *
   * @return максимальный вес боеприпасов
   */
  public int getWeightAmmo() {
    return weightAmmo;
  }
}
