package emelyanov.base;

/**
 * базовое описание самолетов типа Пассажирский
 */
public abstract class Passenger extends Plane {
  private final int passengers;

  /**
   * конструктор инициализирующий параметры самолета
   *
   * @param speed        - скорость
   * @param flightLength - длина палета
   * @param passengers   - количество пассажиров
   */
  public Passenger(int speed, int flightLength, int passengers) {
    super(speed, flightLength);
    this.passengers = passengers;
  }

  /**
   * Получить количество пассажиров
   *
   * @return количество пассажиров
   */
  public int getPassengers() {
    return passengers;
  }
}
