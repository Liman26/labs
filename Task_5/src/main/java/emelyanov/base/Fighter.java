package emelyanov.base;

/**
 * базовое описание самолетов типа Fighter
 */
public abstract class Fighter extends Plane {
  private final int rockets;
  private final int guns;

  /**
   * конструктор инициализирующий параметры самолета
   *
   * @param speed        - скорость
   * @param flightLength - длина палета
   * @param rockets      - количество рокет
   * @param guns         - количество пулеметов
   */
  public Fighter(int speed, int flightLength, int rockets, int guns) {
    super(speed, flightLength);
    this.rockets = rockets;
    this.guns = guns;
  }

  /**
   * Получить количество рокет
   *
   * @return количество рокет
   */
  public int getRockets() {
    return rockets;
  }

  /**
   * Получить количество пулеметов
   *
   * @return количество пулеметов
   */
  public int getGuns() {
    return guns;
  }
}
