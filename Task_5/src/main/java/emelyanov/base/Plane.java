package emelyanov.base;

/**
 * базовое описание самолетов
 */
public abstract class Plane {
  private final int speed;
  private final int flightLength;

  /**
   * конструктор инициализирующий параметры самолета
   *
   * @param speed        - скорость
   * @param flightLength - длина палета
   */
  public Plane(int speed, int flightLength) {
    this.speed = speed;
    this.flightLength = flightLength;
  }

  /**
   * Получить скорость
   *
   * @return скорость
   */
  public int getSpeed() {
    return speed;
  }

  /**
   * Получить длину палета
   *
   * @return длина палета
   */
  public int getFlightLength() {
    return flightLength;
  }
}
