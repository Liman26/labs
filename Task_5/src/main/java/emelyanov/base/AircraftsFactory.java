package emelyanov.base;

/**
 * методы создания разных типов самолетов
 */
public interface AircraftsFactory {
  Bomber createBomber();

  Fighter createFighter();

  Passenger createPassenger();
}
