package emelyanov.boeing;

import emelyanov.base.Bomber;

/**
 * Самолет типа Бомбардировщик корпорацииии Боинг
 */
public class BoeingBomber extends Bomber {
  private final String name="Boeing-17";

  public BoeingBomber() {
    super(10, 100, 400);
  }

  @Override
  public String toString() {
    return "Бомбардировщик корпорации Боинг {" +
        "Название='" + name + '\'' +
        "Скорость='" + getSpeed() + '\'' +
        "Длина полета='" + getFlightLength() + '\'' +
        "Вес боеприпасов='" + getWeightAmmo() + '\'' +
        '}';
  }
}
