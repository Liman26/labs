package emelyanov.boeing;

import emelyanov.base.Passenger;

/**
 * Самолет типа Пассажирский корпорации Боинг
 */
public class BoeingPassenger extends Passenger {
  private final String name = "Boeing-174";

  public BoeingPassenger() {
    super(40, 400, 100);
  }

  @Override
  public String toString() {
    return "Пассажирский корпорации Боинг {" +
        "Название='" + name + '\'' +
        "Скорость='" + getSpeed() + '\'' +
        "Длина полета='" + getFlightLength() + '\'' +
        "Пассажиров='" + getPassengers() + '\'' +
        '}';
  }
}
