package emelyanov.boeing;

import emelyanov.base.Fighter;

/**
 * Самолет типа Fighter корпорации Боинг
 */
public class BoeingFighter extends Fighter {
  private final String name = "Boeing-1";

  public BoeingFighter() {
    super(20, 200, 1, 2);
  }

  @Override
  public String toString() {
    return "Fighter корпорации Боинг {" +
        "Название='" + name + '\'' +
        "Скорость='" + getSpeed() + '\'' +
        "Длина полета='" + getFlightLength() + '\'' +
        "Ракет='" + getRockets() + '\'' +
        "Пулеметов='" + getGuns() + '\'' +
        '}';
  }
}
