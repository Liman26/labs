package emelyanov.boeing;

import emelyanov.base.AircraftsFactory;
import emelyanov.base.Bomber;
import emelyanov.base.Fighter;
import emelyanov.base.Passenger;

/**
 * Фабрика самолетов корпорации Боинг
 */
public class BoeingFactory implements AircraftsFactory {
  /**
   * Создать самолет типа Бомбардировщик
   *
   * @return самолет типа Бомбардировщик
   */
  @Override
  public Bomber createBomber() {
    return new BoeingBomber();
  }

  /**
   * Создать самолет типа Fighter
   *
   * @return самолет типа Fighter
   */
  @Override
  public Fighter createFighter() {
    return new BoeingFighter();
  }

  /**
   * Создать самолет типа Пассажирский
   *
   * @return самолет типа Пассажирский
   */
  @Override
  public Passenger createPassenger() {
    return new BoeingPassenger();
  }
}
