package troitskiy;

/**
 *  Операнд типа Double
 */
public class TDoubleOperand extends TOperand
{
    /**
     * Конструктор
     * @param value - Значение операенда
     */
    public TDoubleOperand(String value)
    {
        super(new Double(value));
    }

}
