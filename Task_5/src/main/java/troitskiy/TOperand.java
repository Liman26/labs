package troitskiy;

/**
 *  Клас операнда для некоторого оператора,
 *  предназначен для хранения значения операнда
 */
public class TOperand
{
    private final Object value;

    /**
     * Конструктор
     * @param value - значение операнда
     */
    public TOperand(Object value)
    {
        this.value = value;
    }

    /**
     *  Возвращает значение операнда
      * @return -значение операнда
     */
    public Object getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return value.toString();
    }
}
