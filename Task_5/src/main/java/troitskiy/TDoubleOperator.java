package troitskiy;

/**
 * Оператор над операндами типа Double
 */
public class TDoubleOperator extends TOperator
{
    /**
     * Конструктор
     * @param name - Обозначение оператора (например +, -)
     */
    public TDoubleOperator(String name)
    {
        super(name);
    }

    /**
     * Выполнить операцию над двумя операндами
     * @param operand1 - первый операнд
     * @param operand2 - второй операнд
     * @return -результат операции
     */
    @Override
    TOperand Exec(TOperand operand1, TOperand operand2)
    {
        double value1, value2;
        value1 = (Double) (operand1.getValue());
        value2 = (Double) (operand2.getValue());

        switch (this.name)
        {
            case "+": return new TDoubleOperand(Double.toString(value1 + value2));
            case "-": return new TDoubleOperand(Double.toString(value1 - value2));
            case "*": return new TDoubleOperand(Double.toString(value1 * value2));
            case "/": return new TDoubleOperand(Double.toString(value1 / value2));
            default : return null;
        }
    }
}
