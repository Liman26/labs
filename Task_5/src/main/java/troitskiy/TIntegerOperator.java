package troitskiy;

/**
 * Оператор над операндами типа Integer
 */
public class TIntegerOperator extends TOperator
{
    /**
     * Конструктор
     * @param name - Обозначение оператора (например +, -)
     */
    public TIntegerOperator(String name)
    {
        super(name);
    }

    /**
     * Выполнить операцию над двумя операндами
     * @param operand1 - первый операнд
     * @param operand2 - второй операнд
     * @return -результат операции
     */
    @Override
    TOperand Exec(TOperand operand1, TOperand operand2)
    {
        int value1 = (Integer) (operand1.getValue());
        int value2 = (Integer) (operand2.getValue());

        switch (this.name)
        {
            case "+": return new TIntegerOperand(Integer.toString(value1 + value2));
            case "-": return new TIntegerOperand(Integer.toString(value1 - value2));
            case "*": return new TIntegerOperand(Integer.toString(value1 * value2));
            case "/": return new TIntegerOperand(Integer.toString(value1 / value2));
            default : return null;
        }
    }
}
