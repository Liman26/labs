package troitskiy;

/**
 *  Бинарный оператор
 */
abstract public class TOperator
{
    String name;

    /**
     * Конструктор
     * @param name - Обозначение оператора (например +, -, MOD)
     */
    public TOperator(String name)
    {
        this.name = name;
    }

    /**
     * Выполнить операцию над двумя операндами
     * @param operand1 - первый операнд
     * @param operand2 - второй операнд
     * @return - результат операции
     */
    abstract TOperand Exec(TOperand operand1, TOperand operand2);


    @Override
    public String toString()
    {
        return " " + name + " ";
    }
}
