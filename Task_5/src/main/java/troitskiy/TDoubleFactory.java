package troitskiy;

/**
 * Фабрика операндов и операторов с плавающей точкой
 */
public class TDoubleFactory extends TOperationFactory
{
    /**
     * Создание оператора над операндами с плавающей точкой
     * @param name -Обозначение оператора (например +, -, MOD)
     * @return - оператор с плавающей точкой
     */
    @Override
    TOperator createOperator(String name) {
        return new TDoubleOperator(name);
    }

    /**
     * Создать операнд типа Double
     * @param value - Значение операнда
     * @return -Операнд
     */
    @Override
    TOperand createOperand(String value) {
        return new TDoubleOperand(value);
    }
}



