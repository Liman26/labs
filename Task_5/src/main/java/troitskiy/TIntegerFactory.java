package troitskiy;

/**
 * Фабрика целочисленных операторов и операндов
 */
public class TIntegerFactory extends TOperationFactory
{
    /**
     * Сделать целочисленный оператор
     * @param name -Обозначение оператора (например +, -, MOD)
     * @return - оператор для целочисленных операндов
     */
    @Override
    TOperator createOperator(String name) {
        return new TIntegerOperator(name);
    }

    /**
     * Сделать целочисленный опреранд
     * @param value - Значение операнда
     * @return - сам операнд
     */
    @Override
    TOperand createOperand(String value) {
        return new TIntegerOperand(value);
    }
}
