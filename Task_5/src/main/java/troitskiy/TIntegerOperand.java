package troitskiy;

/**
 *  Целочисленный операнд
 */
public class TIntegerOperand extends TOperand
{
    /**
     * Конструктор
     * @param value -Значение операнда
     */
    public TIntegerOperand(String value)
    {
        super(new Integer(value));
    }
}
