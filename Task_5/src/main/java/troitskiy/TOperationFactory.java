package troitskiy;

/**
 * Фабрика, умеет делать операнды и операторы
 */
public abstract class TOperationFactory
{
    /**
     * Сделать оператор
     * @param name -Обозначение оператора (например +, -, MOD)
     * @return -возвращает объект оператор
     */
    abstract TOperator createOperator(String name);

    /**
     * Сделать операнд
     * @param value - Значение операнда
     * @return - Возвращает объект операнда
     */
    abstract TOperand  createOperand(String value);
}



