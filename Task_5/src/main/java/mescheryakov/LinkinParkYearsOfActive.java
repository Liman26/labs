package mescheryakov;

/**
 * Класс, предоставляющий информацию о годах активности группы Linkin Park
 */
public class LinkinParkYearsOfActive implements YearsActive {
    public LinkinParkYearsOfActive() {
        System.out.println("1996 — 2017");
    }
}
