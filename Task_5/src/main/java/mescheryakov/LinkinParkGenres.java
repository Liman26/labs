package mescheryakov;

/**
 * Класс, предоставляющий информацию о жанрах, в которых играла группа Linkin Park
 */
public class LinkinParkGenres implements Genres {
    public LinkinParkGenres() {
        System.out.println("Альтернативный рок | Ню-метал | Альтернативный метал | Рэп-рок | Поп-рок");
    }
}
