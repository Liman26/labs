package mescheryakov;

/**
 * Класс, предоставляющий информацию о годах активности группы Nirvana
 */
public class NirvanaYearsOfActive implements YearsActive {
    public NirvanaYearsOfActive() {
        System.out.println("1987 — 1994");
    }
}
