package mescheryakov;

/**
 * Интерфейс MusicGuide, являющийся абстрактной фабрикой и содержащий
 * в себе четыре метода, позволяющих узнать информацию об интересующей группе
 * (участники, годы активности, количество альбомов, жанры).
 */
public interface MusicGuide {
    Members getMembers();
    YearsActive getYearsOfActive();
    NumberOfAlbums getNumberOfAlbums();
    Genres getGenres();
}
