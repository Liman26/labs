package mescheryakov;

/**
 * Класс Nirvana, реализующий абстрактную фабрику MusicGuide, предоставляющий
 * возможность получить информацию об участниках группы, годах активности, количестве альбомов
 * и списку жанров, в которых играла группа.
 */
public class Nirvana implements MusicGuide{
    /**
     * Метод, позволяющий получить информацию об участниках группы.
     * @return возвращает новый класс, содержащий информацию об участниках группы
     */
    @Override
    public Members getMembers() {
        return new NirvanaMembers();
    }

    /**
     * Метод, позволяющий получить информацию о годах активности.
     * @return возвращает новый класс, содержащий информацию о годах активности
     */
    @Override
    public YearsActive getYearsOfActive() {
        return new NirvanaYearsOfActive();
    }

    /**
     * Метод, позволяющий получить информацию о количестве альбомов.
     * @return возвращает новый класс, содержащий информацию о количестве альбомов
     */
    @Override
    public NumberOfAlbums getNumberOfAlbums() {
        return new NirvanaNumberOfAlbums();
    }

    /**
     * Метод, позволяющий получить информацию о жанрах, в которых играла группа.
     * @return возвращает новый класс, содержащий информацию о жанрах
     */
    @Override
    public Genres getGenres() {
        return new NirvanaGenres();
    }
}
