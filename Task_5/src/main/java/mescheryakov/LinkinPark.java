package mescheryakov;

/**
 * Класс LinkinPark, реализующий абстрактную фабрику MusicGuide, предоставляющий
 * возможность получить информацию об участниках группы, годах активности, количестве альбомов
 * и списку жанров, в которых играла группа.
 */
public class LinkinPark implements MusicGuide{
    /**
     * Метод, позволяющий получить информацию об участниках группы.
     * @return возвращает новый класс, содержащий информацию об участниках группы
     */
    @Override
    public Members getMembers() {
        return new LinkinParkMembers();
    }

    /**
     * Метод, позволяющий получить информацию о годах активности.
     * @return возвращает новый класс, содержащий информацию о годах активности
     */
    @Override
    public YearsActive getYearsOfActive() {
        return new LinkinParkYearsOfActive();
    }

    /**
     * Метод, позволяющий получить информацию о количестве альбомов.
     * @return возвращает новый класс, содержащий информацию о количестве альбомов
     */
    @Override
    public NumberOfAlbums getNumberOfAlbums() {
        return new LinkinParkNumberOfAlbums();
    }

    /**
     * Метод, позволяющий получить информацию о жанрах, в которых играла группа.
     * @return возвращает новый класс, содержащий информацию о жанрах
     */
    @Override
    public Genres getGenres() {
        return new LinkinParkGenres();
    }
}
