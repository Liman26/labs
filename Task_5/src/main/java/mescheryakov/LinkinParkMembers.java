package mescheryakov;

/**
 * Класс, предоставляющий информацию об участниках группы Linkin Park
 */
public class LinkinParkMembers implements Members {
    public LinkinParkMembers() {
        System.out.println("""
                Честер Беннингтон
                Майк Шинода
                Брэд Дэлсон
                Дэвид Фаррелл
                Джо Хан
                Роб Бурдон""");
    }
}