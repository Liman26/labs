package mescheryakov;

/**
 * Класс, предоставляющий информацию об участниках группы Nirvana
 */
public class NirvanaMembers implements Members {
    public NirvanaMembers() {
        System.out.println("""
                Курт Кобейн
                Крист Новоселич
                Дэйв Грол""");
    }
}
