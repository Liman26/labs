package zurnachyan;

/**
 * Конкретный класс реализующий сущность ботинок компании Adidas
 */
public class AdidasBoots implements Boots{

    /**
     * Конструктор, выводит сообщение о создании экземпляра
     */
    public AdidasBoots(){
        System.out.println("Created new Adidas boots.");
    }

    /**
     * Метод возвращает строку с информацией об экземпляре класса (используется в тесте)
     * @return строка с информацией
     */
    @Override
    public String getMessage(){
        return "This is Adidas boots";
    }

}
