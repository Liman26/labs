package zurnachyan;

/**
 * Конкретный класс реализующий сущность ботинок компании Reebok
 */
public class ReebokBoots implements Boots{

    /**
     * Конструктор, выводит сообщение о создании экземпляра
     */
    public ReebokBoots(){
        System.out.println("Created new Reebok boots.");
    }

    /**
     * Метод возвращает строку с информацией об экземпляре класса (используется в тесте)
     * @return строка с информацией
     */
    @Override
    public String getMessage(){
        return "This is Reebok boots";
    }
}
