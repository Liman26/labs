package zurnachyan;

/**
 * Конкретный класс реализующий фабрику компании Reebok, на котрой собирают кроссовки или ботинки
 */
public class ReebokFactory implements FootWearFactory{

    /**
     * Метод реализует создание кроссовок по технологиям компании Reebok
     * @return экземпляр кроссовок компании Reebok
     */
    @Override
    public Sneakers makeSneakers(){
        return new ReebokSneakers();
    }

    /**
     * Метод реализует создание ботинок по технологиям компании Reebok
     * @return кземпляр ботинок компании Reebok
     */
    @Override
    public Boots makeBoots(){
        return new AdidasBoots();
    }

}
