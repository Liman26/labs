package zurnachyan;

/**
 * Конкретный класс реализующий сущность кроссовок компании Adidas
 */
public class AdidasSneakers implements Sneakers{

    /**
     * Конструктор, выводит сообщение о создании экземпляра
     */
    public AdidasSneakers(){
        System.out.println("Created new Adidas sneakers.");
    }

    /**
     * Метод возвращает строку с информацией об экземпляре класса (используется в тесте)
     * @return строка с информацией
     */
    @Override
    public String getMessage(){
        return "This is Adidas sneakers";
    }
}
