package zurnachyan;

/**
 * Конкретный класс реализующий сущность кроссовок компании Reebok
 */
public class ReebokSneakers implements Sneakers{

    /**
     * Конструктор, выводит сообщение о создании экземпляра
     */
    public ReebokSneakers(){
        System.out.println("Created new Reebok sneakers.");
    }

    /**
     * Метод возвращает строку с информацией об экземпляре класса (используется в тесте)
     * @return строка с информацией
     */
    @Override
    public String getMessage(){
        return "This is Reebok sneakers";
    }
}
