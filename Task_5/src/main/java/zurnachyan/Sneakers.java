package zurnachyan;

/**
 * Интерфейс для классов кроссовок.
 */
public interface Sneakers {

    /**
     * Метод возвращает сообщение с информацией об экземпляре, добавил его для теста
     * @return сообщение
     */
    String getMessage();
}
