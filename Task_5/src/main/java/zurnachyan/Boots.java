package zurnachyan;

/**
 * Интерфейс для классов ботинок.
 */
public interface Boots {
    /**
     * Метод возвращает сообщение с информацией об экземпляре, добавил его для теста
     * @return сообщение
     */
    String getMessage();
}
