package zurnachyan;

/**
 * Конкретный класс реализующий фабрику компании Adidas, на котрой собирают кроссовки или ботинки
 */
public class AdidasFactory implements FootWearFactory{

    /**
     * Метод реализует создание кроссовок по технологиям компании Adidas
     * @return экземпляр кроссовок компании Adidas
     */
    @Override
    public Sneakers makeSneakers(){
        return new AdidasSneakers();
    }

    /**
     * етод реализует создание ботинок по технологиям компании Adidas
     * @return кземпляр ботинок компании Adidas
     */
    @Override
    public Boots makeBoots(){
        return new AdidasBoots();
    }

}
