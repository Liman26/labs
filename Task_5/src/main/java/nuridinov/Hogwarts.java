package nuridinov;

import nuridinov.professions.Auror;
import nuridinov.professions.CurseBreaker;

/**
 * Фабрика фабрик, в данном случае фабриками служат факультеты
 */
public interface Hogwarts {
    /**
     * метод для создания мракоборца
     *
     * @return мракоборец
     */
    Auror releaseAuror();

    /**
     * метод для создания ликвидатора заклятий
     *
     * @return ликвидатор заклятий
     */
    CurseBreaker releaseCurseBreaker();

}
