package nuridinov.gryffindor;

import nuridinov.Hogwarts;
import nuridinov.professions.Auror;
import nuridinov.professions.CurseBreaker;

/**
 * Класс описывающий факультет Хогвартса, выпускающего студентов с присущими для факультета отличительной чертой
 */
public class Gryffindor implements Hogwarts {
    /**
     * Выпуск студента мракоборца
     *
     * @return возвращает храброго(отличительная черта факультета Гриффиндор ) мракоборца
     */
    @Override
    public Auror releaseAuror() {
        return new GryffindorAuror();
    }
    /**
     * Выпуск студента ликвидатора заклятий
     *
     * @return возвращает храброго(отличительная черта факультета Когтевран ) ликвидатора заклятий
     */
    @Override
    public CurseBreaker releaseCurseBreaker() {
        return new GryffindorCurseBreaker();
    }
}
