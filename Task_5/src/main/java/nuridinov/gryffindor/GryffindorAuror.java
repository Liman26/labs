package nuridinov.gryffindor;

import nuridinov.professions.Auror;
/**
 * класс описывающий Выпускника- Мракоборца
 */
public class GryffindorAuror implements Auror {
    /**
     * конструктор
     */
    public GryffindorAuror(){
        System.out.println("Храбрый мракоборец");
    }
}
