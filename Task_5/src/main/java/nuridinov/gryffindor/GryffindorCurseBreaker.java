package nuridinov.gryffindor;

import nuridinov.professions.CurseBreaker;
/**
 * класс описывающий Выпускника-Ликвидатора заклятий
 */
public class GryffindorCurseBreaker implements CurseBreaker{
    /**
     * Конструктор
     */
    public GryffindorCurseBreaker(){
        System.out.println("Храбрый ликвидатор заклинаний");
    }
}
