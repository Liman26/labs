package nuridinov.hufflepuff;

import nuridinov.Hogwarts;
import nuridinov.professions.Auror;
import nuridinov.professions.CurseBreaker;

/**
 * Класс описывающий факультет Хогвартса, выпускающего студентов с присущими для факультета отличительной чертой
 */
public class Hufflepuff implements Hogwarts {
    /**
     * Выпуск студента мракоборца
     *
     * @return возвращает трудолюбивого(отличительная черта факультета Пуффендуй ) мракоборца
     */
    @Override
    public Auror releaseAuror() {
        return new HufflepuffAuror();
    }
    /**
     * Выпуск студента ликвидатора заклятий
     *
     * @return возвращает трудолюбивого(отличительная черта факультета Пуффендуй ) ликвидатора заклятий
     */
    @Override
    public CurseBreaker releaseCurseBreaker() {
        return new HufflepuffCurseBreaker();
    }
}
