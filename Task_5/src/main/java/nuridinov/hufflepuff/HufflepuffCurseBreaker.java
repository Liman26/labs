package nuridinov.hufflepuff;

import nuridinov.professions.CurseBreaker;

/**
 * класс описывающий Выпускника-Ликвидатора заклятий
 */
public class HufflepuffCurseBreaker implements CurseBreaker {
    /**
     * конструктор
     */
    public HufflepuffCurseBreaker(){
        System.out.println("Трудолюбивый ликвидатор заклятий");
    }
}
