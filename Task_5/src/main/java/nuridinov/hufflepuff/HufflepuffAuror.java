package nuridinov.hufflepuff;

import nuridinov.professions.Auror;
/**
 * класс описывающий Выпускника- Мракоборца
 */
public class HufflepuffAuror implements Auror {
    /**
     * конструктор
     */
    public HufflepuffAuror(){
        System.out.println("Трудолюбивый мракоборец");
    }
}
