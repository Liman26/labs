package nuridinov.slytherin;

import nuridinov.professions.CurseBreaker;

/**
 * класс описывающий Выпускника-Ликвидатора заклятий
 */
public class SlytherinCurseBreaker implements CurseBreaker {
    /**
     * конструктор
     */
    public SlytherinCurseBreaker() {
        System.out.println("Хитрый ликвидатор заклятий");
    }
}
