package nuridinov.slytherin;

import nuridinov.Hogwarts;
import nuridinov.professions.Auror;
import nuridinov.professions.CurseBreaker;

/**
 * Класс описывающий факультет Хогвартса, выпускающего студентов с присущими для факультета отличительной чертой
 */
public class Slytherin implements Hogwarts {
    /**
     * Выпуск студента мракоборца
     *
     * @return возвращает хитрого(отличительная черта факультета Слизерин ) мракоборца
     */
    @Override
    public Auror releaseAuror() {
        return new SlytherinAuror();
    }

    /**
     * Выпуск студента ликвидатора заклятий
     *
     * @return возвращает хитрого(отличительная черта факультета Слизерин ) Ликвидатора заклятий
     */
    @Override
    public CurseBreaker releaseCurseBreaker() {
        return new SlytherinCurseBreaker();
    }
}
