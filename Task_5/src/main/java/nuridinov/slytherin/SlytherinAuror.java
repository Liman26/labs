package nuridinov.slytherin;

import nuridinov.professions.Auror;

/**
 * класс описывающий Выпускника- Мракоборца
 */
public class SlytherinAuror implements Auror {
    /**
     * конструктор
     */
    public SlytherinAuror() {
        System.out.println("Хитрый мракоборец");
    }
}
