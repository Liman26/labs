package nuridinov.ravenclaw;

import nuridinov.Hogwarts;
import nuridinov.professions.Auror;
import nuridinov.professions.CurseBreaker;

/**
 * Класс описывающий факультет Хогвартса, выпускающего студентов с присущими для факультета отличительной чертой
 */
public class Ravenclaw implements Hogwarts {
    /**
     * Выпуск студента мракоборца
     *
     * @return возвращает умного(отличительная черта факультета Когтевран ) мракоборца
     */
    @Override
    public Auror releaseAuror() {
        return new RavenclawAuror();
    }

    /**
     * Выпуск студента ликвидатора заклятий
     *
     * @return возвращает умного(отличительная черта факультета Когтевран ) ликвидатора заклятий
     */
    @Override
    public CurseBreaker releaseCurseBreaker() {
        return new RavenclawCurseBreaker();
    }
}
