package nuridinov.ravenclaw;

import nuridinov.professions.CurseBreaker;
/**
 * класс описывающий Выпускника-Ликвидатора заклятий
 */
public class RavenclawCurseBreaker implements CurseBreaker {
    /**
     * конструктор
     */
    public RavenclawCurseBreaker(){
        System.out.println("Очень умный ликвидатор заклятий");
    }
}
