package nuridinov.ravenclaw;

import nuridinov.professions.Auror;
/**
 * класс описывающий Выпускника- Мракоборца
 */
public class RavenclawAuror implements Auror {
    /**
     * конструктор
     */
    public RavenclawAuror(){
        System.out.println("Очень умный мракоборец");
    }
}
