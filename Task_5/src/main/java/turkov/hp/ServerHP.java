package turkov.hp;

import turkov.Server;

/**
 * Класс, который описывает физический сервер фирмы HP.
 */
public class ServerHP implements Server {
    /**
     * Метод, который описывает функционирование физического сервера фирмы HP.
     */
    @Override
    public String workServer() {
        return "ServerHP working";
    }
}