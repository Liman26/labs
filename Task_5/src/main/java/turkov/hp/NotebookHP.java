package turkov.hp;

import turkov.Notebook;

/**
 * Класс, который описывает настольный ноутбук фирмы HP.
 */
public class NotebookHP implements Notebook {
    /**
     * Метод, который описывает функционирование ноутбука фирмы HP.
     */
    @Override
    public String workNotebook() {
        return "NotebookHP working";
    }
}