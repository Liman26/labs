package turkov.hp;

import turkov.Desktop;
import turkov.FactoryElectronic;
import turkov.Notebook;
import turkov.Server;

/**
 * Класс реализующий шаблон проектирования "Абстрактная фабрика", которая производит средства вычислительной техники
 * (ноутбуки, настольные ПК, сервера) марки HP.
 */
public class FactoryElectronicsHP implements FactoryElectronic {
    /**
     * Метод создания ноутбука
     *
     * @return - объект "ноутбук" марки HP
     */
    @Override
    public Notebook produceNotebook() {
        return new NotebookHP();
    }

    /**
     * Метод создания настольного ПК
     *
     * @return - объект "настольный ПК" марки HP
     */
    @Override
    public Desktop produceDesktop() {
        return new DesktopHP();
    }

    /**
     * Метод создания сервера
     *
     * @return - объект "сервер" марки HP
     */
    @Override
    public Server produceServer() {
        return new ServerHP();
    }
}