package turkov.hp;

import turkov.Desktop;

/**
 * Класс, который описывает настольный ПК фирмы HP.
 */
public class DesktopHP implements Desktop {
    /**
     * Метод, который описывает работу настольного ПК фирмы HP.
     */
    @Override
    public String workDesktop() {
        return "DesktopHP working";
    }
}