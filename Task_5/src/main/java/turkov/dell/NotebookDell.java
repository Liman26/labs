package turkov.dell;

import turkov.Notebook;

/**
 * Класс, который описывает настольный ноутбук фирмы DELL.
 */
public class NotebookDell implements Notebook {
    /**
     * Метод, который описывает функционирование ноутбука фирмы DELL.
     */
    @Override
    public String workNotebook() {
        return "NotebookDELL working";
    }
}