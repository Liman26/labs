package turkov.dell;

import turkov.Desktop;

/**
 * Класс, который описывает настольный ПК фирмы DELL.
 */
public class DesktopDell implements Desktop {

    /**
     * Метод, который описывает работу настольного ПК фирмы DELL.
     */
    @Override
    public String workDesktop() {
        return "DesktopDELL working";
    }
}