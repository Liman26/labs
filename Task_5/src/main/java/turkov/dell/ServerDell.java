package turkov.dell;

import turkov.Server;

/**
 * Класс, который описывает физический сервер фирмы DELL.
 */
public class ServerDell implements Server {
    /**
     * Метод, который описывает функционирование физического сервера фирмы DELL.
     */
    @Override
    public String workServer() {
        return "ServerDELL working";
    }
}