package turkov.dell;

import turkov.Desktop;
import turkov.FactoryElectronic;
import turkov.Notebook;
import turkov.Server;

/**
 * Класс реализующий шаблон проектирования "Абстрактная фабрика", которая производит средства вычислительной техники
 * (ноутбуки, настольные ПК, сервера) марки DELL.
 */
public class FactoryElectronicsDell implements FactoryElectronic {
    /**
     * Метод создания ноутбука
     *
     * @return - объект "ноутбук" марки DELL
     */
    @Override
    public Notebook produceNotebook() {
        return new NotebookDell();
    }

    /**
     * Метод создания настольного ПК
     *
     * @return - объект "настольный ПК" марки DELL
     */
    @Override
    public Desktop produceDesktop() {
        return new DesktopDell();
    }

    /**
     * Метод создания сервера
     *
     * @return - объект "сервер" марки DELL
     */
    @Override
    public Server produceServer() {
        return new ServerDell();
    }
}