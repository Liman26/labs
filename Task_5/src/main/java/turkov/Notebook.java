package turkov;

/**
 * интерфейс, который описывает ноутбук в качестве средства вычислительной техники
 */
public interface Notebook {
    /**
     * Абстрактный метод, который должен реализовать работу ноутбука
     */
    String workNotebook();
}
