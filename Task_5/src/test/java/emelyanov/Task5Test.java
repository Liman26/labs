package emelyanov;

import emelyanov.base.AircraftsFactory;
import emelyanov.boeing.BoeingBomber;
import emelyanov.boeing.BoeingFactory;
import emelyanov.boeing.BoeingFighter;
import emelyanov.boeing.BoeingPassenger;
import emelyanov.sukhoi.SukhoiBomber;
import emelyanov.sukhoi.SukhoiFactory;
import emelyanov.sukhoi.SukhoiFighter;
import emelyanov.sukhoi.SukhoiPassenger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Task5Test {
  @Test
  public void testSukhoiFactory() {
    AircraftsFactory factory = new SukhoiFactory();
    assertEquals(factory.createBomber().getClass(), SukhoiBomber.class);
    assertEquals(factory.createFighter().getClass(), SukhoiFighter.class);
    assertEquals(factory.createPassenger().getClass(), SukhoiPassenger.class);
  }

  @Test
  public void testBoeingFactory() {
    AircraftsFactory factory = new BoeingFactory();
    assertEquals(factory.createBomber().getClass(), BoeingBomber.class);
    assertEquals(factory.createFighter().getClass(), BoeingFighter.class);
    assertEquals(factory.createPassenger().getClass(), BoeingPassenger.class);
  }
}
