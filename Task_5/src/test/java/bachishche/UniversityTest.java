package bachishche;


import bachishche.engineers.OxfordEngineer;
import bachishche.engineers.YaleEngineer;
import bachishche.teachers.OxfordTeacher;
import bachishche.teachers.YaleTeacher;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Тестирование абстрактой фабрики "Университет"
 */
class UniversityTest {
    /**
     * Тестирование выпуска студентов специальностей
     * преподаватель и инженер
     * в университете Oxford
     */
    @Test
    public void oxfordStudentsTest() {
        University university = new OxfordUniversity();
        assertEquals(OxfordUniversity.class, university.getClass());
        assertEquals(OxfordTeacher.class, university.getTeacher().getClass());
        assertEquals(OxfordEngineer.class, university.getEngineer().getClass());
    }

    /**
     * Тестирование выпуска студентов специальностей
     * преподаватель и инженер
     * в университете Yale
     */
    @Test
    public void yaleStudentsTest() {
        University university = new YaleUniversity();
        assertEquals(YaleUniversity.class, university.getClass());
        assertEquals(YaleTeacher.class, university.getTeacher().getClass());
        assertEquals(YaleEngineer.class, university.getEngineer().getClass());
    }
}