package zhirnov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import zhirnov.archers.AllianceArcher;
import zhirnov.archers.HordeArcher;
import zhirnov.factories.AllianceFactory;
import zhirnov.factories.HordeFactory;
import zhirnov.factories.SoldierFactory;
import zhirnov.warriors.AllianceWarrior;
import zhirnov.warriors.HordeWarrior;

/**
 * Класс для проверки работы SoldierFactory, AllianceFactory и HordeFactory
 */
public class Task5Test {
    /**
     * Метод проверки на то, что из HordeFactory получается HordeWarrior
     */
    @Test
    public void testWarriorHordeFactory(){
        SoldierFactory soldierFactory = new HordeFactory();
        Assertions.assertEquals(HordeWarrior.class, soldierFactory.createWarrior().getClass());
    }

    /**
     * Метод проверки на то, что из HordeFactory получается HordeArcher
     */
    @Test
    public void testArcherHordeFactory(){
        SoldierFactory soldierFactory = new HordeFactory();
        Assertions.assertEquals(HordeArcher.class, soldierFactory.createArcher().getClass());
    }

    /**
     * Метод проверки на то, что из AllianceFactory получается AllianceWarrior
     */
    @Test
    public void testWarriorAllianceFactory(){
        SoldierFactory soldierFactory = new AllianceFactory();
        Assertions.assertEquals(AllianceWarrior.class, soldierFactory.createWarrior().getClass());
    }

    /**
     * Метод проверки на то, что из AllianceFactory получается AllianceArcher
     */
    @Test
    public void testArcherAllianceFactory(){
        SoldierFactory soldierFactory = new AllianceFactory();
        Assertions.assertEquals(AllianceArcher.class, soldierFactory.createArcher().getClass());
    }
}
