package vereshchagina;


import org.junit.jupiter.api.Test;
import vereshchagina.villagehiddenleaf.LeafChuunin;
import vereshchagina.villagehiddenleaf.LeafGenin;
import vereshchagina.villagehiddenleaf.LeafJounin;
import vereshchagina.villagehiddenleaf.LeafRegArmy;
import vereshchagina.villagehiddenrain.RainChuunin;
import vereshchagina.villagehiddenrain.RainGenin;
import vereshchagina.villagehiddenrain.RainJounin;
import vereshchagina.villagehiddenrain.RainRegArmy;
import vereshchagina.villagehiddensand.SandChuunin;
import vereshchagina.villagehiddensand.SandGenin;
import vereshchagina.villagehiddensand.SandJounin;
import vereshchagina.villagehiddensand.SandRegArmy;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestTask5 {
    /**
     * тест создания регулярной армии в деревне Скрытого Листа
     * проверка на принадлежность созданной армии к соответствующей деревне
     * проверка принадлежности созданного ниндзя к соответствующему рангу
     */
    @Test
    public void test1() {
        RegularArmy arm = new LeafRegArmy();
        assertEquals(LeafRegArmy.class, arm.getClass());
        assertEquals(LeafChuunin.class, arm.createChuunin().getClass());
        assertEquals(LeafGenin.class, arm.createGenin().getClass());
        assertEquals(LeafJounin.class, arm.createJounin().getClass());
    }

    /**
     * тест создания регулярной армии в деревне Скрытого Дождя
     * проверка на принадлежность созданной армии к соответствующей деревне
     * проверка принадлежности созданного ниндзя к соответствующему рангу
     */
    @Test
    public void test2() {
        RegularArmy arm = new RainRegArmy();
        assertEquals(RainRegArmy.class, arm.getClass());
        assertEquals(RainChuunin.class, arm.createChuunin().getClass());
        assertEquals(RainGenin.class, arm.createGenin().getClass());
        assertEquals(RainJounin.class, arm.createJounin().getClass());
    }

    /**
     * тест создания регулярной армии в деревне Скрытого Песка
     * проверка на принадлежность созданной армии к соответствующей деревне
     * проверка принадлежности созданного ниндзя к соответствующему рангу
     */
    @Test
    public void test3() {
        RegularArmy arm = new SandRegArmy();
        assertEquals(SandRegArmy.class, arm.getClass());
        assertEquals(SandChuunin.class, arm.createChuunin().getClass());
        assertEquals(SandGenin.class, arm.createGenin().getClass());
        assertEquals(SandJounin.class, arm.createJounin().getClass());
    }
}
