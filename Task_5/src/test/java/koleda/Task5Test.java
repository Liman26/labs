package koleda;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Task5Test {
    @Test
        public void test1(){
        TechFactory factory = new SamsungFactory();
        assertEquals(SamsungPhone.class, factory.createPhone().getClass());
    }

    @Test
    public void test2(){
        TechFactory factory = new SamsungFactory();
        assertEquals(SamsungLaptop.class, factory.createLaptop().getClass());
    }

    @Test
    public void test3(){
        TechFactory factory = new AppleFactory();
        assertEquals(ApplePhone.class, factory.createPhone().getClass());
    }

    @Test
    public void test4(){
        TechFactory factory = new AppleFactory();
        assertEquals(AppleLaptop.class, factory.createLaptop().getClass());
    }
}
