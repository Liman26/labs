package turkov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import turkov.dell.DesktopDell;
import turkov.dell.FactoryElectronicsDell;
import turkov.dell.NotebookDell;
import turkov.dell.ServerDell;
import turkov.hp.DesktopHP;
import turkov.hp.FactoryElectronicsHP;
import turkov.hp.NotebookHP;
import turkov.hp.ServerHP;

/**
 * Класс для тестирования реализации паттерна "Абстрактная фабрика" на фабри, которые производят средства
 * вычислительной техники под брендами HP, DELL.
 */
public class AbstractFactoryElectronicTest {

    /**
     * Метод тестирующий производство настольных ПК (desktop), ноутбуков (notebook) и серверов (server)
     * на фабрике бренда DELL. Выполняется проверка принадлежности произведенного оборудования классу (вид и бренд)
     * для производимой техники, а также метода (выводимая строка), который демонстрирует его работу.
     */
    @Test
    public void FactoryElectronicsDellTest() {
        FactoryElectronic factoryElectronicDell = new FactoryElectronicsDell();
        Assertions.assertEquals(DesktopDell.class, factoryElectronicDell.produceDesktop().getClass());
        Assertions.assertEquals(NotebookDell.class, factoryElectronicDell.produceNotebook().getClass());
        Assertions.assertEquals(ServerDell.class, factoryElectronicDell.produceServer().getClass());
        Assertions.assertEquals("DesktopDELL working", factoryElectronicDell.produceDesktop().workDesktop());
        Assertions.assertEquals("NotebookDELL working", factoryElectronicDell.produceNotebook().workNotebook());
        Assertions.assertEquals("ServerDELL working", factoryElectronicDell.produceServer().workServer());
    }

    /**
     * Метод тестирующий производство настольных ПК (desktop), ноутбуков (notebook) и серверов (server)
     * на фабрике бренда HP. Выполняется проверка принадлежности произведенного оборудования классу (вид и бренд)
     * для производимой техники, а также метода (выводимая строка), который демонстрирует его работу.
     */
    @Test
    public void FactoryElectronicsHPTest() {
        FactoryElectronic factoryElectronicHP = new FactoryElectronicsHP();
        Assertions.assertEquals(DesktopHP.class, factoryElectronicHP.produceDesktop().getClass());
        Assertions.assertEquals(NotebookHP.class, factoryElectronicHP.produceNotebook().getClass());
        Assertions.assertEquals(ServerHP.class, factoryElectronicHP.produceServer().getClass());
        Assertions.assertEquals("DesktopHP working", factoryElectronicHP.produceDesktop().workDesktop());
        Assertions.assertEquals("NotebookHP working", factoryElectronicHP.produceNotebook().workNotebook());
        Assertions.assertEquals("ServerHP working", factoryElectronicHP.produceServer().workServer());
    }
}
