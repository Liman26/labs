package nuridinov;

import nuridinov.gryffindor.*;
import nuridinov.ravenclaw.*;
import nuridinov.hufflepuff.*;
import nuridinov.slytherin.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс для проверки фабрики фабрик
 */
public class HogwartsTest {
    /**
     * Метод для проверки типа вызываемой фабрики (факультета)
     * В данном методе ожидается факультет Гриффиндор
     */
    @Test
    public void GryffindorTest(){
        Hogwarts wizard = new Gryffindor();
        assertEquals(Gryffindor.class, wizard.getClass());
    }

    /**
     * Метод для проверки типа(принадлежности к факультету) выпускников мракоборцев/ликвидаторов-заклятий
     * В данном методе ожидается, что они принадлежат факультету Гриффиндор
     */
    @Test
    public void GryffindorAlumniTest(){
        Hogwarts wizard = new Gryffindor();
        assertEquals(GryffindorAuror.class, wizard.releaseAuror().getClass());
        assertEquals(GryffindorCurseBreaker.class, wizard.releaseCurseBreaker().getClass());
    }

    /**
     * Метод для проверки типа вызываемой фабрики (факультета)
     * В данном методе ожидается факультет Когтевран
     */
    @Test
    public void RavenclawTest(){
        Hogwarts wizard = new Ravenclaw();
        assertEquals(Ravenclaw.class, wizard.getClass());
    }

    /**
     * Метод для проверки типа(принадлежности к факультету) выпускников мракоборцев/ликвидаторов-заклятий
     * В данном методе ожидается, что они принадлежат факультету Когтевран
     */
    @Test
    public void RavenclawAlumniTest(){
        Hogwarts wizard = new Ravenclaw();
        assertEquals(RavenclawAuror.class, wizard.releaseAuror().getClass());
        assertEquals(RavenclawCurseBreaker.class, wizard.releaseCurseBreaker().getClass());
    }
    /**
     * Метод для проверки типа вызываемой фабрики (факультета)
     * В данном методе ожидается факультет Пуффендуй
     */
    @Test
    public void HufflepuffTest(){
        Hogwarts wizard = new Hufflepuff();
        assertEquals(Hufflepuff.class, wizard.getClass());
    }
    /**
     * Метод для проверки типа(принадлежности к факультету) выпускников мракоборцев/ликвидаторов-заклятий
     * В данном методе ожидается, что они принадлежат факультету Пуффендуй
     */
    @Test
    public void HufflepuffAlumniTest(){
        Hogwarts wizard = new Hufflepuff();
        assertEquals(HufflepuffAuror.class, wizard.releaseAuror().getClass());
        assertEquals(HufflepuffCurseBreaker.class, wizard.releaseCurseBreaker().getClass());
    }
    /**
     * Метод для проверки типа вызываемой фабрики (факультета)
     * В данном методе ожидается факультет Слизерин
     */
    @Test
    public void SlytherinTest(){
        Hogwarts wizard = new Slytherin();
        assertEquals(Slytherin.class, wizard.getClass());
    }
    /**
     * Метод для проверки типа(принадлежности к факультету) выпускников мракоборцев/ликвидаторов-заклятий
     * В данном методе ожидается, что они принадлежат факультету Слизерин
     */
    @Test
    public void SlytherinAlumniTest(){
        Hogwarts wizard = new Slytherin();
        assertEquals(SlytherinAuror.class, wizard.releaseAuror().getClass());
        assertEquals(SlytherinCurseBreaker.class, wizard.releaseCurseBreaker().getClass());
    }


}
