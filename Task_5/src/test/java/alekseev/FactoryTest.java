package alekseev;

import alekseev.apdu.SmartCardApduReader;
import alekseev.apdu.SmartCardFactory;
import alekseev.apdu.SmartCardNfcReader;
import alekseev.nfc.UnoApduReader;
import alekseev.nfc.UnoNfcReader;
import alekseev.nfc.UnoReaderFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author alekseev.a
 * @since 1.0
 */
class FactoryTest {
    @Test
    void smartcardsTest() {
        CardReadersFactory smartApduFactory = new SmartCardFactory();
        assertEquals(SmartCardApduReader.class, smartApduFactory.createApduReader().getClass());

        CardReadersFactory smartNfcFactory = new SmartCardFactory();
        assertEquals(SmartCardNfcReader.class, smartNfcFactory.createNfcReader().getClass());
    }

    @Test
    void unoTest() {
        CardReadersFactory unoApduFactory = new UnoReaderFactory();
        assertEquals(UnoApduReader.class, unoApduFactory.createApduReader().getClass());

        CardReadersFactory unoNfcFactory = new UnoReaderFactory();
        assertEquals(UnoNfcReader.class, unoNfcFactory.createNfcReader().getClass());
    }


}
