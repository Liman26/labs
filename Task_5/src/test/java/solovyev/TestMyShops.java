package solovyev;

import org.junit.jupiter.api.Test;
import solovyev.asusShop.*;
import solovyev.dellShop.*;
import solovyev.lenovoShop.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Класс для проверки моей реализации абстрактной фабрики
 */
public class TestMyShops {

    /**
     * Метод для проверки типа вызываемого магазина
     * В этом тесте вызвывается магазин LenovoShop.
     */
    @Test
    public void LenovoShopTest() {
        NotebookShop nShop = new LenovoShop();
        assertEquals(LenovoShop.class, nShop.getClass());
    }

    /**
     * Метод для проверки типа продаваемого ноутбука
     * Используемый магазин LenovoShop, поэтому
     * ожидается что при продаже определенного типа ноутбука
     * он будет совпадать с LenovoGamingLaptop и тд.
     */
    @Test
    public void LenovoShopSellTest() {
        NotebookShop nShop = new LenovoShop();
        assertEquals(LenovoGamingLaptop.class, nShop.sellGamingLaptop().getClass());
        assertEquals(LenovoOfficeLaptop.class, nShop.sellOfficeLaptop().getClass());
        assertEquals(LenovoUltrabook.class, nShop.sellUltrabook().getClass());
    }

    /**
     * Метод для проверки типа вызываемого магазина
     * В этом тесте вызвывается магазин DellShop.
     */
    @Test
    public void DellShopTest() {
        NotebookShop nShop = new DellShop();
        assertEquals(DellShop.class, nShop.getClass());
    }

    /**
     * Метод для проверки типа продаваемого ноутбука
     * Используемый магазин DellShop, поэтому
     * ожидается что при продаже определенного типа ноутбука
     * он будет совпадать с DellGamingLaptop и тд.
     */
    @Test
    public void DellShopSellTest() {
        NotebookShop nShop = new DellShop();
        assertEquals(DellGamingLaptop.class, nShop.sellGamingLaptop().getClass());
        assertEquals(DellOfficeLaptop.class, nShop.sellOfficeLaptop().getClass());
        assertEquals(DellUltrabook.class, nShop.sellUltrabook().getClass());
    }

    /**
     * Метод для проверки типа вызываемого магазина
     * В этом тесте вызвывается магазин AsusShop.
     */
    @Test
    public void AsusShopTest() {
        NotebookShop nShop = new AsusShop();
        assertEquals(AsusShop.class, nShop.getClass());
    }

    /**
     * Метод для проверки типа продаваемого ноутбука
     * Используемый магазин AsusShop, поэтому
     * ожидается что при продаже определенного типа ноутбука
     * он будет совпадать с AsusGamingLaptop и тд.
     */
    @Test
    public void AsusShopSellTest() {
        NotebookShop nShop = new AsusShop();
        assertEquals(AsusGamingLaptop.class, nShop.sellGamingLaptop().getClass());
        assertEquals(AsusOfficeLaptop.class, nShop.sellOfficeLaptop().getClass());
        assertEquals(AsusUltrabook.class, nShop.sellUltrabook().getClass());
    }
}
