package zurnachyan;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Task5Test {

    /**
     * Проверка на корректное создание экземпляра кроссовок Adidas через фабрику
     */
    @Test
    public void test1(){
        FootWearFactory factory = new AdidasFactory();
        Sneakers sneak = new AdidasSneakers();
        AdidasSneakers test = new AdidasSneakers();
        assertEquals(test.getMessage(), sneak.getMessage());
    }

    /**
     * Проверка на корректное создание экземпляра ботинок Adidas через фабрику
     */
    @Test
    public void test2(){
        FootWearFactory factory = new AdidasFactory();
        Boots boots = new AdidasBoots();
        AdidasBoots test = new AdidasBoots();
        assertEquals(test.getMessage(), boots.getMessage());
    }

    /**
     * Проверка на корректное создание экземпляра кроссовок Reebok через фабрику
     */
    @Test
    public void test3(){
        FootWearFactory factory = new ReebokFactory();
        Sneakers sneak = new ReebokSneakers();
        ReebokSneakers test = new ReebokSneakers();
        assertEquals(test.getMessage(), sneak.getMessage());
    }

    /**
     * Проверка на корректное создание экземпляра ботинок Reebok через фабрику
     */
    @Test
    public void test4(){
        FootWearFactory factory = new ReebokFactory();
        Boots boots = new ReebokBoots();
        ReebokBoots test = new ReebokBoots();
        assertEquals(test.getMessage(), boots.getMessage());
    }
}
