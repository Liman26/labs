package mescheryakov;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Task5Test {

    @Test
    public void test1() {
        MusicGuide musicGuide = new LinkinPark();
        assertEquals(LinkinParkGenres.class, musicGuide.getGenres().getClass());
    }

    @Test
    public void test2() {
        MusicGuide musicGuide = new LinkinPark();
        assertEquals(LinkinParkMembers.class, musicGuide.getMembers().getClass());
    }

    @Test
    public void test3() {
        MusicGuide musicGuide = new LinkinPark();
        assertEquals(LinkinParkNumberOfAlbums.class, musicGuide.getNumberOfAlbums().getClass());
    }

    @Test
    public void test4() {
        MusicGuide musicGuide = new LinkinPark();
        assertEquals(LinkinParkYearsOfActive.class, musicGuide.getYearsOfActive().getClass());
    }

    @Test
    public void test5() {
        MusicGuide musicGuide = new Nirvana();
        assertEquals(NirvanaGenres.class, musicGuide.getGenres().getClass());
    }

    @Test
    public void test6() {
        MusicGuide musicGuide = new Nirvana();
        assertEquals(NirvanaMembers.class, musicGuide.getMembers().getClass());
    }

    @Test
    public void test7() {
        MusicGuide musicGuide = new Nirvana();
        assertEquals(NirvanaNumberOfAlbums.class, musicGuide.getNumberOfAlbums().getClass());
    }

    @Test
    public void test8() {
        MusicGuide musicGuide = new Nirvana();
        assertEquals(NirvanaYearsOfActive.class, musicGuide.getYearsOfActive().getClass());
    }
}
