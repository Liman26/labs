package troitskiy;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * Проверка работы фабрик TDoubleFactory и TIntegerFactory,
 *
 */
public class Task5Test
{

    /**
     * Проверка работы фабрики TDoubleFactory, часть 1
     */
    @Test
    public void test1()
    {
        TOperationFactory factory = new TDoubleFactory();
        Assertions.assertEquals(TDoubleOperand.class, factory.createOperand("100").getClass());
    }

    /**
     * Проверка работы фабрики TDoubleFactory, часть 2
     */
    @Test
    public void test2()
    {
        TOperationFactory factory = new TDoubleFactory();
        Assertions.assertEquals(TDoubleOperator.class, factory.createOperator("+").getClass());
    }

    /**
     * Проверка работы фабрики TIntegerFactory, часть 1
     */
    @Test
    public void test3()
    {
        TOperationFactory factory = new TIntegerFactory();
        Assertions.assertEquals(TIntegerOperand.class, factory.createOperand("100").getClass());
    }

    /**
     * Проверка работы фабрики TIntegerFactory, часть 2
     */
    @Test
    public void test4()
    {
        TOperationFactory factory = new TIntegerFactory();
        Assertions.assertEquals(TIntegerOperator.class, factory.createOperator("+").getClass());
    }
}
