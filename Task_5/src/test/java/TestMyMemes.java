import gushchin.MemesFactory;
import gushchin.cat.memes.*;
import gushchin.parrot.memes.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestMyMemes {
    /**
     * Тестирование корректного создания объектов Cat<...>Meme с помощью фабрики CatMemeFactory
     */
    @Test
    public void catMemeFactoryTest() {
        MemesFactory factory = new CatMemeFactory();

        assertEquals(CatAudioMeme.class, factory.createAudioMeme().getClass());
        assertEquals(CatTextMeme.class, factory.createTextMeme().getClass());
        assertEquals(CatVideoMeme.class, factory.createVideoMeme().getClass());
    }

    /**
     * Тестирование корректного создания объектов Parrot<...>Meme с помощью фабрики CatMemeFactory
     */
    @Test
    public void parrotMemeFactoryTest() {
        MemesFactory factory = new ParrotMemeFactory();

        assertEquals(ParrotAudioMeme.class, factory.createAudioMeme().getClass());
        assertEquals(ParrotTextMeme.class, factory.createTextMeme().getClass());
        assertEquals(ParrotVideoMeme.class, factory.createVideoMeme().getClass());
    }
}
