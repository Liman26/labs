package zhirnov;

/**
 * Класс, реализующий стек
 */
public class MyStack {
    private Object[] stack;
    private int firstIndex;
    private int lastIndex;

    /**
     * Конструктор класса
     */
    public MyStack() {
        stack = new Object[10];
        this.lastIndex = -1;
    }

    public Object[] getStack() {
        return stack;
    }

    public int getLastIndex() {
        return lastIndex;
    }

    /**
     * Метод, вставляющий объект в стек. Если массив, содержащий объекты стека, переполнен, то он расширяется
     * в 2 раза
     * @param object - вставляемый объект
     */
    public void push(Object object){
        if(object == null){
            System.out.println("Попытка вставить null элемент!");
        }else{
            try{
                int stackLength = stack.length;
                if (lastIndex + 1 == stackLength){
                    Object[] newStack = new Object[stackLength * 2];
                    System.arraycopy(stack, 0, newStack, 0, stackLength);
                    stack = newStack;
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            stack[++lastIndex] = object;
            System.out.println("Элемент вставлен успешно!");
        }
    }

    /**
     * Метод, возвращающий верхний элемент после удаления из стека
     * @return - верхний удаленный элемент стека
     */
    public Object pop(){
        if (isEmpty()){
            System.out.println("Стек пустой!");
            return null;
        }else{
            Object returnObject = stack[lastIndex];
            stack[lastIndex--] = null;
            System.out.println("Элемент удален успешно!");
            return returnObject;
        }

    }

    /**
     * Метод, проверяющий, имеются ли элементы в очереди
     * @return - true, если очередь пустая, иначе false
     */
    public boolean isEmpty(){
        if (stack[0] == null){
            return true;
        }else{
            return false;
        }

    }

    /**
     * Метод, возвращающий верхний элемент без удаления из стека
     * @return - верхний элемент стека
     */
    public Object top(){
        if (isEmpty()){
            System.out.println("Стек пустой!");
            return null;
        }else{
            return stack[lastIndex];
        }
    }
}
