package zhirnov;

/**
 * Класс, реализцющий очередь
 */
public class MyQueue {
    private Object[] queue;
    private int lastIndex;

    /**
     * Конструктор класса
     */
    public MyQueue() {
        queue = new Object[10];
        lastIndex = -1;
    }

    public Object[] getQueue() {
        return queue;
    }

    public int getLastIndex() {
        return lastIndex;
    }

    /**
     * Метод, вставляющий объект в очередь. Если массив, содержащий объекты очереди, переполнен, то он расширяется
     * в 2 раза
     *
     * @param object - вставляемый объект
     */
    public void enqueue(Object object) {
        if (object == null){
            System.out.println("Попытка вставить null элемент!");
        }else{
            try {
                int queueLength = queue.length;
                if (lastIndex + 1 == queueLength) {
                    Object[] newQueue = new Object[queueLength * 2];
                    System.arraycopy(queue, 0, newQueue, 0, queueLength);
                    queue = newQueue;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            lastIndex++;
            queue[lastIndex] = object;
            System.out.println("Элемент вставлен успешно!");
        }
    }

    /**
     * Метод, удаляющий элемент из начала очереди
     */
    public void dequeue() {
        if (isEmpty()) {
            System.out.println("Очередь пустая!");
        } else {
            for (int i = 1; i <= lastIndex; i++){
                queue[i - 1] = queue[i];
            }
            queue[lastIndex] = null;
            lastIndex--;
            System.out.println("Элемент удален успешно!");
        }
    }

    /**
     * Метод, проверяющий, имеются ли элементы в очереди
     *
     * @return - true, если очередь пустая, иначе false
     */
    public boolean isEmpty() {
        if (queue[0] == null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Метод, возвращающий первый элемент в очереди
     * @return - первый элемент в очереди
     */
    public Object top() {
        if (isEmpty()) {
            System.out.println("Очередь пустая!");
            return null;
        } else {
            return queue[0];
        }
    }

    /**
     * Метод, печатающий все элементы очереди
     */
    public void printQueue() {
        for (Object o : queue) {
            if (o != null) {
                System.out.println(o);
            }
        }
    }
}
