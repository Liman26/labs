package koleda;

public class Queue {
    private int front, end, count;
    private final Object[] queue;


    /**
     * Конструктор класса очереди
     * @param _size - задается объем очереди
     */
    public Queue(int _size) {
        queue = new Object[_size];
        front = 0;
        end = 0;
        count = 0;
    }

    /**
     * Вставка элемента в конец очереди
     * @param obj - объект, который следует поместить в очередь
     * @throws UnsupportedOperationException - если очередь переполнена, то вызывается исключение
     */
    public void enqueue(Object obj) throws UnsupportedOperationException{
        if (count == queue.length)
            throw new UnsupportedOperationException("full");
        queue[end++] = obj;
        count++;
        if (end == queue.length){
            end = 0;
        }
    }

    /**
     * Удаление элемента из начала очереди
     * @return - первый элемент очереди
     * @throws UnsupportedOperationException - если очередь пуста, то вызывается исключение
     */
    public Object dequeue() throws UnsupportedOperationException{
        Object obj;
        if (isEmpty() == true){
            throw  new UnsupportedOperationException("empty");
        }
        obj = queue[front];
        queue[front++] = null;
        count--;
        if (front == getSize()){
            front = 0;
        }
        return obj;
    }

    /**
     * Возвращаем первый элемент из очереди
     * @return - первый элемент очереди
     * @throws UnsupportedOperationException - если очередь пуста, то вызывается исключение
     */
    public Object top() throws UnsupportedOperationException{
        if (isEmpty() == true){
            throw new UnsupportedOperationException("Очередь пуста");
        }
        return queue[front];
    }

    /**
     * Проверка очереди на пустоту
     * @return true, если очередь не пуста, иначе false
     */
    public boolean isEmpty() {return (count == 0); }

    public int getSize(){ return queue.length; }


}
