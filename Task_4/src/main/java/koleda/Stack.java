package koleda;

import java.util.EmptyStackException;

public class Stack {
    private int ptr;
    private final Object[] stack;

    /**
     * Конструктор класса
     * @param size - задаем размер стека
     */
    public Stack(int size){
        stack = new Object[size];
        ptr = -1;
    }

    /**
     * Заталкиваем элемент в стек
     * @param obj - помещаем в стек
     * @throws StackOverflowError - если стек переполнен, то возвращаем исключение
     *
     */
    public void push(Object obj) throws StackOverflowError{
        if (ptr == stack.length - 1){
            throw new StackOverflowError("full");
        } else stack[++ptr] = obj;
    }

    /**
     * Выталкиваем верхний элемент из стека
     * @return - верхний элемент
     * @throws EmptyStackException - если стек пуст, то возвращаем исключение
     */
    public Object pop() throws EmptyStackException{
        Object obj;
        if (isEmpty()){
            throw new EmptyStackException();
        } else {
            obj = stack[ptr];
            stack[ptr--] = null;
            return obj;
        }
    }

    /**
     * Забираем верхний элемент без удаления
     * @return - верхний элемент
     * @throws EmptyStackException - если стек пуст, то возвращем исключение
     */
    public Object top() throws EmptyStackException{
        if (isEmpty()){
            throw new EmptyStackException();
        } else {
            return stack[ptr];
        }
    }

    /**
     * Проверка на пустоту стека
     * @return - true, если стек пуст, иначе false
     */
    public boolean isEmpty() {
        return ptr == -1;
    }



}
