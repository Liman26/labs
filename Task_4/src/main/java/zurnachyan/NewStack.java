package zurnachyan;

import java.util.Stack;

/**
 * Класс реализует конструкцию Stack, в котором элементы уходят раньше, если они пришли позже(LIFO)
 */
public class NewStack {

    private Object[] stack;
    private int last;

    /**
     * Конструктор, создает массив на 100 элементов, инициализирует индекс последнего элемента
     */
    public NewStack(){
        stack = new Object[100];
        last = -1;
    }

    /**
     * Метод добавляет элемент в стек, в конец очереди, в случае заполненности бросает исключение
     * @param obj ссылка на внешний элемент, который необходимо добавить
     * @throws ArrayIndexOutOfBoundsException исключение связанное с переполнением стека
     */
    public void push(Object obj) throws ArrayIndexOutOfBoundsException{
        if (last == 99){
            throw new ArrayIndexOutOfBoundsException("Stack is full");
        }
        else{
            stack[++last] = obj;
        }
    }

    /**
     * Метод возвращает ссылку на последний элемент из стека, прежде удалив его из стека, бросает исключение
     * если стек пуст
     * @return последний элемент стека
     * @throws NullPointerException исключение, связанное с пустотой стека
     */
    public Object pop() throws NullPointerException{
        if(last == -1){
            throw new NullPointerException("Stack is empty");
        }
        else {
            Object lastObj = stack[last];
            stack[last--] = null;
            return lastObj;
        }
    }

    /**
     * Метод проверяет пуст ли стек, возвращает истину или ложь
     * @return истина или ложь(пуст / непуст)
     */
    public boolean isEmpty(){
        return last == -1;
    }

    /**
     * Метод возвращает ссылку на последний элемент стека без его удаления
     * @return последний элемент
     */
    public Object top() {
        if(last == -1){
            throw new NullPointerException("Stack is empty");
        }
        else {
            return stack[last];
        }
    }
}
