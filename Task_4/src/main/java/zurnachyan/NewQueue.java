package zurnachyan;

/**
 * Класс реализует конструкцию "Очередь", элементы уходят в том порядке, в котором приходят(FIFO)
 */
public class NewQueue {
    private Object[] queue;
    private int last, first, length;

    /**
     * Конструктор класса, создает массив на 100 элементов, присваивает значения началу, концу и длине очереди
     */
    public NewQueue(){
        queue = new Object[100];
        last = -1;
        first = 0;
        length = 0;
    }

    /**
     * Метод возвращает индекс элемента, который стоит на первом месте на выход из очереди
     * @return номер первого элемента
     */
    public int getFirstIndex(){
        return first;
    }

    /**
     * Метод возвращает индекс элемента, который стоит на последнем месте на выход из очереди
     * @return номер последнего элемента
     */
    public int getLastIndex(){
        return last;
    }

    /**
     * Метод добавляет элемент в очередь, в случае переполнения бросает исключение,
     * так же метод реализует "кольцевую" очередь.
     * @param obj ссылка на внешний объект, который необходимо добавить в очередь
     * @throws ArrayIndexOutOfBoundsException ошибка связанная с переполнением очереди
     */
    public void enqueue(Object obj) throws ArrayIndexOutOfBoundsException{
        if(length == 100){
            throw new ArrayIndexOutOfBoundsException("Queue is full");
        }
        else if(last == 99){
            last = 0;
            queue[last] = obj;
            length++;
        }
        else {
            queue[++last] = obj;
            length++;
        }
    }

    /**
     * Метод удаляет первый в очереди элемент, в случае пустой очереди бросает исключение с сообщением
     * @throws NullPointerException исключение, связанное с пустотой очереди
     */
    public void dequeue() throws NullPointerException{
        if(length == 0){
            throw new NullPointerException("Queue is empty");
        }
        else if(first == 99){
            queue[first] = null;
            first = 0;
            length--;
        }
        else{
            queue[first++] = null;
            length--;
        }
    }

    /**
     * Метод проверяет пуста ли очередь, возвращает истину или ложь
     * @return истина или ложь (пусто / непусто)
     */
    public boolean isEmpty(){
        return length == 0;
    }

    /**
     * Метод возвращает первый элемент без удаления его из самой очереди, в случае если очередь
     * пуста, бросает исключение
     * @return ссылка на первый элемент очереди
     * @throws NullPointerException исключение при пустой очереди
     */
    public Object top() throws NullPointerException{
        if(length == 0){
            throw new NullPointerException("Queue is empty");
        }
        else {
            return queue[first];
        }
    }



}
