package vereshchagina;

/**
 * Класс, реализующий стек на базе массива Object
 * Максимальное количество элементов в стеке (размер) задается пользователем
 */
public class MyStack {
    /**
     * массив Object, в нем хранятся элементы стека
     */
    private final Object[] ArrayStack;
    /**
     * максимально возможное количество элементов в стеке
     */
    private final int size;
    /**
     * индекс верхушки стека (указывает на первую пустую ячейку)
     */
    private int top;

    /**
     * конструктор без параметров
     * вызывает конструктор с параметром, задавая размер стека size = 20 по умолчанию
     */
    public MyStack() {
        this(20);
    }

    /**
     * конструктор с параметром
     *
     * @param size - максимально возможное количество элементов в стеке
     */

    public MyStack(int size) {
        this.size = size;
        ArrayStack = new Object[size];
        top = 0;
    }

    /**
     * метод, вставляющий заданный в параметрах элемент в верхушку стека, если не превышено
     * максимально возможное количество элементов.
     *
     * @param element - вставляемый элемент
     * @return 1 - при успешном добавлении, -1 - в случае переполнения
     */
    public int push(Object element) {
        if (top == size)
            return -1;
        ArrayStack[top++] = element;
        return 1;
    }

    /**
     * метод, возвращающий элемент с верхушки, удаляя его из стека
     *
     * @return null - если стек пустой, иначе - элемент из верхушки стека
     */
    public Object pop() {
        return (top == 0) ? null : ArrayStack[--top];
    }

    /**
     * метод проверки стека на пустоту
     *
     * @return true - если стек пуст, false - если стек содержит элементы
     */
    public boolean isEmpty() {
        return top == 0;
    }

    /**
     * метод, возвращающий верхний элемент без удаления из стека
     *
     * @return null - если стек пустой, иначе - элемент из верхушки стека
     */
    public Object top() {
        return (top == 0) ? null : ArrayStack[top - 1];
    }
}
