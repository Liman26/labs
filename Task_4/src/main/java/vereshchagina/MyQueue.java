package vereshchagina;

/**
 * Класс, реализующий кольцевую очередь на базе массива Object
 * Максимальное количество элементов в очереди (размер) задается пользователем
 */
public class MyQueue {
    /**
     * максимально возможное количество элементов в очереди
     */
    private final int size;
    /**
     * массив Object, в нем хранятся элементы очереди
     */
    private final Object[] ArrayQueue;
    /**
     * start - индекс первого элемента очереди
     * end - индекс последнего элемента очереди
     * count - количество элементов в очереди
     */
    private int start, end, count;

    /**
     * конструктор без параметров
     * вызывает конструктор с параметром, задавая размер очереди size = 20 по умолчанию
     */
    public MyQueue() {
        this(20);
    }

    /**
     * конструктор с параметром
     *
     * @param size - максимально возможное количество элементов в очереди
     */
    public MyQueue(int size) {
        this.size = size;
        ArrayQueue = new Object[size];
        start = count = 0;
        end = -1;
    }

    /**
     * метод, вставляющий заданный в параметрах элемент в конец очереди, если не превышено
     * максимально возможное количество элементов.
     *
     * @param element - вставляемый в очередь элемент
     * @return 1 - при успешном добавлении, -1 - в случае переполнения
     */
    public int enqueue(Object element) {
        if (count == size)
            return -1;
        count++;
        end = (end == size) ? 0 : end + 1;
        ArrayQueue[end] = element;
        return 1;
    }

    /**
     * метод, удаляющий первый элемент из очереди
     *
     * @return 1 - при успешном удалении, -1 - если очередь пуста
     */
    public int dequeue() {
        if (count == 0)
            return -1;
        count--;
        start = (start == size) ? 0 : start + 1;
        return 1;
    }

    /**
     * метод проверки очереди на пустоту
     *
     * @return true - если очередь пуста, false - если очередь содержит элементы
     */
    public boolean isEmpty() {
        return count == 0;
    }

    /**
     * метод, возвращающий первый элемент из очереди
     *
     * @return null - если очередь пуста, иначе - первый элемент
     */
    public Object top() {
        return (count == 0) ? null : ArrayQueue[start];
    }
}
