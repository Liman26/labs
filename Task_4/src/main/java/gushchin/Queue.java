package gushchin;

/**
 * Класс-реализация модели кольцевой очереди с
 * заданными ограничениями элементов типа Object
 */
public class Queue {
    /**
     * stash - Коллекция очереди, содержащая в себе элементы типа Object
     */
    private final Object[] stash;
    /**
     * start - идентификатор начала очереди
     * end - идентификатор конца очереди
     * count - количество элементов в очереди
     */
    private int start, end, count;

    /**
     * Конструктор класса с прямым обозначением максимального размера очереди
     * @param size - максимально возможный размер очереди
     */
    public Queue(int size) {
        this.stash = new Object[size];
        this.start = 0;
        this.end = 0;
        this.count = 0;
    }

    /**
     * Дефолтный конструктор класса
     * По умолчанию максимально возможный размер очереди равен 100
     */
    public Queue() {
        this(100);
    }

    /**
     * Метод, добавляющий новый элемент в конец очереди
     *
     * В реализации кольцевой очереди идентификатор конца очереди приобретает значение 0
     * и последующее добавление элементов производится с нулевого индекса
     *
     * @param newElement - новый элемент, помещаемый в конец очереди
     * @throws UnsupportedOperationException - исключение, возникающее в случае если очередь заполнена
     */
    public void enqueue(Object newElement) throws UnsupportedOperationException {
        if (count == stash.length)
            throw new UnsupportedOperationException("Очередь заполнена");
        stash[end++] = newElement;
        count++;
        if (end == stash.length)
            end = 0;
    }

    /**
     * Метод, удаляющий первый элемент очереди
     *
     * При удалении очереди идентификатор начала очереди инкрементируется, освобожая
     * начальные индексы для последующих элементов. В случае, если идентификатор
     * начала очереди доходит до последнего индекса очередь будет начинаться с
     * нулевого индекса
     *
     * @return - Первый элемент очереди, который был удален
     * @throws UnsupportedOperationException - исключение, возникающее если очередь пуста
     */
    public Object dequeue() throws UnsupportedOperationException {
        if (isEmpty())
            throw new UnsupportedOperationException("Очередь пуста");
        Object element = stash[start];
        stash[start++] = null;
        count--;
        if (start == stash.length)
            start = 0;
        return element;
    }

    /**
     * Метод, возвращающий первый элемент очереди без ее мутации
     *
     * @return - первый элемент очереди
     * @throws UnsupportedOperationException - исключение, возникающее если очередь пуста
     */
    public Object getTop() throws UnsupportedOperationException {
        if (isEmpty())
            throw new UnsupportedOperationException("Очередь пуста");
        return stash[start];
    }

    /**
     * Метод, проверяющий наличие элементов в очереди
     *
     * @return - возвращает true, если в очереди есть хотя бы 1 элемент и
     * false, когда очередь пуста
     */
    public boolean isEmpty() {
        return count == 0;
    }
}
