package gushchin;

import java.util.EmptyStackException;

/**
 * Класс-реализация стека с заданным ограничением элементов типа Object
 */
public class Stack {
    /**
     * stack - Коллекция стека, содержащая элементы типа Object
     */
    private final Object[] stack;
    /**
     * pointer - Указатель на вершину стека
     */
    private int pointer;

    /**
     * Дефолтный конструктор класса Stack
     * По умолчанию максимально возможный размер стека равен 100
     */
    public Stack() {
        this(100);
    }

    /**
     * Конструктор класса с прямым обозначением максимального размера стека
     *
     * @param size - максимально возможный рамер очереди
     */
    public Stack(int size) {
        stack = new Object[size];
        pointer = -1;
    }

    /**
     * Метод, проверяющий наличие элементов в стеке
     *
     * @return - возвращает true, если в стеке есть хотя бы 1 элемент и
     * false, если элементов в стеке нет
     */
    public boolean isEmpty() {
        return pointer == -1;
    }

    /**
     * Метод, добавляющий новый элемент в конец стека
     *
     * @param newElement - новый элемент, добавляемый в конец стека
     * @throws StackOverflowError - исключение, возникающее в случае переполнения стека
     */
    public void push(Object newElement) throws StackOverflowError {
        if (pointer == stack.length - 1)
            throw new StackOverflowError("Стек заполнен");
        stack[++pointer] = newElement;
    }

    /**
     * Метод, удаляющий последний элемент в стеке и возвращающий его значение
     *
     * @return - удаленный элемент стека
     * @throws EmptyStackException - исключение, возникающее при попытке
     *                             удалить элемент из пустого стека
     */
    public Object pop() throws EmptyStackException {
        if (isEmpty())
            throw new EmptyStackException();
        Object element = stack[pointer];
        stack[pointer--] = null;
        return element;
    }

    /**
     * Метод, возвращающий верхний элемент стека без его мутации
     *
     * @return - верхний элемент стека
     * @throws EmptyStackException - исключение, возникающее если стек пустой
     */
    public Object getTop() throws EmptyStackException {
        if (isEmpty())
            throw new EmptyStackException();
        return stack[pointer];
    }
}

