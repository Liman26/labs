package bachishche;
/**
 * Реализация стека, работающего по принципу LIFO
 * Максимальный допустимый размер данной структуры задается при создании
 * объекта класса.
 */
public class BaseStack {

        private Object[] stack;
        private int topPtr;


        /**
         * Создать пустой стек
         *
         * @param maxSize макисмальный размер стека
         */
        public BaseStack(int maxSize) {
            stack = new Object[maxSize];
            topPtr = -1;
        }

        /**
         * Добавляет элемент newElement в вершину стека
         *
         * @param newElement объект, который необходимо добавить
         * @return 1 удачном добавлении, -1 при ошибке
         */
        public int push(Object newElement) {
            if (topPtr == stack.length - 1)
                return -1;
            stack[++topPtr] = newElement;
            return 1;
        }

        /**
         * Возвращает элемент с вершины стека, удаляя его из структуры
         *
         * @return 1 удачном удалении, -1 при ошибке
         */
        public Object pop() {
            if (isEmpty())
                return null;
            return stack[topPtr--];
        }

        /**
         * Проверить стек на пустоту
         *
         * @return true если элемнетов нет, false - в обратном случае
         */
        public boolean isEmpty() {
            return topPtr == -1;
        }

        /**
         * Получить первый элемент из вершины стека без его удаления
         *
         * @return верхний элемент стека, null если стек пуст
         */
        public Object top() {
            if (isEmpty())
                return null;
            return stack[topPtr];
        }

}

