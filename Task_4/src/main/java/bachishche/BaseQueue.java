package bachishche;

/**
 * Реализация циклической очереди, работающей по принципу FIFO
 * Максимальный допустимый размер данной структуры задается при создании
 * объекта класса.
 */
public class BaseQueue {
    private Object[] queue;
    private int beginPtr;
    private int amounOfElements;

    /**
     * Создать пустую очередь заданного размера
     *
     * @param maxSize макисмальный размер очереди
     */
    public BaseQueue(int maxSize) {
        queue = new Object[maxSize];
        beginPtr = 0;
        amounOfElements = 0;
    }

    /**
     * Добавляет элемент newElement в конец очереди.
     *
     * @param newElement объект, который необходимо добавить
     * @return 1 удачном добавлении, -1 при ошибке
     */
    public int enqueue(Object newElement) {
        if (amounOfElements == queue.length)
            return -1;
        int lastInd = (beginPtr + amounOfElements) % queue.length;
        amounOfElements++;
        queue[lastInd] = newElement;
        return 1;
    }

    /**
     * Удаляет элемент из начала очереди.
     *
     * @return 1 удачном удалении, -1 при ошибке
     */
    public int dequeue() {
        if (isEmpty())
            return -1;
        beginPtr = (beginPtr + 1) % queue.length;
        amounOfElements--;
        return 1;
    }

    /**
     * Проверить очередь на пустоту
     *
     * @return true если элемнетов нет, false - в обратном случае
     */
    public boolean isEmpty() {
        return amounOfElements == 0;
    }

    /**
     * Получить первый элемент из очереди без его удаления
     *
     * @return первый элемент в очереди, null если очередь пуста
     */
    public Object top() {
        if (isEmpty())
            return null;
        return queue[beginPtr];
    }
}
