package nuridinov;

/**
 * Класс описывающий структуру данных - стек
 */
public class Stack {
    private final Object[] objects;
    private int index = -1;
    private int objectsInS = 0;

    /**
     * Полчение массива с элементами стека
     *
     * @return массив с элементами стека
     */
    public Object[] getObjects() {
        return objects;
    }

    /**
     * конструктор без параметров
     */
    public Stack() {
        objects = new Object[10];
    }

    /**
     * конструктор принимающий размер стека
     *
     * @param sizeOfStack - размер стека
     */
    public Stack(int sizeOfStack) {
        objects = new Object[sizeOfStack];
    }

    /**
     * добавление в стек
     *
     * @param object - добавленный элемент
     * @throws ArrayIndexOutOfBoundsException - исключение срабатывает при попытке добавить элемент в переполненный стек
     */
    public void push(Object object) throws ArrayIndexOutOfBoundsException {
        if (isFull()) {
            throw new ArrayIndexOutOfBoundsException("StackOverFlow");
        } else {
            objects[++index] = object;
            objectsInS++;
        }
    }

    /**
     * Получение элемента из верхушки стека с его последующим удалением
     *
     * @return верхушка стека
     * @throws NullPointerException - исключение срабатывает при попытке получить верхушку из пустого стека
     */
    public Object pop() throws NullPointerException {
        if (isEmpty()) {
            throw new NullPointerException("Стек пуст");
        } else {
            Object temp = objects[index];
            objects[index--] = null;
            objectsInS--;
            return temp;
        }

    }

    /**
     * получение верхушки стека без удаления
     *
     * @return верхушка стека
     * @throws NullPointerException - исключение срабатывает при попытке получить верхушку из пустого стека
     */
    public Object top() throws NullPointerException {
        if (isEmpty()) {
            throw new NullPointerException("Стек пуст");
        } else {
            return objects[index];
        }
    }

    /**
     * метод проверяющий стек на переполненность
     *
     * @return - true, если он переполнен
     */
    public boolean isFull() {
        return objectsInS == objects.length;
    }

    /**
     * метод проверяющий стек на пустоту
     *
     * @return true, если стек пустой
     */
    public boolean isEmpty() {
        return objectsInS == 0;
    }
}
