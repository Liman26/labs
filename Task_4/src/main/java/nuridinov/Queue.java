package nuridinov;

import javax.swing.plaf.IconUIResource;
import java.util.Objects;

/**
 * Класс описывающий кольцевую очередь
 */
public class Queue {
    private final Object[] objects;
    /**
     * rear - индекс последнего добавленного элемента
     */
    private int rear = -1;
    /**
     * front - индекс первого добавленного элемента
     */
    private int front = 0;
    /**
     * objectsInQ - кол-во элементов в очереди в данный момент
     */
    private int objectsInQ = 0;
    /**
     * size - размер очереди
     */
    private int size;

    /**
     * геттер для получение массива объектов(элементов очереди)
     *
     * @return массив
     */
    public Object[] getObjects() {
        return objects;
    }

    /**
     * Конструктор поумолчанию
     */
    public Queue() {
        objects = new Object[10];
        size = 10;
    }

    /**
     * Конструктор
     *
     * @param size - кол-во элементов очереди
     */
    public Queue(int size) {
        this.size = size;
        objects = new Object[size];
    }

    /**
     * Метод для добавления элемента в конец очереди
     *
     * @param object - элемент который нужно добавить в очередь
     * @throws ArrayIndexOutOfBoundsException - исключение срабатывает, если очередь переволнена
     */
    public void enqueue(Object object) throws ArrayIndexOutOfBoundsException {
        if (isFull()) {
            throw new ArrayIndexOutOfBoundsException("В очереди нет места");
        } else {
            if (rear == size - 1) {
                rear = -1;
            }
            objects[++rear] = object;
            objectsInQ++;
        }
    }

    /**
     * Метод которы удаляет элемент из начала очереди
     *
     * @throws NullPointerException исключение срабатывает, если попытаться удалить элемент из пустой очереди
     */
    public void dequeue() throws NullPointerException {
        if (isEmpty()) {
            throw new NullPointerException("Очередь пуста");
        } else {
            objects[front++] = null;
            if (front == size) {
                front = 0;
            }
        }

    }

    /**
     * Метод для получения первого элемента очереди
     *
     * @return - первый элемент очереди
     * @throws NullPointerException - исключение сработает если попытаться получить элемент из пустой очереди
     */
    public Object top() throws NullPointerException {
        if (isEmpty()) {
            throw new NullPointerException("Очередь пуста");
        } else {
            return objects[front];
        }

    }

    /**
     * Данный метод проверяет очередь на пустоту
     *
     * @return возвращает true, если очередь пуста
     */
    public boolean isEmpty() {
        return objectsInQ == 0;
    }

    /**
     * Данный метод проверяет очередь на переполненность
     *
     * @return возвращает true, если очередь переполнена
     */
    public boolean isFull() {
        return objectsInQ == size;
    }


}
