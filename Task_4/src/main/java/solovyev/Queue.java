package solovyev;

/**
 * Класс, реализующий очередь по принципу FIFO
 */
public class Queue {
    private final Object[] elements;
    private int indexLast = 0;

    /**
     * Пустой конструктор очереди.
     * Если пользователь явно не указал размер очереди,
     * будет создан стек размером 6
     */
    Queue() {
        elements = new Object[6];
    }

    /**
     * Конструктор очереди с параметром size
     * Будет создана очередь размером size
     *
     * @param size желаемый размер очереди
     */
    Queue(int size) {
        elements = new Object[size];
    }

    /**
     * Метод вставляющий элемент в конец
     *
     * @param elem вставляемый элемент типа Object
     * @throws ArrayIndexOutOfBoundsException если очередь уже заполнена
     */
    public void enqueue(Object elem) throws ArrayIndexOutOfBoundsException {
        try {
            elements[indexLast++] = elem;
        } catch (Exception e) {
            throw new ArrayIndexOutOfBoundsException("Очередь уже заполнена.");
        }
    }

    /**
     * Метод удалющий первывй элемент в очереди
     * удаляется за счет смещения массива на шаг
     * последний элемент становится пустым
     * так как при копировании он продублируется
     *
     * @throws NullPointerException если очередь пуста
     */
    public void dequeue() throws NullPointerException {
        if (isEmpty()) {
            throw new NullPointerException("Удалить элемент нельзя. Очередь пуста");
        } else {
            System.arraycopy(elements, 1, elements, 0, --indexLast);
            elements[elements.length - 1] = null;
        }
    }

    /**
     * Метод возвращающий первый элемент очереди
     *
     * @return первый элемент в очереди
     * @throws NullPointerException если очередь пуста
     */
    public Object top() throws NullPointerException {
        if (isEmpty()) {
            throw new NullPointerException("Очередь пуста. Первого элемента нет.");
        } else return elements[0];
    }

    /**
     * Метод проверяющий не является ли очередь пустой
     *
     * @return true если очередь пуста, иначе false
     */
    public boolean isEmpty() {
        if (elements[0] == null) {
            return true;
        } else return false;
    }

    /**
     * Дополнительный метод, используемый для проверки
     * успешного добавления элементов в очередь
     *
     * @return массив элементов представляющих собой очередь
     */
    public Object[] printQueue() {
        return this.elements;
    }
}
