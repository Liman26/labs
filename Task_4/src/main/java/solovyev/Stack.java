package solovyev;

/**
 * Класс, реализующий стек, то есть очередь по принципу LIFO
 */
public class Stack {
    private final Object[] elements;
    private int indexLast = 0;

    /**
     * Пустой конструктор стека.
     * Если пользователь явно не указал размер стека,
     * будет создан стек размером 6
     */
    Stack() {
        elements = new Object[6];
    }

    /**
     * Конструктор стека с параметром size
     * Будет создан стек размером size
     *
     * @param size желаемый размер стека
     */
    Stack(int size) {
        elements = new Object[size];
    }

    /**
     * Метод проверяет не является ли стек пустым
     *
     * @return true если стек пуст, иначе false
     */
    public boolean isEmpty() {
        if (elements[0] == null) {
            return true;
        } else return false;
    }

    /**
     * Метод возвращает верхний элемент без удаления из стека
     *
     * @return верхний элемент в стеке
     * @throws NullPointerException бросает исключение если очердь пуста
     */
    public Object top() throws NullPointerException {
        if (isEmpty()) {
            throw new NullPointerException("Стек пуст");
        } else return elements[indexLast - 1];
    }

    /**
     * Метод вставляет элемент сверху
     *
     * @param elem вставляемый элемент
     * @throws ArrayIndexOutOfBoundsException бросает исключение при попытке
     *                                        вставить элемент в заполненный стек
     */
    public void push(Object elem) throws ArrayIndexOutOfBoundsException {
        try {
            elements[indexLast++] = elem;
        } catch (Exception e) {
            throw new ArrayIndexOutOfBoundsException("Стек уже заполнен.");
        }
    }

    /**
     * Метод возвращает верхний элемент после удаления из стека
     *
     * @return значение удаленного элемента
     * @throws NullPointerException бросает исключение если стек пуст
     */
    public Object pop() throws NullPointerException {
        if (isEmpty()) {
            throw new NullPointerException("Удалить элемент нельзя. Стек пуст.");
        } else {
            Object o = elements[--indexLast];
            elements[indexLast] = null;
            return o;
        }
    }

    /**
     * Дополнительный метод, используемый для проверки
     * успешного добавления элементов в стек
     *
     * @return массив элементов представляющих собой стек
     */
    public Object[] printStack() {
        return this.elements;
    }
}
