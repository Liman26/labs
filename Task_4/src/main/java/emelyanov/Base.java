package emelyanov;

/**
 * Класс реализующий базовые методы и переменные для работы со стэком и очередью
 */
public class Base {
  private final Object[] arrayObject;
  private int size;

  /**
   * конструктор, создаёт массив и иниализирует начальный размер
   *
   * @param maxSize размер массива
   */
  public Base(int maxSize) {
    this.arrayObject = new Object[maxSize];
    size = 0;
  }

  /**
   * Вернёт элемент из начала массива
   *
   * @return вернет null если массив пуст и Object если элемент есть
   */
  public Object getFirst() {
    if (isEmpty()) {
      return null;
    }
    return arrayObject[0];
  }

  /**
   * Вернёт элемент из конца массива
   *
   * @return вернет null если массив пуст и Object если элемент есть
   */
  public Object getLast() {
    if (isEmpty()) {
      return null;
    }
    return arrayObject[size - 1];
  }

  /**
   * Удаляет элемент из начала массива со сдвигом
   *
   * @return вернет -1 если массив пуст и 1 если удаление прошло успешно
   */
  public int removeFirst() {
    if (isEmpty()) {
      return -1;
    }
    size--;
    for (int i = 0; i < size; i++) {
      arrayObject[i] = arrayObject[i + 1];
    }
    return 1;
  }

  /**
   * Удаляет элемент из конца массива
   *
   * @return вернет false если массив пуст и true если удаление прошло успешно
   */
  public int removeLast() {
    if (isEmpty()) {
      return -1;
    }
    size--;
    return 1;
  }

  /**
   * Вставляет элемент в конец массива
   *
   * @return вернет -1 если достигнут максимум и 1 если вставка прошла успешно
   */
  public int addElement(Object element) {
    if (size == arrayObject.length) {
      return -1;
    }
    arrayObject[size] = element;
    size++;
    return 1;
  }

  /**
   * Проверка на пустой размер данных в массиве
   *
   * @return возвращает true, если размер данных в массиве равен 0 иначе false
   */
  public boolean isEmpty() {
    return size == 0;
  }

  /**
   * Сформировать текущее состояние данных в массиве в строку, для выаода в консоль
   *
   * @return Текущее состояние данных в массиве
   */
  @Override
  public String toString() {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < size; i++) {
      result.append(arrayObject[i]).append(" ");
    }
    return result.toString();
  }
}
