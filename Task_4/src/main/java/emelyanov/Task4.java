package emelyanov;

public class Task4 {
  public static void main(String[] args) {
    Queue queue = new Queue(10);
    System.out.println(queue);
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    System.out.println(queue);
    System.out.println(queue.top());
    queue.dequeue();
    System.out.println(queue);
    System.out.println(queue.top());
    queue.dequeue();
    System.out.println(queue);
    System.out.println(queue.top());
    queue.dequeue();
    System.out.println(queue);
    System.out.println();
    Stack stack = new Stack(10);
    System.out.println(stack);
    stack.push(1);
    stack.push(2);
    stack.push(3);
    System.out.println(stack);
    System.out.println(stack.top());
    System.out.println(stack);
    System.out.println(stack.pop());
    System.out.println(stack);
    System.out.println(stack.pop());
    System.out.println(stack);
    System.out.println(stack.pop());
    System.out.println(stack);
    System.out.println(stack.pop());
    System.out.println(stack);
  }
}
