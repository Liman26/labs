package emelyanov;

/**
 * Класс реализующий работу стэка
 */
public class Stack extends Base {

  /**
   * конструктор, создаёт стэк
   *
   * @param maxSize размер стэка
   */
  public Stack(int maxSize) {
    super(maxSize);
  }

  /**
   * Вставляет элемент сверху
   *
   * @return вернет -1 если ошибка вставки и 1 если вставка прошла успешно
   */
  public int push(Object element) {
    return addElement(element);
  }

  /**
   * Возвращает верхний элемент после удаления из стека
   *
   * @return вернет null если ничего сверху не удалил или верхний элемент после удаления из стека если все ок
   */
  public Object pop() {
    Object element = getLast();
    if (removeLast() == -1) {
      return null;
    }
    return element;
  }

  /**
   * Возвращает верхний элемент без удаления из стека
   *
   * @return вернет null если стэк пуст иначе первый элемент стека
   */
  public Object top() {
    return getLast();
  }

  /**
   * Сформировать текущее состояние стэка в строку, для вывода в консоль
   *
   * @return Текущее состояние стэка
   */
  @Override
  public String toString() {
    return "Состояние стэка: " + super.toString();
  }

}
