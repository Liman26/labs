package emelyanov;

/**
 * Класс реализующий работу очереди
 */
public class Queue extends Base {

  /**
   * конструктор, создаёт очередь
   *
   * @param maxSize размер очереди
   */
  public Queue(int maxSize) {
    super(maxSize);
  }

  /**
   * Вставляет элемент в конец очереди
   *
   * @return вернет -1 если ошибка вставки и 1 если вставка прошла успешно
   */
  public int enqueue(Object element) {
    return addElement(element);
  }

  /**
   * Удаляет элемент из начала
   *
   * @return вернет -1 если ошибка удаления и 1 если удаление прошло успешно
   */
  public int dequeue() {
    return removeFirst();
  }

  /**
   * Возвращает первый элемент очереди
   *
   * @return вернет null если очередь пуста иначе первый элемент очереди
   */
  public Object top() {
    return getFirst();
  }

  /**
   * Сформировать текущее состояние очереди в строку, для вывода в консоль
   *
   * @return Текущее состояние очереди
   */
  @Override
  public String toString() {
    return "Состояние очереди: " + new StringBuilder(super.toString()).reverse();
  }

}
