package turkov;

/**
 * Класс реализует модель кольцевой очереди на основе массива arrayQueue фиксированного размера и
 * состоящего из объектов Object. Состояние объекта класса Queue описывается полями:
 * front - указатель на начало очереди;
 * end - указатель на конец очереди;
 * count - счетчик количества элементов находящихся в очереди.
 */
public class Queue {
    private final Object[] arrayQueue;
    private int front, end, count;

    /***
     * Конструктор класса Queue
     * @param size - задает размер массива для объекта очереди.
     */
    public Queue(int size) {
        arrayQueue = new Object[size];
        front = 0;
        end = 0;
        count = 0;
    }

    /**
     * Метод проверяющий пуста очередь или нет.
     *
     * @return true, если в очереди содержится хотя бы один элемент, иначе возвращается false.
     */
    public boolean isEmpty() {
        return (count == 0);
    }

    /**
     * Метод, который вставляет элемент в конец очереди.
     *
     * @param element - объект помещяемый в очередь.
     * @throws UnsupportedOperationException - исключение, которое возвращается в случае, если очередь заполнена.
     */
    public void enqueue(Object element) throws UnsupportedOperationException {
        if (count == arrayQueue.length)
            throw new UnsupportedOperationException("Queue is full");
        arrayQueue[end++] = element;
        count++;
        if (end == arrayQueue.length) {
            end = 0;
        }
    }

    /**
     * Метод, который возвращает элемент из начала очереди, при этом удаляя его.
     *
     * @return - первый элемент очереди.
     * @throws UnsupportedOperationException - исключение, которое возвращается в случае, если очередь пуста.
     */
    public Object dequeue() throws UnsupportedOperationException {
        Object element;
        if (isEmpty()) {
            throw new UnsupportedOperationException("Queue is empty");
        }
        element = arrayQueue[front];
        arrayQueue[front++] = null;
        count--;
        if (front == arrayQueue.length) {
            front = 0;
        }
        return element;
    }

    /**
     * Метод возвращает первый элемент очереди, не изменяя её.
     *
     * @return - первый элемент очереди.
     * @throws UnsupportedOperationException - исключение, которое возвращается в случае, если очередь пуста.
     */
    public Object top() throws UnsupportedOperationException {
        if (isEmpty()) {
            throw new UnsupportedOperationException("Queue is empty");
        }
        return arrayQueue[front];
    }
}
