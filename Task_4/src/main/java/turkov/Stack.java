package turkov;

import java.util.EmptyStackException;

/**
 * Класс реализует модель стека на основе массива arrayStack фиксированного размера и состоящего из объектов Object.
 * Состояние объекта класса Stack описывается полем pointer, которое является указателем вершины стека.
 */
public class Stack {
    private final Object[] arrayStack;
    private int pointer;

    /**
     * Конструктор класса Stack.
     *
     * @param size - задает размер массива для объекта стека.
     */
    public Stack(int size) {
        arrayStack = new Object[size];
        pointer = -1;
    }

    /**
     * Метод проверяющий пуст стек или нет.
     *
     * @return true, если в стеке содержится хотя бы один элемент, иначе возвращается false.
     */
    public boolean isEmpty() {
        return (pointer == -1);
    }

    /**
     * Метод добавляет элемент в стек.
     *
     * @param element - объект помещаемый в стек.
     * @throws StackOverflowError - исключение, которое возвращается в случае, если стек полон.
     */
    public void push(Object element) throws StackOverflowError {
        if (pointer == arrayStack.length - 1) {
            throw new StackOverflowError("stack is full");
        } else arrayStack[++pointer] = element;
    }

    /**
     * Метод возвращает верхний элемент после удаления из стека.
     *
     * @return - верхний элемент.
     * @throws EmptyStackException - исключение, которое возвращается в случае, если стек пуст.
     */
    public Object pop() throws EmptyStackException {
        Object element;
        if (isEmpty()) {
            throw new EmptyStackException();
        } else {
            element = arrayStack[pointer];
            arrayStack[pointer--] = null;
            return element;
        }
    }

    /**
     * Метод возвращает верхний элемент без удаления из стека.
     *
     * @return - верхний элемент стека.
     * @throws EmptyStackException - исключение, которое возвращается в случае, если стек пуст.
     */
    public Object top() throws EmptyStackException {
        if (isEmpty()) {
            throw new EmptyStackException();
        } else {
            return arrayStack[pointer];
        }
    }
}