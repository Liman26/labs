package troitskiy;

/**
 *  Класс описывающий полустатические структуры данных основанные на
 *  массиве объектов, размер массива задается при создании объекта
 *  содержит набор общих методов
 *  На его основе можно создать очередь и стэк
 */
public class SemiStatic {
    static final int VSE_OK    = 1;
    static final int VSE_PLOHO = -1;

    protected Object[] a;
    protected int beginindex;
    protected int kolvo;


    /**
     * Конструктор класса полустатических структур
     * @param k -максимальный размер структуры
     */
    public SemiStatic(int k)
    {
        beginindex = 0;
        kolvo = 0;
        a = new Object[k];
    }

    /**
     * возвращает значение true, если
     * структура пуста
     * @return -возвращает true, если структура пуста
     */
    public boolean isempty()
    {
        return (kolvo ==0);
    }

    /**
     * возвращает верхний элемент без удаления из
     * стека
     * @return -возвращает первый элемент или вернет null если структура пуста
     */
    public Object top()
    {
        if (kolvo <= 0)   return null;
        else              return a[ ( beginindex + kolvo - 1 ) % a.length ];
    }

}
