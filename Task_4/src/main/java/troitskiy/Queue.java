package troitskiy;

/**
 * Ксласс очередь
 * Очередь — структура данных, которая описывает очередь в  магазин:
 * человек в начале очереди (тот, кто пришел первый) будет обслуживаться первым,
 * вновь пришедшие люди будут добавляться в конец очереди. Таким образом,
 * первый человек в очереди обслуживается первым, последний в очереди, обслуживается последним.
 * Сокращенно очереди обозначаются как: FIFO — First In, First Out (первым пришел, первым ушел).
 */
public class Queue extends SemiStatic
{
    /**
     * Конструктор с одним параметром для
     * класса Queue (Очередь).
     * @param n - максимальный размер очереди
     */
    public Queue(int n)
    {
        super(n);
    }

    /**
     *  вставляет элемент в конец очереди
     *
     * @param o -Вставляемый элемент
     * @return  -вернет -1 если ошибка вставки и 1 если вставка прошла успешно
     */
    public int enqueue(Object o)
    {
        if (kolvo < a.length)
        {
            kolvo++;
            if (beginindex == 0)
                beginindex = a.length - 1;
            else
                beginindex--;

            a[beginindex] = o;
            return VSE_OK;
        }
        else
        {
            return VSE_PLOHO;
        }
    }

    /**
     * удаляет элемент из начала
     * @return - вернет -1 если ошибка удаления и 1 если удаление прошло успешно
     */
    public int dequeue()
    {
        if (kolvo == 0)
            return VSE_PLOHO;
        else
        {
            kolvo--;
            return VSE_OK;
        }
    }

}
