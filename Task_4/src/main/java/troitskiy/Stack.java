package troitskiy;


/**
 * класс, представляющий собой список элементов, организованных по принципу
 * LIFO (англ. last in — first out, «последним пришёл — первым вышел»).
 */
public class Stack extends SemiStatic
{
    /**
     * Конструктор с одним параметром для класса Стек
     * @param n - максимальный размер стека
     */
    public Stack(int n)
    {
        super(n);
    }

    /**
     * вставляет элемент сверху
     * @param o -Вставляемый объект
     * @return вернет -1 если ошибка вставки и 1 если вставка прошла успешно
     */
    public int push(Object o)
    {
        if (kolvo < a.length)
        {
            kolvo++;
            a[(beginindex + kolvo -1) % a.length] = o;
            return VSE_OK;
        }
        else
        {
            return VSE_PLOHO;
        }
    }

    /**
     * возвращает верхний элемент после удаления из
     * стека
     * @return -вернет null если ничего сверху не удалил
     *          или верхний элемент после удаления из стека если все ок
     */
    public Object pop()
    {
        if (kolvo > 0)
        {
            return a[ ( beginindex + kolvo -- - 1 ) % a.length ];
        }
        else
        {
            return null;
        }
    }

}
