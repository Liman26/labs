package zhirnov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Класс для проверки методов класса MyQueue
 */
public class TestMyQueue {
    /**
     * Метод для проверки метода класса MyQueue enqueue, который вставляет элемент в очередь
     */
    @Test
    public void enqueue(){
        MyQueue myQueue = new MyQueue();
        Object[] expect = new Object[20];
        for(int i = 0; i < 12; i++){
            Object object = new Object();
            myQueue.enqueue(object);
            expect[i] = object;
        }
        Assertions.assertArrayEquals(expect, myQueue.getQueue());
    }
    /**
     * Метод для проверки метода класса MyQueue dequeue, который удаляет первый элемент в очереди
     */
    @Test
    public void dequeue(){
        MyQueue myQueue = new MyQueue();
        Object[] expect = new Object[20];
        for(int i = 0; i < 12; i++){
            Object object = new Object();
            if(i == 0){
                myQueue.enqueue(object);
            }else{
                myQueue.enqueue(object);
                expect[i-1] = object;
            }
        }
        myQueue.dequeue();

        Assertions.assertArrayEquals(expect, myQueue.getQueue());
    }
    /**
     * Метод для проверки метода класса MyQueue isEmpty, который проверяет, имеются ли элементы в очереди
     */
    @Test
    public void isEmpty(){
        MyQueue myQueue = new MyQueue();
        Assertions.assertTrue(myQueue.isEmpty());
        myQueue.enqueue(new Object());
        Assertions.assertFalse(myQueue.isEmpty());
    }
    /**
     * Метод для проверки метода класса MyQueue top, который возвращает первый элемент в очереди
     */
    @Test
    public void top(){
        MyQueue myQueue = new MyQueue();
        Object topObject = null;
        for(int i = 0; i < 12; i++){
            Object object = new Object();
            myQueue.enqueue(object);
            if (i == 0){
                topObject = object;
            }
        }
        Assertions.assertEquals(topObject, myQueue.top());
    }
}
