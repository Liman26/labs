package zhirnov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Класс для проверки методов класса MyStack
 */
public class TestMyStack {
    Object object1 = new Object();
    Object object2 = new Object();
    Object object3 = new Object();

    /**
     * Метод для инициализации объектов, которые используются в других тестах
     */
    @BeforeEach
    public void createObjects(){
        object1 = new Object();
        object2 = new Object();
        object3 = new Object();
    }
    /**
     * Метод для проверки метода класса MyStack push, который вставляет элемент в стек
     */
    @Test
    public void push(){
        MyStack myStack = new MyStack();
        Object[] expect = new Object[20];
        for (int i = 0; i < 12; i++){
            Object o = new Object();
            myStack.push(o);
            expect[i] = o;
        }

        Assertions.assertArrayEquals(expect, myStack.getStack());
    }
    /**
     * Метод для проверки метода класса MyStack pop, который возвращает элемент из стека после его удаление оттуда
     */
    @Test
    public void pop(){
        MyStack myStack = new MyStack();
        Object[] expect = new Object[10];

        myStack.push(object1);
        myStack.push(object2);
        myStack.push(object3);
        Object o = myStack.pop();

        expect[1] = object2;
        expect[0] = object1;

        Assertions.assertArrayEquals(expect, myStack.getStack());
    }
    /**
     * Метод для проверки метода класса MyStack isEmpty, который проверяет, имеются ли элементы в стеке
     */
    @Test
    public void isEmpty(){
        MyStack myStack = new MyStack();
        Assertions.assertTrue(myStack.isEmpty());
        myStack.push(new Object());
        Assertions.assertFalse(myStack.isEmpty());
    }
    /**
     * Метод для проверки метода класса MyStack top, который возвращает первый элемент в стеке
     */
    @Test
    public void top(){
        MyStack myStack = new MyStack();

        Object object1 = new Object();
        Object object2 = new Object();
        Object object3 = new Object();

        myStack.push(object1);
        myStack.push(object2);
        myStack.push(object3);

        Assertions.assertEquals(object3, myStack.top());
    }
}

