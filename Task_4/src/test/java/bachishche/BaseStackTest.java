package bachishche;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Тестирование класса стек (Stack)
 */
public class BaseStackTest {
    /**
     * Проверка на корректность данных, передаваемых в конструктор
     */
    @Test
    public void stackConstructorTest() {
        assertThrows(NegativeArraySizeException.class, () -> new BaseStack(-1));
    }

    /**
     * Проверка методов добавления элемента и
     * проверки стека на пустоту
     */
    @Test
    public void pushAndEmptyTest() {
        BaseStack st = new BaseStack(5);
        assertTrue(st.isEmpty());
        for (int i = 0; i < 5; i++)
            assertEquals(1, st.push(i));
        assertEquals(-1, st.push(12));
        assertFalse(st.isEmpty());

    }

    /**
     * Проверка методов извдечения из стека с удалением (pop)
     * и без удаления (top)
     * Проверка корректности принципа LIFO
     */
    @Test
    public void popAndTopTest() {
        BaseStack st = new BaseStack(5);
        for (int i = 0; i < 3; i++)
            st.push(i + 1);
        assertEquals(3, st.top());
        assertEquals(3, st.pop());
        assertEquals(2, st.pop());
        assertEquals(1, st.pop());
        assertNull(st.pop());
        assertNull(st.top());
    }
}
