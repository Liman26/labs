package bachishche;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Тестирование класса очередь (Queue)
 */
public class BaseQueueTest {
    /**
     * Проверка на корректность данных, передаваемых в конструктор
     */
    @Test
    public void queueConstructorTest() {
        assertThrows(NegativeArraySizeException.class, () -> new BaseQueue(-1));
    }

    /**
     * Проверка методов добавления элемента, в том числе
     * при превышении размера структуры.
     * Проверка метода проверки очереди на пустоту
     */
    @Test
    public void enqueueAndEmptyTest() {
        BaseQueue que = new BaseQueue(5);
        assertTrue(que.isEmpty());
        for (int i = 0; i < 5; i++)
            assertEquals(1, que.enqueue(i));
        assertEquals(-1, que.enqueue(12));
        assertFalse(que.isEmpty());

    }

    /**
     * Проверка методов просмотра (top) и удаления (dequeue)
     * первого элемента в очереди, в том числе из пустой структуры.
     * Проверка на соблюдение принципа FIFO (порядка
     * элементов, возвращенных из структуры)
     */
    @Test
    public void topAndDequeueTest() {
        BaseQueue que = new BaseQueue(5);
        for (int i = 0; i < 3; i++)
            que.enqueue(i + 1);
        for (int i = 0; i < 3; i++) {
            assertEquals(i + 1, que.top());
            assertEquals(1, que.dequeue());
        }
        assertEquals(-1, que.dequeue());
        assertNull(que.top());
    }

}
