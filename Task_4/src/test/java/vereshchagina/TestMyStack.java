package vereshchagina;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMyStack {

    /**
     * Тестирование метода push
     */
    @Test
    public void testpush() {
        MyStack a = new MyStack(2);
        assertEquals(1, a.push(new Object()));
        assertEquals(1, a.push(new Object()));
        assertEquals(-1, a.push(new Object()));
    }

    /**
     * Тестирование метода pop
     */
    @Test
    public void testpop() {
        MyStack a = new MyStack(2);
        a.push(3);
        a.push(4);
        assertEquals(4, a.pop());
        assertEquals(3, a.pop());
        assertEquals(null, a.pop());
    }

    /**
     * Тестирование метода isEmpty
     */
    @Test
    public void testisEmpty() {
        MyStack a = new MyStack();
        assertEquals(true, a.isEmpty());
        a.push(new Object());
        assertEquals(false, a.isEmpty());
    }

    /**
     * Тестирование метода top
     */
    @Test
    public void testTop() {
        MyStack a = new MyStack();
        assertEquals(null, a.top());
        a.push(1);
        assertEquals(1, a.top());
        a.push(2);
        assertEquals(2, a.top());
    }
}
