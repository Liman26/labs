package vereshchagina;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMyQueue {

    /**
     * Тестирование метода enqueue на корректное добавление элемента в (пустую, не пустую и переполненную) очередь
     */
    @Test
    public void testEnqueue() {
        MyQueue a = new MyQueue(2);
        assertEquals(1, a.enqueue(new Object()));
        assertEquals(1, a.enqueue(new Object()));
        assertEquals(-1, a.enqueue(new Object()));
    }

    /**
     * Тестирование метода dequeue на корректное удаление элемента из очереди
     */
    @Test
    public void testDequeue() {
        MyQueue a = new MyQueue(2);
        a.enqueue(new Object());
        a.enqueue(new Object());
        assertEquals(1, a.dequeue());
        assertEquals(1, a.dequeue());
        assertEquals(-1, a.dequeue());
    }

    /**
     * Тестирование метода isEmpty
     */
    @Test
    public void testisEmpty() {
        MyQueue a = new MyQueue();
        assertEquals(true, a.isEmpty());
        a.enqueue(new Object());
        assertEquals(false, a.isEmpty());
    }

    /**
     * Тестирование метода top
     */
    @Test
    public void testTop() {
        MyQueue a = new MyQueue();
        assertEquals(null, a.top());
        a.enqueue(1);
        assertEquals(1, a.top());
    }
}
