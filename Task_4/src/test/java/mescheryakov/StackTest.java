package mescheryakov;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StackTest {

    @Test
    public void push() {
        MyStack stack = new MyStack(6);

        stack.push("Red");
        stack.push("Orange");
        stack.push("Yellow");
        stack.push("Green");
        stack.push("Blue");
        stack.push("Indigo");
        stack.push("Violet");

        assertEquals("Indigo", stack.top());
    }

    @Test
    public void isEmpty() {
        MyStack stack = new MyStack(1);
        assertTrue(stack.isEmpty());
        stack.push("Red");
        assertFalse(stack.isEmpty());
    }

    @Test
    public void pop() {
        MyStack stack = new MyStack(2);

        stack.pop();

        assertNull(stack.top());

        stack.push("Red");
        stack.push("Orange");

        assertEquals("Orange", stack.pop());
    }

    @Test
    public void top() {
        MyStack stack = new MyStack(2);

        stack.push("Red");
        stack.push("Orange");

        assertEquals("Orange", stack.top());
    }
}