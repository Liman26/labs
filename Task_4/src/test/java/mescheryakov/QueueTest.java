package mescheryakov;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class QueueTest {

    @Test
    public void enqueue() {
        MyQueue queue = new MyQueue(6);

        for (int i = 0; i < 7; i++) {
            Object object = new Object();
            queue.enqueue(object);
        }

        assertEquals(6, queue.size());
    }

    @Test
    public void dequeue() {
        MyQueue queue = new MyQueue(2);

        queue.enqueue("Red");
        queue.enqueue("Orange");

        assertEquals("Red", queue.dequeue());
    }

    @Test
    public void isEmpty() {
        MyQueue queue = new MyQueue(1);
        assertTrue(queue.isEmpty());
        queue.enqueue("Red");
        assertFalse(queue.isEmpty());
    }

    @Test
    public void top() {
        MyQueue queue = new MyQueue(2);

        queue.enqueue("Red");
        queue.enqueue("Orange");

        assertEquals("Red", queue.top());
    }
}