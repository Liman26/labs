package zurnachyan;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс для теста методов стека.
 */
public class NewStackTest {

    /**
     * Тест метода pop() класса NewStack на выбрасывание исключения при удалении элемента из пустого стека
     */
    @Test
    public void popTest(){
        NewStack stack = new NewStack();
        assertThrows(NullPointerException.class, () -> stack.pop());
    }

    /**
     * Тест метода top() класса NewStack на выбрасывание исключения при возвращении последнего элемента из пустого стека
     */
    @Test
    public void topTest(){
        NewStack stack = new NewStack();
        assertThrows(NullPointerException.class, ()-> stack.top());
    }

    /**
     * Тест методов push() и pop() класса NewStack на удачное помещение элемента в стек, и возвращение его с удалением
     */
    @Test
    public void pushTest1(){
        NewStack stack = new NewStack();
        stack.push(12);
        assertEquals(12, stack.pop());
    }

    /**
     * Тест метода isEmpty() класса NewStack на удачное определение пустоты или наполненности стека
     */
    @Test
    public void isEmptyTest(){
        NewStack stack = new NewStack();
        assertTrue(stack.isEmpty());
    }

    /**
     * 2-й тест метода push() класса NewStack на выбрасывание исключения при добавлении элемента в полный стек
     */
    @Test
    public void pushTest2(){
        NewStack stack = new NewStack();
        for(int i = 0; i < 100; i++){
            stack.push(i);
        }
        assertThrows(ArrayIndexOutOfBoundsException.class, ()-> stack.push(1));
    }
}
