package zurnachyan;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс для теста методов очереди
 */
public class NewQueueTest {

    /**
     * Тест метода dequeue() класса NewQueue на выбрасывание исключения при пустой очереди
     */
    @Test
    public void dequeueTest1(){
        NewQueue queue = new NewQueue();
        assertThrows(NullPointerException.class, ()-> queue.dequeue());
    }

    /**
     * Тест метода top() класса NewQueue на выбрасывание исключения при пустой очереди
     */
    @Test
    public void topTest(){
        NewQueue queue = new NewQueue();
        assertThrows(NullPointerException.class, ()-> queue.dequeue());
    }

    /**
     * Тест методов enqueue() класса NewQueue и top() на удачное добавление и возвращение элемента
     */
    @Test
    public void enqueueTest(){
        NewQueue queue = new NewQueue();
        queue.enqueue(13);
        assertEquals(13, queue.top());
    }

    /**
     * Тест метода isEmpty() класса NewQueue на выбрасывание исключения при пустой очереди
     */
    @Test
    public void isEmptyTest(){
        NewQueue queue = new NewQueue();
        assertTrue(queue.isEmpty());
    }

    /**
     * Тест метода enqueue() класса NewQueue на выбрасывание исключения при полной очереди
     */
    @Test
    public void enqueueTest2(){
        NewQueue queue = new NewQueue();
        for(int i = 0; i < 100; i++){
            queue.enqueue(i);
        }
        assertThrows(ArrayIndexOutOfBoundsException.class, ()-> queue.enqueue(1));
    }

    /**
     * Тест реализации кольцевой очереди (индекс последнего жлемента очереди меньше индекса первого)
     */
    @Test
    public void enqueueTest3(){
        NewQueue queue = new NewQueue();
        for(int i = 0; i < 100; i++){
            queue.enqueue(i);
        }
        queue.dequeue();
        queue.enqueue(1);
        assertTrue(queue.getFirstIndex() > queue.getLastIndex());
    }

}
