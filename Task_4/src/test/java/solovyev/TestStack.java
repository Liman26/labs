package solovyev;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс для проверки методов класса Stack
 */
public class TestStack {

    /**
     * Метод для тестирования мтеода isEmpty,
     * возвращающего true, если стек пуст
     * В этом тесте стек пуст, поэтому в
     * результате работы ожидается true
     */
    @Test
    public void isEmptyTest1() {
        Stack s = new Stack();
        assertTrue(s.isEmpty());
    }

    /**
     * Метод для тестирования мтеода isEmpty,
     * возвращающего true, если стек пуст
     * В этом тесте стек не пуст, поэтому в
     * результате работы ожидается false
     */
    @Test
    public void isEmptyTest2() {
        Stack s = new Stack(3);
        s.push(1);
        s.push(2);
        assertFalse(s.isEmpty());
    }

    /**
     * Метод для теситрования метода top,
     * который возвращает верхний элемент в стеке
     * В этом тесте ожидается ошибка NullPointerException,
     * потому что стек пуст
     */
    @Test
    public void topTest1() {
        assertThrows(NullPointerException.class, () -> {
            Stack s = new Stack();
            s.top();
        });
    }

    /**
     * Метод для теситрования метода top,
     * который возвращает верхний элемент в стеке
     * В этом тесте сравнивается ожидаемое значение
     * со значением полученным в результате работы метода top
     */
    @Test
    public void topTest2() {
        Stack s = new Stack();
        s.push(1);
        s.push(2);
        assertEquals(2, s.top());
    }

    /**
     * Метод для теситрования метода top,
     * который возвращает верхний элемент в стеке
     * В этом тесте сравнивается ожидаемое значение
     * со значением полученным в результате работы метода top,
     * после удаления верхнего элемента из стека
     */
    @Test
    public void topTest3() {
        Stack s = new Stack();
        s.push(1);
        s.push(2);
        s.push(3);
        s.push(4);
        s.pop();
        assertEquals(3, s.top());
    }

    /**
     * Метод для теситрования вставки в стек
     * В этом тесте ожидается ошибка ArrayIndexOutOfBoundsException,
     * из за попытки переполнить стек
     */
    @Test
    public void pushTest1() {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            Stack s = new Stack(4);
            for (int i = 1; i < 6; i++) {
                s.push(i);
            }
        });
    }

    /**
     * Метод для тестирования вставки в стек
     * В этом тесте сравнивается ожидаемый массив элементов,
     * с реальным массивом элемментов, который представляет собой стек
     */
    @Test
    public void pushTest2() {
        Stack s = new Stack(3);
        s.push(10);
        s.push(2);
        s.push(14);
        Object[] o = {10, 2, 14};
        assertArrayEquals(o, s.printStack());
    }

    /**
     * Метод для тестирования метода pop,
     * который удаляет верхний элемент из стека
     * В этом тесте ожидается ошибка NullPointerException,
     * потому что стек пуст
     */
    @Test
    public void popTest1() {
        assertThrows(NullPointerException.class, () -> {
            Stack s = new Stack();
            s.pop();
        });
    }

    /**
     * Метод для тестирования метода pop,
     * который удаляет верхний элемент из стека
     * В этом тесте сравнивается ожидаемое значение
     * с результатом работы метода pop
     */
    @Test
    public void popTest2() {
        Stack s = new Stack();
        s.push(2);
        s.push(4);
        s.push(8);
        s.push(16);
        s.pop();
        assertEquals(8, s.top());
    }

    /**
     * Метод для тестирования метода pop,
     * который удаляет верхний элемент из стека
     * В этом тесте проверяется, возвращает ли метод
     * значение удаляемого элемента
     */
    @Test
    public void popTest3() {
        Stack s = new Stack();
        s.push(2);
        s.push(4);
        s.push(8);
        s.push(16);
        Object o = s.pop();
        assertEquals(16, o);
    }
}
