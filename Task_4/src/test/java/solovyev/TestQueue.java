package solovyev;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс для проверки методов класса Queue
 */
public class TestQueue {

    /**
     * Метод для теситрования вставки в очередь
     * В этом тесте ожидается ошибка ArrayIndexOutOfBoundsException,
     * из за попытки переполнить очередь
     */
    @Test
    public void enqueueTest1() {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            Queue q = new Queue(4);
            for (int i = 1; i < 6; i++) {
                q.enqueue(i);
            }
        });
    }

    /**
     * Метод для тестирования вставки в очередь
     * В этом тесте сравнивается ожидаемый массив элементов,
     * с реальным массивом элемментов, который представляет собой очередь
     */
    @Test
    public void enqueueTest2() {
        Queue q = new Queue(3);
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        Object[] o = {1, 2, 3};
        assertArrayEquals(o, q.printQueue());
    }

    /**
     * Метод для тестирования метода dequeue,
     * который удаляет последний элемент из очереди
     * В этом тесте ожидается ошибка NullPointerException,
     * потому что очередь пуста
     */
    @Test
    public void dequeueTest1() {
        assertThrows(NullPointerException.class, () -> {
            Queue q = new Queue();
            q.dequeue();
        });
    }

    /**
     * Метод для тестирования метода dequeue,
     * который удаляет последний элемент из очереди
     * В этом тесте сравнивается ожидаемое значение
     * с результатом работы метода dequeue
     */
    @Test
    public void dequeueTest2() {
        Queue q = new Queue(4);
        q.enqueue(2);
        q.enqueue(4);
        q.enqueue(8);
        q.enqueue(16);
        q.dequeue();

        assertEquals(4, q.top());
    }

    /**
     * Метод для теситрования метода top,
     * который возвращает первый элемент из очереди
     * В этом тесте ожидается ошибка NullPointerException,
     * потому что очередь пуста
     */
    @Test
    public void topTest1() {
        assertThrows(NullPointerException.class, () -> {
            Queue q = new Queue();
            q.top();
        });
    }

    /**
     * Метод для теситрования метода top,
     * который возвращает первый элемент из очереди
     * В этом тесте сравнивается ожидаемое значение
     * со значением полученным в результате работы метода top
     */
    @Test
    public void topTest2() {
        Queue q = new Queue();
        q.enqueue(1);
        q.enqueue(2);
        assertEquals(1, q.top());
    }


    /**
     * Метод для тестирования мтеода isEmpty,
     * возвращающего true, если очередь пуста
     * В этом тесте очередь пустая, поэтому в
     * результате работы ожидается true
     */
    @Test
    public void isEmptyTest1() {
        Queue q = new Queue();
        assertTrue(q.isEmpty());
    }

    /**
     * Метод для тестирования мтеода isEmpty,
     * возвращающего true, если очередь пуста
     * В этом тесте очередь не пустая, поэтому в
     * результате работы ожидается false
     */
    @Test
    public void isEmptyTest2() {
        Queue q = new Queue();
        q.enqueue(1);
        q.enqueue(3);
        assertFalse(q.isEmpty());
    }


}
