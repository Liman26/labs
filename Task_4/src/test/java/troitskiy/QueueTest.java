package troitskiy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class QueueTest {
    private Queue queue;

    @BeforeEach
    public void setUp() {
        queue = new Queue(3);
    }

    @Test
    @DisplayName("Проверка очереди на корректные операции")
    public void testQueueGood() {
        assertEquals(queue.enqueue(10), 1, "Добавить в очередь элемент 10");
        assertEquals(queue.enqueue(20), 1, "Добавить в очередь элемент 20");
        assertEquals(queue.top(), 10, "Считать первый элемент из очереди: 10");
        assertEquals(queue.dequeue(), 1, "Удалить первый элемент из очереди");
        assertEquals(queue.top(), 20, "Считать первый элемент из очереди: 20");
    }

    @Test
    @DisplayName("Проверка очереди на не корректные операции")
    public void testQueueBad() {
        assertEquals(queue.dequeue(), -1, "Попытка удалить элемент из пустой очереди");
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        assertEquals(queue.enqueue(4), -1, "Попытка добавить элемент сверх размера очереди");
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        assertNull(queue.top(), "Попытка считать элемент из пустой очереди");
    }



}
