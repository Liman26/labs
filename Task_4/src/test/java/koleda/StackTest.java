package koleda;

import org.junit.jupiter.api.Test;
import java.util.EmptyStackException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StackTest {

    /**
     * Проверка метода добавления элемента в стек
     */
    @Test
    public void pushTest(){
        Stack stackTest = new Stack(5);
        for (int i = 0; i <= 4; i++) {
            stackTest.push(i);
        }
        assertThrows(StackOverflowError.class, () -> stackTest.push(5));
        for (int i = 4; i >= 0; i--) {
            assertEquals(i, stackTest.pop());
        }
    }

    /**
     * Проверка метода выталкивания элемента из стека
     */
    @Test
    public void popTest() {
        Stack stackTest = new Stack(3);
        assertThrows(EmptyStackException.class, stackTest::pop);
        stackTest.push("a");
        stackTest.push("b");
        assertEquals("b", stackTest.pop());
        stackTest.push("c");
        assertEquals("c", stackTest.pop());
        assertEquals("a", stackTest.pop());
        assertThrows(EmptyStackException.class, stackTest::pop);
    }

    /**
     * Проверка метода получения элемента из стека, без его удаления
     */
    @Test
    public void topTest() {
        Stack stackTest = new Stack(8);
        assertThrows(EmptyStackException.class, stackTest::top);
        stackTest.push(0);
        assertEquals(0, stackTest.top());
        for (int i = 1; i <= 7; i++) {
            stackTest.push(i);
        }
        assertEquals(7, stackTest.top());
        for (int i = 7; i >= 0; i--) {
            assertEquals(i, stackTest.pop());
        }
    }

}
