package koleda;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class QueueTest {

    /**
     * Проверка метода добавления в очередь
     */
    @Test
    public void enqueue() {
        Queue queue = new Queue(5);
        for (int i = 0; i <= 4; i++) {
            queue.enqueue(i);
        }
        assertThrows(UnsupportedOperationException.class, () -> queue.enqueue(5));
        for (int i = 0; i <= 4; i++) {
            assertEquals(i, queue.dequeue());
        }
    }

    /**
     * Проверка метода извлечения из очереди, без удаления из нее
     */
    @Test
    public void top() {
        Queue queue = new Queue(8);
        assertThrows(UnsupportedOperationException.class, queue::top);
        queue.enqueue(0);
        assertEquals(0, queue.top());
        for (int i = 1; i <= 7; i++) {
            queue.enqueue(i);
        }
        assertEquals(0, queue.top());
        for (int i = 0; i <= 7; i++) {
            assertEquals(i, queue.dequeue());
        }
    }

    /**
     * Проверка метода удаления из очереди с удалением элемента из нее
     */
    @Test
    public void dequeue() {
        Queue queue = new Queue(3);
        assertThrows(UnsupportedOperationException.class, queue::dequeue);
        queue.enqueue("a");
        queue.enqueue("b");
        assertEquals("a", queue.dequeue());
        assertEquals("b", queue.dequeue());
        queue.enqueue("c");
        assertEquals("c", queue.dequeue());
        assertThrows(UnsupportedOperationException.class, queue::dequeue);
    }


}
