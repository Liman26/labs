package nuridinov;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Тестированине методов из класса Stack.java
 */
public class TestStack {
    /**
     * Тесторивание метода вставки в стек
     */
    @Test
    public void pushTest1() {
        Stack stack = new Stack(3);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        Object[] o = {1, 2, 3};
        assertArrayEquals(o, stack.getObjects());
    }

    /**
     * Тестирование ожидаемого исключения при попытке добавления элемента в заполненный стек
     */
    @Test
    public void pushTest2() {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            Stack stack = new Stack();
            for (int i = 0; i < 11; i++) {
                stack.push(i);
            }
        });
    }

    /**
     * Тестирование метода удаления из очереди
     * Сработает исключение, так как происходит попытка удаления из пустого стека
     */
    @Test
    public void popTest1() {
        assertThrows(NullPointerException.class, () -> {
            Stack s = new Stack();
            s.pop();
        });
    }

    /**
     * Тестирование удаления из стека
     */
    @Test
    public void popTest2() {
        Stack s = new Stack(3);
        s.push(1);
        s.push(2);
        s.push(3);
        Object[] obj_s = {1, 2, null};
        assertEquals(3, s.pop());
        assertArrayEquals(obj_s, s.getObjects());
    }

    /**
     * Тестирование метода для получения верхушки стека без удаления
     */
    @Test
    public void topTest() {
        Stack s = new Stack();
        Object[] o = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (int i = 0; i < 10; i++) {
            s.push(i);
        }
        assertSame(9, s.top());
        assertArrayEquals(o, s.getObjects());

    }

    /**
     * Тестирование исключения из метода top(), попытка получить первый элемент из пустого стека
     */
    @Test
    public void topTest1() {
        assertThrows(NullPointerException.class, () -> {
            Stack s = new Stack();
            s.top();
        });
    }

    /**
     * Тестирование метода для проверки стека на наличии в ней элементов
     */
    @Test
    public void isEmptyTest() {
        Stack s = new Stack();
        assertTrue(s.isEmpty());
    }

    /**
     * Тестирование методов для проверки стека на переполенность
     */
    @Test
    public void isFullTest() {
        Stack s = new Stack();
        for (int i = 0; i < 10; i++) {
            s.push(i);
        }
        assertTrue(s.isFull());
    }

}
