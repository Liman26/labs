package nuridinov;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Тестированине методов из класса Queue.java
 */
public class TestQueue {
    /**
     * Тесторивание метода вставки в очередь
     */
    @Test
    public void enqueueTest1() {
        Queue queue = new Queue(5);
        Object[] objects = {1, 2, 3, 4, 5};
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        assertArrayEquals(objects, queue.getObjects());
    }

    /**
     * Тестирование ожидаемого исключения при попытке добавления элемента в заполненную очередь
     */
    @Test
    public void enqueueTest2() {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            Queue q = new Queue();
            for (int i = 0; i < 11; i++) {
                q.enqueue(i);
            }
        });
    }

    /**
     * Тестирование метода удаления из очереди
     * Сработает исключение, так как происходит попытка удаления из пустой очереди
     */
    @Test
    public void dequeueTest1() {
        assertThrows(NullPointerException.class, () -> {
            Queue q = new Queue();
            q.dequeue();
        });
    }

    /**
     * Тестирование удаления из очереди
     */
    @Test
    public void dequeueTest2() {
        Queue q = new Queue(3);
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        q.dequeue();
        Object[] obj_s = new Object[3];
        obj_s[1] = 2;
        obj_s[2] = 3;
        assertArrayEquals(obj_s, q.getObjects());
    }

    /**
     * Тестирование метода для получения первого элемента очереди
     */
    @Test
    public void topTest() {
        Queue q = new Queue();
        for (int i = 0; i < 10; i++) {
            q.enqueue(i);
        }
        assertSame(0, q.top());

    }

    /**
     * Тестирование исключения из метода top(), попытка получить первый элемент из пустого списка
     */
    @Test
    public void topTest1() {
        assertThrows(NullPointerException.class, () -> {
            Queue q = new Queue();
            q.top();
        });
    }

    /**
     * Тестирование метода для проверки очереди на наличии в ней элементов
     */
    @Test
    public void isEmptyTest() {
        Queue q = new Queue();
        assertTrue(q.isEmpty());
    }

    /**
     * Тестирование методов для проверки очереди на переполенность
     */
    @Test
    public void isFullTest() {
        Queue q = new Queue();
        for (int i = 0; i < 10; i++) {
            q.enqueue(i);
        }
        assertTrue(q.isFull());
    }

}
