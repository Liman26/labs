package gushchin;

import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.*;

public class StackTest {

    /**
     * Тест на проверку добовления элемента в стек и получения исключения
     */
    @Test
    public void pushTest() {
        Stack stack = new Stack(4);
        for (int i = 0; i < 4; i++) {
            stack.push(i);
        }
        assertThrows(StackOverflowError.class, () -> stack.push(4));
        for (int i = 3; i >= 0; i--) {
            assertEquals(i, stack.pop());
        }
    }

    /**
     * Тест на проверку последнего элемента и метода getTop, возвращающего актуальное значения
     */
    @Test
    public void gettingTopTest() {
        Stack stack = new Stack(4);
        assertThrows(EmptyStackException.class, stack::getTop);
        stack.push(0);
        assertEquals(0, stack.getTop());
        stack.push(123);
        assertEquals(123, stack.getTop());
    }

    /**
     * Тест на проверку удаления последнего элемента в стеке
     */
    @Test
    public void popTest() {
        Stack stack = new Stack(4);
        assertThrows(EmptyStackException.class, stack::pop);
        stack.push(0);
        stack.push(1);
        assertEquals(1, stack.pop());
    }
}
