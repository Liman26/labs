package gushchin;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class QueueTest {

    /**
     * Тест на проверку добавления элементов в очередь и переполнение очереди.
     */
    @Test
    public void enqueueAndOverflowQueueTest() {
        Queue queue = new Queue(10);
        for (int i = 0; i <= 9; i++) {
            queue.enqueue(i);
        }
        assertThrows(UnsupportedOperationException.class, () -> queue.enqueue(10));
        for (int i = 0; i <= 9; i++) {
            assertEquals(i, queue.dequeue());
        }
    }

    /**
     * Тест на проверку оперции получения первого элемента из очереди
     */
    @Test
    public void gettingTopAndCheckEmptyTest() {
        Queue queue = new Queue(5);
        assertThrows(UnsupportedOperationException.class, queue::getTop);
        queue.enqueue(0);
        assertEquals(0, queue.getTop());
        for (int i = 1; i <= 4; i++) {
            queue.enqueue(i);
        }
        assertEquals(0, queue.getTop());
        for (int i = 0; i <= 4; i++) {
            assertEquals(i, queue.dequeue());
        }
    }

    /**
     * Тест на проверку удаления элемента из очереди с возможностью возникновения исключения
     */
    @Test
    public void dequeueAndCheckEmpty() {
        Queue queue = new Queue(4);
        assertThrows(UnsupportedOperationException.class, queue::dequeue);
        for (int i = 0; i <= 2; i++) {
            queue.enqueue(i);
        }
        for (int i = 0; i <= 2; i++) {
            assertEquals(i, queue.dequeue());
        }
        assertThrows(UnsupportedOperationException.class, queue::dequeue);
    }
}

