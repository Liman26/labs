package emelyanov;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class StackTest {
  private Stack stack;

  @BeforeEach
  public void setUp() {
    stack = new Stack(3);
  }

  @Test
  @DisplayName("Проверка стэка на корректные операции")
  public void testQueueGood() {
    assertEquals(stack.push(10), 1, "Добавить в стэк элемент 10");
    assertEquals(stack.push(20), 1, "Добавить в стэк элемент 20");
    assertEquals(stack.top(), 20, "Считать верхний элемент из стэка без удаления: 20");
    assertEquals(stack.pop(), 20, "Считать верхний элемент из стэка с удалением: 20");
    assertEquals(stack.top(), 10, "Считать верхний элемент из стэка без удаления: 10");
  }

  @Test
  @DisplayName("Проверка стэка на не корректные операции")
  public void testQueueBad() {
    assertNull(stack.pop(), "Считать верхний элемент из пустого из стэка с удалением");
    stack.push(1);
    stack.push(2);
    stack.push(3);
    assertEquals(stack.push(4), -1, "Попытка добавить элемент сверх размера стэка");
    stack.pop();
    stack.pop();
    stack.pop();
    assertNull(stack.top(), "Считать верхний элемент из пустого стэка без удаления");
  }
}
