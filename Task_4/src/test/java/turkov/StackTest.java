package turkov;

import org.junit.jupiter.api.Test;
import java.util.EmptyStackException;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс проверки работы методов, которые реализуют операции над стеком.
 */
public class StackTest {

    /**
     * Метод проверки операции добавления в стек с учетом возможности повяления исключения при его переполнении.
     * В итоге проверяется содержимое очереди после операций над ней.
     */
    @Test
    public void pushAndOverflowTest() {
        Stack stackTest = new Stack(5);
        for (int i = 0; i <= 4; i++) {
            stackTest.push(i);
        }
        assertThrows(StackOverflowError.class, () -> stackTest.push(5));
        for (int i = 4; i >= 0; i--) {
            assertEquals(i, stackTest.pop());
        }
    }

    /**
     * Метод проверки операции извлечения элемента без удаления из стека. Учитывается возможность появления исключения
     * во время операции, когда стек пуст. В итоге проверяется содержимое очереди после операций над ней.
     */
    @Test
    public void topAndEmptyTest() {
        Stack stackTest = new Stack(8);
        assertThrows(EmptyStackException.class, stackTest::top);
        stackTest.push(0);
        assertEquals(0, stackTest.top());
        for (int i = 1; i <= 7; i++) {
            stackTest.push(i);
        }
        assertEquals(7, stackTest.top());
        for (int i = 7; i >= 0; i--) {
            assertEquals(i, stackTest.pop());
        }
    }

    /**
     * Метод проверки операции извлечения верхнего элемента с удалением из стека.Учитывается возможность появления
     * исключения во время операции, когда стек пуст.Проверяется содержимое стека после операций над ним.
     */
    @Test
    public void popAndEmptyTest() {
        Stack stackTest = new Stack(3);
        assertThrows(EmptyStackException.class, stackTest::pop);
        stackTest.push("One");
        stackTest.push("Two");
        assertEquals("Two", stackTest.pop());
        stackTest.push("Three");
        assertEquals("Three", stackTest.pop());
        assertEquals("One", stackTest.pop());
        assertThrows(EmptyStackException.class, stackTest::pop);
    }
}
