package turkov;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс проверки работы методов, которые реализуют операции над очередью.
 */
public class QueueTest {

    /**
     * Метод проверки операции добавления в очередь с учетом возможности появления исключения при её переполнении.
     * В итоге проверяется содержимое очереди после операций над ней.
     */
    @Test
    public void enqueueAndOverflowTest() {
        Queue queueTest = new Queue(5);
        for (int i = 0; i <= 4; i++) {
            queueTest.enqueue(i);
        }
        assertThrows(UnsupportedOperationException.class, () -> queueTest.enqueue(5));
        for (int i = 0; i <= 4; i++) {
            assertEquals(i, queueTest.dequeue());
        }
    }

    /**
     * Метод проверки операции извлечения элемента из начала очереди с учетом возможности появления исключения,
     * когда она пуста. В итоге проверяется содержимое очереди после операций над ней.
     */
    @Test
    public void topAndEmptyTest() {
        Queue queueTest = new Queue(8);
        assertThrows(UnsupportedOperationException.class, queueTest::top);
        queueTest.enqueue(0);
        assertEquals(0, queueTest.top());
        for (int i = 1; i <= 7; i++) {
            queueTest.enqueue(i);
        }
        assertEquals(0, queueTest.top());
        for (int i = 0; i <= 7; i++) {
            assertEquals(i, queueTest.dequeue());
        }
    }

    /**
     * Метод проверки операции извлечения с удалением из начала очереди с учетом возможности появления исключения,
     * когда она пуста.
     */
    @Test
    public void dequeueAndEmptyTest() {
        Queue queueTest = new Queue(3);
        assertThrows(UnsupportedOperationException.class, queueTest::dequeue);
        queueTest.enqueue("One");
        queueTest.enqueue("Two");
        assertEquals("One", queueTest.dequeue());
        assertEquals("Two", queueTest.dequeue());
        queueTest.enqueue("Three");
        assertEquals("Three", queueTest.dequeue());
        assertThrows(UnsupportedOperationException.class, queueTest::dequeue);
    }
}