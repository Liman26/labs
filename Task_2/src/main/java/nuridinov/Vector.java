package nuridinov;


import java.util.Random;
import java.util.Scanner;

/**
 * Класс описывающий вектор в трехмерном пространстве
 */
public final class Vector {
    private final double x;
    private final double y;
    private final double z;

    /**
     * Конструктор класса
     *
     * @param x - координата x
     * @param y - координата y
     * @param z - координата z
     */
    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * получение значение поля x
     *
     * @return значение поля x
     */
    public double getX() {
        return x;
    }

    /**
     * получение значение поля y
     *
     * @return значение поля y
     */
    public double getY() {
        return y;
    }

    /**
     * получение значение поля z
     *
     * @return значение поля z
     */
    public double getZ() {
        return z;
    }

    /**
     * Вычисление длины вектора
     *
     * @param v - вектор длину которого нужно найти
     * @return длина вектора
     */
    public double vectorLength(Vector v) {
        return Math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
    }

    /**
     * Получение сколярного произведения двух векторов
     *
     * @param v1 - первый вектор
     * @param v2 - второй вектор
     * @return результат скалярного произведения векторов
     */
    public double scalarProduct(Vector v1, Vector v2) {
        return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    }

    /**
     * Получение векторного произведения двух векторов
     *
     * @param v1 - первый вектор
     * @param v2 - второй вектор
     * @return новый вектор
     */
    public Vector crossProduct(Vector v1, Vector v2) {
        return new Vector(v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x);
    }

    /**
     * Вычисление угла между двумя векторами
     *
     * @param a - первый вектор
     * @param b - второй вектор
     * @return угол между векторами
     */
    public double angleBetween(Vector a, Vector b) {
        return (scalarProduct(a, b) / Math.abs(vectorLength(a)) * Math.abs(vectorLength(b)));
    }

    /**
     * Сложение двух векторов
     *
     * @param a - первый вектор
     * @param b - второй вектор
     * @return новый вектор
     */
    public Vector vectorsAddition(Vector a, Vector b) {
        return new Vector(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    /**
     * Вычитание двух векторов
     *
     * @param a - первый вектор
     * @param b - второй вектор
     * @return новый вектор
     */
    public Vector vectorsSubtraction(Vector a, Vector b) {
        return new Vector(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    /**
     * Создание массива векторов
     *
     * @param n - число элементов массива
     * @return массива векторов
     */
    public static Vector[] arrayOfVectors(int n) {
        Vector[] vectors = new Vector[n];
        Random random = new Random();
        double min = -100.0;
        double max = 100.0;
        double diff = max - min;
        for (int i = 0; i < n; i++) {
            vectors[i] = new Vector(min + diff * random.nextDouble(), min + diff * random.nextDouble(), min + diff * random.nextDouble());
        }
        return vectors;
    }

    /**
     * Отображение значений полей вектора
     */
    public void printInfo() {
        System.out.println("X:" + x);
        System.out.println("Y:" + y);
        System.out.println("Z:" + z);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("введите кол-во векторов: ");
        int n = scanner.nextInt();
        Vector[] vectors;
        vectors = Vector.arrayOfVectors(n);

        for (int i = 0; i < n; i++) {
            System.out.println("Вектор №" + i);
            vectors[i].printInfo();
        }
    }
}
