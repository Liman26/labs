package zurnachyan;

/**
 * Класс является реализацией геометрического трех-мерного вектора с его основными операциями
 */
final public class Vector3D {
    final private double x, y, z;

    /**
     * Конструтор
     *
     * @param x координата x
     * @param y координата y
     * @param z координата z
     */
    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    /**
     * Статический метод создает массив векторов заданного размера
     *
     * @param n количество элементов в массиве
     * @return массив со случайными векторами
     */
    public static Vector3D[] listOfVectors(int n) {
        Vector3D[] vectors = new Vector3D[n];
        for (int i = 0; i < n; i++) {
            vectors[i] = new Vector3D(Math.random() * 100, Math.random() * 100, Math.random() * 100);
        }
        return vectors;
    }

    /**
     * Метод возвращает длину данного вектора
     *
     * @return длина
     */
    public double length() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * Метод возвращает скалярное произведение данного вектора с внешним
     *
     * @param other внешний вектор
     * @return результат скалярного произведения
     */
    public double scalarMultiple(Vector3D other) {
        return (x * other.x + y * other.y + z * other.z);
    }

    /**
     * Метод возвращает вектор, являющийся векторным произведением данного вектора с внешним
     *
     * @param other внешний вектор
     * @return вектор - произведение
     */
    public Vector3D vectorMultiple(Vector3D other) {
        return new Vector3D(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x);
    }

    /**
     * Метод возвращает косинус угла между данным ветором и внешним
     *
     * @param other внешний вектор
     * @return косинус угла
     */
    public double angle(Vector3D other) {
        return (this.scalarMultiple(other) / (this.length() * other.length()));
    }

    /**
     * Метод возвращает вектор, являющийся суммой данного вектора с внешним
     *
     * @param other внешний вектор
     * @return вектор - сумма
     */
    public Vector3D sum(Vector3D other) {
        return new Vector3D(x + other.x, y + other.y, z + other.z);
    }

    /**
     * Метод возвращает вектор, являющийся разностью между данным вектором и внешним
     *
     * @param other внешний вектор
     * @return вектор - разность
     */
    public Vector3D diff(Vector3D other) {
        return new Vector3D(x - other.x, y - other.y, z - other.z);
    }

}
