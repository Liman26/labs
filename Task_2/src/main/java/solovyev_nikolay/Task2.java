package solovyev_nikolay;

/**
 * Класс, содержащий набор методов по работе с одним или парой векторов.
 */
public class Task2 {
    private final double x,y,z;

    /**
     * Конструктор класса Task2
     * @param x
     * @param y
     * @param z
     */
    
    public Task2(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Метод вычисления длины вектора
     * @return значение длины вектора
     */
    double getLength() {
        return Math.sqrt(x * x + y * y + z * z);
    }
    
    /**
     * Метод вычисления скалярного произведения векторов
     * @return результат вычисления скалярного произведения 2 векторов
     * @param vector - вектор, на который произодится скалярное умножение
     */
    double getSkalar(Task2 vector) {
        return (this.x * vector.x + this.y * vector.y + this.z * vector.z);
    }
    /**
     * Метод вычисления векторного произведения
     * @return возвращает новый вектор, полученный как векторное произведение
     * @param vector - вектор, на который произодится векторное умножение
     */
    Task2 getVectorMultiply(Task2 vector) {
          return new Task2(this.y *vector.z - this.z *vector.y,this.z *vector.x - this.x *vector.z,this.x*vector.y - this.y *vector.x);
    }
    /**
     * Метод вычисления косинуса угла между векторами
     * @return результат вычисления косинуса угла между двумя векторами
     * @param vector - вектор, с которым проводится вычисление угла
     */
    double getAngle(Task2 vector) {
        return (this.getSkalar(vector) / ( this.getLength() * vector.getLength() ));
    }
    /**
     * Метод вычисления суммы векторов
     * @return возвращает новый вектор, полученный как сумма векторов
     * @param vector - вектор, с которым произодится векторное сложение
     */
    Task2 getSum(Task2 vector) {
        return new Task2(this.x + vector.x, this.y + vector.y, this.z + vector.z);
    }
    /**
     * Метод вычисления разности векторов
     * @return возвращает новый вектор, полученный как разность векторов
     * @param vector - вектор, с которым произодится векторная разность
     */
    Task2 getDif(Task2 vector) {
        return new Task2(this.x - vector.x, this.y - vector.y, this.z - vector.z);
    }
    /**
     * Метод, формирующий массив векторов, с произвольными значениями параметров
     * @return возвращает массив векторов
     * @param n - размер массива
     */
    static Task2[] randomArrayN(int n){
        Task2 [] randomArrayN = new Task2[n];

        for (int i = 0; i < n; i++) {
            System.out.println("Случайный вектор " + i + ": ");
            randomArrayN[i] = new Task2(Math.random() *10,Math.random() *10,Math.random() *10);
            System.out.printf(randomArrayN[i].x + " " + randomArrayN[i].y + " " + randomArrayN[i].z);
            System.out.println();
        }
        return randomArrayN;
    }


    public static void main(String[] args) {
        Task2 vector1 = new Task2(1,2,3);
        Task2 vector2 = new Task2(4,5,6);
        System.out.println(vector1);
        System.out.println(vector1.getLength());
        System.out.println(vector1.getSkalar(vector2));
        System.out.println(vector1.getVectorMultiply(vector2));
        System.out.println(vector1.getAngle(vector2));
        System.out.println(vector1.getSum(vector2));
        System.out.println(vector1.getDif(vector2));
        randomArrayN(5);
    }
}

