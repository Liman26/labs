package koleda;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        Vector vector1 = new Vector(1, 0, 0);
        Vector vector2 = new Vector(0, 0, 1);

        System.out.println("Длина вектора: " + vector1.len());
        System.out.println("Скалярное произведение: " + vector1.scalarM(vector2));
        System.out.println("Векторное произведение: " + vector1.vectorM(vector2));
        System.out.println("Угол между векторами в градусах: " + vector1.alpha(vector2));
        System.out.println("Сумма векторов: " + vector1.add(vector2));
        System.out.println("Разность векторов: " + vector1.dif(vector2));

        System.out.print("Введите количество векторов, которые нужно сгенерировать случайным образом: ");
        int n = input.nextInt();
        Vector[] array = Vector.randN(n);
        for (int i = 0; i < array.length; i++) {
            System.out.printf("Вектор %d: (%.2f, %.2f, %.2f)\n", i + 1, array[i].getX(), array[i].getY(), array[i].getZ());
        }

    }
}
