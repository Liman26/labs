package koleda;

public class Vector {
    final private double x, y, z;


    /**
     *
     * @return - значение x объекта класса Vector
     */
    public double getX() {
        return x;
    }

    /**
     *
     * @return - значение y объекта класса Vector
     */
    public double getY() {
        return y;
    }

    /**
     *
     * @return - значение z объекта класса Vector
     */
    public double getZ() {
        return z;
    }

    public Vector(double a, double b, double c)
    {
        this.x = a;
        this.y = b;
        this.z = c;
    }

    /**
     *
     * @return - длина вектора
     */
    public double len()
    {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     *
     * @param a - второй вектор
     * @return  - скалярное произведение векторов
     */
    public double scalarM(Vector a)
    {
        return x * a.x + y * a.y + z * a.z;
    }

    /**
     *
     * @param a - второй вектор
     * @return  - векторное произведение векторов
     */
    public Vector vectorM(Vector a)
    {
        return new Vector(y*a.z - z*a.y, z*a.x - x*a.z, x*a.y - y*a.x);
    }

    /**
     *
     * @param a - второй вектор
     * @return  - угол между векторами
     */
    public double alpha(Vector a)
    {
        return this.scalarM(a)/(this. len() * a.len());
    }

    /**
     *
     * @param a - второй вектор
     * @return  - сумма векторов
     */
    public Vector add(Vector a)
    {
        return new Vector(x + a.x, y + a.y, z + a.z);
    }

    /**
     *
     * @param a - второй вектор
     * @return  - разность векторов
     */
    public Vector dif(Vector a)
    {
        return new Vector(x - a.x, y - a.y, z - a.z);
    }

    /**
     *
     * @param n - размерность массива
     * @return  - массив размером N
     */
    public static Vector[] randN(int n)
    {
        Vector[] v = new Vector[n];
        for (int i = 0; i < n; i++)
        {
            v[i] = new Vector(Math.random(), Math.random(), Math.random());
        }
        return v;
    }

}
