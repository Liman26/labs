package mescheryakov;

/**
 * неизменяемый класс с тремя приватными параметрами
 */
public final class Vector {
    private final double x;
    private final double y;
    private final double z;

    /**
     * конструктор с параметрами
     * @param x координата
     * @param y координата
     * @param z координата
     */
    Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * метод, вычисляющий длину вектора
     * @return возвращает корень из суммы квадратов
     */
    double dlVector() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * метод, вычисляющий скалярное произведение
     * @param b второй вектор
     * @return возвращает скалярное произведение
     */
    double skVector(Vector b) {
        return x * b.x + y * b.y + z * b.z;
    }

    /**
     * метод, вычисляющий векторное произведение с другим вектором
     * @param b второй вектор
     * @return возвращает векторное произведение как новый вектор
     */
    Vector prVector(Vector b) {
        return new Vector(y * b.z - z * b.y, z * b.x - x * b.z, x * b.y - y * b.x);
    }

    /**
     * метод, вычисляющий угол между векторами
     * @param b второй вектор
     * @return возвращает арккосинус угла между векторами
     */
    double csVector(Vector b) {
        double s = this.skVector(b);
        return Math.acos(s / this.dlVector() * b.dlVector());
    }

    /**
     * метод для вычисления суммы векторов
     * @param b второй вектор
     * @return возвращает сумму как новый вектор
     */
    Vector smVector(Vector b) {
        return new Vector(x + b.x, y + b.y, z + b.z);
    }

    /**
     * метод для вычисления разности векторов
     * @param b второй вектор
     * @return возращает разность как новый вектор
     */
    Vector rzVector(Vector b) {
        return new Vector(x - b.x, y - b.y, z - b.z);
    }

    /**
     * статический метод, который принимает число N, и возвращает массив случайных векторов размером N
     * @param n размер массива
     * @return возвращает массив случайных векторов
     */
    static Vector[] massiv(int n) {
        Vector[] vectors = new Vector[n];
        for (int i = 0; i < n; i++) {
            vectors[i] = new Vector(Math.random(), Math.random(), Math.random());
        }
        return vectors;
    }

    public static void main(String[] args) {
        double x = Double.parseDouble(args[0]);
        double y = Double.parseDouble(args[1]);
        double z = Double.parseDouble(args[2]);
        Vector b = new Vector(x, y, z);
    }
}