package troitskiy;

public class Vector3 {
    final private double x, y, z;


    /**
     *
     * @return - первая компонента вектора
     */
    public double getX() {
        return x;
    }

    /**
     *
     * @return - вторая компонента вектора
     */
    public double getY() {
        return y;
    }

    /**
     *
     * @return - третья компонента вектора
     */
    public double getZ() {
        return z;
    }

    /**
     *
     * @param a - первая компонента вектора
     * @param b - вторая компонента вектора
     * @param c - третья компонента вектора
     */
    public Vector3(double a, double b, double c)
    {
        x = a;
        y = b;
        z = c;
    }

    /**
     *
     * @return - Модуль (длина) вектора
     */
    public double Mod()
    {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     *
     * @param a - другой вектор
     * @return  - скалярное произведение векторов
     */
    public double SM(Vector3 a)
    {
        return x * a.x + y * a.y + z * a.z;
    }

    /**
     *
     * @param a - другой вектор
     * @return  - векторное произведение векторов
     */
    public Vector3 VM(Vector3 a)
    {
        return new Vector3(y*a.z - z*a.y, z*a.x - x*a.z, x*a.y - y*a.x);
    }

    /**
     *
     * @param a - другой вектор
     * @return  - угол между векторами (или косинус угла)
     */
    public double Alpha(Vector3 a)
    {
        return this.SM(a)/(Mod()*a.Mod());
    }

    /**
     *
     * @param a - другой вектор
     * @return  - Сумма векторов
     */
    public Vector3 Add(Vector3 a)
    {
        return new Vector3(x + a.x, y + a.y, z + a.z);
    }

    /**
     *
     * @param a - другой вектор
     * @return  - Разность векторов
     */
    public Vector3 Sub(Vector3 a)
    {
        return new Vector3(x - a.x, y - a.y, z - a.z);
    }

    /**
     *
     * @param n - целое число N (размер возвращаемого массива векторов)
     * @return  - массив  случайных векторов размером N
     */
    public static Vector3[] RandVA(int n)
    {
        Vector3[] v = new Vector3[n];
        for (int i = 0; i < n; i++)
        {
            v[i] = new Vector3(Math.random(), Math.random(), Math.random());
        }
        return v;
    }

}
