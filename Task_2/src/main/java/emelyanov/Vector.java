package emelyanov;

public class Vector {
  private final int x;
  private final int y;
  private final int z;

  Vector(int x, int y, int z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  /**
   * Геттер координаты X
   * @return координата X
   */
  public int getX() {
    return x;
  }

  /**
   * Геттер координаты Y
   * @return координата Y
   */
  public int getY() {
    return y;
  }

  /**
   * Геттер координаты Z
   * @return координата Z
   */
  public int getZ() {
    return z;
  }

  /**
   * Длина вектора
   * @return длину вектора
   */
  public double length() {
    return Math.sqrt(x * x + y * y + z * z);
  }

  /**
   * Скалярное произведение с другим вектором
   * @param vector вектор на который производиться операция
   * @return скалярное произведение
   */
  public double scalarProduct(Vector vector) {
    return x * vector.getX() + y * vector.getY() + z * vector.getZ();
  }

  /**
   * Векторное произведение с другим вектором
   * @param vector вектор с которым производиться операция
   * @return векторное произведение
   */
  public Vector vectorProduct(Vector vector) {
    return new Vector(y * vector.getZ() - z * vector.getY(),
        z * vector.getX() - x * vector.getZ(),
        x * vector.getY() - y * vector.getX());
  }

  /**
   * Угол между векторами
   * @param vector вектор с которым производиться операция
   * @return угол между векторами
   */
  public double angle(Vector vector) {
    return scalarProduct(vector) / (length() * vector.length());
  }

  /**
   * Сумма векторов
   * @param vector вектор с которым производиться операция
   * @return сумма векторов
   */
  public Vector sum(Vector vector) {
    return new Vector(x + vector.getX(), y + vector.getY(), z + vector.getZ());
  }

  /**
   * Разность векторов
   * @param vector вектор с которым производиться операция
   * @return разность векторов
   */
  public Vector sub(Vector vector) {
    return new Vector(x - vector.getX(), y - vector.getY(), z - vector.getZ());
  }

  /**
   * Генератор векторов
   * @param count количество генерируемых векторов
   * @return массив созданных векторов
   */
  public static Vector[] generateVectors(int count) {
    Vector[] results = new Vector[count];
    for (int i = 0; i < count; i++) {
      results[i] = new Vector(Vector.randomInteger(), Vector.randomInteger(), Vector.randomInteger());
    }
    return results;
  }

  /**
   * Генератор случайных чиссел
   * @return случайное число типа int от 0 до Максимального типа int
   */
  public static int randomInteger() {
    return (int) Math.round(Math.random() * Integer.MAX_VALUE);
  }

  /**
   * Строковое представление вектра
   * @return строковое представление вектра
   */
  @Override
  public String toString() {
    return "Vector{" +
        "x=" + x +
        ", y=" + y +
        ", z=" + z +
        '}';
  }
}
