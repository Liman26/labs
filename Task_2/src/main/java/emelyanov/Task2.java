package emelyanov;

public class Task2 {
  public static void main(String[] args) {
    Vector vectorOne = new Vector(1, 2, 3);
    Vector vectorTwo = new Vector(4, 5, 6);
    System.out.println("One: " + vectorOne);
    System.out.println("Two: " + vectorTwo);
    System.out.println("Длина One: " + vectorOne.length());
    System.out.println("Скалярное произведение One и Two: " + vectorOne.scalarProduct(vectorTwo));
    System.out.println("Векторное произведение One и Two: " + vectorOne.vectorProduct(vectorTwo));
    System.out.println("Угол между One и Two: " + vectorOne.angle(vectorTwo));
    System.out.println("Сумма One и Two: " + vectorOne.sum(vectorTwo));
    System.out.println("Разность One и Two: " + vectorOne.sub(vectorTwo));

    System.out.println("Сгенерим массив из 10 векоторов:");
    Vector[] vectors = Vector.generateVectors(10);
    for (int i = 0; i < 10; i++) {
      System.out.println(i + ": " + vectors[0]);
    }
  }
}
