package kokhan;

/**
 * Класс, описывающий векторы в трехмерном пространстве и производящий операции над ними
 */

class VectorXYZ {
    final private double x, y, z;

    /**
     * Конструктор с параметрами в виде списка координат x, y, z
     *
     * @param x координата
     * @param y координата
     * @param z координата
     */

    public VectorXYZ(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Метод, вычисляющий длину вектора
     *
     * @return Длина вектора
     */

    public double length() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * Метод, вычисляющий скалярное произведение
     *
     * @param anotherCoordinate вектор (с координатами, отличными от данного вектора)
     * @return Результат вычисления скалярного произведения
     */

    public double scalarProductOfNumbers(VectorXYZ anotherCoordinate) {
        return (x * anotherCoordinate.x + y * anotherCoordinate.y + z * anotherCoordinate.z);
    }

    /**
     * Метод, вычисляющий векторное произведение с другим вектором
     *
     * @param anotherCoordinate вектор (с координатами, отличными от данного вектора)
     * @return Результат вычисления векторного произведения
     */

    public VectorXYZ crossProductOfNumbers(VectorXYZ anotherCoordinate) {
        return new VectorXYZ(
                y * anotherCoordinate.z - z * anotherCoordinate.y,
                z * anotherCoordinate.x - x * anotherCoordinate.z,
                x * anotherCoordinate.y - y * anotherCoordinate.x
        );
    }

    /**
     * Метод, вычисляющий угол между векторами (или косинус угла)
     *
     * @param anotherCoordinate вектор (с координатами, отличными от данного вектора)
     * @return Косинус угла
     */

    public double calculatingCos(VectorXYZ anotherCoordinate) {
        return scalarProductOfNumbers(anotherCoordinate) / (length() * anotherCoordinate.length());
    }

    /**
     * Метод, вычисляющий сумму векторов
     *
     * @param anotherCoordinate вектор (с координатами, отличными от данного вектора)
     * @return Результат сложения
     */

    public VectorXYZ sumOfValues(VectorXYZ anotherCoordinate) {
        return new VectorXYZ(x + anotherCoordinate.x, y + anotherCoordinate.y, z + anotherCoordinate.z);
    }

    /**
     * Метод, вычисляющий разность векторов
     *
     * @param anotherCoordinate вектор (с координатами, отличными от данного вектора)
     * @return Результат вычитания
     */

    public VectorXYZ differenceValues(VectorXYZ anotherCoordinate) {
        return new VectorXYZ(x - anotherCoordinate.x, y - anotherCoordinate.y, z - anotherCoordinate.z);
    }

    /**
     * Статический метод, который принимает целое число n, и возвращает массив случайных векторов размера n
     *
     * @param n целое число, задающее количество элементов в массиве
     * @return Массив случайных векторов размера n
     */

    public static VectorXYZ[] generateVectorsSizeN(int n) {
        VectorXYZ[] vectorsSizeN = new VectorXYZ[n];
        for (int i = 0; i < n; i++) {
            vectorsSizeN[i] = new VectorXYZ(Math.random() * 100, Math.random() * 100, Math.random() * 100);
        }
        return vectorsSizeN;
    }
}
