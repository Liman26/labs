package kokhan;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Задай размер массива векторов: n = ");
        int n = scan.nextInt();
        VectorXYZ[] vectorsSizeN = VectorXYZ.generateVectorsSizeN(n);
        System.out.println("Массив случайных векторов размера n = " + n + ":");
        for (int i = 0; i < n; i++) {
            System.out.println((i + 1) + ".  " + vectorsSizeN[i].length());
        }

    }
}
