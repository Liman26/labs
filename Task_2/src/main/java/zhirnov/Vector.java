package zhirnov;

import java.util.Random;

public final class Vector {
    private final double x;
    private final double y;
    private final double z;

    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Получение значения поля x объекта класса Vector
     * @return значение x объекта класса Vector
     */
    public double getX() {
        return x;
    }

    /**
     * Получение значения поля y объекта класса Vector
     * @return значение y объекта класса Vector
     */
    public double getY() {
        return y;
    }

    /**
     * Получение значения поля z объекта класса Vector
     * @return значение z объекта класса Vector
     */
    public double getZ() {
        return z;
    }

    /**
     * Получение длины вектора
     * @return длину вектора
     */
    public double Len() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * Скалярное произведение 2-х векторов
     * @param vector2 - второй вектор
     * @return длину вектора
     */
    public double ScalarMulti(Vector vector2) {
        return x * vector2.getX() + y * vector2.getY() + z * vector2.getZ();
    }

    /**
     * Вектроное произведение 2-х векторов
     * @param vector2 - второй вектор
     * @return новый вектор
     */
    public Vector VectorMulti(Vector vector2) {
        return new Vector(y * vector2.getZ() - z * vector2.getY(),
                z * vector2.getX() - x * vector2.getZ(),
                x * vector2.getY() - y * vector2.getX());
    }

    /**
     * Угол между 2-мя векторами
     * @param vector2 - второй вектор
     * @return угол между векторами
     */
    public double AngleBetweenVectors(Vector vector2) {
        return this.ScalarMulti(vector2) / (this.Len() * vector2.Len());
    }

    /**
     * Сумма 2-х векторов
     * @param vector2 - второй вектор
     * @return новый вектор
     */
    public Vector SumVectors(Vector vector2) {
        return new Vector(x + vector2.getX(),
                y + vector2.getY(),
                z + vector2.getZ());
    }

    /**
     * Разность 2-х векторов
     * @param vector2 - второй вектор
     * @return новый вектор
     */
    public Vector DifVectors(Vector vector2) {
        return new Vector(x - vector2.getX(),
                y - vector2.getY(),
                z - vector2.getZ());
    }

    /**
     * Получение массива случайных векторов размерности N
     * @param N - размерность массива
     * @return массив векторов
     */
    public static Vector[] ArrayOfVectors(int N){
        Vector[] vectors = new Vector[N];
        Random random = new Random();
        for (int i = 0; i < N; i++){
            vectors[i] = new Vector(random.nextDouble(), random.nextDouble(), random.nextDouble());
        }
        return vectors;
    }

}
