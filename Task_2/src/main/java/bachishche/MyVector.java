package bachishche;

/**
 * Вектор в трехмерном пространстве
 */
public class MyVector {
    private final double x, y, z;

    /**
     * @param x значение абциссы
     * @param y значение ординаты
     * @param z значение аппликаты
     */
    MyVector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Вычисление длины вектора
     *
     * @return длина вектора
     */
    double length() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * Вычисление скалярного произведения текущего вектора
     * с вектором b
     *
     * @param b другой вектор
     * @return скалярное произведение
     */
    double scalarProd(MyVector b) {
        return x * b.x + y * b.y + z * b.z;
    }

    /**
     * Вычисление векторного произведения текущего вектора и заданного
     *
     * @param b другой вектор
     * @return векторное произведение текущего вектора и вектора b
     */
    MyVector vectorProd(MyVector b) {
        double cx, cy, cz;
        cx = y * b.z - z * b.y;
        cy = z * b.x - x * b.z;
        cz = x * b.y - y * b.x;
        return new MyVector(cx, cy, cz);
    }

    /**
     * Вычисление косинуса угла между текущим вектором и заданным
     *
     * @param b другой вектор
     * @return косинус угла
     */
    double cosOfAngle(MyVector b) {
        return this.scalarProd(b) / (this.length() * b.length());
    }

    /**
     * Вычисление суммы текущего вектора и заданного
     *
     * @param b другой вектор
     * @return вектор, координаты которого - сумма соответствующих
     * координат исходного вектора и вектора b
     */
    MyVector sum(MyVector b) {
        return new MyVector(this.x + b.x, this.y + b.y, this.z + b.z);
    }

    /**
     * Вычисление разности текущего вектора и заданного
     *
     * @param b другой вектор
     * @return вектор, координаты которого - разность соответствующих
     * координат исходного вектора и вектора b
     */
    MyVector residual(MyVector b) {
        return new MyVector(this.x - b.x, this.y - b.y, this.z - b.z);
    }

    /**
     * Создать массив векторов со случаными координатами
     *
     * @param n размер массива
     * @return массив векторов со случайными координатами
     */
    public static MyVector[] randomVectors(int n) {
        MyVector[] vects = new MyVector[n];
        for (int i = 0; i < n; i++)
            vects[i] = new MyVector(Math.random(), Math.random(), Math.random());
        return vects;
    }
}