package turkov;

/**
 * Класс описывающий вектор (в трёхмерном пространстве).
 */
final class Vector3D {
    private final double x, y, z;

    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return "{" + x + "; " + y + "; " + z + "}";
    }

    /**
     * Метод вычисляющий длину вектора
     *
     * @return возвращает значение длины вектора
     */
    double length() {
        double x = this.x;
        double y = this.y;
        double z = this.z;
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * Метод вычисляющий скалярное произведение векторов 2-х трёхмерных векторов
     *
     * @param vector - вектор, с которым производится перемножение
     * @return - значение операции скалярного произведения векторов
     */
    double scalarMultiplier(Vector3D vector) {
        double x = vector.x;
        double y = vector.y;
        double z = vector.z;
        return (x * this.x + y * this.y + z * this.z);
    }

    /**
     * Метод вычисляющий угол между двумя трёхмерными векторами
     *
     * @param vector - вектор, с которым вычисялется угол
     * @return - угол между векоторами в градусах
     */
    double angle(Vector3D vector) {
        double cosinusOfAngle;

        cosinusOfAngle = this.scalarMultiplier(vector) / (this.length() * vector.length());
        return Math.toDegrees(Math.acos(cosinusOfAngle));
    }

    /**
     * Метод вычисляющий векторное произведение 2-х трехмерных векторов
     *
     * @param vector - вектор, с которым производится перемножение
     * @return - вектор как результат операции векторного произведения
     */
    Vector3D vectorMultiplier(Vector3D vector) {
        double x1 = this.x;
        double y1 = this.y;
        double z1 = this.z;
        double x2 = vector.x;
        double y2 = vector.y;
        double z2 = vector.z;
        return new Vector3D(y1 * z2 - y2 * z1, x2 * z1 - x1 * z2, x1 * y2 - x2 * y1);
    }

    /**
     * Метод вычисляющий сумму 2-х трехмерных векторов
     *
     * @param vector - вектор, с которым производится сложение
     * @return - вектор как результат операции сложения
     */
    Vector3D amount(Vector3D vector) {
        return new Vector3D(this.x + vector.x, this.y + vector.y, this.z + vector.z);
    }

    /**
     * Метод вычисляющий разность 2-х трехмерных векторов
     *
     * @param vector - вектор, который вычитается
     * @return - вектор как результат операции вычитания
     */
    Vector3D differential(Vector3D vector) {
        return new Vector3D(this.x - vector.x, this.y - vector.y, this.z - vector.z);
    }

    /**
     * Метод генерирующий N случайных трехмерных векторов
     *
     * @param N - количество векторов
     * @return - массив N случайных векторов
     */
    static Vector3D[] arrayOfVectors(int N) {
        Vector3D[] arrayOfVectors = new Vector3D[N];
        final byte MIN_AXIS = Byte.MIN_VALUE;
        final byte MAX_AXIS = Byte.MAX_VALUE;
        for (int i = 0; i < N; i++) {
            byte x = (byte) (Math.random() * (MAX_AXIS - MIN_AXIS) + MIN_AXIS);
            byte y = (byte) (Math.random() * (MAX_AXIS - MIN_AXIS) + MIN_AXIS);
            byte z = (byte) (Math.random() * (MAX_AXIS - MIN_AXIS) + MIN_AXIS);
            arrayOfVectors[i] = new Vector3D(x, y, z);
        }
        return arrayOfVectors;
    }
}
