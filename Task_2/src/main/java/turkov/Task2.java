package turkov;

import java.util.Scanner;


/**
 * Класс-драйвер для тестирования методов класса Vector3D
 */
public class Task2 {
    public static void main(String[] args) {
        Vector3D vectorOne = new Vector3D(1, 0, 0);
        Vector3D vectorTwo = new Vector3D(0, 1, 0);
        Scanner input = new Scanner(System.in);

        System.out.println("Длина вектора: " + vectorOne.length());
        System.out.println("Скалярное произведение: " + vectorOne.scalarMultiplier(vectorTwo));
        System.out.println("Векторное произведение: " + vectorOne.vectorMultiplier(vectorTwo));
        System.out.println("Угол между векторами в градусах: " + vectorOne.angle(vectorTwo));
        System.out.println("Сумма векторов: " + vectorOne.amount(vectorTwo));
        System.out.println("Разность векторов: " + vectorOne.differential(vectorTwo));
        System.out.print("\nВведите целое число (кол-во случайных 3D векторов): ");
        for (Vector3D v : Vector3D.arrayOfVectors(input.nextInt())) {
            System.out.println(v);
        }
    }
}
