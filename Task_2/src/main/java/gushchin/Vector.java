package gushchin;

/**
 * Класс трехмерного вектора с базовым набором методов
 */
public class Vector {
    private final double x;
    private final double y;
    private final double z;

    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Метод, вычисляющий длину вектора
     * @return длина вектора
     */
    public double getVectorLength() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * Метод, вычисляющий скалярное произведение с другим вектором
     * @param vector другой вектор
     * @return скалярное произведение двух векторов
     */
    public double getScalarProduct(Vector vector) {
        return x * vector.x + y * vector.y + z * vector.z;
    }

    /**
     * Метод, вычисляющий векторное произведение с другим вектором
     * @param vector другой вектор
     * @return векторное произведение двух векторов
     */
    public Vector getVectorProduct(Vector vector) {
        return new Vector(y * vector.z - z * vector.y, z * vector.x - x * vector.z, x * vector.y - y * vector.x);
    }

    /**
     * Метод, вычисляющий косинус угла между двумя векторами
     * @param vector другой вектор
     * @return косинус угла
     */
    public  double getVectorsAngle(Vector vector) {
        double scalarProduct = this.getScalarProduct(vector);

        return Math.cos(scalarProduct / Math.abs(this.getVectorLength()) * Math.abs(vector.getVectorLength()));
    }

    /**
     * Метод, вычисляющий сумму двух векторов
     * @param vector другой вектор
     * @return результирующий вектор от суммы
     */
    public Vector getVectorsSum(Vector vector) {
        return new Vector(x + vector.x, y + vector.y, z + vector.z);
    }

    /**
     * Метод, вычислящий разность двух векторов
     * @param vector другой вектор
     * @return результирующий вектор от разности
     */
    public Vector getVectorsDifference(Vector vector) {
        return new Vector(x - vector.x, y - vector.y, z - vector.z);
    }

    /**
     * Статический метод для генерации коллекции, с заданным размером, содержащей вектора со случайными координатами
     * @param n размер коллекции
     * @return коллекция векторов
     */
    static Vector[] getRandomVectorsByCurrentSize(int n) {
        var vectors = new Vector[n];
        for (var i = 0; i < n; i++) {
            vectors[i] = new Vector(Math.random(), Math.random(), Math.random());
        }

        return vectors;
    }
}
