package solovyev;

import java.util.Scanner;
import java.util.Random;

public class Task2 {


    public final static class Vector {
        private final double x;
        private final double y;
        private final double z;


        /**
         * Конструктор
         * @param x координата по оси абсцисс
         * @param y координата по оси ординат
         * @param z координата по оси аппликат
         */
        Vector(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /**
         * Геттер для значения x
         * @return значение x
         */
        public double getX() {
            return x;
        }

        /**
         * Геттер для значения y
         * @return значение y
         */
        public double getY() {
            return y;
        }

        /**
         * Геттер для значения z
         * @return значение z
         */
        public double getZ() {
            return z;
        }

        /**
         * Метод выводящий координаты вектора
         */
        public void displayVector() {
            System.out.println("X: " + x + " Y: " + y + " Z: " + z);
        }

        /**
         * Метод рассчитывающий длину вектора
         *
         * @return возвращает рассчитанную длину length
         */
        public double vectorLength() {
            return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
        }

        /**
         * Метод для нахождения скалярного произведения
         *
         * @param v получает на вход объект класса Vector
         * @return scProd найденное скалярное произведение
         */
        public double scalarProduct(Vector v) {
            return x * v.getX() + y * v.getY() + z * v.getZ();

        }

        /**
         * Метод для нахождения векторного произведения
         *
         * @param v на вход получает объект класса Vector
         * @return новый объект класса Vector
         */
        public Vector vectorProduct(Vector v) {
            return new Vector((y * v.getZ() - z * v.getY()),
                    (z * v.getX() - x * v.getZ()),
                    (x * v.getY() - y * v.getX()));
        }

        /**
         * Метод для вычисления косинуса угла между двумя векторами
         *
         * @param v на вход получает объект класса Vector
         * @return возвращает косинус угла между ними
         */
        public double angle(Vector v) {
            return this.scalarProduct(v) / (this.vectorLength() * v.vectorLength());
        }

        /**
         * Метод для сложения двух векторов
         *
         * @param v получает на вход объект класса Vector
         * @return возвращает новый объект класса Vector
         */
        public Vector summa(Vector v) {
            return new Vector((x + v.getX()), (y + v.getY()), (z + v.getZ()));
        }

        /**
         * Метод для нахождения разности двух векторов
         *
         * @param v получает на вход объект класса Vector
         * @return возвращает новый объект класса Vector
         */
        public Vector minus(Vector v) {
            return new Vector((x - v.getX()), (y - v.getY()), (z - v.getZ()));
        }

        /**
         * Метод генерирующий массив из N векторов
         *
         * @param n количество генерируемых векторов
         * @return массив веторов
         */
        public static Vector[] Generator(int n) {
            Vector[] vecMas = new Vector[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++) {
                vecMas[i] = new Vector(rnd.nextInt(11) - 5, rnd.nextInt(11) - 5, rnd.nextInt(11) - 5);
                vecMas[i].displayVector();
            }
            return vecMas;
        }


    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Введите координаты вектора 1 (через enter или пробел)");

        Vector v1 = new Vector(input.nextDouble(), input.nextDouble(), input.nextDouble());

        v1.displayVector();

        System.out.println("Длина вектора: " + v1.vectorLength());

        System.out.println("Введите координаты вектора 2 (через enter или пробел)");

        Vector v2 = new Vector(input.nextDouble(), input.nextDouble(), input.nextDouble());

        v2.displayVector();

        System.out.println("Скалярное произведение вектора 1 на вектор 2 = " + v1.scalarProduct(v2));

        Vector v3 = v1.vectorProduct(v2);
        System.out.println("Вектор полученный в результате векторного произведения вектора 1 и 2:");
        v3.displayVector();

        System.out.println("Косинус угла между векторами 1 и 2 = " + v1.angle(v2));

        Vector v4 = v1.summa(v2);
        System.out.println("Сумма векторов 1 и 2:");
        v4.displayVector();

        Vector v5 = v1.minus(v2);
        System.out.println("Разность векторов 1 и 2:");
        v5.displayVector();

        System.out.println("Введите число N (для генерации массива случайных веторов)");
        int N = input.nextInt();

        Vector.Generator(N);

    }
}
