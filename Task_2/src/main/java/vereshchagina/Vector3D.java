package vereshchagina;

/**
 *Класс описывает вектор в трехмерном пространстве
 */
public final class Vector3D {
    private final double x, y, z;

    /**
     * конструктор с параметрами в виде списка координат x, y, z
     *
     * @param x - координата по оси X
     * @param y - координата по оси Y
     * @param z - координата по оси Z
     */
    Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * метод возвращает значение координаты x
     *
     * @return координата x
     */
    public double getX() {
        return x;
    }

    /**
     * метод возвращает значение координаты y
     *
     * @return координата y
     */
    public double getY() {
        return y;
    }

    /**
     * метод возвращает значение координаты z
     *
     * @return координата z
     */
    public double getZ() {
        return z;
    }

    /**
     * метод вычисляет длину вектора
     *
     * @return возвращает длину вектора
     */
    public double Length() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * метод вычисляет скалярное произведение двух векторов
     * (вектора, от которого вызван метод, и переданного в параметрах)
     *
     * @param mult второй множитель
     * @return возвращает скалярное произведение
     */
    public double ScalarProduct(Vector3D mult) {
        return x * mult.getX() + y * mult.getY() + z * mult.getZ();
    }

    /**
     * метод вычисляет векторное произведение двух векторов
     * (вектора, от которого вызван метод, и переданного в параметрах)
     *
     * @param mult второй множитель
     * @return возвращает новый вектор
     */
    public Vector3D VectorProduct(Vector3D mult) {
        return new Vector3D(y * mult.getZ() - z * mult.getY(), z * mult.getX() - x * mult.getZ(), x * mult.getY() - y * mult.getX());
    }

    /**
     * метод вычисляет угол между двумя векторами
     * (вектором, от которого вызван метод, и переданным в параметрах)
     *
     * @param vect вектор
     * @return возвращает угол
     */
    public double AngleBetweenVectors(Vector3D vect) {
        return Math.acos(this.ScalarProduct(vect) / (this.Length() * vect.Length()));
    }

    /**
     * метод вычисляет сумму двух векторов (вектора, от которого вызван метод, и переданного в параметрах)
     *
     * @param vect второе слагаемое
     * @return новый вектор
     */
    public Vector3D SumVectors(Vector3D vect) {
        return new Vector3D(x + vect.getX(), y + vect.getY(), z + vect.getZ());
    }

    /**
     * метод вычисляет разность двух векторов (вектора, от которого вызван метод, и переданного в параметрах)
     *
     * @param vect вычитаемое
     * @return новый вектор
     */
    public Vector3D SubVectors(Vector3D vect) {
        return new Vector3D(x - vect.getX(), y - vect.getY(), z - vect.getZ());
    }

    /**
     * метод генерирует массив векторов (значение одной координаты выбирается из диапазона [0, 100])
     *
     * @param N задает размер генерируемого массива
     * @return возвращает массив векторов
     */
    public static Vector3D[] VectorsGeneration(int N) {
        Vector3D[] VectArray = new Vector3D[N];
        for (int i = 0; i < N; i++)
            VectArray[i] = new Vector3D(Math.random() * 101, Math.random() * 101, Math.random() * 101);
        return VectArray;
    }
    /**
     * Метод выводит вектор в формате (x, y, z)
     */
    public void PrintVect() {
        System.out.print("(" + x + ", " + y + ", " + z + ")");
    }
}

