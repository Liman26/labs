package vereshchagina;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

        Scanner inputData = new Scanner(System.in);
        int N;
        double x, y, z;
        Vector3D first, second;
        Vector3D[] ArVect;

        System.out.println("Введите через пробел или Enter три координаты 1-го вектора :");
        x = inputData.nextDouble();
        y = inputData.nextDouble();
        z = inputData.nextDouble();
        first = new Vector3D(x, y, z);

        System.out.println("Введите через пробел или Enter три координаты 2-го вектора :");
        x = inputData.nextDouble();
        y = inputData.nextDouble();
        z = inputData.nextDouble();
        second = new Vector3D(x, y, z);

        System.out.println("длина 2-го вектора равна " + second.Length());

        System.out.print("скалярное произведение (");
        first.PrintVect();
        System.out.print(", ");
        second.PrintVect();
        System.out.println(") = " + first.ScalarProduct(second));

        System.out.print("векторное произведение [");
        first.PrintVect();
        System.out.print(", ");
        second.PrintVect();
        System.out.print("] = ");
        first.VectorProduct(second).PrintVect();

        System.out.println("\nугол между векторами 1 и 2 равен " + first.AngleBetweenVectors(second));

        System.out.print("сумма векторов ");
        first.PrintVect();
        System.out.print(" + ");
        second.PrintVect();
        System.out.print(" = ");
        first.SumVectors(second).PrintVect();

        System.out.print("\nразность векторов ");
        first.PrintVect();
        System.out.print(" - ");
        second.PrintVect();
        System.out.print(" = ");
        first.SubVectors(second).PrintVect();

        System.out.println("\nВведите размер массива векторов:");
        N = inputData.nextInt();

        ArVect = Vector3D.VectorsGeneration(N);

        System.out.println("массив векторов:");
        for (int i = 0; i < N; i++) {
            ArVect[i].PrintVect();
            System.out.println();
        }
    }
}