package nuridinov;

import static org.junit.jupiter.api.Assertions.*;



import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Класс для тестирование методов класса Utils
 */
public class UtilsTest {
    private Collection<UserSber> collA;
    private Collection<UserSber> collB;
    private long timeOfStart;

    /**
     * заполнение коллекций данными
     */
    @BeforeEach
    public void fillingInLists() {
        collA = new ArrayList<>();
        collB = new ArrayList<>();
        UserSber user;
        for (int i = 0; i < 99_999; i++) {
            user = new UserSber("Ivan" + i, i + ".mail.ru");
            collA.add(user);
            collB.add(user);
        }
        collA.add(new UserSber("Jack1", "mail.com"));
        collB.add(new UserSber("Jack2", "mail.com"));
        timeOfStart = System.currentTimeMillis();
    }

    /**
     * Тест для первого метода из класса Utils
     */
    @Test
    public void findDuplicatesTest1() {
        try {
            assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
                assertEquals(99_999, Utils.findDuplicates1(collA, collB).size());
                System.out.println("Время работы: " + (System.currentTimeMillis() - timeOfStart) + "мс");
            }, "Время работы метода, больше 0.2 секунд");
        } catch (AssertionFailedError error) {
            System.out.println("Время работы: " + (System.currentTimeMillis() - timeOfStart) + "мс");
        }
    }

    /**
     * Тест для второго метода из Utils
     */
    @Test
    public void findDuplicatesTest2() {
        try {
            assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
                assertEquals(99_999, Utils.findDuplicates2(collA, collB).size());
                System.out.println("Время работы: " + (System.currentTimeMillis() - timeOfStart) + "мс");
            }, "Время работы метода, больше 0.2 секунд");
        } catch (AssertionFailedError error) {
            System.out.println("Время работы: " + (System.currentTimeMillis() - timeOfStart) + "мс");
        }

    }

    /**
     * Тест для третьего метода из Utils
     */
    @Test
    public void findDuplicatesTest3() {
        try {
            assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
                assertEquals(99_999, Utils.findDuplicates3(collA, collB).size());
                System.out.println("Время работы: " + (System.currentTimeMillis() - timeOfStart) + "мс");
            }, "Время работы метода, больше 0.2 секунд");
        } catch (AssertionFailedError error) {
            System.out.println("Время работы: " + (System.currentTimeMillis() - timeOfStart) + "мс");
        }


    }
}
