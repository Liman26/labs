package troitskiy;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static java.time.Duration.ofMillis;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;



public class Task6Test
{
    private static final ArrayList<UserSber> collection1 = new ArrayList<>();
    private static final ArrayList<UserSber> collection2 = new ArrayList<>();

    /**
     * Подготовка тестовых коллекций
     * содержат 100 дубликатов
     */
    @BeforeAll
    private static void beforeAll()
    {
        for (int i = 0; i < 100000; i++)
        {
            collection1.add(new UserSber(Integer.toString(i),Integer.toString(i)));

            if (i % 1000 == 0) collection2.add(new UserSber(Integer.toString( i),Integer.toString( i)));
            else               collection2.add(new UserSber(Integer.toString(-i),Integer.toString(-i)));
        }
    }

    /**
     * Тест первого метода на время выполнения
     */
    @Test
    public void test1()
    {
        assertTimeoutPreemptively(ofMillis(200), () -> TUtil.findDuplicates(collection1, collection2));
    }

    /**
     * Тест первого метода на корректность работы
     */
    @Test
    public void test2()
    {
         assertEquals(100, TUtil.findDuplicates(collection1, collection2).size());

    }

    /**
     * Тест второго метода на время выполнения
     */
    @Test
    public void test3()
    {
        assertTimeoutPreemptively(ofMillis(200), () -> TUtil.findDuplicates2(collection1, collection2));
    }

    /**
     * Тест второго метода на корректность работы
     */
    @Test
    public void test4()
    {
        assertEquals(100, TUtil.findDuplicates2(collection1, collection2).size());
    }
}
