package bachishche;

import org.junit.jupiter.api.*;

import java.time.Duration;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс, проверяющий утилитные методы класса DuplicateFinder
 * на корректность и скорость работы
 */
public class DuplicateFinderTest {
    private Collection<UserSber> collA;
    private Collection<UserSber> collB;
    private List<UserSber> rightDuplicates;

    private double startTime;

    /**
     * Конструктор класса-тестроващика.
     * Выполняет инициализацию коллекций перед тестированием
     * и поиск дубликатов в массивах
     */
    public DuplicateFinderTest() {
        collA = new ArrayList<>();
        collB = new ArrayList<>();

        for (int i = 0; i < 100_000; i++) {
            UserSber user = new UserSber("Username" + (i % 1000), "username" + i + "@mail.com");
            collA.add(user);
            collB.add(user);
        }
        rightDuplicates = new ArrayList<>(collA);
        rightDuplicates.retainAll(collB);
    }

    /**
     * Проверка метода на скорость поиска дубликатов
     */
    @Test
    public void timeTest() {
        assertTimeoutPreemptively(Duration.ofMillis(200),
                () -> DuplicateFinder.findDuplicates3(collA, collB),
                "Время выполнения превышает 2 мс.");
    }

    /**
     * Проверка методов на корректный поиск дубликатов
     */
    @Test
    public void findDuplicatesTest() {
        List<UserSber> duplicates = DuplicateFinder.findDuplicates1(collA, collB);
        assertTrue(rightDuplicates.size() == duplicates.size() && rightDuplicates.containsAll(duplicates),
                "Не все дубликаты найдены");
        duplicates = DuplicateFinder.findDuplicates2(collA, collB);
        assertTrue(rightDuplicates.size() == duplicates.size() && rightDuplicates.containsAll(duplicates),
                "Не все дубликаты найдены");
        duplicates = DuplicateFinder.findDuplicates3(collA, collB);
        assertTrue(rightDuplicates.size() == duplicates.size() && rightDuplicates.containsAll(duplicates),
                "Не все дубликаты найдены");
        duplicates = DuplicateFinder.findDuplicates4(collA, collB);
        assertTrue(rightDuplicates.size() == duplicates.size() && rightDuplicates.containsAll(duplicates),
                "Не все дубликаты найдены");
    }

    /**
     * Проверка методов на срабатывние исключений
     */
    @Test
    void exceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> DuplicateFinder.findDuplicates1(collA, null));
        assertThrows(IllegalArgumentException.class, () -> DuplicateFinder.findDuplicates2(null, null));
        assertThrows(IllegalArgumentException.class, () -> DuplicateFinder.findDuplicates3(null, null));
        assertThrows(IllegalArgumentException.class, () -> DuplicateFinder.findDuplicates4(null, null));

    }
}