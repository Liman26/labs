package koleda;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

public class Task6Test {
    private static List<UserSber> list;
    private static List<UserSber> list2;
    private static List<UserSber> list3;

    @BeforeEach
    public void Lists() {
        list = new ArrayList<>();
        list2 = new ArrayList<>();
        list3 = new ArrayList<>();

        for (int i = 1; i < 100000; i++) {
            list.add(new UserSber(String.valueOf(i), String.valueOf(i)));
            list2.add(new UserSber(String.valueOf(-i), String.valueOf(-i)));
        }
        list.add(new UserSber(String.valueOf(100000), String.valueOf(100000)));
        list2.add(new UserSber(String.valueOf(100000), String.valueOf(100000)));
        list3.add(new UserSber(String.valueOf(100000), String.valueOf(100000)));
    }

    @Test
    public void test1() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            List<UserSber> duplicate;
            long start = System.currentTimeMillis();
            duplicate = Task6.findDuplicates1(list, list2);
            assertArrayEquals(list3.toArray(), duplicate.toArray());
            System.out.println("Время выполнения метода findDuplicates1:" + (System.currentTimeMillis() - start));
        }, () -> "Слишком долго");
    }

    @Test
    public void test2() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            List<UserSber> duplicate;
            long start = System.currentTimeMillis();
            duplicate = Task6.findDuplicates2(list, list2);
            assertArrayEquals(list3.toArray(), duplicate.toArray());
            System.out.println("Время выполнения метода findDuplicates2:" + (System.currentTimeMillis() - start));
        }, () -> "Слишком долго");

    }

    @Test
    public void test3() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            List<UserSber> duplicate;
            long start = System.currentTimeMillis();
            duplicate = Task6.findDuplicates3(list, list2);
            assertArrayEquals(list3.toArray(), duplicate.toArray());
            System.out.println("Время выполнения метода findDuplicates3:" + (System.currentTimeMillis() - start));
        }, () -> "Слишком долго");
    }

}
