package emelyanov;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static emelyanov.Task6.*;
import static java.time.Duration.ofMillis;
import static org.junit.jupiter.api.Assertions.*;

public class Task6Test {

    private static final ArrayList<UserSber> alCollection1 = new ArrayList<>();
    private static final ArrayList<UserSber> alCollection2 = new ArrayList<>();

    private long startTime;

    @BeforeAll
    static void beforeAll() {
        alCollection1.add(new UserSber("Name_1", "Email_1"));
        alCollection1.add(new UserSber("Name_1", "Email_1"));
        alCollection1.add(new UserSber("Name_2", "Email_2"));
        alCollection1.add(new UserSber("Name_3", "Email_3"));
        alCollection1.add(new UserSber("Name_4", "Email_4"));
        alCollection2.add(new UserSber("Name_1", "Email_1"));
        alCollection2.add(new UserSber("Name_20", "Email_20"));
        alCollection2.add(new UserSber("Name_20", "Email_20"));
        alCollection2.add(new UserSber("Name_30", "Email_30"));
        alCollection2.add(new UserSber("Name_4", "Email_4"));
    }

    @BeforeEach
    void beforeEach(){
        startTime = System.currentTimeMillis();
    }

    @Test
    @DisplayName("Проверка первой реализации")
    public void testFindDuplicates1() {
        List<UserSber> result = findDuplicates1(alCollection1, alCollection2);
        System.out.println("Time findDuplicates1: " + (System.currentTimeMillis() - startTime) + " Count: " + result.size());
        assertEquals(2, result.size());
        assertTrue(result.contains(new UserSber("Name_1", "Email_1")));
        assertTrue(result.contains(new UserSber("Name_4", "Email_4")));
        assertFalse(result.contains(new UserSber("Name_2", "Email_2")));
        assertFalse(result.contains(new UserSber("Name_20", "Email_20")));
    }

    @Test
    @DisplayName("Проверка второй реализации")
    public void testFindDuplicates2() {
        List<UserSber> result = findDuplicates2(alCollection1, alCollection2);
        System.out.println("Time findDuplicates2: " + (System.currentTimeMillis() - startTime) + " Count: " + result.size());
        assertEquals(2, result.size());
        assertTrue(result.contains(new UserSber("Name_1", "Email_1")));
        assertTrue(result.contains(new UserSber("Name_4", "Email_4")));
        assertFalse(result.contains(new UserSber("Name_2", "Email_2")));
        assertFalse(result.contains(new UserSber("Name_20", "Email_20")));
    }

    @Test()
    @DisplayName("Проверка третьей реализации")
    public void testFindDuplicates3() {
        List<UserSber> result = findDuplicates3(alCollection1, alCollection2);
        System.out.println("Time findDuplicates3: " + (System.currentTimeMillis() - startTime) + " Count: " + result.size());
        assertEquals(2, result.size());
        assertTrue(result.contains(new UserSber("Name_1", "Email_1")));
        assertTrue(result.contains(new UserSber("Name_4", "Email_4")));
        assertFalse(result.contains(new UserSber("Name_2", "Email_2")));
        assertFalse(result.contains(new UserSber("Name_20", "Email_20")));
    }
}
