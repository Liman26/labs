package emelyanov;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static emelyanov.Task6.*;
import static java.time.Duration.*;
import static org.junit.jupiter.api.Assertions.*;

public class Task6TestTime {

    private static final ArrayList<UserSber> alCollection1 = new ArrayList<>();
    private static final ArrayList<UserSber> alCollection2 = new ArrayList<>();

    private long startTime;

    @BeforeAll
    static void beforeAll() {
        for (int i = 0; i < 100_000; i++) {
            UserSber userSber= new UserSber("Name" + randomInteger(MAX_NAME), "Email" + randomInteger(MAX_NAME));
            alCollection1.add(userSber);
            alCollection2.add(userSber);
        }
    }

    @BeforeEach
    void beforeEach(){
        startTime = System.currentTimeMillis();
    }

    @Test
    @DisplayName("Проверка первой реализации (работает меньше 0.2 сек)")
    public void testFindDuplicates1() {
        assertTimeoutPreemptively(ofMillis(200), () -> {
            List<UserSber> result = findDuplicates1(alCollection1, alCollection2);
            System.out.println("Time findDuplicates1: " + (System.currentTimeMillis() - startTime) + " Count: " + result.size());
        });
    }

    @Test
    @DisplayName("Проверка второй реализации (работает меньше 0.2 сек)")
    public void testFindDuplicates2() {
        assertTimeoutPreemptively(ofMillis(200), () -> {
            List<UserSber> result = findDuplicates2(alCollection1, alCollection2);
            System.out.println("Time findDuplicates2: " + (System.currentTimeMillis() - startTime) + " Count: " + result.size());
        });
    }

    @Test()
    @DisplayName("Проверка третьей реализации (работает больше 0.2 сек)")
    public void testFindDuplicates3(){
        assertTimeoutPreemptively(ofMillis(200), () -> {
            List<UserSber> result = findDuplicates3(alCollection1, alCollection2);
            System.out.println("Time findDuplicates3: " + (System.currentTimeMillis() - startTime) + " Count: " + result.size());
        });
    }
}
