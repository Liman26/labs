package mescheryakov;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

/**
 * Класс, реализвующий тесты методов UtilityMethods
 */
public class Task6Test {
    List<UserSber> list;
    List<UserSber> list2;
    List<UserSber> list3;

    /**
     * Метод, создающий списки для сравнения
     */
    @BeforeEach
    public void Lists() {
        list = new ArrayList<>();
        list2 = new ArrayList<>();
        list3 = new ArrayList<>();

        for (int i = 0; i < 99999; i++) {
            list.add(new UserSber(String.valueOf(i), String.valueOf(i)));
            list2.add(new UserSber(String.valueOf(i + 100001), String.valueOf(i + 100001)));
        }
        list.add(new UserSber(String.valueOf(100000), String.valueOf(100000)));
        list2.add(new UserSber(String.valueOf(100000), String.valueOf(100000)));
        list3.add(new UserSber(String.valueOf(100000), String.valueOf(100000)));
    }


    /**
     * Тест метода findDuplicates. Проверяет корректность и время выполнения.
     * Этот метод является у меня самым быстрым, его выполнение занимает менее 0,2 секунд.
     */
    @Test
    public void test1() {
        List<UserSber> duplicate;
        long go = System.currentTimeMillis();
        duplicate = UtilityMethods.findDuplicates(list, list2);
        assertArrayEquals(list3.toArray(), duplicate.toArray());
        System.out.println("Время выполнения метода findDuplicates:" + (System.currentTimeMillis() - go));
    }

    /**
     * Тест метода findDuplicates2. Его работа ограничена временным интервалом,
     * в целях сокращения времени ожидания. Если работа метода занимает более
     * 0,2 секунд, то тест прекращает выполнение и появляется сообщение о превышении
     * временного интервала.
     */
    @Test
    public void test2() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            List<UserSber> duplicate;
            long go = System.currentTimeMillis();
            duplicate = UtilityMethods.findDuplicates2(list, list2);
            assertArrayEquals(list3.toArray(), duplicate.toArray());
            System.out.println("Время выполнения метода findDuplicates2:" + (System.currentTimeMillis() - go));
        }, () -> "Метод работает более 0,2 секунд");
    }

    /**
     * Тест метода findDuplicates3. Его работа ограничена временным интервалом,
     * в целях сокращения времени ожидания. Если работа метода занимает более
     * 0,2 секунд, то тест прекращает выполнение и появляется сообщение о превышении
     * временного интервала.
     */
    @Test
    public void test3() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            List<UserSber> duplicate;
            long go = System.currentTimeMillis();
            duplicate = UtilityMethods.findDuplicates3(list, list2);
            assertArrayEquals(list3.toArray(), duplicate.toArray());
            System.out.println("Время выполнения метода findDuplicates3:" + (System.currentTimeMillis() - go));
        }, () -> "Метод работает более 0,2 секунд");
    }
}
