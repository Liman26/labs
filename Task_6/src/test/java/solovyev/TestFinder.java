package solovyev;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс тестирующий методы класса Finder
 */
public class TestFinder {
    private Collection<UserSber> a;
    private Collection<UserSber> b;
    private long startTime;

    /**
     * Заполнение двух коллекций перед каждым тестов,
     * а так же запись времени запуска теста
     */
    @BeforeEach
    public void preparations() {
        a = new HashSet<>();
        b = new HashSet<>();

        for (int i = 0; i < 100_000; i++) {
            UserSber uS = new UserSber("Andrew" + i, "@mail.com");
            a.add(uS);
            b.add(uS);
        }
        startTime = System.currentTimeMillis();
    }

    /**
     * После каждого теста вывод его продолжительности
     */
    @AfterEach
    public void workTime() {
        System.out.println(System.currentTimeMillis() - startTime);
    }

    /**
     * Метод тестирующий метод TestFindDuplicates1.
     * Ожидается что метод найдет 100_000 дубликатов,
     * так как ему на сравнение переданы полностью идентичные коллекции.
     * В случае если тест будет длиться дольше 0.2 секунд,
     * он будет досрочно завершен
     */
    @Test
    public void TestFindDuplicates1() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            assertEquals(100_000, Finder.findDuplicates1(a, b).size());
        }, "Время теста было превышено.");
    }

    /**
     * Метод тестирующий метод TestFindDuplicates2.
     * Ожидается что метод найдет 100_000 дубликатов,
     * так как ему на сравнение переданы полностью идентичные коллекции.
     * В случае если тест будет длиться дольше 0.2 секунд,
     * он будет досрочно завершен
     */
    @Test
    public void TestFindDuplicates2() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            assertEquals(100_000, Finder.findDuplicates2(a, b).size());
        }, "Время теста было превышено.");
    }

    /**
     * Метод тестирующий метод TestFindDuplicates3.
     * Ожидается что метод найдет 100_000 дубликатов,
     * так как ему на сравнение переданы полностью идентичные коллекции.
     * В случае если тест будет длиться дольше 0.2 секунд,
     * он будет досрочно завершен
     */
    @Test
    public void TestFindDuplicates3() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            assertEquals(100_000, Finder.findDuplicates3(a, b).size());
        }, "Время теста было превышено.");
    }

    /**
     * Метод для проверки выбрасываемых исключений,
     * во всех методах поиска дубликатов
     * (После него так же выводится время работы теста)
     */
    @Test
    public void TestExceptionFindDuplicates() {
        Collection<UserSber> first = null;
        Collection<UserSber> second = null;
        assertThrows(NullPointerException.class, () -> Finder.findDuplicates1(first, second));
        assertThrows(NullPointerException.class, () -> Finder.findDuplicates2(first, second));
        assertThrows(NullPointerException.class, () -> Finder.findDuplicates3(first, second));
    }
}
