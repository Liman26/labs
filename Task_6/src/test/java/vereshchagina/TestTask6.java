package vereshchagina;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

public class TestTask6 {
    List<UserSber> list1;
    List<UserSber> list2;
    List<UserSber> result;

    /**
     * метод, создающий заготовку для тестов
     */
    @BeforeEach
    public void createLists() {
        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
        result = new ArrayList<>();

        for (int i = 0; i < 99999; i++) {
            list1.add(new UserSber("Test1" + String.valueOf(i), String.valueOf(i) + "@mail.ru"));
            list2.add(new UserSber("Test2" + String.valueOf(i * 2), String.valueOf(i * 2) + "@mail.ru"));
        }

        list1.add(new UserSber("duplicate", "duplicate"));
        list2.add(new UserSber("duplicate", "duplicate"));
        result.add(new UserSber("duplicate", "duplicate"));
    }


    /**
     * Тест метода findDuplicates (вариант 1).
     */
    @Test
    public void test1() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            long start = System.currentTimeMillis();
            assertArrayEquals(result.toArray(), Task6.findDuplicates1(list1, list2).toArray());
            System.out.println("Время выполнения (вариант 1):" + (System.currentTimeMillis() - start));
        }, () -> "больше 0,2 секунд");
    }

    /**
     * Тест метода findDuplicates (вариант 2).
     */
    @Test
    public void test2() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            long start = System.currentTimeMillis();
            assertArrayEquals(result.toArray(), Task6.findDuplicates2(list1, list2).toArray());
            System.out.println("Время выполнения (вариант 2):" + (System.currentTimeMillis() - start));
        }, () -> "больше 0,2 секунд");
    }

    /**
     * Тест метода findDuplicates (вариант 3).
     */
    @Test
    public void test3() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            long start = System.currentTimeMillis();
            assertArrayEquals(result.toArray(), Task6.findDuplicates3(list1, list2).toArray());
            System.out.println("Время выполнения (вариант 3):" + (System.currentTimeMillis() - start));
        }, () -> "больше 0,2 секунд");
    }

}
