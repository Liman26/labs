package turkov;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UserSberTest {
    private static final List<UserSber> userListA = new ArrayList<>();
    private static final List<UserSber> userListB = new ArrayList<>();
    private static final List<UserSber> userDuplicate = new ArrayList<>();
    private long startTime;

    /**
     * Метод, который инциализирует коллекции (userListA и userListB), выступающие в качестве параметров
     * утилилитных методов класса UserSber, и userDuplicate, которая используется для проверки корректности результата.
     * Во входящие коллекции намеренно добавляются данные, которые дублируют объекты.
     */
    @BeforeAll
    static void setUsersList() {
        for (int i = 0; i <= 99999; i++) {
            userListA.add(new UserSber("User" + i, i + "@sberbank.ru"));
            userListB.add(new UserSber("User" + (i + 99999), (i + 99999) + "@sberbank.ru"));
        }
        userListA.add(new UserSber("Sergey", "Sergey@sberbank.ru"));
        userListA.add(new UserSber("Sergey", "Sergey@sberbank.ru"));
        userListA.add(new UserSber("Olga", "Olga@sberbank.ru"));
        userListB.add(new UserSber("Sergey", "Sergey@sberbank.ru"));
        userListB.add(new UserSber("Olga", "Olga@sberbank.ru"));
        userListB.add(new UserSber("Olga", "Olga@sberbank.ru"));
        userDuplicate.add(new UserSber("User" + 99999, 99999 + "@sberbank.ru"));
        userDuplicate.add(new UserSber("Olga", "Olga@sberbank.ru"));
        userDuplicate.add(new UserSber("Sergey", "Sergey@sberbank.ru"));
    }

    /**
     * Метод инициализации текущего времени запуска для каждого теста.
     */
    @BeforeEach
    public void setStartTime() {
        startTime = System.currentTimeMillis();
    }

    /**
     * Проверка времени и корректности работы метода UserSber.findDuplicatesOne.
     */
    @Test
    public void findDuplicatesOneTest() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            List<UserSber> duplicate = UserSber.findDuplicatesOne(userListA, userListB);
            System.out.println("Productivity of method findDuplicatesOne " + (System.currentTimeMillis() - startTime) +
                    " ms. " + "Find " + duplicate.size() + " duplicates");
        }, () -> "Productivity > 200 ms ");
        assertTrue(userDuplicate.containsAll(UserSber.findDuplicatesOne(userListA, userListB)));
    }

    /**
     * Проверка времени и корректности работы метода UserSber.findDuplicatesTwo. Самый медленный из трёх (> 200 ms).
     */
    @Test
    public void findDuplicatesTwoTest() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            List<UserSber> duplicate = UserSber.findDuplicatesTwo(userListA, userListB);
            System.out.println("Productivity of method findDuplicatesTwo " + (System.currentTimeMillis() - startTime) +
                    " ms. " + "Find " + duplicate.size() + " duplicates");
        }, () -> "Productivity > 200 ms");
        assertEquals(userDuplicate, UserSber.findDuplicatesTwo(userListA, userListB));
    }

    /**
     * Проверка времени и корректности работы метода UserSber.findDuplicatesThree.
     */
    @Test
    public void findDuplicatesThreeTest() {
        assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            List<UserSber> duplicate = UserSber.findDuplicatesThree(userListA, userListB);
            System.out.println("Productivity of method findDuplicatesTwo " + (System.currentTimeMillis() - startTime) +
                    " ms. " + "Find " + duplicate.size() + " duplicates");
        }, () -> "Productivity > 200 ms");
        assertTrue(userDuplicate.containsAll(UserSber.findDuplicatesThree(userListA, userListB)));
    }
}