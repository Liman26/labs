package zurnachyan;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс для тестов 6-й лабораторной работы
 */
public class Task6Tests {
    List<UserSber> listA;
    List<UserSber> listB;
    List<UserSber> testduplicates;
    UserSber testdup = new UserSber("duplicate", "dup@mail.com");

    /**
     * Метод, выполняющийся перед каждым тестом, "конструтокр" подготавливающий данные для тестов
     */
    @BeforeEach
    public void construct(){
        listA = new ArrayList<>();
        listB = new ArrayList<>();
        testduplicates = new ArrayList<>();

        testduplicates.add(testdup);

        for(int i = 0; i < 99999; i++){
            listA.add(new UserSber(i + "a", i + "@mail.com"));
            listB.add(new UserSber(i + "b", i + "@gmail.com"));
        }

        listA.add(testdup);
        listB.add(testdup);
        listA.get(99999).setPasswordHash(testdup.getPasswordHash());
        listB.get(99999).setPasswordHash(testdup.getPasswordHash());
    }

    /**
     * Тест первого способа поиска дубликатов, методу дается 5 секунд на поиск, иначе выполнение прерывается,
     * выводится сообщение
     */
    @Test
    public void test1(){
        long startTime = System.currentTimeMillis();
        try{
            assertTimeoutPreemptively(Duration.ofMillis(5000), () -> {
                assertEquals(testduplicates, Utils.findDuplicates1(listA, listB));
                System.out.println("It takes " + (System.currentTimeMillis() - startTime) + "ms");
            });
        } catch (Error e) {
            System.out.println("Method works more than 5 seconds!");
        }
    }

    /**
     * Тест второго способа поиска дубликатов, методу дается 5 секунд на поиск, иначе выполнение прерывается,
     * выводится сообщение
     * (данный метод выполняется не дольше 0.2 секунды, по условию)
     */
    @Test
    public void test2(){
        long startTime = System.currentTimeMillis();
        try{
            assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
                assertEquals(testduplicates, Utils.findDuplicates2(listA, listB));
                System.out.println("It takes " + (System.currentTimeMillis() - startTime) + "ms");
            });
        } catch (Error e) {
            System.out.println("Method works more than 0.2 seconds!");
        }
    }

    /**
     * Тест первого способа поиска дубликатов, методу дается 5 секунд на поиск, иначе выполнение прерывается,
     * выводится сообщение
     */
    @Test
    public void test3(){
        long startTime = System.currentTimeMillis();
        try{
            assertTimeoutPreemptively(Duration.ofMillis(5000), () -> {
                assertEquals(testduplicates, Utils.findDuplicates3(listA, listB));
                System.out.println("It takes " + (System.currentTimeMillis() - startTime) + "ms");
            });
        } catch (Error e) {
            System.out.println("Method works more than 5 seconds!");
        }
    }
}
