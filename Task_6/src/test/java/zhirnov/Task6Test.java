package zhirnov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Task6Test {
    List<UserSber> list1;
    List<UserSber> list2;
    List<UserSber> expect;

    /**
     * Метод, инициализирующий два списка
     */
    @BeforeEach

    public void initLists() {
        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
        expect = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 99_998; i++) {
            list1.add(new UserSber(String.valueOf(i), String.valueOf(i)));
            list2.add(new UserSber(String.valueOf(i + 100_001), String.valueOf(i + 100_001)));
        }
        list1.add(new UserSber(String.valueOf(99_999), String.valueOf(99_999)));
        list2.add(new UserSber(String.valueOf(99_999), String.valueOf(99_999)));
        list2.add(new UserSber(String.valueOf(100_000), String.valueOf(100_000)));
        list1.add(new UserSber(String.valueOf(100_000), String.valueOf(100_000)));
        expect.add(new UserSber(String.valueOf(99_999), String.valueOf(99_999)));
        expect.add(new UserSber(String.valueOf(100_000), String.valueOf(100_000)));
    }

    /**
     * Тест первого метода поиска дубликатов класса CollectionsUtilClass
     */
    @Test
    public void test1() {
        Assertions.assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            List<UserSber> duplicates;
            long start = System.currentTimeMillis();
            duplicates = CollectionsUtilClass.findDuplicates1(list1, list2);
            System.out.println("Test1 time :" + (System.currentTimeMillis() - start) + " ms");
            Assertions.assertArrayEquals(expect.toArray(), duplicates.toArray());
        }, () -> "Время выполнение превышает 2 секунды!");
    }

    /**
     * Тест второго метода поиска дубликатов класса CollectionsUtilClass
     */
    @Test

    public void test2() {
        Assertions.assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            List<UserSber> duplicates;
            long start = System.currentTimeMillis();
            duplicates = CollectionsUtilClass.findDuplicates2(list1, list2);
            System.out.println("Test2 time :" + (System.currentTimeMillis() - start) + " ms");
            Assertions.assertArrayEquals(expect.toArray(), duplicates.toArray());
        }, () -> "Время выполнение превышает 2 секунды!");
    }

    /**
     * Тест третьего метода поиска дубликатов класса CollectionsUtilClass
     */
    @Test
    public void test3() {
        Assertions.assertTimeoutPreemptively(Duration.ofMillis(200), () -> {
            List<UserSber> duplicates;
            long start = System.currentTimeMillis();
            duplicates = CollectionsUtilClass.findDuplicates1(list1, list2);
            System.out.println("Test3 time :" + (System.currentTimeMillis() - start) + " ms");
            Assertions.assertArrayEquals(expect.toArray(), duplicates.toArray());
        }, () -> "Время выполнение превышает 2 секунды!");
    }
}
