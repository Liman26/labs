package gushchin;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.IntStream;

import static java.time.Duration.ofMillis;
import static org.junit.jupiter.api.Assertions.*;

public class Task6Test {
    private final Collection<UserSber> collA = new ArrayList<>();
    private final Collection<UserSber> collB = new ArrayList<>();

    /**
     * Инициализация данных перед выполнением тестов
     * Инициализирует коллекцию collA 100000 элементов
     * Инициализирует коллекцию collB каждым элементом, кратным 1000 в пределах от 0 до 100000
     */
    @BeforeEach
    public void init() {
        IntStream.rangeClosed(0, 99_999).forEach(i -> {
            collA.add(new UserSber("Name" + i, i + "com"));
            if (i % 1000 == 0) {
                collB.add(new UserSber("Name" + i, i + "com"));
            } else {
                collB.add(new UserSber("Name" + -i, -i + "com"));
            }
        });
    }

    /**
     * Тест первого метода поиска дубликатов на время выполнения
     */
    @Test
    public void findDuplicatesTimeTest() {
        assertTimeoutPreemptively(ofMillis(200), () -> Task6.findDuplicates(collA, collB));
    }

    /**
     * Тест первого метода поиска дубликатов на корректность выполнения
     */
    @Test
    public void findDuplicatesCorrectnessTest() {
        assertEquals(100, Task6.findDuplicates2(collA, collB).size());
    }

    /**
     * Тест второго метода поиска дубликатов на время выполнения
     */
    @Test
    public void findDuplicates2TimeTest() {
        assertTimeoutPreemptively(ofMillis(200), () -> Task6.findDuplicates2(collA, collB));
    }

    /**
     * Тест второго метода поиска дубликатов на корректность выполнения
     */
    @Test
    public void findDuplicates2CorrectnessTest() {
        assertEquals(100, Task6.findDuplicates2(collA, collB).size());
    }
}
