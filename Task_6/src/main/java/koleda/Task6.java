package koleda;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс, содержащий утилитные методы, для поиска дубликатов в коллекциях
 * Одинаковые юзеры - у которых совпадают все 3 поля: username, email, passwordHash
 */
public class Task6 {

    /**
     * Утилитный метод, который возвращает дубликаты: юзеров, которые есть в обеих коллекциях.
     * Одинаковыми считаем юзеров, у которых совпадают все 3 поля: username, email, passwordHash.
     * Используем метод retainAll, удаляющий элементы, которые не содержатся в указанной коллекции
     * @param collA коллекция A
     * @param collB коллекция B
     * @return Список дубликатов юзеров
     */
    public static List<UserSber> findDuplicates1 (Collection<UserSber> collA, Collection<UserSber> collB) {
        List<UserSber> collC = new ArrayList<>(collA);
        collC.retainAll(collB);
        return collC;
    }

    /**
     * Утилитный метод, который возвращает дубликаты: юзеров, которые есть в обеих коллекциях.
     * Одинаковыми считаем юзеров, у которых совпадают все 3 поля: username, email, passwordHash.
     * Проверяем каждого юзера из первой коллекции на вхождение во второй коллекции.
     * @param collA коллекция A
     * @param collB коллекция B
     * @return Список дубликатов юзеров
     */
    public static List<UserSber> findDuplicates2 (Collection<UserSber> collA, Collection<UserSber> collB) {
        Set<UserSber> set = new HashSet<>();
        Set<UserSber> setA = new HashSet<>(collA);
        Set<UserSber> setB = new HashSet<>(collB);
        for (UserSber user: setA){
            if (setB.contains(user)){
                set.add(user);
            }
        }
        return new ArrayList<>(set);
    }

    /**
     * Утилитный метод, который возвращает дубликаты: юзеров, которые есть в обеих коллекциях.
     * Одинаковыми считаем юзеров, у которых совпадают все 3 поля: username, email, passwordHash.
     * Используем потоковый фильтр.
     * @param collA коллекция A
     * @param collB коллекция B
     * @return Список дубликатов юзеров
     */
    public static List<UserSber> findDuplicates3 (Collection<UserSber> collA, Collection<UserSber> collB) {
        return collA.stream().filter(collB::contains).collect(Collectors.toList());
    }


}
