package gushchin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Класс, описывающий утилиты для поиска дубликатов в двух коллекциях типа UserSber
 * Дубликатами считаются объекты, у которых все три поля (userName, email, passwordHash) совпадают
 */
public class Task6 {
    /**
     * Первый способ поиска дубликатов, основанный на использовании класса List
     * @param collA - первая коллекция
     * @param collB - вторая коллекция
     * @return - Коллекция дубликатов типа UserSber
     */
    public static List<UserSber> findDuplicates(Collection<UserSber> collA, Collection<UserSber> collB) {
        checkCollectionsForNull(collA, collB);
        List<UserSber> result = new ArrayList<>(collA);
        result.retainAll(collB);

        return result;
    }

    /**
     * Второй способ поиска дубликатов, основанный на использованнии HashSet
     * @param collA - первая коллекция
     * @param collB - вторая коллекция
     * @return - Коллекция дубликатов типа UserSber
     */
    public static List<UserSber> findDuplicates2(Collection<UserSber> collA, Collection<UserSber> collB) {
        checkCollectionsForNull(collA, collB);
        HashSet<UserSber> mapA = new HashSet<>(collA);
        HashSet<UserSber> mapB = new HashSet<>(collB);
        mapA.retainAll(mapB);

        return new ArrayList<>(mapA);
    }

    /**
     * Метод для сравнивания двух коллекций с нулом
     * В случае если хотя бы одна из коллекций является null будет выброшено исключение NullPointerException
     * @param collA - первая коллекция
     * @param collB - вторая коллекция
     */
    private static void checkCollectionsForNull(Collection<UserSber> collA, Collection<UserSber> collB) {
        if (collA == null || collB == null)
            throw new NullPointerException();
    }
}
