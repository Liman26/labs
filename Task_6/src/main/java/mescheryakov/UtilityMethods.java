package mescheryakov;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс, содержащий утилитные методы класса UserSber
 */
public class UtilityMethods {

    /**
     * Метод, возвращающий дубликаты между двумя коллекциями с помощью HashSet
     * @param collA первая коллекция
     * @param collB вторая коллекция
     * @return возвращает дубликаты
     */
    public static List<UserSber> findDuplicates(Collection<UserSber> collA, Collection<UserSber> collB) {
        if (collA == null || collB == null)
            throw new NullPointerException();

        List<UserSber> duplicate = new ArrayList<>();
        Set<UserSber> unique = new HashSet<>(collA);
        for(UserSber n : collB) {
            if(!unique.add(n)) {
                duplicate.add(n);
            }
        }
        return duplicate;
    }

    /**
     * Метод, возвращающий дубликаты между двумя коллекциями с помощью equals
     * @param collA первая коллекция
     * @param collB вторая коллекция
     * @return возвращает дубликаты
     */
    public static List<UserSber> findDuplicates2(Collection<UserSber> collA, Collection<UserSber> collB) {
        if (collA == null || collB == null)
            throw new NullPointerException();

        List<UserSber> duplicate = new ArrayList<>();
        for (UserSber n1 : collA)
            for (UserSber n2 : collB)
                if (n1.equals(n2)) {
                    duplicate.add(n1);
                }
        return duplicate;
    }

    /**
     * Метод, возвращающий дубликаты между двумя коллекциями с помощью параллельного потока
     * @param collA первая коллекция
     * @param collB вторая коллекция
     * @return возвращает дубликаты
     */
    public static List<UserSber> findDuplicates3(Collection<UserSber> collA, Collection<UserSber> collB) {
        if (collA == null || collB == null)
            throw new NullPointerException();

        return collA.parallelStream().filter(collB::contains).distinct().collect(Collectors.toList());
    }
}
