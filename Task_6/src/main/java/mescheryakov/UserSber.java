package mescheryakov;

import java.util.Arrays;
import java.util.Objects;

/**
 * Класс, реализующий хранение информации о пользователях
 */
public class UserSber {
    private String username;
    private String email;
    private byte[] passwordHash;

    /**
     * Констркутор, содержащий такие данные как: имя пользователя,
     * емэйл и пароль.
     * @param username имя пользователя
     * @param email емэйл
     */
    public UserSber(String username, String email) {
        this.username = username;
        this.email = email;
        this.passwordHash = username.getBytes();
    }

    /**
     * Метод, возвращающий имя пользователя
     * @return возвращает имя пользователя
     */
    public String getUsername() {
        return username;
    }

    /**
     * Метод, устанавливающий значение имени пользователя
     * @param username имя пользователя
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Метод, возвращающий емэйл
     * @return возвращает емэйл
     */
    public String getEmail() {
        return email;
    }

    /**
     * Метод, устанавливающий значение емэйла
     * @param email емэйл
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Метод, возвращающий пароль
     * @return возвращает пароль
     */
    public byte[] getPasswordHash() {
        return passwordHash;
    }

    /**
     * Метод, устанавливающий значение пароля
     * @param passwordHash пароль
     */
    public void setPasswordHash(byte[] passwordHash) {
        this.passwordHash = passwordHash;
    }

    /**
     * Переопределенный метод equals
     * @param o объект, принятый для сравнения
     * @return возвращает true или false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSber userSber = (UserSber) o;
        return Objects.equals(username, userSber.username) && Objects.equals(email, userSber.email) && Arrays.equals(passwordHash, userSber.passwordHash);
    }

    /**
     * Переопределенный метод hashCode
     * @return возвращает hashCode
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(username, email);
        result = 31 * result + Arrays.hashCode(passwordHash);
        return result;
    }
}