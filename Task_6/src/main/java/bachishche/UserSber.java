package bachishche;

import java.util.Arrays;
import java.util.Objects;

/**
 * Исходный класс, описывающий пользователя Сбера
 */
public class UserSber {
    private String username;
    private String email;
    private byte[] passwordHash;

    /**
     * Конструктор пользователя Сбера
     *
     * @param username имя пользователя
     * @param email электронная почта пользователя
     */
    public UserSber(String username, String email) {
        this.username = username;
        this.email = email;
        this.passwordHash = username.getBytes();
    }

    /**
     * Метод, возвращающий имя пользователя
     *
     * @return имя пользователя
     */
    public String getUsername() {
        return username;
    }

    /**
     * Метод, изменяющий имя пользователя
     *
     * @param username новое имя пользователя
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Метод, возвращающий электронную почту пользователя
     *
     * @return электронная почта пользователя
     */
    public String getEmail() {
        return email;
    }

    /**
     * Метод, изменяющий электронную почту пользователя
     *
     * @param email новый адрес электронной почты
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Метод, возвращающий хэш-код пароля пользователя
     *
     * @return хэш-код пароля
     */
    public byte[] getPasswordHash() {
        return passwordHash;
    }

    /**
     * Метод, изменяющий хэш-код пароля пользователя
     *
     * @param passwordHash новый хэш-код
     */
    public void setPasswordHash(byte[] passwordHash) {
        this.passwordHash = passwordHash;
    }

    /**
     * Метод сравнения пользователей.
     * Одинаковыми считаем юзеров, у которых совпадают все 3 поля:
     * username, email, passwordHash.
     *
     * @param o объект, сравниваемый с текущим
     * @return true - объекты равны,  false - в ином случае
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSber userSber = (UserSber) o;
        return Objects.equals(username, userSber.username) &&
                Objects.equals(email, userSber.email) &&
                Arrays.equals(passwordHash, userSber.passwordHash);
    }

    /**
     * Метод, вычисляющий хэш-код объекта
     *
     * @return хэш-код объекта
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(username, email);
        result = 31 * result + Arrays.hashCode(passwordHash);
        return result;
    }
}