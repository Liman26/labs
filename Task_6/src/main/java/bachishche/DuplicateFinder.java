package bachishche;

import java.util.*;

/**
 * Класс, содержащий утилитные методы для поиска дубликатов
 * в коллекциях, содержащих объекты UserSber
 */
public class DuplicateFinder {

    /**
     * Метод поиска дубликатов объектов UserSber, которые содержатся
     * в обоих переданных коллекциях. Работа метода основана на коллекции
     * ArrayList.
     *
     * @param collA коллекция объектов UserSber
     * @param collB коллекция объектов UserSber
     * @return список найденных дубликатов
     */
    public static List<UserSber> findDuplicates1(Collection<UserSber> collA, Collection<UserSber> collB) {
        if (collA == null || collB == null)
            throw new IllegalArgumentException("Передана неинициализированная коллекция");
        ArrayList<UserSber> arrayA = new ArrayList<>(collA);
        ArrayList<UserSber> arrayB = new ArrayList<>(collB);
        List<UserSber> duplicate = new ArrayList<>();
        for (UserSber user : arrayA)
            if (arrayB.contains(user))
                duplicate.add(user);
        return duplicate;
    }

    /**
     * Метод поиска дубликатов объектов UserSber, которые содержатся
     * в обоих переданных коллекциях. Работа метода основана на коллекции
     * ArrayList и методе retainAll.
     *
     * @param collA коллекция объектов UserSber
     * @param collB коллекция объектов UserSber
     * @return список найденных дубликатов
     */
    public static List<UserSber> findDuplicates2(Collection<UserSber> collA, Collection<UserSber> collB) {
        if (collA == null || collB == null)
            throw new IllegalArgumentException("Передана неинициализированная коллекция");
        ArrayList<UserSber> arrayA = new ArrayList<>(collA);
        ArrayList<UserSber> arrayB = new ArrayList<>(collB);
        List<UserSber> duplicate = new ArrayList<>(arrayA);
        duplicate.retainAll(arrayB);
        return duplicate;
    }

    /**
     * Метод поиска дубликатов объектов UserSber, которые содержатся
     * в обоих переданных коллекциях.
     * Каждый элемент первой коллекции ищется во второй коллекции,
     * для ускорения этого процесса вторая коллекция приводится к типу HashSet.
     * Полученные дубликаты добавляются в LinkedList
     *
     * @param collA коллекция объектов UserSber
     * @param collB коллекция объектов UserSber
     * @return список найденных дубликатов
     */
    public static List<UserSber> findDuplicates3(Collection<UserSber> collA, Collection<UserSber> collB) {
        if (collA == null || collB == null)
            throw new IllegalArgumentException("Передана неинициализированная коллекция");
        HashSet<UserSber> arrayB = new HashSet<>(collB);
        LinkedList<UserSber> duplicates = new LinkedList<>();
        for (UserSber user : collA)
            if (arrayB.contains(user))
                duplicates.add(user);
        return duplicates;
    }

    /**
     * Метод поиска дубликатов объектов UserSber, которые содержатся
     * в обоих переданных коллекциях. Метод основан на стандартном
     * методе retainAll, минимизировано преобразование разных коллекций друг к другу.
     *
     * @param collA коллекция объектов UserSber
     * @param collB коллекция объектов UserSber
     * @return список найденных дубликатов
     */
    public static List<UserSber> findDuplicates4(Collection<UserSber> collA, Collection<UserSber> collB) {
        if (collA == null || collB == null)
            throw new IllegalArgumentException("Передана неинициализированная коллекция");
        collA.retainAll(collB);
        return (List<UserSber>) collA;
    }

}
