package emelyanov;

import java.util.*;

public class Task6 {
    public static int MAX_NAME = 10000;

    public static void main(String[] args) {
        ArrayList<UserSber> alCollection1 = new ArrayList<>();
        ArrayList<UserSber> alCollection2 = new ArrayList<>();
        List<UserSber> result;
        for (int i = 0; i < 10_000; i++) {
            alCollection1.add(new UserSber("Name" + randomInteger(MAX_NAME), "Email" + randomInteger(MAX_NAME)));
            alCollection2.add(new UserSber("Name" + randomInteger(MAX_NAME), "Email" + randomInteger(MAX_NAME)));
        }
        long start = System.currentTimeMillis();
        result = findDuplicates1(alCollection1, alCollection2);
        System.out.println("Time findDuplicates1: " + (System.currentTimeMillis() - start) + " Count: " + result.size());
        start = System.currentTimeMillis();
        result = findDuplicates2(alCollection1, alCollection2);
        System.out.println("Time findDuplicates2: " + (System.currentTimeMillis() - start) + " Count: " + result.size());
        long start2 = System.currentTimeMillis();
        result = findDuplicates3(alCollection1, alCollection2);
        System.out.println("Time findDuplicates3: " + (System.currentTimeMillis() - start) + " Count: " + result.size());
    }

    /**
     * Генератор случайных чиссел
     *
     * @return случайное число типа int от 0 до maxNumber
     */
    public static int randomInteger(int maxNumber) {
        return (int) Math.round(Math.random() * maxNumber);
    }

    /**
     * Получить список элементов класса UserSber входящих в обе коллекции
     *
     * @param collA коллекция элементов класса UserSber
     * @param collB коллекция элементов класса UserSber
     * @return список элементов класса UserSber входящих в обе коллекции
     */
    public static List<UserSber> findDuplicates1(Collection<UserSber> collA, Collection<UserSber> collB) {
        Collection<UserSber> result = new HashSet<>();
        Set<UserSber> setA = new HashSet<>(collA);
        Set<UserSber> setB = new HashSet<>(collB);
        for (UserSber itemA : setA) {
            if (setB.contains(itemA)) {
                result.add(itemA);
            }
        }
        return new ArrayList<>(result);
    }

    /**
     * Получить список элементов класса UserSber входящих в обе коллекции
     *
     * @param collA коллекция элементов класса UserSber
     * @param collB коллекция элементов класса UserSber
     * @return список элементов класса UserSber входящих в обе коллекции
     */
    public static List<UserSber> findDuplicates2(Collection<UserSber> collA, Collection<UserSber> collB) {
        Set<UserSber> setA = new HashSet<>(collA);
        Set<UserSber> setB = new HashSet<>(collB);
        setA.retainAll(setB);
        return new ArrayList<>(setA);
    }

    /**
     * Получить список элементов класса UserSber входящих в обе коллекции
     *
     * @param collA коллекция элементов класса UserSber
     * @param collB коллекция элементов класса UserSber
     * @return список элементов класса UserSber входящих в обе коллекции
     */
    public static List<UserSber> findDuplicates3(Collection<UserSber> collA, Collection<UserSber> collB) {
        List<UserSber> result = new ArrayList<>();
        for (UserSber itemA : collA) {
            if (collB.contains(itemA)) {
                if (!result.contains(itemA)) {
                    result.add(itemA);
                }
            }
        }
        return result;
    }
}
