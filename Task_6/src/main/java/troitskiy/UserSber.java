package troitskiy;

import java.util.Arrays;
import java.util.Objects;

/**
 * Дан класс UserSber, модифицировать его поля не можем. Нужно написать утилитный метод
 * public static List< UserSber > findDuplicates(Collection< UserSber > collA, Collection< UserSber > collB);
 * который возвращает дубликаты: юзеров, которые есть в обеих коллекциях.
 * Одинаковыми считаем юзеров, у которых совпадают все 3 поля: username, email, passwordHash.
 * Требования производительности: метод findDuplicates должен работать не больше 0.2 сек. если на вход
 * получает 2 коллекции по 100 тысяч элементов в каждой.
 * Рассмотреть несколько вариантов решения задачи, замерить время выполнения, вывести
 * результат на экран.
 * Один из вариантов должен удовлетворять требованиям по
 * производительности
 */
public class UserSber
{
    private String username;
    private String email;
    private byte[] passwordHash;

    public UserSber(String username, String email)
    {
        this.username = username;
        this.email = email;
        this.passwordHash = username.getBytes();
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public byte[] getPasswordHash()
    {
        return passwordHash;
    }

    public void setPasswordHash(byte[] passwordHash)
    {
        this.passwordHash = passwordHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSber userSber = (UserSber) o;
        return Objects.equals(username, userSber.username) && Objects.equals(email, userSber.email) && Arrays.equals(passwordHash, userSber.passwordHash);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(username, email);
        result = 31 * result + Arrays.hashCode(passwordHash);
        return result;
    }

}
