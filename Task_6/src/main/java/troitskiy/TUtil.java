package troitskiy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;


/**
 *  Класс содержащий утилитные методы вида
 *  public static List< UserSber > findDuplicates(Collection< UserSber > collA, Collection< UserSber > collB);
 *  которые возвращают дубликаты: юзеров, которые есть в обеих коллекциях.
 *  Одинаковыми считаем юзеров, у которых совпадают все 3 поля: username, email, passwordHash.
 */
public class TUtil
{

    /**
     * Утилитный метод который возвращает дубликаты: юзеров, которые есть в обеих коллекциях.
     * Одинаковыми считаем юзеров, у которых совпадают все 3 поля: username, email, passwordHash.
     * @param collA - первая коллекция
     * @param collB -вторая коллекция
     * @return -список юзеров, которые есть в обеих коллекциях
     */
    public static List<UserSber> findDuplicates(Collection<UserSber> collA, Collection<UserSber> collB)
    {
         List<UserSber> collC = new ArrayList<>(collA);
         collC.retainAll(collB);
         return collC;
    }


    /**
     * Утилитный метод который возвращает дубликаты: юзеров, которые есть в обеих коллекциях.
     * Одинаковыми считаем юзеров, у которых совпадают все 3 поля: username, email, passwordHash.
     * @param collA - первая коллекция
     * @param collB -вторая коллекция
     * @return -список юзеров, которые есть в обеих коллекциях
     */
    public static List<UserSber> findDuplicates2(Collection<UserSber> collA, Collection<UserSber> collB)
    {
        HashSet<UserSber> mapA = new HashSet<>(collA);
        HashSet<UserSber> mapB = new HashSet<>(collB);
        mapA.retainAll(mapB);

        return new ArrayList<>(mapA);
    }

}
