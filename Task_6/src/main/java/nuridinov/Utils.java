package nuridinov;

import java.util.*;

public class Utils {
    /**
     * Метод для поиска дубликатов, используется метод HashSet - .add(), который возвращет false
     * в случае, если элемент уже содержится в колллекции
     *
     * @param collA - первая коллекция
     * @param collB - вторая коллекция
     * @return список дубликатов
     */
    public static List<UserSber> findDuplicates1(Collection<UserSber> collA, Collection<UserSber> collB) {
        List<UserSber> duplicates = new ArrayList<>();
        Set<UserSber> set = new HashSet<>(collA);
        for (UserSber user : collB) {
            if (set.add(user)) {
                continue;
            }
            duplicates.add(user);
        }
        return duplicates;
    }

    /**
     * Метод для поиска дубликатов с помощью метода коллекций .retainAll(), который удаляет элементы не
     * принадлежащие переданной в метод коллекции
     *
     * @param collA - первая коллекция
     * @param collB - вторая коллекция
     * @return список дубликатов
     */
    public static List<UserSber> findDuplicates2(Collection<UserSber> collA, Collection<UserSber> collB) {
        Set<UserSber> duplicates = new HashSet<>(collA);
        duplicates.retainAll(new HashSet<>(collB));
        return new ArrayList<>(duplicates);
    }

    /**
     * Метод для поиска дубликатов, с помощью метода коллекций .contains(), который возвращает true
     * если элемент содержится в коллекции
     *
     * @param collA - первая коллекция
     * @param collB - вторая коллекция
     * @return список дубликатов
     */
    public static List<UserSber> findDuplicates3(Collection<UserSber> collA, Collection<UserSber> collB) {
        List<UserSber> duplicates = new ArrayList<>();
        Set<UserSber> set1 = new HashSet<>(collA);
        Set<UserSber> set2 = new HashSet<>(collB);
        for (UserSber user : set1) {
            if (set2.contains(user)) {
                duplicates.add(user);
            }
        }
        return duplicates;
    }
}
