package nuridinov;

import java.util.Arrays;
import java.util.Objects;

/**
 * Класс описывающий пользователя сбера
 */
public class UserSber {
    private String username;
    private String email;
    private byte[] passwordHash;

    /**
     * конструктор класса
     *
     * @param username - имя пользователя
     * @param email    - почта пользователя
     */
    public UserSber(String username, String email) {
        this.username = username;
        this.email = email;
        this.passwordHash = username.getBytes();
    }

    /**
     * геттер для получения имени пользователя
     *
     * @return - имя пользователя
     */
    public String getUsername() {
        return username;
    }

    /**
     * сеттер для назначение имени пользователя
     *
     * @param username - имя пользователя
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * геттер для получение почты пользователя
     *
     * @return - почта пользователя
     */
    public String getEmail() {
        return email;
    }

    /**
     * сеттер для назначения почты пользователя
     *
     * @param email - почта пользователя
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * геттер для получения хеш-пароля пользователя
     *
     * @return хеш пароля
     */
    public byte[] getPasswordHash() {
        return passwordHash;
    }

    /**
     * сеттер для назначения хеш пароля пользователя
     *
     * @param passwordHash - хеш пароля
     */
    public void setPasswordHash(byte[] passwordHash) {
        this.passwordHash = passwordHash;
    }

    /**
     * Переопределенный метод equals, для сравнения экземпляров класса UserSber
     *
     * @param o сравниваемый объект
     * @return true, если имя, почта и хеш пароля одинаковы, иначе false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || o.getClass() != this.getClass()) return false;
        UserSber userSber = (UserSber) o;
        return username.equals(userSber.username) && email.equals(userSber.email) && Arrays.equals(passwordHash, userSber.passwordHash);
    }


    /**
     * переопределенный метод hashCode, для получение хешкода экземпляров класса UserSber
     *
     * @return хешкод
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(username, email);
        result = 31 * result + Arrays.hashCode(passwordHash);
        return result;
    }
}
