package zhirnov;

import java.util.*;

/**
 * Класс утильных метод для коллекций
 */
public class CollectionsUtilClass {
    /**
     * Метод, который ищет одинаковые элементы в двух коллекциях, сравнивая поэлементно
     * @param collA - первая коллекция
     * @param collB - вторая коллекция
     * @return arraylist со всеми элементами, которые находились как в первой коллекции, так и во второй
     */
    public static List<UserSber> findDuplicates1(Collection<UserSber> collA, Collection<UserSber> collB){
        List<UserSber> duplicatesList = new ArrayList<>();
        for (UserSber userSber1 : collA){
            for (UserSber userSber2 : collB){
                if (userSber1.equals(userSber2)){
                    duplicatesList.add(userSber1);
                }
            }
        }
        return duplicatesList;
    }

    /**
     * Метод, который ищет одинаковые элементы в двух коллекциях, используя такое свойсво hashset, как отсутствие дубликатов.
     * @param collA - первая коллекция
     * @param collB - вторая коллекция
     * @return arraylist со всеми элементами, которые находились как в первой коллекции, так и во второй
     */
    public static List<UserSber> findDuplicates2(Collection<UserSber> collA, Collection<UserSber> collB){
        List<UserSber> duplicatesList = new ArrayList<>();
        HashSet<UserSber> hashSet = new HashSet<>(collA);

        for (UserSber userSber : collB){
            if (!hashSet.add(userSber)){
                duplicatesList.add(userSber);
            }
        }

        return duplicatesList;
    }

    /**
     * Метод, который ищет одинаковые элементы в двух коллекциях, используя метод retainAll
     * @param collA - первая коллекция
     * @param collB - вторая коллекция
     * @return arraylist со всеми элементами, которые находились как в первой коллекции, так и во второй
     */
    public static List<UserSber> findDuplicates3(Collection<UserSber> collA, Collection<UserSber> collB){
        List<UserSber> duplicatesList = new ArrayList<>(collA);
        duplicatesList.retainAll(collB);
        return duplicatesList;
    }
}
