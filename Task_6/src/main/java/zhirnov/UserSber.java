package zhirnov;

import java.util.Arrays;
import java.util.Objects;

/**
 * Класс UserSber
 */
public class UserSber {
    private String username;
    private String email;
    private byte[] passwordHash;

    public UserSber(String username, String email) {
        this.username = username;
        this.email = email;
        this.passwordHash = username.getBytes();
    }

    /**
     * Метод, возвращающий username объекта класса UserSber
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Метод, меняющий значение поля username объекта класса UserSber
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Метод, возвращающий email объекта класса UserSber
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Метод, меняющий значение поля email объекта класса UserSber
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Метод, возвращающий email объекта класса UserSber
     * @return passwordHash
     */
    public byte[] getPasswordHash() {
        return passwordHash;
    }

    /**
     * Метод, меняющий значение поля passwordHash объекта класса UserSber
     */
    public void setPasswordHash(byte[] passwordHash) {
        this.passwordHash = passwordHash;
    }

    /**
     * Переопределенный метод equal
     * @param o - объект, с которым происходит сравнивание
     * @return возвращает true, если переменные эквивалентны, иначе false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSber userSber = (UserSber) o;
        return Objects.equals(username, userSber.username) && Objects.equals(email, userSber.email) && Arrays.equals(passwordHash, userSber.passwordHash);
    }

    /**
     * Переопределенный метод hashCode
     * @return хэш-код
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(username, email);
        result = 31 * result + Arrays.hashCode(passwordHash);
        return result;
    }
}
