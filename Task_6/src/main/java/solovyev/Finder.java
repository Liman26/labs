package solovyev;

import java.util.*;

/**
 * Утилитный класс Finder, содержащий 3 метода для поиска дубликатов в двух коллекциях
 */
public class Finder {
    /**
     * Метод для поиска дубликатов на основе HashSet-ов,
     * использует метод retainAll, удаляющий элементы не принадлежащие данному сету
     *
     * @param collA коллекция А
     * @param collB коллекция В
     * @return список дубликатов
     * @throws NullPointerException если какая либо из коллекций пуста, либо обе пусты
     */
    public static List<UserSber> findDuplicates1(Collection<UserSber> collA, Collection<UserSber> collB) throws NullPointerException {
        if (collA == null || collB == null)
            throw new NullPointerException();
        else {
            Set<UserSber> setCollA = new HashSet<>(collA);
            Set<UserSber> setCollB = new HashSet<>(collB);
            setCollA.retainAll(setCollB);
            return new ArrayList<>(setCollA);
        }
    }

    /**
     * Метод для поиска дубликатов на основе HashSet-ов,
     * использует особенности хеш сетов, в хеш сетах хранятся
     * только уникальные элементы.
     * Сначала находятся уникальные пользователи, после удаляются из второй коллекции
     *
     * @param collA коллекция А
     * @param collB коллекция В
     * @return список дубликатов
     * @throws NullPointerException если какая либо из коллекций пуста, либо обе пусты
     */
    public static List<UserSber> findDuplicates2(Collection<UserSber> collA, Collection<UserSber> collB) throws NullPointerException {
        if (collA == null || collB == null)
            throw new NullPointerException();
        else {
            Set<UserSber> unicalSet = new HashSet<>();
            Set<UserSber> setCollA = new HashSet<>(collA);
            Set<UserSber> setCollB = new HashSet<>(collB);
            unicalSet.addAll(setCollA);
            unicalSet.addAll(setCollB);
            unicalSet.removeAll(setCollA);
            setCollB.removeAll(unicalSet);
            return new ArrayList<>(setCollB);
        }
    }

    /**
     * Метод для поиска дубликатов на основе HashSet-ов,
     * проверяет каждого пользователя первой коллекции, на содержание
     * во второй коллеции, через contains, и если пользователь содержится,
     * то он добавляется в список дубликатов
     *
     * @param collA колллеция А
     * @param collB коллекция В
     * @return список дубликатов
     * @throws NullPointerException если какая либо из коллекций пуста, либо обе пусты
     */
    public static List<UserSber> findDuplicates3(Collection<UserSber> collA, Collection<UserSber> collB) throws NullPointerException {
        if (collA == null || collB == null)
            throw new NullPointerException();
        else {
            Set<UserSber> dublies = new HashSet<>();
            Set<UserSber> setCollA = new HashSet<>(collA);
            Set<UserSber> setCollB = new HashSet<>(collB);
            for (UserSber user : setCollA) {
                if (setCollB.contains(user)) {
                    dublies.add(user);
                }
            }
            return new ArrayList<>(dublies);
        }
    }
}
