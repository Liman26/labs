package solovyev;

import java.util.Arrays;
import java.util.Objects;

/**
 * Класс описывающий пользователя СБЕРа
 */
public class UserSber {
    private String username;
    private String email;
    private byte[] passwordHash;

    /**
     * Конструктор пользователя
     *
     * @param username имя пользователя
     * @param email    его почта
     */
    public UserSber(String username, String email) {
        this.username = username;
        this.email = email;
        this.passwordHash = username.getBytes();
    }

    /**
     * Переопределенный метод toString
     *
     * @return строку содержащую все данные пользователя
     */
    @Override
    public String toString() {
        return "UserSber{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", passwordHash=" + Arrays.toString(passwordHash) +
                '}';
    }

    /**
     * Переопределенный метод equals, переопределен
     * для корректного сравнения пользователей
     *
     * @param o Объект, который будет приведен к классу UserSber, после его поля будут сравниваться
     * @return true/false в зависимости от идентичности сравниваемых объектов
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSber userSber = (UserSber) o;
        return username.equals(userSber.username) && email.equals(userSber.email) && Arrays.equals(passwordHash, userSber.passwordHash);
    }

    /**
     * Переопределенный метод hashCode
     *
     * @return хеш код пользователя
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(username, email);
        result = 31 * result + Arrays.hashCode(passwordHash);
        return result;
    }

    /**
     * Метод возвращающий имя пользователя
     *
     * @return имя пользователя
     */
    public String getUsername() {
        return username;
    }

    /**
     * Метод устанавливающий имя пользователя
     *
     * @param username имя пользователя
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Метод возвращающий почту пользователя
     *
     * @return почту пользователя
     */
    public String getEmail() {
        return email;
    }

    /**
     * Метод устанавливающий почту пользователя
     *
     * @param email почта пользователя
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Метод возвращающий хеш код пароля пользователя
     *
     * @return хеш код пароля
     */
    public byte[] getPasswordHash() {
        return passwordHash;
    }

    /**
     * Метод устанавливащий хеш код пароля пользователя
     *
     * @param passwordHash устанавлвиваемый хеш код пароля
     */
    public void setPasswordHash(byte[] passwordHash) {
        this.passwordHash = passwordHash;
    }
}

