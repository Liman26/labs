package vereshchagina;

import java.util.*;

/**
 * класс, содержащий 3 варианта утилитного метод
 * public static List< UserSber > findDuplicates(Collection< UserSber > collA, Collection< UserSber > collB);
 * который возвращает дубликаты юзеров, которые есть в обеих коллекциях.
 */
public class Task6 {
    /**
     * 1. Поиск одинаковых элементов в двух коллекциях посредством перебора элементов в цикле и
     * сравнении каждого элемента первой коллекции с каждым элементом второй.
     *
     * @param collA первая коллекция
     * @param collB вторая коллекция
     * @return список дубликатов
     */

    public static List<UserSber> findDuplicates1(Collection<UserSber> collA, Collection<UserSber> collB) {
        List<UserSber> result = new ArrayList<>();
        for (UserSber nodeA : collA)
            for (UserSber nodeB : collB)
                if (collB.equals(nodeA)) {
                    result.add(nodeA);
                    break;
                }
        return result;
    }

    /**
     * 2. Поиск одинаковых элементов в двух коллекциях посредством метода retainAll
     * (удаляем из первой коллекции все элементы, которых нет во второй)
     *
     * @param collA первая коллекция
     * @param collB вторая коллекция
     * @return список дубликатов
     */
    public static List<UserSber> findDuplicates2(Collection<UserSber> collA, Collection<UserSber> collB) {
        List<UserSber> result = new ArrayList<>(collA);
        result.retainAll(collB);
        return result;
    }

    /**
     * 3. Поиск одинаковых элементов в двух коллекциях с использованием метода add коллекции HashSet
     * (возвращает False, если добавляемый элемент уже присутствует в HashSet)
     *
     * @param collA первая коллекция
     * @param collB вторая коллекция
     * @return список дубликатов
     */
    public static List<UserSber> findDuplicates3(Collection<UserSber> collA, Collection<UserSber> collB) {
        List<UserSber> result = new ArrayList<>();
        Set<UserSber> dop = new HashSet<>(collA);
        for (UserSber nodeB : collB)
            if (!dop.add(nodeB))
                result.add(nodeB);
        return result;
    }
}
