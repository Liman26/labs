package vereshchagina;

import java.util.Arrays;
import java.util.Objects;

/**
 * Класс, содержащий данные о пользователе
 */
public class UserSber {
    private String username;
    private String email;
    private byte[] passwordHash;

    /**
     * конструктор
     *
     * @param username имя пользователя
     * @param email    почта пользователя
     */
    public UserSber(String username, String email) {
        this.username = username;
        this.email = email;
        this.passwordHash = username.getBytes();
    }

    /**
     * переопределенный метод equals(Object o)
     *
     * @return true - если сравниваемы значения одинаковые, иначе false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || o.getClass() != getClass()) return false;
        UserSber u = (UserSber) o;
        return username.equals(u.username) && email.equals(u.email) && Arrays.equals(passwordHash, u.passwordHash);
    }

    /**
     * переопределенный метод hashCode()
     *
     * @return числовое представление объекта
     */
    @Override
    public int hashCode() {
        return Objects.hash(username, email);
    }

    /**
     * метод, возвращающий имя пользователя
     *
     * @return имя пользователя
     */
    public String getUsername() {
        return username;
    }

    /**
     * метод присвоения нового имени пользователю
     *
     * @param username имя
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * метод, возвращающий почту пользователя
     *
     * @return почта пользователя
     */
    public String getEmail() {
        return email;
    }

    /**
     * метод присвоения новой почты пользователю
     *
     * @param email почта
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * метод, возвращающий пароль пользователя
     *
     * @return пароль пользователя
     */
    public byte[] getPasswordHash() {
        return passwordHash;
    }

    /**
     * метод присвоения нового пароля пользователю
     *
     * @param passwordHash пароль
     */
    public void setPasswordHash(byte[] passwordHash) {
        this.passwordHash = passwordHash;
    }
}
