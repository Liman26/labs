package turkov;

import java.util.*;

/**
 * Класс описывающий тип объектов UserSber и их утилитные методы.
 */
public class UserSber {

    private String username;
    private String email;
    private byte[] passwordHash;

    /**
     * Конструктор UserSber.
     *
     * @param username - имя пользователя
     * @param email    - адрес электронной почты
     */
    public UserSber(String username, String email) {
        this.username = username;
        this.email = email;
        this.passwordHash = username.getBytes();
    }

    /**
     * Метод №1, который возвращает дубликаты: пользователи, которые есть в обеих коллекциях.
     *
     * @param collA - первый набор данных UserSber.
     * @param collB - первый набор данных UserSber.
     * @return - список пользователей, которые есть в обеих входящих коллекциях.
     */
    public static List<UserSber> findDuplicatesOne(Collection<UserSber> collA, Collection<UserSber> collB) {
        Set<UserSber> setCollA = new HashSet<>(collA);
        Set<UserSber> duplicates = new HashSet<>();
        for (UserSber user : collB) {
            if (!setCollA.add(user))
                duplicates.add(user);
        }
        return new LinkedList<>(duplicates);
    }

    /**
     * Метод №2, который возвращает дубликаты: пользователи, которые есть в обеих коллекциях.
     *
     * @param collA - первый набор данных UserSber.
     * @param collB - первый набор данных UserSber.
     * @return - список пользователей, которые есть в обеих входящих коллекциях.
     */
    public static List<UserSber> findDuplicatesTwo(Collection<UserSber> collA, Collection<UserSber> collB) {
        List<UserSber> duplicates = new LinkedList<>();
        for (UserSber user : collA) {
            if (!duplicates.contains(user) && collB.contains(user))
                duplicates.add(user);
        }
        return duplicates;
    }

    /**
     * Метод №3, который возвращает дубликаты: пользователи, которые есть в обеих коллекциях.
     *
     * @param collA - первый набор данных UserSber.
     * @param collB - первый набор данных UserSber.
     * @return - список пользователей, которые есть в обеих входящих коллекциях.
     */
    public static List<UserSber> findDuplicatesThree(Collection<UserSber> collA, Collection<UserSber> collB) {
        HashMap<Integer, UserSber> mapCollA = new HashMap<>();
        Set<UserSber> duplicates = new HashSet<>();
        for (UserSber user : collA) {
            mapCollA.put(user.hashCode(), user);
        }
        for (UserSber user : collB) {
            if (mapCollA.containsKey(user.hashCode()))
                duplicates.add(user);
        }
        return new LinkedList<>(duplicates);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSber userSber = (UserSber) o;
        return Objects.equals(username, userSber.username) && Objects.equals(email, userSber.email) && Arrays.equals(passwordHash, userSber.passwordHash);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(username, email);
        result = 31 * result + Arrays.hashCode(passwordHash);
        return result;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(byte[] passwordHash) {
        this.passwordHash = passwordHash;
    }
}