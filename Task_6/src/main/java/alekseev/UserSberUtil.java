package alekseev;

import java.util.*;

/**
 * Утильный класс для получения List с повторящимися элементами из 2 коллекций.
 *
 * @author alekseev.a
 * @since 1.0
 */
public class UserSberUtil {

    UserSberUtil() {
    }

    /**
     * Возвращает список с одинаковыми элементами из 2 коллекций
     *
     * @param collA коллекция из элементов класса UserSber
     * @param collB коллекция из элементов класса UserSber
     * @return список с одинаковыми элементами из 2 коллекций
     */
    public static List<UserSber> findDuplicates1(Collection<UserSber> collA, Collection<UserSber> collB) {
        Set<UserSber> setA = new HashSet<>(collA);
        Set<UserSber> setB = new HashSet<>(collB);

        Collection<UserSber> dublicates = new HashSet<>();
        for (UserSber userSber : setA) {
            if (setB.contains(userSber)) {
                dublicates.add(userSber);
            }
        }
        return new ArrayList<>(dublicates);
    }

    /**
     * Возвращает список с одинаковыми элементами из 2 коллекций
     *
     * @param collA коллекция из элементов класса UserSber
     * @param collB коллекция из элементов класса UserSber
     * @return список с одинаковыми элементами из 2 коллекций
     */
    public static List<UserSber> findDuplicates2(Collection<UserSber> collA, Collection<UserSber> collB) {
        HashSet<UserSber> dublicates = new HashSet<>(collA);
        HashSet<UserSber> setB = new HashSet<>(collB);
        dublicates.retainAll(setB);
        return new ArrayList<>(dublicates);
    }

    /**
     * Получить список элементов класса UserSber входящих в обе коллекции
     *
     * @param collA коллекция элементов класса UserSber
     * @param collB коллекция элементов класса UserSber
     * @return список элементов класса UserSber входящих в обе коллекции
     */
    public static List<UserSber> findDuplicates3(Collection<UserSber> collA, Collection<UserSber> collB) {
        List<UserSber> dublicates = new ArrayList<>();
        for (UserSber userSber : collA) {
            if (collB.contains(userSber) && !dublicates.contains(userSber)) {
                dublicates.add(userSber);
            }
        }
        return dublicates;
    }
}
