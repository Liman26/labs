package zurnachyan;
import java.util.*;

/**
 * Класс, реализующий пользователя какого-то сервиса компании Сбербанк
 */
public class UserSber {

    private String username;
    private String email;
    private byte[] passwordHash;

    /**
     * Конструктор, принимает имя пользователя и почту, сохраняет, так же формирует passwordHash
     * @param username имя пользователя
     * @param email адрес эл.почты
     */
    public UserSber(String username, String email) {
        this.username = username;
        this.email = email;
        this.passwordHash = username.getBytes();
    }

    /**
     * Метод, возвращающий имя пользователя
     * @return имя пользователя
     */
    public String getUsername() {
        return username;
    }
    /**
     * Метод, позволяющий изменить имя пользователя
     * @param username новое имя пользователя
     */
    public void setUsername(String username) {
        this.username = username;
    }
    /**
     * Метод, возвращающий адрес эл. почты пользователя
     * @return адрес
     */
    public String getEmail() {
        return email;
    }
    /**
     * Метод, позволяющий изменить адрес эл. почты пользователя
     * @param email новый адрес
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Метод, возвращающий passwordHash ользователя
     * @return passwordHash
     */
    public byte[] getPasswordHash() {
        return passwordHash;
    }

    /**
     * Метод, позволяющий изменить passwordHash ользователя
     * @param passwordHash новый passwordHash
     */
    public void setPasswordHash(byte[] passwordHash) {
        this.passwordHash = passwordHash;
    }

    /**
     * Переопределенный метод сравнения двух экземпляров класса UserSber, взвращает "истину" или "ложь"
     * @param obj внешний экземпляр доя сравнения
     * @return "истина" или "ложь"
     */
    @Override
    public boolean equals(Object obj){
        if (obj == null) return false;
        else if (this.getClass() != obj.getClass()) return false;
        else if (this == obj) return true;
        UserSber other = (UserSber) obj;
        return this.username.equals(other.username) &&
                this.email.equals(other.email) &&
                Arrays.equals(this.passwordHash, other.passwordHash);
    }

    /**
     * Переопределенный метод, генерирующий hash code для экземпляра класса.
     * @return hash code
     */
    @Override
    public int hashCode(){
        return Objects.hash(username, email);
    }
}
