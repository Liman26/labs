package zurnachyan;

import java.net.CookieHandler;
import java.util.*;

/**
 * Утилитный класс для класса UserSber, в котором реализованы статические методы поиска дубликатов
 * в списках клиентов Сбербанка
 */
public class Utils {

    /**
     * Первый, самый очевидный метод поиска дубликатов путем перебора всех элементов двух данных списков и сравнения их.
     * Метод принимает два списка клиентов, возвращает новый список, содержащий дубликаты.
     * @param collA 1-й список
     * @param collB 2-й список
     * @return список дубликатов
     */
    public static List<UserSber> findDuplicates1 (Collection<UserSber> collA, Collection<UserSber> collB){
        List<UserSber> duplicates = new ArrayList<>();
        for(UserSber i: collA){
            for(UserSber j: collB){
                if(collA.equals(collB)){
                    duplicates.add(i);
                }
            }
        }
        return duplicates;
    }

    /**
     * Второй метод, основанный на особенности класса HashSet, в котором не могут храниться дубликаты.
     * Метод принимает два списка клиентов, возвращает новый список, содержащий дубликаты.
     * @param collA 1-й список
     * @param collB 2-й список
     * @return список дупликатов
     */
    public static List<UserSber> findDuplicates2 (Collection<UserSber> collA, Collection<UserSber> collB){
        List<UserSber> duplicates = new ArrayList<>();
        Set<UserSber> temp = new HashSet<>(collA);
        for(UserSber user: collB){
            if(!temp.add(user)) duplicates.add(user);
        }
        return duplicates;
    }

    /**
     * Третий метод, основанный на методе retainAll, удаляющем из данного списка все элементы,
     * которые есть во внешнем списке.
     * Метод принимает два списка клиентов, возвращает новый список, содержащий дубликаты.
     * @param collA 1-й список
     * @param collB 2-й список
     * @return список дупликатов
     */
    public static List<UserSber> findDuplicates3 (Collection<UserSber> collA, Collection<UserSber> collB){
        ArrayList<UserSber> duplicates = (ArrayList<UserSber>) collA;
        duplicates.retainAll(collB);
        return duplicates;
    }
}
