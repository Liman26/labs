package mescheryakov;

import java.util.*;

/**
 * Класс, реализующий перебор двумерного массива, с несколькими методами
 * @param <T> тип массива
 */
public class ArrayIterator <T> implements Iterator<T> {
    private final T[] a;
    private int cursor;

    /**
     * Конструктор, определящий параметры итератора
     * @param a двумерный массив
     */
    public ArrayIterator(T[][] a) {
        this.a = toSnakeArray(a);
        cursor = 0;
    }

    /**
     * Переопределенный метод, проверяющий, есть ли еще элементы в массиве
     * @return true - если элементы есть, false - если элементов нет
     */
    @Override
    public boolean hasNext() {
        return cursor != a.length;
    }

    /**
     * Переопределенный метод, возвращающий следующий элемент массива
     * @return следующий элемент массива
     */
    @Override
    public T next() {
        if (a.length == cursor)
            throw new NoSuchElementException();

        return a[cursor++];
    }

    /**
     * Метод, преобразующий двумерный массив в одномерный
     * @param a двумерный массив
     * @return одномерный массив
     */
    private T[] toSnakeArray(T[][] a) {
        final List<T> result = new ArrayList<>();
        for (T[] arrays : a) result.addAll(Arrays.asList(arrays));
        return (T[]) result.toArray();
    }
}
