package vereshchagina;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * класс, реализующий итератор для двумерного массива заданного типа Е
 *
 * @param <E> заданный тип массива
 */
public class ArrayIterator<E> implements Iterator {
    /**
     * двумерный массив
     */
    private E[][] array;
    /**
     * указатель на очередной элемент массива
     */
    private int cursor;

    /**
     * конструктор итератора
     *
     * @param array зданный двумерный массив
     */
    public ArrayIterator(E[][] array) {
        this.array = array;
        cursor = 0;
    }

    /**
     * переопределенный метод hasNext() для получения информации о наличии следующего элемента
     *
     * @return true - если есть следующий элемент, false - если элементов больше нет
     */
    @Override
    public boolean hasNext() {
        return cursor != array.length * array[0].length;
    }

    /**
     * переопределенный метод next() для получения значения следующего элемента
     *
     * @return значение следующего элемента, если он есть
     * @throws NoSuchElementException если массив закончился и итератор пытается вывести несуществующий элемент
     */
    @Override
    public E next() throws NoSuchElementException {
        if (hasNext()) {
            int i = cursor / array[0].length;
            int j = cursor % array[0].length;
            cursor++;
            return array[i][j];
        }
        throw new NoSuchElementException();

    }
}
