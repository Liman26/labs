package gushchin;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayIterator<T> implements Iterator<T> {
    private final T[][] array;
    private int row, column;

    public ArrayIterator(T[][] array) {
        this.array = array;
    }

    @Override
    public boolean hasNext() {
        return row < array.length;
    }

    @Override
    public T next() throws NoSuchElementException {
        if (!this.hasNext())
            throw new NoSuchElementException();
        T result = array[row][column];
        column++;

        if (column >= array[row].length) {
            column = 0;
            row++;
        }
        return result;
    }
}
