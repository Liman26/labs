package bachishche;

import java.util.Iterator;

/**
 * Итератор, который умеет работать с двумерными массивами Т[][]
 *
 * @param <T> тип объектов, хранящихся в массиве
 */
public class ArrayIterator<T> implements Iterator<T> {
    private int cursor;
    private T[][] a;

    public ArrayIterator(T[][] b) {
        a = b;
        cursor = -1;
    }

    /**
     * Проверка массива на наличие следующего элемента
     *
     * @return true, если следующий элемент есть,
     * false - если итератор достиг границы массива
     */
    @Override
    public boolean hasNext() {
        int e = end();
        boolean a = cursor < end();
        return cursor < end();
    }

    /**
     * Вернуть следующий объект в массиве.
     * Численное значение курсора вычисляется по формуле
     * cursor = i * columnNum + j,
     * где i - индекс текущей строки,
     * j - индекс текущего столбца, columnNum - количество столбцов.
     * Индексы элемента выражаются из данной формулы и значения курсора.
     *
     * @return следующий элемент в массиве
     */
    @Override
    public T next() {
        if (!hasNext())
            throw new IndexOutOfBoundsException();
        cursor++;
        return a[cursor / columnNum()][cursor % columnNum()];
    }

    /**
     * Получить количество строк в массиве
     *
     * @return количество строк
     */
    public int rowNum() {
        return a.length;
    }

    /**
     * Получить количество столбцов в массиве
     *
     * @return количество столбцов
     */
    public int columnNum() {
        if (rowNum() == 0)
            return 0;
        return a[0].length;
    }

    /**
     * Максимально допустимое для данного массива
     * значение курсора, указывающее на последний элемент
     *
     * @return Значение курсора при достижении им последнего
     * элемента в массиве
     */
    private int end() {
        return columnNum() * rowNum() - 1;
    }
}
