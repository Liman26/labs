package zurnachyan;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Класс реализует интерфейс итератор, для двумерных массивов
 * @param <E> обобщенный тип элементов двумерного массива
 */
public class ArrayIterator <E> implements Iterator<E> {

    private E[][] array;
    private int firstCoursor;
    private int secondCoursor;

    /**
     * Конструктор, сохраняет в локальную переменную обрабатываемы массив,
     * назначает указатели строк и столбоцов на начальные позиции
     * @param arr обрабатываемый массив
     */
    public ArrayIterator(E[][] arr){
        array = arr;
        firstCoursor = 0;
        secondCoursor = 0;
    }

    /**
     * Дополнительный метод, проверяющий, есть ли еще элемент во внутреннем массиве,
     * который в свою очередь является элементом в исходном двумерном массиве.
     * @return true - если есть еще элемент, false - иначе.
     */
    private boolean hasNextElement(){
        return secondCoursor != array[firstCoursor].length;
    }


    /**
     * Переопределенный метод реализуемого интерфейса,
     * проверяет остались ли вообще какие-либо элементы во всем двумерном массиве,
     * также переносит указатель строки на следующую.
     * @return true - если есть еще элемент, false - иначе.
     */
    @Override
    public boolean hasNext()  {
        if (!hasNextElement()){
            firstCoursor++;
            secondCoursor = 0;
        }
        return !(firstCoursor == array.length);
    }

    /**
     * Переопределенный метод реализуемого интерфейса,
     * возвращает следующий элемент массива если он есть, иначе выбрасывает исключение.
     * @return следующий элемент массива.
     */
    @Override
    public E next() {
        if(hasNext()) {
            return array[firstCoursor][secondCoursor++];
        }
        else throw new NoSuchElementException();
    }
}
