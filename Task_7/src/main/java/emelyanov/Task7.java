package emelyanov;

public class Task7 {
    public static void main(String[] args) {
        Integer[][] a = {{1, 2, 3}, {}, {4, 5, 6}, {7, 8}};
        ArrayIterator<Integer> iterator = new ArrayIterator<>(a);
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            System.out.println(next);
        }
    }
}
