package emelyanov;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Класс реализующий Iterator с двумерным массивом
 *
 * @param <T> тип элементов массива
 */
public class ArrayIterator<T> implements Iterator<T> {
    private final T[][] data;
    private int current_row;
    private int current_column;

    /**
     * Конструктор
     *
     * @param data двуменрный массив элементов типа T
     */

    public ArrayIterator(T[][] data) {
        this.data = data;
        current_row = 0;
        current_column = -1;
        findNext();
    }

    /**
     * Узнать можно ли получить следующий элемент
     *
     * @return можно ли получить следующий элемент
     */
    @Override
    public boolean hasNext() {
        return current_row != data.length;
    }

    /**
     * Получить следующий элемент или NoSuchElementException, если элементы закончились
     *
     * @return следующий элемент
     */
    @Override
    public T next() {
        if (hasNext()) {
            T result = data[current_row][current_column];
            findNext();
            return result;
        }
        throw new NoSuchElementException();
    }

    /**
     * Спозиционировать указатели на следующий элемент данных, если нельзя, то current_row=data.length
     */
    private void findNext() {
        if (current_row == data.length) {
            return;
        }
        if (data[current_row] == null || data[current_row].length == 0 || current_column + 1 == data[current_row].length) {
            current_column = -1;
            current_row++;
            findNext();
        } else {
            current_column++;
        }
    }
}
