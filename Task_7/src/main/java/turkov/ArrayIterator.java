package turkov;

/**
 * Класс ArrayIterator, который работает с массивом T[][], возвращая его элементы
 *
 * @param <T> - тип объекта итератора
 */
public class ArrayIterator<T> {
    private final T[][] array;
    private int idElemOneLevel, idElemTwoLevel;

    /**
     * Контструктор итератора
     * @param array - входящий двумерный массив
     *        idElemOneLevel - индекс элемента массива T[][]
     *        idElemTwoLevel - индекс элемента массива внутри T[idElemOneLevel][]
     */
    public ArrayIterator(T[][] array) {
        this.array = array;
        idElemOneLevel = 0;
        idElemTwoLevel = 0;
    }

    /**
     * Метод проверяет, не выходит ли следующая итерация за конец исходного массива и воможно взять следующий элемент
     *
     * @return true, если возможна еще хотя бы одна итерация;
     * false, если пройден весь массив.
     */
    public boolean hasNext() {
        return idElemOneLevel <= (array.length - 1) && idElemTwoLevel < array[idElemOneLevel].length;
    }

    /**
     * Метод возвращает текущий элемент массива
     *
     * @return - элемента массива типа T[][]
     */
    public T next() {
        T elem = array[idElemOneLevel][idElemTwoLevel];
        if (idElemTwoLevel == array[idElemOneLevel].length - 1) {
            idElemOneLevel++;
            idElemTwoLevel = 0;
        } else {
            idElemTwoLevel++;
        }
        return elem;
    }
}
