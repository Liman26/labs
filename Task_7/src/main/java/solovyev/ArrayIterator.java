package solovyev;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Класс ArrayIterator, реализует интерфейс Iterator
 * Работает с двумерными массивами любых размерностей
 *
 * @param <E> Тип данных
 */
public class ArrayIterator<E> implements Iterator<E> {
    /**
     * внутренний двумерный массив
     */
    private final E[][] array;
    /**
     * указатель на строку
     */
    private int pointerRow;
    /**
     * указатель на столбец(элемент в строке)
     */
    private int pointerColumn;

    /**
     * Конструктор инициализирующий параметры итератора,
     * выставляя указатели на первый элемент.
     *
     * @param array двумерный массив типа данных E
     */
    ArrayIterator(E[][] array) {
        this.array = array;
        this.pointerRow = 0;
        this.pointerColumn = 0;
    }

    /**
     * Переопределенный метод hasNext, проверяет есть ли в массиве следующий элемент
     *
     * @return true, если перебраны не все элементы, иначе false
     */
    @Override
    public boolean hasNext() {
        return pointerRow != array.length - 1 || pointerColumn != array[pointerRow].length;
    }

    /**
     * Переопределенный метод next
     *
     * @return следующий элемент массива
     * @throws NoSuchElementException если итератор пытается вывести несуществующий элемент
     */
    @Override
    public E next() throws NoSuchElementException {
        if (pointerColumn == array[pointerRow].length) {
            pointerColumn = 0;
            pointerRow++;
        }
        if (array.length == pointerRow)
            throw new NoSuchElementException();
        return array[pointerRow][pointerColumn++];
    }
}
