package nuridinov;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Класс-Итератор, который умеет работать с двумерными массивами
 * @param <E>
 */
public class ArrayIterator<E> implements Iterator<E> {
    private final E[][] a;
    /**
     * указатель курсора на строки
     */
    private int rowsCursor;
    /**
     * указатель курсора на столбцы
     */
    private int columnsCursor;

    /**
     * конструктор класса
     * @param a - двумерный массив
     */
    public ArrayIterator(E[][] a) {
        this.a = a;
        rowsCursor = 0;
        columnsCursor = 0;
    }

    /**
     * переопредленный метод интерфейса Iterator
     * @return true, если в массиве есть след элемент, иначе false
     */
    @Override
    public boolean hasNext() {
        return rowsCursor != a.length;
    }

    /**
     * переопредленный метод интерфейса Iterator
     * @return возвращает текущий элемент и сдвигает курсор
     * @throws NoSuchElementException если в массиве нет след элемента, выбрасывает это исключение
     */
    @Override
    public E next() throws NoSuchElementException {
        if(hasNext()){
            E elementOfArray = a[rowsCursor][columnsCursor];
            if(columnsCursor < a[rowsCursor].length){
                columnsCursor++;
            }
            if (columnsCursor >= a[rowsCursor].length){
                columnsCursor = 0;
                rowsCursor++;
            }
            return elementOfArray;
        }
        else throw new NoSuchElementException();

    }
}
