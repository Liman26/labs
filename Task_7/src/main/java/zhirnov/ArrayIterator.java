package zhirnov;

import java.util.Iterator;

/**
 * Класс, являющийся итератором для двумерного массива
 *
 * @param <T> - тип массива
 */
public class ArrayIterator<T> implements Iterator<T> {
    private T [][] array;
    private int coursor_col; // Столбцы
    private int coursor_row; // Строки

    public ArrayIterator(T[][] array) {
        this.array = array;
        coursor_col = 0;
        coursor_row = 0;
    }

    /**
     * Метод, проверяющий, имеется ли следующий элемент в массиве
     *
     * @return true, если имеется, иначе false
     */
    @Override
    public boolean hasNext() {
        return coursor_row != array.length;
    }

    /**
     * Метод, возвращающий элемент из массива
     *
     * @return элемент массива
     */
    @Override
    public T next() {
        T element = array[coursor_row][coursor_col];
        if (coursor_col < array[coursor_row].length - 1) {
            coursor_col++;
        } else {
            coursor_row++;
            coursor_col = 0;
        }
        return element;
    }
}
