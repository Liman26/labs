package koleda;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Класс, который умеет работать с массивами T[][]
 */
public class ArrayIterator<T> implements Iterator<T> {
    private T[] array;
    private int index;

    public ArrayIterator(T[][] array){
        this.array = convertToOneDim(array);
        index = 0;
    }

    /**
     * Метод, котороый проверяет, остались ли еще элементы в массиве
     * @return true - остались
     *         false - не остались
     */
    @Override
    public boolean hasNext(){
        return index !=array.length;
    }

    /**
     * Метод, который возвращает следующий элемент массива
     * @return след. элемент массива
     */
    @Override
    public T next(){
        if (array.length == index)
            throw new NoSuchElementException();
        return array[index++];
    }

    /**
     * Метод, который преобразует двумерный массив в одномерный
     * @param array двумерный массив
     * @return одномерный массив
     */
    private T[] convertToOneDim(T[][] array) {
        List<T> list = new ArrayList<>();
        IntStream.range(0, array.length).forEach(i -> list.addAll(Arrays.asList(array[i])));
        return (T[]) list.toArray();
    }
}
