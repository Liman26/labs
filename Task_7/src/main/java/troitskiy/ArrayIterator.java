package troitskiy;


import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Необходимо написать класс ArrayIterator который умеет работать с массивами T[][]
 *
 * Пример его использования на картинке (во вложении)
 * В примере представлен способ обращения к итератору
 *
 * Покрыть итератор тестами
 */
public class ArrayIterator<T> implements Iterator<T>
{
    private T[][] array;
    private int col, row;

    /**
     * Конструктор
     * @param array - двумерный массив
     */
    public ArrayIterator(T[][] array)
    {
        col = 0;
        row = 0;
        this.array = array;
    }

    /**
     * Returns {@code true} if the iteration has more elements.
     * (In other words, returns {@code true} if {@link #next} would
     * return an element rather than throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    @Override
    public boolean hasNext()
    {
        return row < array.length;
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    @Override
    public T next() throws NoSuchElementException
    {
        if (!hasNext()) throw new NoSuchElementException();

        T ret = array[row][col];
        col++;

        if (col >= array[row].length)
        {
            col = 0;
            row++;
        }

        return ret;
    }
}
