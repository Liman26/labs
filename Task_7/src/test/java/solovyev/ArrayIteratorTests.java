package solovyev;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Класс тестирующий мой итератор
 */
public class ArrayIteratorTests {

    /**
     * Тест проверяет корректность работы ArrayIterator-а с типом данных Character.
     * Сравнивает изначально заданный массив,
     * с массивом в который записаны элементы возвращаемые методом next.
     */
    @Test
    public void ArrayIteratorTest1() {
        Character[][] arr = {{'a', 'b', 'c'}, {'d'}, {'e', 'f'}, {'g', 'i', 'k', 'l'}};
        Character[][] actual = actualArraySize(arr);
        Iterator<Character> iterator = new ArrayIterator<>(arr);
        while (iterator.hasNext()) {
            for (int i = 0; i < arr.length; i++)
                for (int j = 0; j < arr[i].length; j++) {
                    actual[i][j] = iterator.next();
                }
        }
        assertArrayEquals(arr, actual);
    }

    /**
     * Тест проверяет корректность работы ArrayIterator-а с типом данных Integer.
     * Сравнивает изначально заданный массив,
     * с массивом в который записаны элементы возвращаемые методом next.
     */
    @Test
    public void ArrayIteratorTest2() {
        Integer[][] arr = {{1}, {2, 3}, {4, 5, 6}, {7, 8, 9, 10}};
        Integer[][] actual = actualArraySize(arr);
        ArrayIterator<Integer> iterator = new ArrayIterator<>(arr);
        while (iterator.hasNext()) {
            for (int i = 0; i < arr.length; i++)
                for (int j = 0; j < arr[i].length; j++) {
                    actual[i][j] = iterator.next();
                }
        }
        assertArrayEquals(arr, actual);
    }

    /**
     * Тест проверяет корректность работы next построчно,
     * записывая в массив actual лишь вторую строку в массиве
     * присваивая countDown число указывающее количество элементов в первой строке массива
     * Сравнивается вторая строка изначального массива,
     * с массивом в который записаны элементы возвращаемые методом next,
     * после отсчета элементов первой строки.
     */
    @Test
    public void ArrayIteratorTest3() {
        Integer[][] arr = {{4, 5, 6}, {7, 8, 9, 10}};
        int countDown = arr[0].length;
        int currentIter = 0;
        Integer[] actual = new Integer[arr[1].length];
        Iterator<Integer> iterator = new ArrayIterator<>(arr);
        while (iterator.hasNext()) {
            if (currentIter >= countDown)
                for (int j = 0; j < actual.length; j++) {
                    actual[j] = iterator.next();
                    System.out.println(actual.length);
                }
            else
                iterator.next();
            currentIter++;
        }
        assertArrayEquals(arr[1], actual);
    }

    /**
     * Тест проверяет корректность работы ArrayIterator-а с типом данных Integer.
     * Сравнивает изначально заданный массив,
     * с массивом в который записаны элементы возвращаемые методом next.
     */
    @Test
    public void ArrayIteratorTest4() {
        Integer[][] arr = {{5, 6}, {8, 8, 8, 8}, {13, 17, 1}};
        Integer[][] actual = actualArraySize(arr);
        ArrayIterator<Integer> iterator = new ArrayIterator<>(arr);
        while (iterator.hasNext()) {
            for (int i = 0; i < arr.length; i++)
                for (int j = 0; j < arr[i].length; j++) {
                    actual[i][j] = iterator.next();
                }
        }
        assertArrayEquals(arr, actual);
    }


    /**
     * Тест проверяет метод next на вызываемое исключение NoSuchElementException
     * При повторном вызове метода next, после достижения конца массива.
     */
    @Test
    public void TrowExceptionTest() {
        Integer[][] arr = {{1, 2}, {3}};
        ArrayIterator<Integer> iterator = new ArrayIterator<>(arr);
        while (iterator.hasNext())
            iterator.next();

        assertThrows(NoSuchElementException.class, () -> {
            iterator.next();
        });
    }

    /**
     * Специальный метод для копирования размерности массива типа Integer.
     * Пришлось сделать перегрузку метода для разных типов данных,
     * так как инициализировать массив типа generic нельзя.
     *
     * @param arr масссив размерность которого копируется
     * @return массив такой же размерности что и arr
     */
    public Integer[][] actualArraySize(Integer[][] arr) {
        Integer[][] actual = new Integer[arr.length][];
        for (int i = 0; i < arr.length; i++) {
            actual[i] = new Integer[arr[i].length];
        }
        return actual;
    }

    /**
     * Специальный метод для копирования размерности массива типа Character.
     * Пришлось сделать перегрузку метода для разных типов данных,
     * так как инициализировать массив типа generic нельзя.
     *
     * @param arr масссив размерность которого копируется
     * @return массив такой же размерности что и arr
     */
    public Character[][] actualArraySize(Character[][] arr) {
        Character[][] actual = new Character[arr.length][];
        for (int i = 0; i < arr.length; i++) {
            actual[i] = new Character[arr[i].length];
        }
        return actual;
    }
}
