package zurnachyan;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс для тестов итератора.
 */
public class Task7Tests {

    private Integer[][] a = new Integer[4][4];
    ArrayIterator<Integer> iter = new ArrayIterator<>(a);
    private ArrayList<Integer> test = new ArrayList<>();

    /**
     * Метод выполняющийся перед тестами, в котором происходит инициализация массива случайными числами
     */
    @BeforeEach
    public void construct(){
        Random rand = new Random();
        for(int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                a[i][j] = rand.nextInt(50);
            }
        }
    }

    /**
     * Тест корректной работы метода hasNext().
     */
    @Test
    public void test1(){
        Integer testContainer;
        for(int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                testContainer = iter.next();
            }
        }
        assertFalse(iter.hasNext());
    }

    /**
     * Тест корректной работы метода next().
     */
    @Test
    public void test2(){
        while(iter.hasNext()){
            test.add(iter.next());
        }
        for(int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                assertEquals(a[i][j], test.get(4 * i + j), "Test was failed in i = " + i + ", j = " + j);
            }
        }
    }
}

