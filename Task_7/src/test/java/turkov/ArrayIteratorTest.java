package turkov;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayIteratorTest {

    /**
     * Метод, где производится инциализация тестового массива целых чисел и проверка возвращаемых итератором значений
     */
    @Test
    public void ArrayIteratorOfIntegerTest() {
        Integer[][] a = {{1, 2, 3, 4}, {5, 5}, {4, 3, 2}, {1}};
        ArrayIterator<Integer> iterator = new ArrayIterator<>(a);
        assertTrue(iterator.hasNext());
        Stream.of(1, 2, 3, 4, 5, 5, 4, 3, 2, 1).forEach(i -> assertEquals(i, iterator.next()));
        assertFalse(iterator.hasNext());
    }

    /**
     * Метод, где производится инциализация тестового массива строк и проверка возвращаемых итератором значений
     */
    @Test
    public void ArrayIteratorOfIStringTest() {
        String[][] a = {{"One", "Two", "Three"}, {"One"}, {"Two", "Three"}};
        ArrayIterator<String> iterator = new ArrayIterator<>(a);
        assertTrue(iterator.hasNext());
        Stream.of("One", "Two", "Three", "One", "Two", "Three").forEach(i -> assertEquals(i, iterator.next()));
        assertFalse(iterator.hasNext());
    }
}
