package mescheryakov;

import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Класс, содержащий тесты для методов класса ArrayIterator
 */
public class ArrayIteratorTest {

    /**
     * Метод, в котором создается двумерный массив из рандомных чисел
     * @return двумерный массив
     */
    public Integer[][] massiv() {
        Integer[][] a = new Integer[3][3];

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = (int) (Math.random() * 10);
            }
        }
        for (int i = 0; i < a.length; i++, System.out.println()){
            for (int j = 0; j < a[i].length; j++){
                System.out.print(a[i][j] + " ");
            }
        }
        return a;
    }

    /**
     * Тест, в котором сравнивается последний элемент исходного массива а и
     * последний элемент нового массива, полученный с помощью итератора.
     */
    @Test
    public void test1() {
        Integer[][] a = massiv();
        ArrayIterator<Integer> iter = new ArrayIterator<>(a);

        Integer next = null;
        while (iter.hasNext()) {
            next = iter.next();
        }
        assertEquals(a[a.length - 1][a.length - 1], next);
    }

    /**
     * Тест, в котором проверяется работа NoSuchElementException при выходе за границу массива
     */
    @Test
    public void test2() {
        Integer[][] a = massiv();
        ArrayIterator<Integer> iter = new ArrayIterator<>(a);

        assertThrows(NoSuchElementException.class, () -> {
            while (true) {
                iter.next();
            }
        });
    }
}
