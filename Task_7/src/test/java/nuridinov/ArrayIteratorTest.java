package nuridinov;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс для тестирования ArrayIterator
 */
public class ArrayIteratorTest {
    /**
     * Тестовый двумерный массив
     */
    private static Integer[][] a;
    /**
     * Массив ожидемых значений
     */
    private static Integer[] expected;

    /**
     * заполнение массиво рандомными числами
     */
    @BeforeAll
    public static void arrayFilling() {
        a = new Integer[10][10];
        expected = new Integer[100];
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                a[i][j] = random.nextInt(10);

            }
        }
        for(int i = 0; i < 10; i++){
            System.arraycopy(a[i], 0, expected, i * 10, 10);
        }

    }

    /**
     * Тестирование метода .next() из ArrayIterator
     * (проверка возвращаемых значений метода)
     */
    @Test
    public void nextText() {
        int i = 0;
        Integer[] actual = new Integer[100];
        ArrayIterator<Integer> iterator = new ArrayIterator<>(a);
        while (iterator.hasNext()){
            actual[i] = iterator.next();
            i++;
        }
        assertArrayEquals(expected, actual);
    }

    /**
     * Тестирование метода .next из ArrayIterator
     * (проверка выкидываемого исключения)
     */
    @Test
    public void nextText2(){
        assertThrows(NoSuchElementException.class, ()->{
            ArrayIterator<Integer> iterator = new ArrayIterator<>(a);
            for (int i = 0; i <101;i++){
                iterator.next();
            }
        });
    }
}
