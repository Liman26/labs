package gushchin;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.NoSuchElementException;
import java.util.stream.IntStream;

/**
 * Тестирование класса ArrayIterator
 * Поля:
 * rowSize - максимально возможная длина массива первого порядка
 * columnSize - Максимально возвожная длина массивов второго порядка
 * testArray - двумерный массив элементов типа Integer для дальнейшего формирования итератора
 * counter - служебная константа для инициализации массива разными значениями
 * iterator - объект типа ArrayIterator, работающий с двумерным массивом Integer[][]
 */
public class Task7Test {
    private final int rowSize = 20;
    private final int columnSize = 5;
    private final Integer[][] testArray = new Integer[rowSize][columnSize];
    private int counter = 0;
    static ArrayIterator<Integer> iterator;

    /**
     * Инициализация итератора
     */
    @BeforeEach
    public void initIterator() {
        IntStream.rangeClosed(0, rowSize - 1).forEach(i -> IntStream.rangeClosed(0, columnSize - 1).forEach(j -> {
            testArray[i][j] = counter;
            counter++;
        }));
        iterator = new ArrayIterator<>(testArray);
    }

    /**
     * Тестирование метода ArrayIterator<>.hasNext()
     * Тест считается успешным, если последним элементом итератора является
     * произведение rowSize и columnSize
     * Последний элемент итератора получается потем инкремента i при условии, что
     * hasNext() равен true
     */
    @Test
    public void hasNextTest() {
        int i = 0;
        while (iterator.hasNext()) {
            iterator.next();
            i++;
        }
        assertEquals(rowSize * columnSize, i);
    }

    /**
     * Тестирование метода ArrayIterator<>.next()
     * Проверяется равенство инкрементируемого значение i с вызовом метода .next()
     * В случае, если в итераторе не осталось значений, проверяется получение
     * исключения NoSuchElementException
     */
    @Test
    public void nextTest() {
        int i = 0;
        while (iterator.hasNext()) {
            assertEquals(iterator.next(), i);
            i++;
        }
        assertThrows(NoSuchElementException.class, () -> iterator.next());
    }
}
