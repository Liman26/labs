package bachishche;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

/**
 * Класс-тетсироващик итератора для двумерного массива.
 * Тестовые массивы генерируются случайно с помощью библиотеки Random.
 */
public class ArrayIteratorTest {
    final static Random rand = new Random();
    static Double[][] a;
    static int rowNum = 7, columnNum = 15;

    /**
     * Инициализация исходного массива перед тестами
     */
    @BeforeAll
    public static void init() {
        a = new Double[rowNum][columnNum];
        for (int i = 0; i < rowNum; i++)
            for (int j = 0; j < columnNum; j++)
                a[i][j] = rand.nextDouble();
    }

    /**
     * Проверка метода итератора hasNext
     */
    @Test
    public void hasNextTest() {
        ArrayIterator<Double> iterator = new ArrayIterator<>(a);
        for (int i = 0; i < iterator.rowNum(); i++)
            for (int j = 0; j < iterator.columnNum(); j++) {
                assertTrue(iterator.hasNext());
                iterator.next();
            }
        assertFalse(iterator.hasNext());
    }

    /**
     * Проверка метода итератора Next,
     * поэлементная проверка на соответсвие массива, переданного итератору
     * и значений элементов массива, возвращаемых итератором
     */
    @Test
    public void nextTest() {
        ArrayIterator<Double> iterator = new ArrayIterator<>(a);
        for (int i = 0; i < iterator.rowNum(); i++)
            for (int j = 0; j < iterator.columnNum(); j++) {
                var d = iterator.next();
                assertEquals(a[i][j], d, "Итератор вернул элемент не по тому адресу!");
            }
        assertThrows(IndexOutOfBoundsException.class, iterator::next,
                "Выход за границы массива!");
    }

    /**
     * Проверка методов итератора columnNum,
     * количество столбцов, возвращаемое итератором должно быть
     * равно количеству столбцов массива, переданного
     * конструктору итератора в качетсве параметра
     */
    @Test
    public void columnNumTest() {
        ArrayIterator<Double> iterator = new ArrayIterator<>(a);
        assertEquals(iterator.columnNum(), columnNum);
    }

    /**
     * Проверка методов итератора rowNum,
     * количество строк, возвращаемое итератором должно быть
     * равно количеству строк массива, переданного
     * конструктору итератора в качетсве параметра
     */
    @Test
    public void rowNumTest() {
        ArrayIterator<Double> iterator = new ArrayIterator<>(a);
        assertEquals(iterator.rowNum(), rowNum);
    }
}
