package vereshchagina;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.Random;

public class ArrayIteratorTest {

    private static Integer[][] a;
    private static int m, n;

    /**
     * создание и заполнение двумерного массива случайным образом
     */
    @BeforeAll
    private static void beforeAll() {
        Random random = new Random();
        n = random.nextInt(100) + 1;
        m = random.nextInt(100) + 1;
        a = new Integer[n][m];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                a[i][j] = random.nextInt(1000) + 1;
    }

    /**
     * проверка метода hasNext()
     */
    @Test
    public void hasNextTest() {
        ArrayIterator<Integer> iter = new ArrayIterator<>(a);
        int coutIter = 0;
        while (iter.hasNext()) {
            iter.next();
            coutIter++;
        }
        Assertions.assertEquals(coutIter, a.length * a[0].length);

    }

    /**
     * проверка метода next() при переборе элементов в массиве
     * (проверяется каждый элемент)
     */

    @Test
    public void nextTest1() {
        ArrayIterator<Integer> iter = new ArrayIterator<>(a);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++) {
                Assertions.assertEquals(iter.next(), a[i][j]);
            }
    }

    /**
     * проверка метода next() в случае выхода за границу массива
     */
    @Test
    public void nextTest2() {
        ArrayIterator<Integer> iter = new ArrayIterator<>(a);
        while (iter.hasNext()) {
            iter.next();
        }
        Assertions.assertThrows(NoSuchElementException.class, () -> iter.next());
    }
}
