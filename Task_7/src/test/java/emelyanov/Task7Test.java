package emelyanov;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class Task7Test {
    private Integer[][] data;
    private final int MAX_SIZE = 10;
    private int countElement;
    private int sumElement;

    @BeforeEach
    public void beforeEach() {
        int sizeRow = randomInteger(MAX_SIZE);
        data = new Integer[sizeRow][];
        countElement = 0;
        sumElement = 0;
        for (int i = 0; i < sizeRow; i++) {
            int sizeColumn = randomInteger(MAX_SIZE);
            data[i] = new Integer[sizeColumn];
            for (int k = 0; k < sizeColumn; k++) {
                int value = randomInteger(Integer.MAX_VALUE);
                countElement++;
                sumElement += value;
                data[i][k] = value;
            }
        }
    }

    @Test
    @DisplayName("Проверка на количество итераций")
    public void testCount() {
        ArrayIterator<Integer> iterator = new ArrayIterator<>(data);
        int count = 0;
        while (iterator.hasNext()) {
            count++;
            assertEquals(Integer.class, iterator.next().getClass());
        }
        assertEquals(countElement, count, "Размер массива равен количеству вызовов next");
    }

    @Test
    @DisplayName("Проверка на сумму всех значений")
    public void testSumma() {
        ArrayIterator<Integer> iterator = new ArrayIterator<>(data);
        int summa = 0;
        while (iterator.hasNext()) {
            summa += iterator.next();
        }
        assertEquals(sumElement, summa, "Сумма элементов массива равна сумме элементов возвращаемых next");
    }

    @Test
    @DisplayName("Проверка на превышение количества вызовов next")
    public void testException() {
        ArrayIterator<Integer> iterator = new ArrayIterator<>(data);
        while (iterator.hasNext()) {
            iterator.next();
        }
        assertThrows(NoSuchElementException.class, () -> {
            iterator.next();
        });
    }

    /**
     * Генератор случайных чиссел
     *
     * @return случайное число типа int от 0 до Максимального maxSize
     */
    public static int randomInteger(int maxSize) {
        return (int) Math.round(Math.random() * maxSize);
    }

}
