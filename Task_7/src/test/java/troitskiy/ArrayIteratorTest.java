package troitskiy;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;


class ArrayIteratorTest {

    static int rowSize = 100;
    static final int colSize = 50;
    static Integer [][] array;
    static ArrayIterator<Integer> iterator;

    /**
     * Подготовка тестового массива где каждый элемент это
     * номер по порядку работы итератора (начинается с 0)
     */
    @BeforeAll
    public static void init()
    {
        array = new Integer[rowSize][colSize];
        int c = 0;

        for(int i = 0; i < rowSize; i++)
        {
            for(int j = 0; j < colSize; j++)
            {
                array[i][j] = c;
                c++;
            }
        }
    }

    /**
     *  Подготовка итератора перед тестом
     */
    @BeforeEach
    void setUp()
    {
        iterator = new ArrayIterator<>(array);
    }

    /**
     * Тест метода hasNext
     * Должен выдать false когда пройдет всю матрицу
     */
    @Test
    void hasNext()
    {
        int i = 0;

        while (iterator.hasNext())
        {
            iterator.next();
            i++;
        }
        assertEquals(rowSize*colSize, i);
    }

    /**
     * Тест метода Next
     * Должен выдавать последовательно элементы
     */
    @Test
    void next()
    {
        int i = 0;

        while (iterator.hasNext())
        {
            assertEquals(iterator.next(), i);
            i++;
        }
    }


    /**
     * Тест на исключение
     * когда элементы кончились
     */
    @Test
    void NSEExcept()
    {
        assertThrows(NoSuchElementException.class, () -> { int i=0; while (i <= rowSize*colSize) iterator.next();} );
    }


}