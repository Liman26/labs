package koleda;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class Task7Test {
    private static Integer[][] a;
    private static int row = 5;
    private static int col = 5;

    /**
     * Метод, в котором инициализируем массив
     */
    @BeforeAll
    public static void init(){
        a = new Integer[row][col];

        for (int i = 0; i < a.length; i++){
            for (int j = 0; j < a[i].length; j++){
                a[i][j]  = (int) (Math.random() * 10);
            }
        }
        for (int i = 0; i < a.length; i++,System.out.println()){
            for (int j = 0; j < a[i].length; j++){
                System.out.print(a[i][j] + " ");
            }
        }
    }

    /**
     * Тест, который проверяет количесвто элементов в массиве
     */
    @Test
    void testHasNext() {
        ArrayIterator<Integer> iter = new ArrayIterator<>(a);
        int count = 0;
        while (iter.hasNext()) {
            iter.next();
            count++;
            }
        Assertions.assertEquals(row * col, count);
    }

    /**
     *Тест, который проверяет каждый элемент массива
     */
    @Test
    void testNext() {
        ArrayIterator<Integer> iter = new ArrayIterator<>(a);
        while (iter.hasNext()) {
            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 5; j++) {
                    Integer next = iter.next();
                    Assertions.assertEquals(a[i][j], next);
                }
        }
    }

    /**
     * Тест, который проверяет работу NoSuchElementException
     */
    @Test
    public void testNoSuchElementException() {
        ArrayIterator<Integer> iter = new ArrayIterator<>(a);
        while (iter.hasNext()) {
            iter.next();
        }
        assertThrows(NoSuchElementException.class, () -> {
            iter.next();
        });
    }

}
