package zhirnov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

/**
 * Класс, отвечающий за тестирование итератора для двумерных массивов
 */
public class Test7 {
    private static Integer[][] array;
    private static StringBuilder stringBuilderExpected;
    private static StringBuilder stringBuilderActual;

    /**
     * Метод, инициализирующий все статические переменные
     */
    @BeforeEach
    public void init() {
        Random random = new Random();
        stringBuilderExpected = new StringBuilder();
        array = new Integer[5][5];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(10);
                stringBuilderExpected.append(array[i][j]);
            }
        }
        stringBuilderActual = new StringBuilder();
    }

    /**
     * Метод, который сравнивает ожидаемый результат и получаемый.
     */
    @Test
    public void test() {
        ArrayIterator<Integer> arrayIterator = new ArrayIterator<>(array);
        while (arrayIterator.hasNext()) {
            stringBuilderActual.append(arrayIterator.next());
        }
        Assertions.assertEquals(stringBuilderExpected.toString(), stringBuilderActual.toString());
    }
}
