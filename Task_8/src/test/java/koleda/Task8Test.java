package koleda;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Task8Test {

    /**
     * Тест на проверку упорядоченного вывода слов
     * Количество слов в тексте:
     * a - 4 раза
     * ab - 3 раза
     * abc - 2 раза
     * . - 1 раз
     */
    @Test
    void testFrequencyOfWords(){
        assertArrayEquals(new String[] {"a", "ab", "abc", "."}, Task8.commonWords("a . ab a abc a ab ab a abc"));
    }

    /**
     * Тест на проверку влияния регистра слов
     * Количество слов в тексте:
     * a - 4 раза
     * ab - 3 раза
     * abc - 2 раза
     * . - 1 раз
     */
    @Test
    void testUpperAndLowerCase(){
        assertArrayEquals(new String[] {"a", "ab", "abc", "."}, Task8.commonWords("A . Ab a aBc A ab aB a abC"));
    }

    /**
     * Тест на лексический порядок слов
     * Количество слов в тексте:
     * aaa - 2 раз
     * bbb - 2 раз
     * ccc - 1 раз
     * . - 1 раз
     */
    @Test
    void testLexicalOrder(){
        assertArrayEquals(new String[] {"aaa", "bbb", "ccc", "."}, Task8.commonWords("aaa ccc . aaa bbb bbb"));
    }

}
