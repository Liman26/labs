package bachishche;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс-тестировщик утилитного метода, определяющего
 * 10 слов, которые чаще всего встретились в тексте
 */
public class WordsCounterTest {
    /**
     * Тестирование метода поиска на корректность: метод должен вернуть список из 10
     * наиболее употребляемых слов (или всех слов, если их меньше 10)
     * в порядке убывания частоты употребления
     */
    @Test
    public void wordsFrequencyTest() {
        String text = "a b c d d d d c b c";
        assertArrayEquals(new String[]{"d", "c", "b", "a"}, WordsCounter.getTop10Word(text));
        text = "a1 b4 b4 d3 d3 a2, d3 z2. z2. b4 b4 " +
                "a5 a5 a5 a5 a5 c7 c7 c7 c7 c7 c7 c7 " +
                "n6 n6 n6 n6 n6 n6 h8 h8 h8 h8 h8 h8 h8 h8 " +
                "d9 d9 d9 d9 d9 d9 d9 k1 d9 d9 a2, " +
                "s10 s10 s10 s10 s10 f1 s10 s10 s10 s10 s10";
        assertArrayEquals(WordsCounter.getTop10Word(text),
                new String[]{"s10", "d9", "h8", "c7", "n6", "a5", "b4", "d3", "a2,", "z2."});
    }

    /**
     * Тестирование метода на лексикографический порядок возвращаемого списка,
     * если частота появления слов идентична
     */
    @Test
    public void lexicographicOrderTest() {
        String text = "j x d f y a h b z e c g i";
        String[] d = WordsCounter.getTop10Word(text);
        assertArrayEquals(new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"}, WordsCounter.getTop10Word(text));
    }

    /**
     * Тестирование метода поиска на обработку исключений
     */
    @Test
    public void exceptionTest() {
        assertThrows(NullPointerException.class, () -> WordsCounter.getTop10Word(null));
    }
}