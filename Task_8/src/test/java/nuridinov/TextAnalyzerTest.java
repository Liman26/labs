package nuridinov;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс тестов для TextAnalyzer
 */
public class TextAnalyzerTest {

    /**
     * Проверка соблюдения порядка, при одинаковой частоте появления
     */
    @Test
    public void Test1() {
        String str = "z z z z f f f f ";
        String[] expected = {"F", "Z"};
        assertArrayEquals(expected, TextAnalyzer.frequencyAnalyzer(str));
    }

    /**
     * Слово с большей частотой появления должны быть первым
     */
    @Test
    public void Test2() {
        String str = "z z z z z f f f f ";
        String[] expected = {"Z", "F"};
        assertArrayEquals(expected, TextAnalyzer.frequencyAnalyzer(str));
    }

    /**
     * Проверка исключения, при передаче в метод пустого массива
     */
    @Test
    public void Test3() {
        assertThrows(NullPointerException.class, () -> {
            String str = "";
            TextAnalyzer.frequencyAnalyzer(str);
        });
    }

    /**
     * В итоговом массиве должны быть не более 10 элементов
     */
    @Test
    public void Test4() {
        String str = "z z z z z f f f f a b c d e j i i i i n m . . . . . . . ";
        String[] expected = {".", "Z", "F", "I", "A", "B", "C", "D", "E", "J"};
        assertArrayEquals(expected, TextAnalyzer.frequencyAnalyzer(str));
    }

}
