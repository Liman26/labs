package zurnachyan;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для тестов 8-й лабораторной работы
 */
public class TestTask8 {
    private String text;
    private List<String> test = new ArrayList<>();

    /**
     * Тест подсчета количества повторений слов и сортировка этих слов в убывающем порядке повторений.
     */
    @Test
    public void test1(){
        text = "Hello world world world world world java java";
        test.add("world"); //1-й элемент, встречется чаще всего
        test.add("java"); //2-й элемент по кол-ву повторений
        test.add("hello"); //3-й элемент по кол-ву повторений
        assertEquals(test, Task8.countWords(text));
    }

    /**
     * Тест сортировки в лексикографическом порядке, при суловии, что количество повторений одинаково
     */
    @Test
    public void test2(){
        text = "zz xx yy aa";
        test.add("aa"); //1-й элемент, стоит раньше всех в лексикографическом порядке
        test.add("xx"); //2-й элемент, стоит вторым в лексикографическом порядке
        test.add("yy"); //3-й элемент, стоит третьим в лексикографическом порядке
        test.add("zz"); //4-й элемент, стоит четвертым в лексикографическом порядке
        assertEquals(test, Task8.countWords(text));
    }

    /**
     * Тест ограничения в 10 элементов
     */
    @Test
    public void test3(){
        text = "a b c d e f g h i j k l m n";
        test.add("a");
        test.add("b");
        test.add("c");
        test.add("d");
        test.add("e");
        test.add("f");
        test.add("g");
        test.add("h");
        test.add("i");
        test.add("j");
        assertEquals(test, Task8.countWords(text));
    }
}
