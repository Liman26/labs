package emelyanov;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static emelyanov.Task8.calcWord;
import static org.junit.jupiter.api.Assertions.*;

public class Task8Test {

    @Test
    @DisplayName("Проверка на статичном тексте < 10 слов")
    public void test1() {
        String text = "3word 3word 1word 2word 2word 1word 1word 4word 4word 5word";
        String[] expectedResult = {"1word", "2word", "3word", "4word", "5word"};
        String[] result = calcWord(text, 10);
        assertEquals(5, result.length, "Размер массива равен количеству уникальных слов");
        assertArrayEquals(expectedResult, result, "Проверка на правильный результат");
    }

    @Test
    @DisplayName("Проверка на статичном тексте > 10 слов")
    public void test2() {
        String text = "06word 07word 03word 03word 01word 02word 09word 10word 02word 01word 01word 04word 04word 05word 08word 11word 12word";
        String[] expectedResult = {"01word", "02word", "03word", "04word", "05word", "06word", "07word", "08word", "09word", "10word"};
        String[] result = calcWord(text, 10);
        assertEquals(10, result.length, "Размер массива равен количеству уникальных слов");
        assertArrayEquals(expectedResult, result, "Проверка на правильный результат");
    }

    @Test
    @DisplayName("Проверка на сгенерированном тексте в 1000 слов")
    public void test3() {
        String[] arrayWords = new String[100];
        for (int i = 0; i < 100; i++) {
            arrayWords[i] = i + "word";
        }
        StringBuilder text = new StringBuilder(arrayWords[randomInteger(99)]);
        for (int i = 0; i < 1000; i++) {
            text.append(" ");
            text.append(arrayWords[randomInteger(99)]);
        }
        String[] result = calcWord(text.toString(), 10);
        assertTrue(result.length > 1 && result.length <= 10, "Размер массива равен количеству уникальных слов");
    }

    /**
     * Генератор случайных чиссел
     *
     * @return случайное число типа int от 0 до Максимального maxSize
     */
    public static int randomInteger(int maxSize) {
        return (int) Math.round(Math.random() * maxSize);
    }

}
