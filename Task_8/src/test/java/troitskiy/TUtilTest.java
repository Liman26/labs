package troitskiy;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс для тестирования статического метода TUtil.Task8
 *  TUtil.Task8 метод, который на вход принимает текст
 *              Метод должен посчитать частоту появления слов, и отдать массив из 10 слов которые чаще всего встретились в тексте. Если в тексте меньше 10 уникальных слов, то метод отдаст сколько есть. Если в тексте некоторые слова имеют одинаковую частоту, то отдаем их в лексикографическом порядке.
 *              Слово это любая непрерывная последовательность символов. Даже с символами точка, запятая
 *              Подсчет слов будем делать без учета регистра
 *              Для решения задачи не использовать циклы а использовать стримы:)
 */
class TUtilTest {

    @Test
    void task8()
    {
        String data = "1 2 2 3 3 3 4 4 4 4 5 5 5 5 5 6 6 6 6 6 6 7 7 7 7 7 7 7 8 8 8 8 8 8 8 8 9 9 9 9 9 9 9 9 9 0 0 0 0 0 0 0 0 0 0 aA AA aa";
        assertArrayEquals(new String[] {"0","9","8","7","6","5","4","3","aa","2"}, TUtil.Task8(data));
    }

    @Test
    void task81()
    {
        String data = "word word word word tord tord tord tord abc cba qax e g b";
        assertArrayEquals(new String[] {"tord", "word", "abc", "b", "cba", "e", "g", "qax" }, TUtil.Task8(data));
    }

    @Test
    void task82()
    {
        String data = "A a B b B C c c C";
        assertArrayEquals(new String[] {"c", "b", "a" }, TUtil.Task8(data));
    }

    @Test
    void task83()
    {
        String data = "AaAAA aAAAA aaaaa";
        assertArrayEquals(new String[] {"aaaaa"}, TUtil.Task8(data));
    }
}