package gushchin;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class StringUtilTest {
    /**
     * Тест, входными данными которого является строка с уникальными значениями.
     * Служит для проверки корректной сортировки
     */
    @Test
    public void uniqueWordsText() {
        String testString = "A B, BA C; D. E F G,,, H....I:::J;K";
        String[] expectedResult = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
        assertArrayEquals(expectedResult, StringUtil.getCommonsWordsByText(testString));
    }

    /**
     * Тест, входными данными которого является строка конструкции Lorem из 100 слов
     */
    @Test
    public void commonsWordsTest() {
        String testString = "Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
                " Alias architecto est excepturi reiciendis reprehenderit ut, veritatis! Eos" +
                " eveniet facilis neque provident recusandae reiciendis totam vero? Consequatur deleniti" +
                " dignissimos error explicabo fuga iste perspiciatis repellat sit, voluptates. Ab" +
                " accusantium ad adipisci aliquam aliquid atque consectetur consequatur culpa cupiditate" +
                " delectus dicta distinctio dolore doloremque, dolorum earum eius illo illum maxime" +
                " minima nam necessitatibus obcaecati odit officia optio provident quae quam, quas" +
                " qui quis reprehenderit rerum sequi soluta unde ut veniam voluptate voluptatem!" +
                " Aliquam cupiditate ea eius expedita impedit ipsum laborum libero modi nihil non optio" +
                " quaerat quia sunt ullam vel, veniam voluptatibus.";
        String[] expectedResult = {"cupiditate", "consequatur", "ut",
                "ipsum", "reiciendis", "veniam", "optio", "sit", "aliquam", "eius"};
        assertArrayEquals(expectedResult, StringUtil.getCommonsWordsByText(testString));
    }
}
