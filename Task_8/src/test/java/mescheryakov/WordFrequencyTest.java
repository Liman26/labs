package mescheryakov;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

/**
 * Класс, содержащий тесты метода frequency() класса WordFrequency,
 * которые проверяют его работу в различных ситуациях
 */
public class WordFrequencyTest {

    /**
     * Тест, выполняющий проверку работы метода frequency() в ситуации, когда
     * все буквы слов одного регистра, но некоторые слова повторяются одинаковое количество раз.
     * Мы должны получить их в лексикографическом порядке.
     * Количество повторяющихся слов менее 10.
     */
    @Test
    public void test1() {
        String text = "hello how how hello how speak is speak speak are";
        assertArrayEquals(new String[]{"how", "speak", "hello", "are", "is"}, WordFrequency.frequency(text, 5));
    }

    /**
     * Тест, выполняющий проверку работы метода frequency() в ситуации, когда
     * на вход в виде строки принимаются только числа.
     * Количество повторяющихся слов равно 10.
     */
    @Test
    public void test2() {
        String text = "1 2 3 4 5 6 7 8 9 10 1 2 3 4 5";
        assertArrayEquals(new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, WordFrequency.frequency(text, 10));
    }

    /**
     * Тест, выполняющий проверку работы метода frequency() в ситуации, когда
     * слова на входе отличаются регистром. Метод должен определять их как уникальные, то есть
     * повторяющиеся только один раз.
     */
    @Test
    public void test3() {
        String text = "AAA BBB aaa aaa bbb";
        assertArrayEquals(new String[]{"aaa"}, WordFrequency.frequency(text, 1));
    }
}
