package turkov;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

/**
 * Класс для тестирования метода WordCounter.counter(), который производит подсчет частоты появления уникальных
 * слов в тексте.
 */
public class WordCounterTest {

    /**
     * Тестирование на примере текста из 11 уникальных слов разделенных пробелами и знаками препинания.
     * Частота появления слов в тестируемом тексте:
     * BEAR, CAT, DOG, JUICE: 3;
     * HORSE, KEY, ORANGE: 2;
     * APPLE, QUEEN, SUN, UMBRELLA: 1
     */
    @Test
    public void WordCounterTestTextSyntheticBig() {
        String example = "   Dog Dog dog; Cat cat Cat; juice juice juice; Bear bear BEAR. " +
                "Umbrella, Apple, Sun, Queen? " +
                "Orange orange: Key key: Horse horse! ";
        String[] target = {"BEAR", "CAT", "DOG", "JUICE", "HORSE", "KEY", "ORANGE", "APPLE", "QUEEN", "SUN"};
        assertArrayEquals(target, WordCounter.counting(example));
    }

    /**
     * Тестирование на примере стишка с 8 уникальными словами разделенными пробелами и знаками препинания.
     * Частота появления слов в тестируемом тексте:
     * ONE, TWO: 3;
     * CATS, DOG, LITTLE, RUN, SEE: 1
     */
    @Test
    public void WordCounterTestTextSmall() {
        String example = " One, one, one - " +
                "Little dog, run!" +
                " Two, two, two - " +
                "Cats see you.";
        String[] target = {"ONE", "TWO", "CATS", "DOG", "LITTLE", "RUN", "SEE", "YOU"};
        assertArrayEquals(target, WordCounter.counting(example));
    }

    /**
     * Тестирование на примере текста состоящего из набора 13 уникальных чисел разделенных пробелами.
     * Частота появления чисел в тестируемом тексте:
     * "1, 2, 3": 3;
     * "4, 5, 6": 2;
     * "0, 22, 33, 44, 7, 8, 9": 1
     */
    @Test
    public void WordCounterTestSymbolAndDigits() {
        String example = " 1 2 3 1 2 3 " +
                " 4 5 6 " +
                "0 9 8 7 6 5 4 3 2 1 " +
                "22 33 44 ";

        String[] target = {"1", "2", "3", "4", "5", "6", "0", "22", "33", "44"};
        assertArrayEquals(target, WordCounter.counting(example));
    }

    /**
     * Тестирование на примере текста состоящего из набора 13 уникальных "слов" (7 чисел и 6 слов)
     * разделенных пробеламии и знаками препинания. Частота появления чисел в тестируемом тексте:
     * 1, 2, 3, C, JAVA=4;
     * PYTHON: 3;
     * Cpp, rating, TIOBE, 2018, 2019, 2020, 2021: 1
     */
    @Test
    public void WordCounterTestHybrid() {
        String example = " TIOBE rating " +
                "2021: C - 1, Java - 2, Python - 3; " +
                "2020: Java - 1, C - 2, Python - 3; " +
                "2019: Java - 1, C - 2, Python - 3; " +
                "2018: Java - 1, C - 2, Cpp - 3";
        String[] target = {"1", "2", "3", "C", "JAVA", "PYTHON", "2018", "2019", "2020", "2021"};
        assertArrayEquals(target, WordCounter.counting(example));
    }
}
