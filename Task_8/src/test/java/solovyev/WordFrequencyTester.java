package solovyev;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Тестирующий класс для WordFrequency
 */
public class WordFrequencyTester {

    /**
     * Тестирование на выбрасывание исключений
     */
    @Test
    public void ExceptionTest() {
        String text = null;
        String text1 = "";
        String text2 = " ";
        String text3 = "  ";
        assertThrows(NullPointerException.class, () -> WordFrequency.analyze(text));
        assertThrows(NullPointerException.class, () -> WordFrequency.analyze(text1));
        assertThrows(NullPointerException.class, () -> WordFrequency.analyze(text2));
        assertThrows(NullPointerException.class, () -> WordFrequency.analyze(text3));
    }

    /**
     * Тест сравнивающий ожидаемый массив слов,
     * с массивом слов возвращаемым методом analyze
     */
    @Test
    public void AnalyzerTest1() {
        String text = " e e E e E m mM m M m Mm MM";
        String[] expected = {"E", "M", "MM"};

        assertArrayEquals(expected, WordFrequency.analyze(text));
    }

    /**
     * Тест сравнивающий ожидаемый массив слов,
     * с массивом слов возвращаемым методом analyze
     */
    @Test
    public void AnalyzerTest2() {
        String text = " E";
        String[] expected = {"E"};

        assertArrayEquals(expected, WordFrequency.analyze(text));
    }

    /**
     * Тест сравнивающий ожидаемый массив слов,
     * с массивом слов возвращаемым методом analyze
     */
    @Test
    public void AnalyzerTest3() {
        String text = "IN trava SIdel IN IN in tr tr";
        String[] expected = {"IN", "TR", "SIDEL", "TRAVA"};

        assertArrayEquals(expected, WordFrequency.analyze(text));
    }

    /**
     * Тест сравнивающий ожидаемый массив слов,
     * с массивом слов возвращаемым методом analyze
     */
    @Test
    public void AnalyzerTest4() {
        String text = "1 2 3 3 3 4 5 5 5 5 ";
        String[] expected = {"5", "3", "1", "2", "4"};

        assertArrayEquals(expected, WordFrequency.analyze(text));
    }

}
