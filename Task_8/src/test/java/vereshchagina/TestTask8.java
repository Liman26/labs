package vereshchagina;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static vereshchagina.Task8.hitRateFinder;

public class TestTask8 {
    /**
     * строка слов, встречающихся с разной частотой
     */
    @Test
    void test1() {
        assertArrayEquals(new String[]{"jj", "kkk", "oo", "g"},
                hitRateFinder("jj g jj kkk oo jj jj oo kkk jj kkk"));
    }

    /**
     * строка слов, встречающихся с одинаковой частотой
     */
    @Test
    void test2() {
        assertArrayEquals(new String[]{"a", "b", "c", "d", "e", "f", "g"},
                hitRateFinder("g d b e f a c"));
    }

    /**
     * строка, состоящая из более чем 10 слов
     * частота:
     * "hh" - 13, "ooo" - 10, "rt" - 10, "kk" - 9, "or" - 8, "hgf" - 6, "trr" - 6, "yu" - 5, "ig" - 3, "kg" - 3
     * а также "trb" - 3, оастальные 2 или 1
     */
    @Test
    void test3() {
        assertArrayEquals(new String[]{"hh", "ooo", "rt", "kk", "or", "hgf", "trr", "yu", "ig", "kg"},
                hitRateFinder("rt qw qw hgf hh ooo kk cvbd kk kk or  trb trb trb ig or ig dfg hgf ig or or yu kk kk kk yu kg kg kg yu yu yu kjh kk kk kk hh rt hgf rt or or hgf or hgf or hgf rt rt rt rt rt rt ooo rt trr hh trr trr trr trr trr ooo hh ooo hh ooo hh hh ooo hh ooo hh ooo hh ooo hh ooo hh hh"));
    }

    /**
     * тест с пустой строкой
     */
    @Test
    void test4() {
        assertThrows(NullPointerException.class, () -> hitRateFinder(""));
    }
}
