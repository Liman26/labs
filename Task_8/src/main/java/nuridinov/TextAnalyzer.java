package nuridinov;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * класс для анализа текста
 */
public class TextAnalyzer {
    /**
     * Метод для частотного анализа текста
     * @param string исходный текст
     * @return Массив из максимум 10, наиболее частых слов
     * @throws NullPointerException исключение срабатывает, если в метод передать пустую строку
     */
    public static String[] frequencyAnalyzer(String string) throws NullPointerException {
        if (!string.isEmpty()) {
            String divider = " ";
            String[] arrayOfWords = string.split(divider);
            HashMap<String, Integer> map = new HashMap<>();
            List<String> listOfWords = Stream.of(arrayOfWords)
                    .map(String::toUpperCase).sorted()
                    .collect(Collectors.toList());
            listOfWords.forEach(word -> map.put(word, Collections.frequency(listOfWords, word)));

            return map.entrySet().stream()
                    .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                    .limit(10)
                    .map(Map.Entry::getKey)
                    .toArray(String[]::new);
        } else throw new NullPointerException();
    }
}
