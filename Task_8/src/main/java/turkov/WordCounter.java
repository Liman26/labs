package turkov;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Класс, который имеет метод подчета уникальных слов в тексте.
 */
public class WordCounter {

    /**
     * Метод, который принимает в качестве параметра текст, где слова разделены пробелами или знаками припинания и
     * выполняет подсчет частоты появления уникальных слов. Подсчет слов осуществляется без учета регистра.
     * Словом считается любая непрерывная последовательность символов (буквы и цифры).
     *
     * @param text - исходный текст для обработки
     * @return массив из <=10 (в зависимости от текста) уникальных слов, который отсотритрован сначала
     * по частоте упоминания, а затем в лексикографическом порядке (среди слов с одинаковой частотой)
     */
    public static String[] counting(String text) {

        return Stream.of(text.trim().split("[\\s\\W]+"))
                .map(String::toUpperCase)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .sorted(Map.Entry.<String, Long>comparingByValue().reversed().thenComparing(Map.Entry.comparingByKey()))
                .limit(10)
                .map(Map.Entry::getKey)
                .toArray(String[]::new);
    }
}