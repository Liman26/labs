package bachishche;


import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс, реализующий утилитный метод, который принимает на вход текст и
 * вычисляет частоту повторения слов без учета регистра
 */
public class WordsCounter {

    /**
     * Метод, определяющий частоту появления слов в тексте.
     * Слова разделены пробелами. Метод находит 10 часто встречающихся слов
     * и возвращает список, в котором слова расположены в порядке увеличения числа повторений,
     * если слова повторяются одинаковое число раз - они возвращаются в лексикографическом порядке.
     * Если слов меньше, чем 10 - возвращаются все слова.
     *
     * @param text Исходный текст
     * @return отсортированный список из 10 (или меньше)
     * наиболее часто встречающихся в тексте слов.
     */
    static String[] getTop10Word(String text) {
        if (text == null)
            throw new NullPointerException();
        String[] words = text.toLowerCase(Locale.ROOT).split(" ");

        return Arrays.stream(words)
                .distinct().collect(Collectors
                        .toMap(x -> x, x -> (int) Arrays.stream(words).
                                filter(y -> y.equals(x)).count()))
                .entrySet().stream()
                .sorted((x, y) -> {
                    if (x.getValue().equals(y.getValue()))
                        return x.getKey().compareTo(y.getKey());
                    return y.getValue() - x.getValue();
                })
                .map(Map.Entry::getKey).limit(10)
                .toArray(String[]::new);
    }

}

