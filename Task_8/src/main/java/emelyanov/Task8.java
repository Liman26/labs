package emelyanov;

import java.util.*;
import java.util.stream.Collectors;

public class Task8 {
    public static void main(String[] args) {
        String[] result = calcWord("3three 3three 1One 2two 2two 1one 1one 4fore 4fore 5five", 3);
        Arrays.stream(result).forEach(System.out::println);
    }

    /**
     * Написать метод, который на вход принимает текст
     * Метод должен посчитать частоту появления слов, и отдать массив из 10 слов которые чаще всего встретились в тексте.
     * Если в тексте меньше 10 уникальных слов, то метод отдаст сколько есть.
     * Если в тексте некоторые слова имеют одинаковую частоту, то отдаем их в лексикографическом порядке.
     * Слово это любая непрерывная последовательность символов. Даже с символами точка, запятая
     * Подсчет слов будем делать без учета регистра
     * Для решения задачи не использовать циклы а использовать стримы
     *
     * @param text  - текст по которуму ножно считать слова
     * @param count - количество искомых слов
     * @return массив слов которые чаще всего встретились в тексте
     */
    public static String[] calcWord(String text, int count) {
        if (text == null || count <= 0) {
            return new String[]{};
        }
        try {
            return Arrays.stream(text.split(" ")).map(String::toLowerCase)
                    .collect(Collectors.groupingBy(item -> item, Collectors.counting()))
                    .entrySet()
                    .stream()
                    .sorted((k1, k2) -> {
                        int compareValue = -k1.getValue().compareTo(k2.getValue());
                        if (compareValue == 0) {
                            return k1.getKey().compareTo(k2.getKey());
                        }
                        return compareValue;
                    })
                    .limit(count)
                    .map(Map.Entry::getKey)
                    .toArray(String[]::new);
        } catch (Exception err) {
            err.printStackTrace();
        }
        return new String[]{};
    }
}
