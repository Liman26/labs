package solovyev;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Класс WordFrequency,
 * содержит один метод для анализа текста на частоту появления слов в тексте
 */
public class WordFrequency {
    /**
     * Статический метод analyze,
     * записывает в хеш мапу текст разделенный на слова,
     * считая сколько раз они появялются в тексте.
     * Словом считает набор символов отделенный пробелом.
     *
     * @param text исходный текст
     * @return массив из наиболее часто встречаемых слов (до 10 ти)
     * @throws NullPointerException, если на вход поступает пустая строка
     */
    public static String[] analyze(String text) throws NullPointerException {
        if (text.trim().isEmpty())
            throw new NullPointerException();

        ArrayList<String> words = Stream.of(text.trim().split(" +")).map(String::toUpperCase).collect(Collectors.toCollection(ArrayList::new));
        HashMap<String, Integer> map = new HashMap<>();

        words.stream().forEach(word -> map.put(word, Collections.frequency(words, word)));

        return map.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .limit(10)
                .map(Map.Entry::getKey)
                .toArray(String[]::new);
    }
}
