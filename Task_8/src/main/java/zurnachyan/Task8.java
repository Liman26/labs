package zurnachyan;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Класс для реализации лабораторной работы.
 */
public class Task8 {
    public static void main(String[] args) {
        System.out.println(countWords("Hello world hello World HELLO WORLD Hello Steve how are you Steve My name is Valeriy"));
    }

    /**
     * Метод получает строку(текст), разбивает на слова и подсчитывает количества повторений
     * разных слов в данном тексте. На выход метод подает список 10 (если есть, иначе сколько есть) слов,
     * которые встречаются в тексте чаще всего, список отсортирован по количеству повторений, в порядке убывания.
     * @param text исходный текст
     * @return отсортированный список 10 (или меньше) слов, наиболее часто встречающихся в исходном тексте
     */
    public static List<String> countWords(String text){
        HashMap<String, Integer> wordsAndCounts = new HashMap<>();

        Stream.of(text).map(x-> x.split(" ")).flatMap(Arrays::stream).map(String::toLowerCase)
                .distinct().sorted().forEach(x-> wordsAndCounts.put(x, 0));

        Stream.of(text).map(x-> x.split(" ")).flatMap(Arrays::stream).map(String::toLowerCase)
                .forEach(x-> wordsAndCounts.replace(x, wordsAndCounts.get(x) + 1));

        return wordsAndCounts.entrySet().stream().sorted((x1, x2) ->{
            if(x1.getValue() != x2.getValue())
                return x2.getValue() - x1.getValue();
            else
                return x1.getKey().compareTo(x2.getKey());
        }).limit(10).map(Map.Entry::getKey).collect(Collectors.toList());
    }
}
