package troitskiy;

import java.util.*;
import java.util.stream.*;

/**
 * Класс с методом для выполнения задания по 8 ЛР
 *
 * Написать метод, который на вход принимает текст
 * Метод должен посчитать частоту появления слов, и отдать массив из 10 слов которые чаще всего встретились в тексте. Если в тексте меньше 10 уникальных слов, то метод отдаст сколько есть. Если в тексте некоторые слова имеют одинаковую частоту, то отдаем их в лексикографическом порядке.
 * Слово это любая непрерывная последовательность символов. Даже с символами точка, запятая
 *
 *
 * Подсчет слов будем делать без учета регистра
 * Для решения задачи не использовать циклы а использовать стримы:)
 */
public class TUtil
{
    /**
     * метод, который на вход принимает текст
     * Метод должен посчитать частоту появления слов, и отдать массив из 10 слов которые чаще всего встретились в тексте. Если в тексте меньше 10 уникальных слов, то метод отдаст сколько есть. Если в тексте некоторые слова имеют одинаковую частоту, то отдаем их в лексикографическом порядке.
     * Слово это любая непрерывная последовательность символов. Даже с символами точка, запятая
     *
     * Подсчет слов будем делать без учета регистра
     * Для решения задачи не использовать циклы а использовать стримы:)
     *
     * @param str -Входной текст
     * @return -Масив из 10 слов которые чаще всего встретились в тексте
     */
    public static String[] Task8(String str)
    {
        class WordCount implements Comparable<WordCount>
        {
            public final String word;
            public final long count;

            /**
             * Конструктор
             * @param word -слово
             * @param count -число таких слов в строке
             */
            public WordCount(String word, long count)
            {
                this.word = word;
                this.count = count;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                WordCount wordCount = (WordCount) o;
                return Objects.equals(word, wordCount.word);
            }

            @Override
            public int hashCode() {
                return Objects.hash(word);
            }

            @Override
            public int compareTo(WordCount o) {
                if (count > o.count) return -1;
                if (count < o.count) return 1;

                return word.compareTo(o.word);
            }

        }

        return Stream.of(str.toLowerCase().split("\\s"))
                                .map((x)-> new WordCount(x, Stream.of(str.toLowerCase().split("\\s")).filter(x::equals).count()))
                                .distinct()
                                .sorted()
                                .map((x)-> x.word)
                                .limit(10)
                                .toArray(String[]::new);
    }
}





