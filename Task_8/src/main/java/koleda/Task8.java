package koleda;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Task8 {
    public static String [] commonWords(String text){
        Map<String, Integer> map = new HashMap<>();

        Arrays.stream(text.split(" ")).map(String::toLowerCase).forEach(word -> {
            if (map.containsKey(word)) map.put(word, map.get(word) + 1);
            else                       map.put(word, 1);
        });

        return map.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .limit(10)
                .map(Map.Entry::getKey)
                .toArray(String[]::new);
    }
}
