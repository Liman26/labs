package mescheryakov;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс, содержащий метод по определению частоты появления слов в тексте
 */
public class WordFrequency {

    /**
     * Статистический метод, реализующий извлечение из текста
     * наиболее повторящихся слов в виде массива
     *
     * @param text  принятый на входе текст
     * @param count количество повторяющихся слов
     * @return массив повторящихся слов, от наиболее повторящихся к менее
     */
    public static String[] frequency(String text, int count) {
        Map<String, Integer> map = new HashMap<>();

        Arrays.stream(text.split(" ")).map(String::toLowerCase).forEach(sym -> {
            if (map.containsKey(sym)) {
                map.replace(sym, map.get(sym) + 1);
            } else {
                map.put(sym, 1);
            }
        });
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .limit(count)
                .map(Map.Entry::getKey)
                .toArray(String[]::new);

    }
}
