package vereshchagina;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс, содержащий метод, определяющий топ 10 слов, повторяющихся в тексте чаще всего
 */
public class Task8 {
    /**
     * Метод определяет частоту появления слов, и возвращает массив из 10 слов,
     * которые чаще всего встретились в тексте. Если в тексте меньше 10 уникальных слов,
     * то метод отдаст сколько есть. Если в тексте некоторые слова имеют одинаковую частоту,
     * то отдаем их в лексикографическом порядке.
     * Слово это любая непрерывная последовательность символов. Даже с символами точка, запятая
     *
     * @param text заданный текст
     * @return массив String из 10 (или меньше) уникальных слов
     * @throws NullPointerException если передали пустую строку
     */
    public static String[] hitRateFinder(String text) {
        if (text.length() == 0)
            throw new NullPointerException();
        String[] array = text.toLowerCase().split(" ");
        return Arrays.stream(array).collect(Collectors.groupingBy(str -> str, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted((a1, a2) -> {
                            int t = a2.getValue().compareTo(a1.getValue());
                            return t == 0 ? a1.getKey().compareTo(a2.getKey()) : t;
                        }
                )
                .limit(10)
                .map(Map.Entry::getKey)
                .toArray(String[]::new);

    }

}
