package gushchin;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Класс с кастомной утилитой для String, для поиска часто используемых слов в строке
 */
public class StringUtil {
    /**
     * Метод, возвращающий массив, состящий из не более чем 10 самых частоиспользуемых слов.
     * Словом в контексте метода является любая последовательность буквенных и/или цифровых символов.
     * Частота использования слов учитываться без к привязки к регистру буквенных символов
     *
     * @param text - входной текст
     * @return массив из не более чем 10 самых часто используемых слов, отсортированных по частоте использование
     * и по лексикографическому порядку
     */
    public static String[] getCommonsWordsByText(String text) {
        return Stream.of(text.trim().split("[\\s\\W]+"))
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(t -> t, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
                .limit(10)
                .map(Map.Entry::getKey)
                .toArray(String[]::new);
    }
}
