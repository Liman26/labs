package nuridinov;

import java.io.IOException;
import java.util.Scanner;

/**
 * Консольный калькулятор, умеющий производить базовык операции
 */
public class Calculator {

    /**
     * Выполнение простейших операций между двумя числами
     *
     * @param a         - первое числа(операнд)
     * @param operation - выполняемая операция
     * @param b         - второе число(операнд)
     * @return результат
     */
    public static double Calculation(double a, char operation, double b) {
        double result = 0;
        switch (operation) {
            case ('+') -> result = a + b;
            case ('-') -> result = a - b;
            case ('*') -> result = a * b;
            case ('/') -> result = a / b;
            default -> System.out.println("Выбрана неверная операция!");
        }
        return result;
    }

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        char answer = 'n';
        while (answer == 'n') {
            System.out.println("Введите первый операнд: ");
            double a = scanner.nextDouble();
            System.out.println("Выберите операцию('+', '-', '*', '/'): ");
            char operation = scanner.next().charAt(0);
            System.out.println("Введите второй операнд: ");
            double b = scanner.nextDouble();
            while (operation == '/' && b == 0) {
                System.out.println("Деление на ноль не возможно, введите другое число!");
                b = scanner.nextDouble();
            }
            System.out.println("Введенное выражение: " + a + operation + b);
            System.out.println("Результат вычислений: " + Calculation(a, operation, b));
            System.out.println("Завершить работу? y/n");
            answer = scanner.next().charAt(0);
        }


    }
}
