package zhirnov;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        Scanner scanner = new Scanner(System.in);
        String term;
        System.out.println("Вводите выражение, вставляя пробел между символами");
        System.out.println("Для завершения работы программы введите Exit");
        while (true) {
            term = scanner.nextLine();
            if (term.equals("Exit")) {
                break;
            }
            System.out.println(calculator.calculate(term));
        }
    }
}
