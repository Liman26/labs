package zhirnov;

/**
 * Класс, реализующий функционал калькулятора
 */
public class Calculator {
    /**
     * Метод, который разбирает выражение на состовляющие, определяет нужную операцию и вызывает ее.
     *
     * @param term - разбираемое выражение, написанное через пробелы
     * @return - результат введенного выражения
     */
    public String calculate(String term) {
        String[] string = term.split(" ");
        String result = null;
        try {
            double number1 = Double.parseDouble(string[0]);
            double number2 = Double.parseDouble(string[2]);
            String sign = string[1];
            switch (sign){
                case "+": result = add(number1, number2); break;
                case "-": result = dif(number1, number2); break;
                case "*": result = mult(number1, number2); break;
                case "/": result = div(number1, number2); break;
                case "%": result = remOfDiv(number1, number2); break;
                case "^": result = pow(number1, number2);break;
                default: result = "Введено некорректное выражение!";break;
            }
        } catch (Exception e) {
            result = "Выражение введено в неверном формате!";
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Метод, вычисляющий сумму двух чисел
     *
     * @param a - первое число
     * @param b - второе число
     * @return - результат суммы
     */
    private String add(double a, double b) {
        return a + " + " + b + " = " + (a + b);
    }

    /**
     * Метод, вычисляющий разность двух чисел
     *
     * @param a - первое число
     * @param b - второе число
     * @return - результат разности
     */
    private String dif(double a, double b) {
        return a + " - " + b + " = " + (a - b);
    }

    /**
     * Метод, вычисляющий произведение двух чисел
     *
     * @param a - первое число
     * @param b - второе число
     * @return - результат умножения
     */
    private String mult(double a, double b) {
        return a + " * " + b + " = " + (a * b);
    }

    /**
     * Метод, вычисляющий деление двух чисел
     *
     * @param a - первое число
     * @param b - второе число
     * @return - результат деления
     */
    private String div(double a, double b) {
        if (b == 0) {
            return "Делить на ноль нельзя!";
        } else {
            return a + " / " + b + " = " + (a / b);
        }
    }

    /**
     * Метод, вычисляющий число в степени
     *
     * @param a - число
     * @param b - заданная степень
     * @return - результат возведения в степень
     */
    private String pow(double a, double b) {
        double result = a;
        for (int i = 2; i <= b; i++) {
            result *= a;
        }
        return a + " ^ " + b + " = " + result;
    }

    /**
     * Метод, вычисляющий остаток от деления
     *
     * @param a - первое число
     * @param b - второе число
     * @return - остаток от деления
     */
    private String remOfDiv(double a, double b) {
        if (b == 0) {
            return "Делить на ноль нельзя!";
        } else {
            return a + " % " + b + " = " + (a % b);
        }

    }
}
