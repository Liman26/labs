package koleda;

import java.util.Scanner;

public class Task3 {

    /**
     * Реализация калькулятора
     */
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int a = 1;
        while (a == 1) {
            System.out.println("Введите первое число: ");
            double x = sc.nextDouble();
            System.out.println("Введите второе число: ");
            double y = sc.nextDouble();
            System.out.println("Введите оператор: ");
            String o = sc.next();
            switch (o) {
                case "+":
                    System.out.println(x + y);
                    break;
                case "-":
                    System.out.println(x - y);
                    break;
                case "*":
                    System.out.println(x * y);
                    break;
                case "/":
                    if (y == 0)
                        System.out.println("На ноль делить нельзя");
                    else
                        System.out.println(x / y);
                    break;
                default:
                    System.out.println("Ошибка");
            }
            System.out.println("Повторить? (1 - Да, 0 - Нет)");
            a = sc.nextInt();
        }
    }
}
