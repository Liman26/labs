package mescheryakov;

import java.util.Scanner;

public class Calculate {
    static Scanner scanner = new Scanner(System.in);

    /**
     * статический метод, реализующий ввод желаемой операции
     * @return возвращает введенный в консоли char-символ
     */
    static char getOperation() {
        System.out.println("Введите операцию: ");
        return scanner.next().charAt(0);
    }

    /**
     * статический метод, реализующий ввод двух чисел
     * @return возвращает массив из двух, введеных в консоли, чисел
     */
    static double[] getNumbers() {
        System.out.println("Введите числа через пробел: ");
        double[] numbers = new double[2];
        for (int i = 0; i < 2; ++i) {
            numbers[i] = scanner.nextDouble();
        }
        return numbers;
    }

    /**
     * статический метод, производящий вычисления над введенными числами
     * @param numbers массив из двух чисел
     * @param operation операция над числами
     * @return возращает результат вычисления
     */
    static double calc(double[] numbers, char operation) {
        double result;
        if (operation == '+') {
            result = numbers[0] + numbers[1];
        } else if (operation == '-') {
            result = numbers[0] - numbers[1];
        } else if (operation == '*') {
            result = numbers[0] * numbers[1];
        } else if (operation == '/') {
            result = numbers[0] / numbers[1];
        } else {
            System.out.println("Вы ошиблись при вводе оператора. Повторите ввод..");
            result = calc(numbers, getOperation());
        }
        return result;
    }

    public static void main(String[] args) {
        String a;
        do {
            char operation = getOperation();
            double[] numbers = getNumbers();
            double result = calc(numbers, operation);
            System.out.println("Результат равен " + result);
            System.out.println("Хотите произвести еще одно вычисление? Тогда введите 'Да', либо 'Нет', если хотите выйти");
            a = scanner.next();
        } while (a.equalsIgnoreCase("Да"));
    }
}