package gushchin;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, описывающий арифметическое выражение
 */
public class Expression {
    private final String x;
    private final String y;
    private final String operator;

    public Expression(String x, String y, String operator) {
        this.x = x;
        this.y = y;
        this.operator = operator;
    }

    /**
     * Getter поля x
     * @return x
     */
    public String getX() {
        return x;
    }

    /**
     * Getter поля y
     * @return y
     */
    public String getY() {
        return y;
    }

    /**
     *  Getter поля operand
     * @return operand
     */
    public String getOperator() {
        return operator;
    }

    /**
     * Метод, проверяющий, что операдны выражения введены корректно
     * @return результат проверки корректно введенных операндов
     */
    public boolean checkOperandsCorrectness() {
        String pattern = "(\\d+[,.]\\d+)|(\\d+)";

        return Pattern.matches(pattern, x) && Pattern.matches(pattern, y);
    }

    /**
     * Метод, проверяющий, что оператор выражения введен корректно
     * @return результат проверки корректно введенного оператора
     */
    public boolean checkOperatorCorrectness() {
        return Pattern.matches("[*\\-+/]", operator) && operator.length() == 1;
    }

    /**
     *  Метод для парсинга строкового операнда, введенного в консоле, к типу double
     * @param operand - строковый операнд выражения
     * @return распаршеный операнд типа double
     */
    static double parseOperand(String operand) {
        Pattern pattern = Pattern.compile(",");
        Matcher matcher = pattern.matcher(operand);

        if(matcher.find()) {
            return Double.parseDouble(matcher.replaceFirst("."));
        }

        return Double.parseDouble(operand);
    }
}
