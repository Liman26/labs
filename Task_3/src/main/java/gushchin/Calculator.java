package gushchin;

import java.util.regex.Pattern;

/**
 * Класс калькулятора для выполнения простых арифметических операций над 2 числами
 */
public class Calculator {
    /**
     * Метод, вычисляющий полученное строковое выражение
     * @param inputExpression арифметическое выражение
     * @return результат арифметического выражения или сообщение об ошибке и 0
     */
    public double calculate(String inputExpression) {
        Expression expression = parseExpression(inputExpression);

        if (!expression.checkOperandsCorrectness()) {
            System.out.println("Операнды введены некорректно");
            return 0;
        }

        if(!expression.checkOperatorCorrectness()) {
            System.out.println("Оператор введен некорректно");
            return 0;
        }

        return getOperationResult(expression);
    }

    /**
     * Метод, разбирающий строку на атомарные компоненты
     * @param expression арифметическое выражение
     * @return объект класса Expression с атомарными компонентами арифметического выражения
     */
    private static Expression parseExpression(String expression) {
        Pattern pattern = Pattern.compile("\\s");
        String[] matches = pattern.split(expression);

        return new Expression(matches[0], matches[2], matches[1]);
    }

    /**
     * Метод для суммирования двух операндов
     * @param x операнд
     * @param y операнд
     * @return сумма
     */
    private double getSum(double x, double y) {
        return x + y;
    }

    /**
     * Метод для вычитания двух операндов
     * @param x операнд
     * @param y операнд
     * @return разность
     */
    private double getDifference(double x, double y) {
        return x - y;
    }

    /**
     * Метод для умножения двух операндов
     * @param x операнд
     * @param y операнд
     * @return произведение
     */
    private double getProduct(double x, double y) {
        return x * y;
    }

    /**
     * Метод для деления двух операндов
     * @param x операнд
     * @param y операнд
     * @return частное
     */
    private double getQuotient(double x, double y) {
        return x / y;
    }

    /**
     * Метод, производящий арифметическую операцию над выражением
     * @param expression объект класса арифметического выражения
     * @return результат арифметического выражения
     */
    private double getOperationResult(Expression expression) {
        double x = Expression.parseOperand(expression.getX());
        double y = Expression.parseOperand(expression.getY());
        double result;

        switch (expression.getOperator()) {
            case "+" -> result = getSum(x, y);
            case "-" -> result = getDifference(x, y);
            case "/" -> result = getQuotient(x, y);
            case "*" -> result = getProduct(x, y);
            default -> result = 0;
        }

        return result;
    }
}
