package gushchin;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите арифметическую операцию в формате [Операнд] [Оператор] [Операнд]");
        System.out.println("[Операнд] - положительное, нецелое(опционально) число");
        System.out.println("[Оператор] - арифметическая операция (+, -, *, /)");
        String inputExpression = scanner.nextLine();
        if(!inputExpression.trim().isEmpty()) {
            System.out.println("Результат: " + calculator.calculate(inputExpression));
        }
    }
}
