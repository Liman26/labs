package zurnachyan;

import java.util.Scanner;

/**
 * Основной класс лабораторной работы
 */
public class Task3 {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);

        double a, b;
        String operator;


        while(true){
            System.out.println("Введите оперцию (+, -, *, /, %, exit)... ");
            operator = scan.nextLine();

            if (operator.equals("exit")) {
                break;
            } else {
                if (operator.length() == 1){
                    System.out.println("Введите первый операнд ...");
                    a = scan.nextDouble();
                    System.out.println("Введите второй операнд ...");
                    b = scan.nextDouble();
                    NewCalc.calc(a, b, operator);
                }
            }

        }
    }
}
