package zurnachyan;

/**
 * Класс реализует элементарный калькулятор с элементарными математическими операциями
 * (один вызов - одно действие).
 */
public class NewCalc {

    /**
     * Метод определяет какую операцию нужно выполнить, затем выводит результат в консоль.
     * @param a 1-й операнд
     * @param b 2-й операнд
     * @param op строка, содержащая символ необходимой операции
     */
    public static void calc(double a, double b, String op){
        switch (op) {
            case "+" : System.out.println("Ответ: " + plus(a, b));
            case "-" : System.out.println("Ответ: " + minus(a, b));
            case "*" : System.out.println("Ответ: " + multiple(a, b));
            case "/" : System.out.println("Ответ: " + div(a, b));
            case "%" : System.out.println("Ответ: " + mod(a, b));
            default : System.out.println("Операция не поддерживается");
        }
    }

    /**
     * Метод возвращает сумму двух вещественных чисел
     * @param a 1-е слагаемое
     * @param b 2-е слагаемое
     * @return сумма
     */
    private static double plus(double a, double b){
        return a + b;
    }

    /**
     * Метод возвращает разность двух вещественных чисел
     * @param a уменьшаемое
     * @param b вычитаемое
     * @return разность
     */
    private static double minus(double a, double b){
        return a - b;
    }

    /**
     * Метод возвращает произведение двух вещественных чисел
     * @param a 1-й множитель
     * @param b 2-й множитель
     * @return произведение
     */
    private static double multiple(double a, double b){
        return a * b;
    }

    /**
     * Метод возвращает частное двух вещественных чисел
     * @param a делимое
     * @param b делитель
     * @return частное
     */
    private static double div(double a, double b){
        return a / b;
    }

    /**
     * Метод возвращает остаток от деления двух вещественных чисел
     * @param a делимое
     * @param b делитель
     * @return остаток от деления
     */
    private static double mod(double a, double b){
        return a % b;
    }
}
