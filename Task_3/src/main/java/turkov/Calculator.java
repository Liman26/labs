package turkov;

import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Класс, реализующий простейший калькулятор, который может выполнять основные арифметические операции с двумя числами:
 * сложение +
 * вычитание -
 * умножение *
 * деление /
 * остаток от деления %
 */
public class Calculator {

    private static final Scanner input = new Scanner(System.in);

    /**
     * Метод получает на вход два числа и тип операции, в зависимости от которой произодит соответствующие вычисления
     *
     * @param operandOne - первое число (левый операнд)
     * @param operandTwo - второе число (правый операнд)
     * @param operation  - тип операции
     * @return - результат операции надо полученными числами
     */
    private static double calculate(double operandOne, double operandTwo, char operation) {
        switch (operation) {
            case '+' -> operandOne += operandTwo;
            case '-' -> operandOne -= operandTwo;
            case '*' -> operandOne *= operandTwo;
            case '/' -> operandOne /= operandTwo;
            case '%' -> operandOne %= operandTwo;
        }
        return operandOne;
    }

    /**
     * Метод, который считывает операнд для выбранной пользовталем арифметической операции и выводит предупреждение,
     * если польльзователь ввел не число или слишком большое значение числа.
     *
     * @param message - строка, в которой передается сообщение пользователю о вводе 1-го и 2-го операндов
     * @return - числовое значение полученное от пользователя
     */
    private static double inputOperand(String message) {
        double numbers;
        Scanner in = new Scanner(System.in).useLocale(Locale.ROOT);

        while (true) {
            System.out.print(message);
            String operand = in.nextLine();

            try {
                numbers = Double.parseDouble(operand);
                if (Double.isNaN(numbers) || Double.isInfinite(numbers)) {
                    throw new NumberFormatException("Введено NaN или Infinity");
                } else return numbers;
            } catch (NumberFormatException ex) {
                System.out.println("Ошибка, введено некорректное значение. " + ex.getMessage());
            }
        }
    }

    /**
     * Метод, который считывает и проверяет на корректность тип желаемой операции
     *
     * @return - символ операции
     */
    private static char inputOperator() {
        do {
            System.out.print("Введите символ типа операции (+ - / * %): ");
            String operator = input.next();
            boolean matches = Pattern.matches("[-+*/%]", operator);
            if (matches) {
                return operator.charAt(0);
            } else {
                System.out.println("Ошибка, введена неподдерживаемая операция");
            }
        } while (true);
    }

    public static void main(String[] args) {
        double operandOne, operandTwo;

        do {
            operandOne = inputOperand("Введите первый операнд (число): ");
            char operator = inputOperator();
            operandTwo = inputOperand("Введите второй операнд (число): ");
            System.out.printf("%.2e %s %.2e = %.2e", operandOne, operator, operandTwo, calculate(operandOne, operandTwo, operator));
            System.out.print("\n\nДля окончания вычислений введите \"Q\", для продолжения любой символ: ");
            if (input.next().equals("Q")) break;
        } while (true);
    }
}