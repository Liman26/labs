package solovyev_nikolay;

import java.util.Scanner;

/** Класс TASK3 формирует работу простого калькулятора в цикле;
 *  В классе используются переменные:
 * @param firstI - переменная целого типа, хранящая 1 введенное число;
 * @param secondI - переменная целого типа, хранящая 2 введенное число;
 * @param flagI - переменная целого типа, флаг присвоения введенного числа переменной firstI;
 * @param flagExit - переменная целого типа, флаг определения выхода из цикла метода Main;
 * @param znak - переменная строкового типа,  хранит введенную операцию;
 * @param scanner - переменная, создает экземпляр объекта типа Scanner;
 */

public class Task3 {
    static int firstI;
    static int secondI;
    static int flagI;
    static int flagExit;
    static String znak;
    static Scanner scanner = new Scanner(System.in);

    /**
     * метод, реализующий алгоритм работы калькулятора:
     * Ввод первого числа, ввод второго числа, ввод операции;
     * Проверка на завершение бесконечного цикла метода Main;
     */
    static void calculator() {
        flagI = 0;
        System.out.println("Введите первое число(int):");
        checkInt();
        System.out.println("Введите второе число(int):");
        checkInt();
        System.out.println("Введите операцию(одну из +-*/):");
        checkZnak();
        System.out.println("Для выхода -  введите символ ~, для повторного расчета - введите любой символ:");
        if (scanner.next().equals("~")) {
            flagExit=1;
            System.out.println("Выход!");
        }else{
            System.out.println("Начинаем новый расчет");
        }
    }

    /**
     * метод проверки на ввод первого и второго целого числа;
     */
    static void checkInt() {
        if (scanner.hasNextInt()) {
            int number = scanner.nextInt();
            if (flagI == 0) {
                firstI = number;
                flagI = 1;
            }else{
                secondI = number;
                flagI = 2;
            }
        } else {
            System.out.println("Введенное значение не является целым числом типа int, попробуйте еще раз!");
            scanner.next();
            calculator();
        }
    }

    /**
     * метод проверки символа операции.
     */
    static void checkZnak() {
                znak = scanner.next();
                if(znak.equals("+")) {
                    System.out.println("Сумма введенных чисел равна " + (firstI + secondI));
                }else if(znak.equals("-")) {
                    System.out.println("Разность введенных чисел равна " +  (firstI - secondI));
                }else if(znak.equals("*")) {
                    System.out.println("Произведение введенных чисел равно " + (firstI * secondI));
                }else if(znak.equals("/")) {
                    if (secondI == 0) {
                        System.out.println("Делить на ноль нельзя! Начинаем сначала.");
                        calculator();
                    } else {
                        System.out.println("Частное от деления введенных чисел равно " + (firstI / secondI));
                    }
                }else if (znak.length() > 0){
                    System.out.println("Введена неверная операция. Попробуйте еще раз!");
                    System.out.println("Введите операцию(одну из +-*/):");
                    scanner.next();
                    checkZnak();
                }
    }
    /**
     * Основное тело программы. Запуск метода расчета.
     * @param args
     */
    public static void main(String[] args) {
        while (flagExit == 0){
            calculator();
        }
    }
}

