package vereshchagina;

import java.util.Scanner;

/**
 * Класс Calculator, выполняет указанную арифметическую операцию (+, -, *, /)
 * над двумя указанными операндами типа double
 * содержит 6 методов (1 публичный, для вызова в программе,
 * 5 приватных)
 */
public class Calculator {
    /**
     * метод, вызываемый в программе.
     * Выполняет роль интерфейса, т.е. отвечает за взаимодействие с пользователем,
     * указывает, какие данные необхордимо ввести и запускает вычисление,
     * содержит цикл, избавляющий пользователя от необходимости заново запускать метод для новых вычислений
     */
    public static void interCalculations() {
        char flag, operator;
        double operand1, operand2;
        Scanner scan = new Scanner(System.in);

        System.out.println("Вы запустили калькулятор.\nОбозначения при вводе оператора:\nсложение: +\nвычитание: -\nумножение: *\nделение: /");
        do {
            System.out.println("Введите два операнда (через пробел или enter):");

            operand1 = scan.nextDouble();
            operand2 = scan.nextDouble();

            System.out.println("Введите оператор:");
            operator = scan.next().charAt(0);
            startOfCalculations(operator, operand1, operand2);
            System.out.println("Чтобы продолжить работу в калькуляторе, введите символ \"r\", для выхода любой другой символ");
            flag = scan.next().charAt(0);
        } while (flag == 'r');
    }

    /**
     * метод, определяющий введенную операцию и запускающий выполнение соответствующей функции
     * на вход получает оператор и два операнда.
     * вызывает метод для вычисления, соответствующий указанному оператору,
     * или же сообщает об ошибке, если оператор введен не верно.
     * в первом случае выведет результат и вернет true
     * во втором выведет сообщение об ошибке и вернет false
     *
     * @param operator оператор
     * @param operand1 первый операнд
     * @param operand2 второй операнд
     * @return true при успешном выполнении операции, false при не верном вводе оператора
     */
    private static boolean startOfCalculations(char operator, double operand1, double operand2) {
        double res;
        switch (operator) {
            case '+' -> res = add(operand1, operand2);
            case '-' -> res = sub(operand1, operand2);
            case '*' -> res = mul(operand1, operand2);
            case '/' -> res = dif(operand1, operand2);
            default -> {
                System.out.println("Ошибка ввода");
                return false;
            }
        }
        System.out.println(operand1 + " " + operator + " " + operand2 + " = " + res);
        return true;
    }

    /**
     * метод вычисления суммы двух операндов
     *
     * @param operand1 первое слагаемое
     * @param operand2 второе слагаемое
     * @return сумма двух слагаемых
     */
    private static double add(double operand1, double operand2) {
        return operand1 + operand2;
    }

    /**
     * метод вычисления разности двух операндом
     *
     * @param operand1 уменьшаемое
     * @param operand2 вычитаемое
     * @return разность операндов
     */
    private static double sub(double operand1, double operand2) {
        return operand1 - operand2;
    }

    /**
     * метод вычисления произведения двух операндов
     *
     * @param operand1 первый множитель
     * @param operand2 второй множитель
     * @return произведение операндов
     */
    private static double mul(double operand1, double operand2) {
        return operand1 * operand2;
    }

    /**
     * метод вычисления частного двух операндов
     *
     * @param operand1 делимое
     * @param operand2 делитель
     * @return результат деления
     */
    private static double dif(double operand1, double operand2) {
        return operand1 / operand2;
    }
}
