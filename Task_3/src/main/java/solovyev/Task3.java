package solovyev;

import java.util.Scanner;

public class Task3 {

    /**
     * Класс реализующий функционал калькулятора
     */
    public static class Calculator {

        /**
         * Статический метод производящий вычисления
         * @param n1 первое число
         * @param op производимая операция
         * @param n2 второе число
         * @return результат операции
         */
        public static double Calculate(double n1, char op, double n2) {
            if (op == '+') {
                return (n1 + n2);
            } else if (op == '-') {
                return n1 - n2;
            } else if (op == '/') {
                return n1 / n2;
            } else if (op == '*') {
                return n1 * n2;
            } else if (op == '%') {
                return n1 % n2;
            } else {
                System.out.println("Такой операции не существует");
                return 0;
            }
        }

        /**
         * Статический метод для проверки деления на ноль
         * @param op проверяемая операция
         * @param n проверяемое число
         * @return true, если пользователь хочет произвести деление на ноль
         */
        public static boolean Checker(char op, double n) {
            if (op == '/' && n == 0) {
                return true;
            } else
                return false;
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        double numb1, numb2;
        char operation;
        boolean pass = true;

        while (pass) {
            System.out.println("Введите число 1 :");
            numb1 = input.nextDouble();

            System.out.println("Введите желаемую операцию :");
            operation = input.next().charAt(0);

            System.out.println("Введите число 2 :");
            numb2 = input.nextDouble();

            while (Calculator.Checker(operation, numb2)) {
                System.out.println("На ноль делить нельзя, введите другое число.");
                numb2 = input.nextDouble();
            }

            System.out.println("Ваше выражение " + numb1 + " " + operation + " " + numb2);

            System.out.println("Резльтат работы калькулятора: " + Calculator.Calculate(numb1, operation, numb2));

            System.out.println("Введите 1, если хотите произвести новое вычисление.");
            System.out.println("Любое другое число, если нет.");
            if (input.nextInt() != 1)
                pass = false;
        }
    }
}
