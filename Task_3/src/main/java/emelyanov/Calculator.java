package emelyanov;

/**
 * калькулятор, умеющий производить базовые вычисления
 */
public class Calculator {
  static public final String OPERATIONS = "+-*/%";
  private String error = "";
  double result;

  /**
   * Ошибка при работе калькулятора
   *
   * @return описние ошибки
   */
  public String getError() {
    return error;
  }

  /**
   * Результат работы калькулятора
   *
   * @return результат вычисления
   */
  public double getResult() {
    return result;
  }

  /**
   * Вычисление полученного выражения
   *
   * @param expression - выражения, которое необходимо посчитать
   * @return результат работы метода: = true-успешно, =false-произошла ошибка
   */
  public boolean run(String expression) {
    error = "";
    String operation = this.getOperator(expression);
    if (!error.isEmpty()) {
      return false;
    }

    int argument1 = this.parseInt(expression.substring(0, expression.indexOf(operation)).trim());
    if (!error.isEmpty()) {
      return false;
    }
    int argument2 = this.parseInt(expression.substring(expression.indexOf(operation) + 1).trim());
    if (!error.isEmpty()) {
      return false;
    }
    switch (operation) {
      case "+":
        result = argument1 + argument2;
        break;
      case "-":
        result = argument1 - argument2;
        break;
      case "*":
        result = argument1 * argument2;
        break;
      case "/":
        if (argument2 == 0) {
          error = "нельзя делить на 0";
          return false;
        }
        result = (double) argument1 / argument2;
        break;
      case "%":
        result = argument1 % argument2;
        break;
    }
    return true;
  }

  /**
   * получить одну первую операцию из выражения
   *
   * @param expression - выражения
   * @return найденная операция
   */
  private String getOperator(String expression) {
    for (int i = 0; i < OPERATIONS.length(); i++) {
      if (expression.indexOf(OPERATIONS.charAt(i)) >= 0) {
        return OPERATIONS.substring(i, i + 1);
      }
    }
    this.error = "в выражении не найдена операция";
    return "";
  }

  /**
   * Преобразовать строку в целое число
   *
   * @param argument строка содержащая число
   * @return результат преобразования
   */
  private int parseInt(String argument) {
    try {
      return (Integer.parseInt(argument));
    } catch (NumberFormatException err) {
      error = "не смог преобразовать аргумент <" + argument + "> к целому числу";
    }
    return 0;
  }
}
