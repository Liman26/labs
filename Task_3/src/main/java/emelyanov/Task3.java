package emelyanov;

import java.util.Scanner;

public class Task3 {
  public static void main(String[] args) {
    Calculator calculator = new Calculator();
    Scanner input = new Scanner(System.in);
    StringBuilder result = new StringBuilder();
    while (true) {
      System.out.println("Введите вычисляемое выражение в формате [Аргумент1][Оператор][Аргумент1], где");
      System.out.println("[Аргумент1],[Аргумент2] - положительные целые числа");
      System.out.println("[Оператор] - знак выполняемого действия над аргументами (допустимы: " + Calculator.OPERATIONS + ")");
      System.out.println("Пример: 1+2");
      System.out.println("Пустая строчка ввода означет конец работы с программой:");
      String inputExpression = input.nextLine();
      if (inputExpression.trim().isEmpty()) {
        break;
      }
      result.setLength(0);
      if (calculator.run(inputExpression.trim())) {
        result.append(inputExpression).append(" = ").append(calculator.getResult());
      } else {
        result.append("Ошибка при вычислении: ").append(calculator.getError());
      }
      System.out.println(result);
      System.out.println();
    }
    System.out.println("Спасибо !!!");
  }
}
