package bachishche;

import java.util.Scanner;

/**
 * Консольный калькулятор, производящий базовые вычисления.
 */
public class Calculator {

    private final Scanner scan = new Scanner(System.in);

    /**
     * Выделить в исходной строке операнды и знак операции,
     * посчитать результат выражения.
     * @param expr строка с исходным выражением
     * @return результат выражения
     */
    private double calculateExpr(String expr) {
        double a, b;
        char sign;
        String[] vars = expr.split(" ");
        a = Double.parseDouble(vars[0]);
        b = Double.parseDouble(vars[2]);
        sign = vars[1].charAt(0);
        return switch (sign) {
            case '+' -> a + b;
            case '-' -> a - b;
            case '*' -> a * b;
            case '%' -> a % b;
            case '/' -> a / b;
            default -> throw new IllegalStateException("Неверный знак операции: " + sign);
        };
    }

    /**
     * Вывод инструкции для пользователя, ввод переменных
     * и печать результата. Метод выполняется в цикле,
     * пока пользователь не введёт символ выхода 'n'
     */
    public void run() {
        String expr;
        while (true) {
            System.out.println("""
                    Введите, разделяя пробелами, строку вида a X b, где:
                    a - первый операнд
                    X - знак операции (+, -, *, /, %)
                    b - второй операнд.
                    Для завершения вычислений - введите любой символ 'n'
                    """);
            expr = scan.nextLine();
            if (expr.charAt(0) == 'n')
                return;
            System.out.println("Результат: " + calculateExpr(expr) + "\n");
        }
    }
}
