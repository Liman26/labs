package troitskiy;

import java.util.Scanner;

public class calc
{
    /**
     * Калькулятор
     * @param args -игнорируются
     */
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        String f = "д";

        while (f.equals("д"))
        {
            System.out.print("Введите первое число:");
            double x = scanner.nextDouble();
            System.out.print("Введите второе число:");
            double y = scanner.nextDouble();
            System.out.print("Введите оператор:");
            String o = scanner.next();
            double z;

            System.out.print("Ответ:");
            switch (o)
            {
                case "+": System.out.println(x + y); break;
                case "-": System.out.println(x - y); break;
                case "*": System.out.println(x * y); break;
                case "/": System.out.println(x / y); break;
                default : System.out.println("Ошибка");
            }

            System.out.print("Еще? (д/н):");
            f = scanner.next();
        }
    }
}
