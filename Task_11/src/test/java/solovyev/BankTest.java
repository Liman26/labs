package solovyev;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.*;
import static solovyev.MyProperties.loadProperties;

/**
 * Класс тестирующий работу банка
 */
public class BankTest {

    /**
     * Положительный сценарий 1 при котором запущено более 70 счетов и клиентов и при этом все хорошо.
     */
    @Test
    public void Scenario1() {

        loadProperties("configManyAccountsManyClients.properties");
        int bankAmount = MyProperties.BANK_AMOUNT;
        int quantityOfAccounts = MyProperties.QUANTITY_OF_ACCOUNTS;
        int quantityOfClients = MyProperties.QUANTITY_OF_CLIENTS;

        Bank bank = new Bank(BigDecimal.valueOf(bankAmount));
        List<Account> initList = accountsListGenerator(quantityOfAccounts, bankAmount);
        bank.setAccountList(initList);

        Daemon daemon = new Daemon(bank, bankAmount);
        daemon.setDaemon(true);
        daemon.start();

        startClients(quantityOfClients);

        try {
            Thread.sleep(400);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertTrue(daemon.isAlive());
    }

    /**
     * Положительный сценарий 2, запущено много клиентов, но есть только 2 счета, при этом все хорошо.
     */
    @Test
    public void Scenario2() {

        loadProperties("configManyClientsTwoAccounts.properties");
        int bankAmount = MyProperties.BANK_AMOUNT;
        int quantityOfAccounts = MyProperties.QUANTITY_OF_ACCOUNTS;
        int quantityOfClients = MyProperties.QUANTITY_OF_CLIENTS;

        Bank bank = new Bank(BigDecimal.valueOf(bankAmount));
        List<Account> initList = accountsListGenerator(quantityOfAccounts, bankAmount);
        bank.setAccountList(initList);

        Daemon daemon = new Daemon(bank, bankAmount);
        daemon.setDaemon(true);
        daemon.start();

        startClients(quantityOfClients);

        try {
            Thread.sleep(400);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertTrue(daemon.isAlive());
    }

    /**
     * Негативный сценарий
     * На один из счетов поступает сумма сверх общей денежной массы
     * Демон должен сообщить об ошибке при первой же проверке
     */
    @Test
    public void Scenario3() {

        loadProperties("configManyAccountsManyClients.properties");
        int bankAmount = MyProperties.BANK_AMOUNT;
        int quantityOfAccounts = MyProperties.QUANTITY_OF_ACCOUNTS;
        int quantityOfClients = MyProperties.QUANTITY_OF_CLIENTS;


        Bank bank = new Bank(BigDecimal.valueOf(bankAmount));
        List<Account> initList = accountsListGenerator(quantityOfAccounts, bankAmount);
        bank.setAccountList(initList);

        Daemon daemon = new Daemon(bank, bankAmount);
        daemon.setDaemon(true);

        bank.getAccountList().get(0).incoming(BigDecimal.valueOf(2000000));

        daemon.start();

        startClients(quantityOfClients);

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertFalse(daemon.isAlive());
    }

    /**
     * Метод для запуска новых клиентов
     *
     * @param n количество клиентов
     */
    public static void startClients(int n) {
        for (int i = 0; i < n; i++) {
            new Client().start();
        }
    }

    /**
     * Метод для генерирации счетов, после генерации проверяется, что сгенерировано ровно n счетов
     *
     * @param n          количество счетов
     * @param bankAmount денежная масса в банке
     * @return список из n счетов с рандомным балансом на счете
     */
    public static List<Account> accountsListGenerator(int n, int bankAmount) {
        List<Account> initList = new ArrayList<>();
        for (int i = 0; i < n - 1; i++) {
            if (bankAmount != 0) {
                int randomSum = randomAccountSum((bankAmount / n * 4));
                if (randomSum == 1) {
                    randomSum = randomAccountSum((bankAmount / n * 2) + 1);
                }
                bankAmount -= randomSum;


                initList.add(new Account(BigDecimal.valueOf(randomSum)));
            }
        }
        initList.add(new Account(BigDecimal.valueOf(bankAmount)));

        System.err.println(initList.size() + " accounts generated");

        return initList;
    }

    /**
     * Метод позволяющий генерировать количество денег на счету
     *
     * @param bankRest еще не распределенная по счетам денежная масса
     * @return случайное число в диапозоне от нуля, до bankRest+1
     */
    public static int randomAccountSum(int bankRest) {
        return ThreadLocalRandom.current().nextInt(1, bankRest + 1);
    }

}
