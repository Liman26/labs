package koleda;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class Task11Test {

    private int amountAccounts;
    private int amountClients;
    private int maxAmountTransaction;

    /**
     * 1. Положительный сценарий:
     * Запускаем N клиентов M счетов(не менее 70). Все ок.
     */
    @Test
    void positiveTest(){
        initCfg("src/test/resources/koleda/cfg1.properties");
        List<Account> accounts = new ArrayList<>();
        for (int i = 0; i < amountAccounts; i++)
            accounts.add(new Account(maxAmountTransaction));
        Bank bank = new Bank(accounts);
        Daemon daemon = Daemon.startDaemon(bank);
        for (int i = 0; i < amountClients; i++)
            new Client(bank).start();
        assertTrue(daemon.isAlive());
    }

    /**
     * 2. Негативный сценарий:
     * Даем какому-нибудь клиенту слишком большую сумму (больше общего amount). запускаем N клиентов  М счетов (не менее 70).
     * Ожидаем, что будет исключение от демона.
     */
    @Test
    void negativeTest() {
        initCfg("src/test/resources/koleda/cfg2.properties");

        List<Account> accounts = new ArrayList<>();
        for (int i = 0; i < amountAccounts; i++)
            accounts.add(new Account(maxAmountTransaction));
        if (MyConfig.getValueProperty(MyConfig.FAIL) == 1)
            accounts.get(0).setBalance(10000);

        Bank bank = new Bank(accounts);
        Daemon daemon = Daemon.startDaemon(bank);

        for (int i = 1; i < amountClients; i++)
            new Client(bank).start();
        assertFalse(daemon.isAlive());
    }

    /**
     * 3. Положительный сценарий.
     * Запускаем N клиентов (не менее 70), которые работают только с двумя счетами. Все ок.
     */
    @Test
    void positiveTwoAccountsTest() {
        initCfg("src/test/resources/koleda/cfg3.properties");

        List<Account> accounts = new ArrayList<>();
        for (int i = 0; i < amountAccounts; i++)
            accounts.add(new Account(maxAmountTransaction));
        Bank bank = new Bank(accounts);
        Daemon daemon = Daemon.startDaemon(bank);

        for (int i = 1; i < amountClients; i++)
            new Client(bank).start();
        assertTrue(daemon.isAlive());
    }

    private void initCfg(String url) {
        MyConfig.init(url);
        amountAccounts = MyConfig.getValueProperty(MyConfig.AMOUNT_ACCOUNTS);
        amountClients = MyConfig.getValueProperty(MyConfig.AMOUNT_CLIENTS);
        maxAmountTransaction = MyConfig.getValueProperty(MyConfig.MAX_AMOUNT_TRANSACTION);
    }

}
