package troitskiy;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.io.InputStream;
import java.util.Properties;

/**
 * Клас для тестирования работы банка
 * время каждого теста по 20 сек
 */
public class BankTest {

    private TBank myBank;

    /**
     * Считать параметры из файла и создать соответствующий банк
     * всех запустить, подождать 10 секунд, если надо то сломать баланс и еще подождать 10 сек
     * @param propertyFileName -имя фпайла
     * @throws Exception       -не сумел считать файл
     */
    public void LoadParamAndMakeBank(String propertyFileName) throws Exception {
        Properties properties = new Properties();
        InputStream in = TBank.class.getResourceAsStream(propertyFileName);
        properties.load(in);
        int kolvoClients = Integer.parseInt(properties.getProperty("kolvoClients", "100"));
        int kolvoAccounts = Integer.parseInt(properties.getProperty("kolvoAccounts", "1000"));
        long balans = Long.parseLong(properties.getProperty("balans", "1000000"));
        long ukrali = Long.parseLong(properties.getProperty("ukrali", "0"));

        myBank = new TBank(kolvoClients, kolvoAccounts, balans);

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        synchronized (myBank.accounts[0]) {
            myBank.accounts[0].SubRest(ukrali, myBank.useDelta);
        }

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * 1. Положительный сценарий:
     * Запускаем N клиентов M счетов(не менее 70). Все ок.
     * @throws Exception -выкинет исключение если не считает параметры
     */
    @Test
    public void test1() throws Exception {
        LoadParamAndMakeBank("bank1.properties");
        assertFalse(myBank.isFail);
    }

    /**
     * 2. Негативный сценарий:
     * Даем какому-нибудь клиенту слишком большую сумму (больше общего amount). запускаем N клиентов  М счетов (не менее 70).
     * Ожидаем, что будет исключение от демона. (заместо исключения останавливаем банк и сообщаем об этом)
     * @throws Exception -выкинет исключение если не считает параметры
     */
    @Test
    public void test2() throws Exception {
        LoadParamAndMakeBank("bank2.properties");
        assertTrue(myBank.isFail);
    }

    /**
     * 3. Положительный сценарий.
     * Запускаем N клиентов (не менее 70), которые работают только с двумя счетами. Все ок.
     * @throws Exception -выкинет исключение если не считает параметры
     */
    @Test
    public void test3() throws Exception {
        LoadParamAndMakeBank("bank3.properties");
        assertFalse(myBank.isFail);
    }
}
