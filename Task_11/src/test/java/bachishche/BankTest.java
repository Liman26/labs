package bachishche;

import org.junit.jupiter.api.Test;

import java.util.concurrent.*;

import static bachishche.BankUtils.*;
import static bachishche.BankTestProperties.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Тестирование работы клиентов с переводами между счетами в банке,
 * проверка потока-демона на определение некорректных счетов.
 */
public class BankTest {
    /**
     * Тест работы N клиентов с M счетами (не менее 70).
     * Исключений возникать не должно.
     *
     * @throws InterruptedException, ExecutionException
     */
    @Test
    public void manyAccountAndClientsTest() throws InterruptedException, ExecutionException {
        normalExecute("configTest1.properties");
    }

    /**
     * Тест работы с общей суммой счетов не равной начальной общей сумме.
     * Демон выдает сообщение об ошибке и прерывает поток, что в методе
     * Future.get() вызывает исключение ExecutionException
     */
    @Test
    public void incorrectAccountBalanceTest() {
        assertThrows(ExecutionException.class, () -> executeWithIncorrectAccountBalance("configTest2.properties"));
    }

    /**
     * Тест работы N клиентов (не менее 70) с двумя счетами.
     * Исключений (в том числе deadlock) возникать не должно.
     *
     * @throws InterruptedException
     * @throws ExecutionException   в случае обнаружения демоном ошибки
     */
    @Test
    public void twoAccountAndManyClientsTest() throws InterruptedException, ExecutionException {
        normalExecute("configTest3.properties");
    }

    /**
     * Чтение начальных конфирураций из проперти файла и запуск в соответствии с ними
     * потоков-клиентов и потока-демона.
     *
     * @param propertiesFileName имя файла с настройками в расширении .property
     * @throws InterruptedException
     * @throws ExecutionException   в случае обнаружения демоном ошибки
     */
    private static void normalExecute(String propertiesFileName) throws InterruptedException, ExecutionException {
        BankTestProperties bprop = new BankTestProperties(loadProperties(propertiesFileName));
        Bank bank = new Bank(bprop.getAccountNumber(),
                bprop.getTotalBalance());
        Daemon daemon = new Daemon(bank, bprop.getTotalBalance());
        daemon.setDaemon(true);
        ExecutorService daemonControl = Executors.newSingleThreadExecutor();
        var daemonResult = daemonControl.submit(daemon);

        ExecutorService clientsService = Executors.newFixedThreadPool(bprop.getClientNumber());
        clientsService.invokeAll(getClientList(bprop.getClientNumber(), bprop.getMaxTransaction(), bank));

        clientsService.shutdown();
        daemon.interrupt();
        daemonResult.get();
    }

    /**
     * Чтение начальных конфирураций из проперти файла, добавление в один из счетов банка
     * суммы, нарушающей начальную общую сумму счетов. Запуск потоков-клиентов и потока-демона.
     *
     * @param propertiesFileName имя файла с настройками в расширении .property
     * @throws InterruptedException
     * @throws ExecutionException   в случае обнаружения демоном ошибки
     */
    private static void executeWithIncorrectAccountBalance(String propertiesFileName) throws ExecutionException, InterruptedException {
        BankTestProperties bprop = new BankTestProperties(loadProperties(propertiesFileName));
        Bank bank = new Bank(bprop.getAccountNumber(),
                bprop.getTotalBalance());
        bank.accounts.get(0).addMoney(bprop.getTotalBalance());

        Daemon daemon = new Daemon(bank, bprop.getTotalBalance());
        daemon.setDaemon(true);
        ExecutorService daemonControl = Executors.newSingleThreadExecutor();
        var daemonResult = daemonControl.submit(daemon);

        ExecutorService clientsService = Executors.newFixedThreadPool(bprop.getClientNumber());
        clientsService.invokeAll(getClientList(bprop.getClientNumber(), bprop.getMaxTransaction(), bank));

        clientsService.shutdown();
        daemon.interrupt();
        daemonResult.get();
    }
}


