package bachishche;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Properties;

/**
 * Структура, поля которой равны полям в проперти файле.
 */
public class BankTestProperties {
    private final BigDecimal totalBalance;
    private final BigDecimal maxTransaction;
    private final int accountNumber;
    private final int clientNumber;

    /**
     * Заполнение полей структуры из проперти файла
     *
     * @param prop имя файла расширения .property с конфигурациями
     *             для тестироавния многопоточного банковского приложения
     */
    public BankTestProperties(Properties prop) {
        this.totalBalance = new BigDecimal(prop.getProperty("total_balance"));
        this.maxTransaction = new BigDecimal(prop.getProperty("max_transaction"));
        this.clientNumber = Integer.parseInt(prop.getProperty("client_number"));
        this.accountNumber = Integer.parseInt(prop.getProperty("account_number"));
    }

    /**
     * @param fileName имя файла с расширением .properties
     * @return объект класса Properties из заданного файла
     */
    public static Properties loadProperties(String fileName) {
        Properties prop = new Properties();
        try (InputStream input = BankUtils.class.getResourceAsStream(fileName)) {
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return prop;
    }

    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public BigDecimal getMaxTransaction() {
        return maxTransaction;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getClientNumber() {
        return clientNumber;
    }
}
