package emelyanov;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Properties;

import static emelyanov.Auditor.startAuditor;
import static emelyanov.Client.startClients;
import static emelyanov.Util.getProperties;
import static org.junit.jupiter.api.Assertions.*;

public class Task11Test {
    private int limitRestAccount;
    private int countAccount;
    private int countClient;
    private int countThreadInPool;

    @Test
    @DisplayName("Проверка, когда много счетов")
    public void testGood1() {
        readProperties("configManyAccount.properties");
        Bank bank = new Bank();
        bank.generateAccounts(limitRestAccount, countAccount);
        startAuditor(bank);
        startClients(limitRestAccount, countClient, countThreadInPool, bank);
        assertEquals(AuditorResults.GOOD, bank.getResultAudit());
        assertEquals(countClient,bank.getCounterTransaction());
    }

    @Test
    @DisplayName("Проверка, когда счета два")
    public void testGood2(){
        readProperties("configTwoAccount.properties");
        Bank bank = new Bank();
        bank.generateAccounts(limitRestAccount, countAccount);
        startAuditor(bank);
        startClients(limitRestAccount, countClient, countThreadInPool, bank);
        assertEquals(AuditorResults.GOOD, bank.getResultAudit());
        assertEquals(countClient,bank.getCounterTransaction());
    }
    @Test
    @DisplayName("Проверка, когда счет поменял остаток не через перевод")
    public void testBad(){
        readProperties("configManyAccount.properties");
        Bank bank = new Bank();
        bank.generateAccounts(limitRestAccount, countAccount);
        startAuditor(bank);
        bank.getAccountList().get(1).setRest(bank.getAccountList().get(1).getRest().add(BigDecimal.ONE));
        startClients(limitRestAccount, countClient, countThreadInPool, bank);
        assertEquals(AuditorResults.ERROR_LIMIT, bank.getResultAudit());
    }

    private void readProperties(String fileName) {
        Properties properties = getProperties(fileName);
        limitRestAccount = Integer.parseInt(properties.getProperty("limit.rest.account", "10"));
        countAccount = Integer.parseInt(properties.getProperty("count.account", "10"));
        countClient = Integer.parseInt(properties.getProperty("count.client", "10"));
        countThreadInPool = Integer.parseInt(properties.getProperty("count.thread.in.pool", "10"));
    }
}
