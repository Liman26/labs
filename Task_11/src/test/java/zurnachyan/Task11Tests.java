package zurnachyan;

import chupikov.Config;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Properties;

/**
 * Класс для тестов лабораторной работы.
 */
public class Task11Tests {
    private int numberOfAccounts;
    private int numberOfClients;
    private BigDecimal amountOfMoney;

    /**
     * Первый тест с положительным сценарием: 1000 клиентов, 1000 счетов, все работает
     * без потерь и переполнений
     */
    @Test
    public void test1(){
        init("configurations1.properties");

        Bank bank = new Bank(amountOfMoney, numberOfAccounts, numberOfClients);
        Daemon observer = new Daemon(bank);
        observer.start();
        for(int i = 0; i < bank.getNumberOfClients(); i++){
            new Client(bank).start();
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertFalse(bank.getLossOfMoney());
    }

    /**
     * Второй тест с отрицательным сценарием: 1000 клиентов, 1000 счетов, но
     * один из счетов намеренно переполняем и проверяем реакцию наблюдателя.
     */
    @Test
    public void test2(){
        init("configurations1.properties");

        Bank bank = new Bank(amountOfMoney, numberOfAccounts, numberOfClients);
        Daemon observer = new Daemon(bank);
        observer.start();
        bank.accountList.get(1).setSumm(BigDecimal.valueOf(20000));
        for(int i = 0; i < bank.getNumberOfClients(); i++){
            new Client(bank).start();
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertTrue(bank.getLossOfMoney());
    }

    /**
     * Третий тест с положительным исходом: 1000 клиентов, 2 счета, все
     * работает без потерь и переполнений
     */
    @Test
    public void test3(){
        init("configurations2.properties");

        Bank bank = new Bank(amountOfMoney, numberOfAccounts, numberOfClients);
        Daemon observer = new Daemon(bank);
        observer.start();
        for(int i = 0; i < bank.getNumberOfClients(); i++){
            new Client(bank).start();
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertFalse(bank.getLossOfMoney());
    }

    private void init(String propertiesFile){

        Properties properties = new Properties();

        try {
            InputStream inputStream = Bank.class.getResourceAsStream(propertiesFile);
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        numberOfAccounts = Integer.parseInt(properties.getProperty("account_number"));
        numberOfClients = Integer.parseInt(properties.getProperty("client_number"));
        amountOfMoney = BigDecimal.valueOf(Double.parseDouble(properties.getProperty("amount_money")));
    }
}
