package nuridinov;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static nuridinov.BankProperties.loadProperties;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Тестирование "Банка"
 */
public class SberTest {
    /**
     * Сценарий 1. Положительный сценарий:
     * Запускаем N клиентов M счетов(не менее 70). Все ок.
     *
     */
    @Test
    public void Scenario1(){

        loadProperties("config1(scenario1).properties");

        int amountOfMoney = BankProperties.AMOUNT_OF_MONEY;
        int numberOfClients = BankProperties.NUMBER_OF_CLIENTS;
        int numberOfAccounts = BankProperties.NUMBER_OF_ACCOUNTS;

        Bank sber = new Bank(BigDecimal.valueOf(amountOfMoney));
        List<Account> accounts = setAccountList(numberOfAccounts, amountOfMoney);
        sber.setAccounts(accounts);

        Daemon daemon = new Daemon(sber);
        daemon.setDaemon(true);
        daemon.start();

        for (int i = 0; i< numberOfClients; i++){
            Client client = new Client();
            client.start();
            //расскоментировать, если хотите увидеть упорядоченные сообщения транзакций
            /*try {
                client.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
        }

        assertTrue(daemon.isAlive());


    }



    /**
     *  Сценарий 2. Негативный сценарий:
     * Даем какому-нибудь клиенту слишком большую сумму (больше общего amount).
     * запускаем N клиентов  М счетов (не менее 70). Ожидаем, что будет исключение от демона.
     */
    @Test
    public void Scenario2(){
        loadProperties("config1(scenario1).properties");

        int amountOfMoney = BankProperties.AMOUNT_OF_MONEY;
        int numberOfClients = BankProperties.NUMBER_OF_CLIENTS;
        int numberOfAccounts = BankProperties.NUMBER_OF_ACCOUNTS;

        Bank sber = new Bank(BigDecimal.valueOf(amountOfMoney));
        List<Account> accounts = setAccountList(numberOfAccounts, amountOfMoney);
        sber.setAccounts(accounts);
        accounts.get(0).transactIN(BigDecimal.valueOf(500000000));

        Daemon daemon = new Daemon(sber);
        daemon.setDaemon(true);
        daemon.start();
        for (int i = 0; i<numberOfClients; i++){
            new Client().start();
        }

        assertFalse(daemon.isAlive());
    }

    /**
     * 3. Положительный сценарий.
     * Запускаем N клиентов (не менее 70), которые работают только с двумя счетами. Все ок.
     */
    @Test
    public void Scenario3(){
        loadProperties("config2(scenario3).properties");

        int amountOfMoney = BankProperties.AMOUNT_OF_MONEY;
        int numberOfClients = BankProperties.NUMBER_OF_CLIENTS;
        int numberOfAccounts = BankProperties.NUMBER_OF_ACCOUNTS;

        Bank sber = new Bank(BigDecimal.valueOf(amountOfMoney));
        List<Account> accounts = setAccountList(numberOfAccounts, amountOfMoney);
        sber.setAccounts(accounts);

        Daemon daemon = new Daemon(sber);
        daemon.setDaemon(true);
        daemon.start();
        for (int i = 0; i<numberOfClients; i++){
            new Client().start();
        }

        assertTrue(daemon.isAlive());
    }

    /**
     * Метод для создание списка счетов, с разнимым балансом
     * @param size - длина списка
     * @param amountOfMoney - общее кол-во денег
     * @return список счетов
     */
    public List<Account> setAccountList(int size, int amountOfMoney){
        List<Account> randomAccounts = new ArrayList<>();
        for (int i = 0; i < size - 1; i++){
            if (amountOfMoney > 1){
                int someSum = ThreadLocalRandom.current().nextInt(1, amountOfMoney);
                amountOfMoney -= someSum;
                randomAccounts.add(new Account(BigDecimal.valueOf(someSum)));
            }
        }
        randomAccounts.add(new Account(BigDecimal.valueOf(amountOfMoney)));
        return randomAccounts;
    }
}
