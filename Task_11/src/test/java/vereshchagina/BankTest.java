package vereshchagina;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

public class BankTest {
    /**
     * количество клиентов 1000
     * количество счетов 1000
     * общая сумма 1000000
     * максимальная сумма перевода 100
     * <p>
     * Положительный сценарий:
     * Запускаем N клиентов
     * M счетов(генерируются при инициализации банка).
     * Все ок.
     */

    @Test
    public void testPositiveSituation() {
        Bank bank = new Bank("config1.properties");
        MyDaemon daemon = new MyDaemon(bank);
        daemon.start();
        for (int i = 0; i < Data.COUNT_CLIENTS; i++)
            new Client(bank).start();
        assertTrue(daemon.isAlive());
    }

    /**
     * количество клиентов 1000
     * количество счетов 700
     * общая сумма 1000000
     * максимальная сумма перевода 5000
     * <p>
     * Негативный сценарий:
     * Даем какому-нибудь клиенту слишком большую сумму (больше общего amount).
     * запускаем N клиентов  М счетов.
     * Ожидаем, что будет исключение от демона.
     */

    @Test
    public void testBigMoney() {
        Bank bank = new Bank("config3.properties");
        MyDaemon daemon = new MyDaemon(bank);

        bank.getListAccount().get(0).setAmount(BigDecimal.valueOf(1000001));

        daemon.start();
        for (int i = 0; i < Data.COUNT_CLIENTS; i++)
            new Client(bank).start();
        assertFalse(daemon.isAlive());
    }

    /**
     * количество клиентов 1000
     * количество счетов 2
     * общая сумма 1000000
     * максимальная сумма перевода 1000
     * Положительный сценарий.
     * <p>
     * Запускаем N клиентов (не менее 70),
     * которые работают только с двумя счетами. Все ок.
     */
    @Test
    public void testTwoAccount() {
        Bank bank = new Bank("config2.properties");
        MyDaemon daemon = new MyDaemon(bank);
        daemon.start();
        for (int i = 0; i < Data.COUNT_CLIENTS; i++)
            new Client(bank).start();
        assertTrue(daemon.isAlive());
    }

}
