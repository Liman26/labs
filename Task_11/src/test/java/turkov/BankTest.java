package turkov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Класс-тест работы банка и клиентов
 */
public class BankTest {
    /**
     * Положительный сценарий: запускаем 100 клиентов с 500 счетами. Все ок.
     */
    @Test
    public void positiveTestBankConfig1() {
        Bank bank = new Bank();
        bank.startBank("configBank1.properties", bank);
        System.out.printf("Create bank: %d clients; %d accounts; %.2f total amount\n",
                bank.getNumberBankClients(), bank.getNumberBankAccounts(), bank.getStartTotalAccounts());
    }

    /**
     * Негативный сценарий: запускаем 100 клиентов с 500 счетами. С помощью метода emulateFail добавляем на 0-й счет
     * дополнительную сумму средств. Ожидаем, что будет исключение от "Daemon of Bank" и индикатор работы банка
     * BankWork будет установлен в false.
     */
    @Test
    public void negativeTestBankConfig1() throws InterruptedException {
        Bank bank = new Bank();
        bank.startBank("configBank1.properties", bank);
        System.out.printf("Create bank: %d clients; %d accounts; %.2f total amount\n",
                bank.getNumberBankClients(), bank.getNumberBankAccounts(), bank.getStartTotalAccounts());
        bank.emulateFail(99.11);
        Thread.sleep(20);
        Assertions.assertFalse(bank.bankWorking());
    }

    /**
     * Положительный сценарий: запускаем 100 клиентов с 2 счетами. Все ок.
     */
    @Test
    public void positiveTestBankConfig2() {
        Bank bank = new Bank();
        bank.startBank("configBank2.properties", bank);
        System.out.printf("Create bank: %d clients; %d accounts; %.2f total amount\n",
                bank.getNumberBankClients(), bank.getNumberBankAccounts(), bank.getStartTotalAccounts());
    }
}