package mescheryakov;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Класс, реализующий тестирование работы банка со счетами
 */
public class TestBank {
    private Bank Sber;

    /**
     * Метод, считывающий параматеры из файла properties, и создающий соотвествующий банк.
     * Поток 2 раза "засыпает" на 5 секунд, чтобы была возможность реализовать утечку средств (для проверки).
     *
     * @param fileName имя файла properties
     */
    public void createBank(String fileName) {
        Properties properties = new Properties();
        InputStream inputStream = Bank.class.getResourceAsStream(fileName);
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int numberOfClients = Integer.parseInt(properties.getProperty("numberOfClients", "100"));
        int numberOfAccounts = Integer.parseInt(properties.getProperty("numberOfAccounts", "1000"));
        long balance = Long.parseLong(properties.getProperty("balance", "1000000"));
        long lost = Long.parseLong(properties.getProperty("lost", "0"));
        long extra = Long.parseLong(properties.getProperty("extra", "0"));

        Sber = new Bank(numberOfClients, numberOfAccounts, balance);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (Sber.accounts[0]) {
            Sber.accounts[0].subMoney(lost, Sber.difference);
            Sber.accounts[0].addMoney(extra, Sber.difference);
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Тест положительного сценария работы/
     * Запускаем N клиентов M счетов(не менее 70)
     *
     * @throws Exception ислючение на случай отсутствия файла
     */
    @Test
    public void test1() throws Exception {
        createBank("config1.properties");
        assertFalse(Sber.isFail);
    }

    /**
     * Тест отрицательного сценария работы.
     * Реализуем утечку средств с баланса счета.
     * Демон ловит ошибку в балансе и выкидывает error "баланс недступен", после этого
     * работа банка останавливается
     *
     * @throws Exception ислючение на случай отсутствия файла
     */
    @Test
    public void test2() throws Exception {
        createBank("config2.properties");
        assertTrue(Sber.isFail);
    }

    /**
     * Тест положительного сценария работы.
     * Запускаем N клиентов (не менее 70), которые работают только с двумя счетами
     *
     * @throws Exception ислючение на случай отсутствия файла
     */
    @Test
    public void test3() throws Exception {
        createBank("config3.properties");
        assertFalse(Sber.isFail);
    }

    /**
     * Тест отрицательного сценария работы.
     * Реализуем добавление лишних средств на счет во время операции.
     * Демон ловит ошибку несовпадения суммы перевода с балансом и выкидывает
     * error "баланс недоступен", после этого работа банка останавливается
     *
     * @throws Exception ислючение на случай отсутствия файла
     */
    @Test
    public void test4() throws Exception {
        createBank("config4.properties");
        assertTrue(Sber.isFail);
    }
}