package vereshchagina;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;

/**
 * класс, описывающий клиента банка
 */
public class Client extends Thread {

    /**
     * банк, которым пользуется клиент
     */
    private Bank bank;

    /**
     * конструктор клиента
     *
     * @param bank экземпляр банка
     */
    public Client(Bank bank) {
        this.bank = bank;
    }

    /**
     * метод, реализующий перевод клиентом денежных средств с одного счёта на другой
     * (сначала генерируются два счета, затем генерируется предполагаемая суммая перевода и
     * затем запускается операция перевода, поток завершается либо удачным переводом, либо провальным в случае
     * нехватки денежных средств)
     */
    @Override
    public void run() {
        var accountsList = bank.getTwoRandAccount();
        BigDecimal transferAmount = randAmount();
        bank.transferOperation(accountsList.get(0), accountsList.get(1), transferAmount);
    }

    /**
     * метод, генерирующий сумму перевода в пределах указанного максимального перевода
     *
     * @return сумму перевода
     */
    private BigDecimal randAmount() {
        ThreadLocalRandom rand = ThreadLocalRandom.current();
        return BigDecimal.valueOf(rand.nextInt(Data.MAX_TRANSFER) + 1);

    }
}
