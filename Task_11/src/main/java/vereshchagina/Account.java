package vereshchagina;

import java.math.BigDecimal;
import java.util.concurrent.locks.ReentrantLock;

/**
 * класс, описывающий счёт в банке
 */
public class Account {
    /**
     * сумма, лежащая на счёте
     */
    private BigDecimal amount;

    /**
     * блокировка счёта
     */
    private ReentrantLock lockAccount;

    /**
     * конструктор
     *
     * @param sum принимает сумму, лежащую на счете
     */
    public Account(int sum) {
        amount = BigDecimal.valueOf(sum);
        lockAccount = new ReentrantLock();
    }

    /**
     * геттер для суммы
     *
     * @return сумму
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * сеттер для суммы
     *
     * @param amount сумма
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * метод, блокирующий доступ к счету
     */
    public void lockAccount() {
        lockAccount.lock();
    }

    /**
     * метод, снимающий блокировку со счета
     */
    public void unlockAccount() {
        lockAccount.unlock();
    }


}
