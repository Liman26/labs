package vereshchagina;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * класс, описывающий работу банка
 */
public class Bank {
    /**
     * Список счетов
     */
    private List<Account> listAccounts;

    /**
     * конструктор банка
     * считывает данные из файла
     * генерирует список счетов со случайным распределением денежных сумм в рамках общей указанной
     *
     * @param nameFile имя файла
     */
    public Bank(String nameFile) {
        Data.readData(nameFile);
        listAccounts = generateListAccounts(Data.TOTAL_AMOUNT, Data.COUNT_ACCOUNTS);
    }

    /**
     * геттер для списка счетов
     *
     * @return список счетов
     */
    public List<Account> getListAccount() {
        return listAccounts;
    }


    /**
     * метод, распределяющий общую сумму банка на количество счетов случайным образом
     * вызывается при создании  банка
     *
     * @return список счетов
     */
    private List<Account> generateListAccounts(int total_sum, int count) {

        List<Account> list = new ArrayList<>();

        if (count > 0) {
            int remains = total_sum;
            Random random = new Random();
            for (int i = 0; i < count - 1; i++) {
                int rand = random.nextInt(remains + 1);
                list.add(new Account(rand));
                remains -= rand;
            }
            list.add(new Account(remains));
        }
        return list;
    }

    /**
     * метод, возвращающий 2 случайных аккаунта из списка счетов
     *
     * @return список с двумя случайными аккаунтами
     */
    public List<Account> getTwoRandAccount() {
        ThreadLocalRandom rand = ThreadLocalRandom.current();
        return IntStream.range(0, 2)
                .map(n -> rand.nextInt(Data.COUNT_ACCOUNTS))
                .mapToObj(listAccounts::get).collect(Collectors.toList());
    }

    /**
     * метод, позволяющий совершить денежный перевод между счетами
     * если на счету отправителя недостаточно средств для указанного перевода,
     * операция отменяется
     *
     * @param from           счет отправителя
     * @param to             счет получателя
     * @param transferAmount сумма перевода
     */
    public void transferOperation(Account from, Account to, BigDecimal transferAmount) {
        if (checkingSender(from, transferAmount)) {
            sendingToRecipient(to, transferAmount);
            System.err.println("The payment in the amount of " + transferAmount + " rubles was successfully completed");
        } else
            System.err.println("The payment failed!");
    }

    /**
     * метод, проверяющий счет клиента отправителя,
     * если на счету не достаточно средств, возвращает faulse,
     * иначе, снимает заданную сумму и возвращает true
     *
     * @param from           счет отправителя
     * @param transferAmount сумма перевода
     * @return faulse или true
     */
    private boolean checkingSender(Account from, BigDecimal transferAmount) {
        from.lockAccount();
        try {
            if (from.getAmount().compareTo(transferAmount) >= 0) {
                from.setAmount(from.getAmount().subtract(transferAmount));
                return true;
            }
            return false;
        } finally {
            from.unlockAccount();
        }
    }

    /**
     * метод, работающий со счетом получателя,
     * в случае, если отправителю удалось перевести деньги,
     * метод вызывается и добавляет отправленную сумму к имеющейся на счету получателя
     *
     * @param to             счет получателя
     * @param transferAmount сумма перевода
     */
    private void sendingToRecipient(Account to, BigDecimal transferAmount) {
        to.lockAccount();
        try {
            to.setAmount(to.getAmount().add(transferAmount));
        } finally {
            to.unlockAccount();
        }
    }
}
