package vereshchagina;


import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * класс, реализующий поток, который выступает в роли наблюдателя
 * за денежной массой в системе, его задача следить за тем, чтобы не было утечек.
 */
public class MyDaemon extends Thread {
    /**
     * Банк, за которым наблюдает демон
     */
    private Bank bank;

    /**
     * конструктор
     *
     * @param bank банк
     */
    public MyDaemon(Bank bank) {
        this.bank = bank;
    }

    /**
     * метод, реализующий сравнение указанной в исходниках общей суммы,
     * с суммой сбережений со всех счетов. в случае расхождения сумм выбрасывает RuntimeException
     */
    @Override
    public void run() {
        BigDecimal amount = new BigDecimal(Data.TOTAL_AMOUNT);

        while (true) {
            if (amount.compareTo(summAllAmount()) != 0) {
                throw new RuntimeException("data leak!");
            } else System.out.println("All good!");
            try {
                sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * метод, возвращающий копию списка счетов
     *
     * @param list на вход получает исходный список
     * @return копия
     */
    private List<Account> copyListAccounts(List<Account> list) {
        lockAllAccount(list);
        var copyList = list.stream().map(account -> new Account(account.getAmount().intValue())).collect(Collectors.toList());
        unlockAllAccount(list);
        return copyList;
    }

    /**
     * Метод, возвращающий сумму сбережений на счетах
     */
    private BigDecimal summAllAmount() {
        return copyListAccounts(bank.getListAccount())
                .stream()
                .map(Account::getAmount)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    /**
     * метод, блокирующий доступ ко всем счетам из списка
     */
    private void lockAllAccount(List<Account> list) {
        list.forEach(Account::lockAccount);
    }

    /**
     * метод, снимающий блокировку со всех счетов из списка
     */
    private void unlockAllAccount(List<Account> list) {
        list.forEach(Account::unlockAccount);
    }


}
