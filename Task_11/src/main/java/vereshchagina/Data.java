package vereshchagina;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * вспомогательный класс, содержит основные количественные данные и метод для их считывания
 */
public class Data {
    /**
     * количество счетов в банке
     */
    public static int COUNT_ACCOUNTS;

    /**
     * количество клиентов
     */
    public static int COUNT_CLIENTS;

    /**
     * общая сумма, хранящаяся в банке
     */
    public static int TOTAL_AMOUNT;

    /**
     * максимальная сумма перевода
     */
    public static int MAX_TRANSFER;

    /**
     * метод получает данные из файла и инициализирует статические переменные
     *
     * @param nameFile имя файла
     */
    public static void readData(String nameFile) {
        Properties properties = new Properties();
        try {
            InputStream inputStream = Data.class.getResourceAsStream(nameFile);
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        COUNT_ACCOUNTS = Integer.parseInt(properties.getProperty("howManyAccounts"));
        COUNT_CLIENTS = Integer.parseInt(properties.getProperty("howManyClients"));
        TOTAL_AMOUNT = Integer.parseInt(properties.getProperty("totalAmount"));
        MAX_TRANSFER = Integer.parseInt(properties.getProperty("maxTransfer"));
    }

}
