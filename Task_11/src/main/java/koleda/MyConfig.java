package koleda;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class MyConfig {

    /**
     * Конфигурационные настройки, находящиеся в .properties файле.
     */
    public static String MAX_AMOUNT_TRANSACTION = "max_amount_transaction";
    public final static String AMOUNT_ACCOUNTS = "amount_accounts";
    public final static String AMOUNT_CLIENTS = "amount_clients";
    public final static String FAIL = "fail";

    private static final Properties properties = new Properties();

    /**
     * Метод, с помощью которого можно подключить конфигурационный файл
     * @param nameCfg название конфигурации
     */
    public static void init(String nameCfg){
        try {
            properties.load(new FileInputStream(nameCfg));
        } catch (IOException e) {
            throw new RuntimeException("Файл не был найден в конфигурации...");
        }
    }

    /**
     * Метод, с помощью которого можно получить значение файла .properties по ключу
     * @param key ключ
     * @return значение из конфигурации
     */
    public static int getValueProperty(String key) {
        if (key.equals("fail") && !foundValueProperty(key))
            return 0;

        if (foundValueProperty(key))
            return Integer.parseInt(properties.getProperty(key));
        else
            throw new RuntimeException(String.format("Не удалось найти параметр %s в файле...", key));
    }

    /**
     * Метод, который сообщает о наличии в конфигурационном файле значения по ключу
     * @param key ключ
     * @return true - если значение найдено
     *         false - если значение не найдено
     */
    private static boolean foundValueProperty(String key) {
        return properties.getProperty(key) != null;
    }

}
