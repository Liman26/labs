package koleda;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Randomizer {

    /**
     * Метод, с помощью которого получаем два случайных счета в банке
     * @param accounts список всех счетов
     * @return список из двух счетов
     */
    public static List<Account> randomAccounts(List<Account> accounts) {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        return IntStream.range(0, 2)
                .map(x -> random.nextInt(accounts.size()))
                .mapToObj(accounts::get)
                .peek(Account::lock)
                .collect(Collectors.toList());
    }

    /**
     * Метод, который гнерирует случайную сумму для транзакции
     * @return случайная сумма
     */
    public static long randomAmount() {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        return MyConfig.getValueProperty(MyConfig.MAX_AMOUNT_TRANSACTION);
    }

}
