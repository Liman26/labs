package koleda;

import java.util.List;

/**
 * Класс наблюдателя, который следит за тем, что нет никаких утечек
 */
public class Daemon extends Thread{

    /**
     * Банк, за которым следит наблюдатель
     */
    private final Bank bank;

    public Daemon(Bank bank) {
        this.bank = bank;
    }

    /**
     * Проверяет, сходится ли текущий остаток с первоначальным.
     * Если нет, то выбрасываем исключение
     */
    @Override
    public void run(){
        long value1 = MyConfig.getValueProperty(MyConfig.MAX_AMOUNT_TRANSACTION);
        long value2 = MyConfig.getValueProperty(MyConfig.AMOUNT_ACCOUNTS);
        long bankAmount = value1 * value2;

        while (true) {
            bank.getAccounts().forEach(Account::lock);
            long calculateAmount = calculate(bank.getAccounts());
            bank.getAccounts().forEach(Account::unlock);
            if (bankAmount != calculateAmount)
                throw new RuntimeException("Суммы не совпадают!");

            sleep();
        }
    }

    /**
     * Метод, который подсчитывает общую сумму остатков на счетах в банке
     * @param accounts список всех счетов
     * @return общая сумма
     */
    private long calculate(List<Account> accounts) {
        long value = 0;
        for (var account : accounts){
            account.lock();
            value = value + account.getBalance();
            account.unlock();
        }
        return value;
    }

    /**
     * Статический метод, который позволяет создать демона-наблюдателя за транзакциями клиентов
     * @param bank проверяемый банк
     * @return созданный объект-наблюдатель
     */
    public static Daemon startDaemon(Bank bank) {
        Daemon daemon = new Daemon(bank);
        daemon.setDaemon(true);
        daemon.start();
        return daemon;
    }

    /**
     * Режим ожидания
     */
    private void sleep(){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
