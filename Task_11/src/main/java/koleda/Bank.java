package koleda;

import java.util.List;

public class Bank {

    private final List<Account> accounts;

    public Bank(List<Account> accounts) {
        this.accounts = accounts;
    }

    /**
     * Совершает перевод с одного счета на другой
     * @param from перевести с этого счета
     * @param to перевести на этот счет
     * @param amount сумма для перевода
     */
    public void transferOfTheAmount(Account from, Account to, long amount){
        if(from.writeOffAmount(amount))
            to.addAmount(amount);
    }

    /**
     * возвращаем список всех счетов
     * @return список счетов
     */
    public List<Account> getAccounts() {
        return accounts;
    }
}
