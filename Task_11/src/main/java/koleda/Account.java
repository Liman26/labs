package koleda;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account {
    private Lock lock;
    private long balance;

    public Account(long balance) {
        this.balance = balance;
        lock = new ReentrantLock();
    }

    public long getBalance(){
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    /**
     * Метод, ограничивающий общий доступ к счету
     */
    public void lock() {
        try {
            lock.lockInterruptibly();
         } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод, снимающий ограничение к счету
     */
    public void unlock() { lock.unlock(); }

    /**
     * Добавить сумму на счет
     * @param amount сумма, которую необходимо добавить
     */
    public void addAmount(long amount){
        balance += amount;
    }

    /**
     * Списать сумму со счета
     * @param amount сумма, которую необходимо списать
     * @return true - если необходимая сумма имеется на счете
     *         false - если необходимой суммы не имеется на счете
     */
    public boolean writeOffAmount(long amount){
        if (balance >= amount){
            balance -= amount;
            return true;
        }
        else
            return false;
    }

}
