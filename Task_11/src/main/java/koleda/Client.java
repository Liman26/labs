package koleda;

import java.util.List;

/**
 * Класс, реализующий клиента банка
 */
public class Client extends Thread{

    /**
     * Банк, через который клиент совершает транзакцию
     */
    private Bank bank;

    public Client(Bank bank) {
        this.bank = bank;
    }

    /**
     * Метод, с помощью которого соверщается транзакция между двумя случайными счетами
     * Сумма для транзакции также рандомная
     */
    @Override
    public void run(){
        List<Account> accounts = Randomizer.randomAccounts(bank.getAccounts());
        bank.transferOfTheAmount(accounts.get(0), accounts.get(1), Randomizer.randomAmount());
        accounts.get(0).unlock();
        accounts.get(1).unlock();
    }
}
