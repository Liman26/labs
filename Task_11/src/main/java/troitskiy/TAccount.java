package troitskiy;

/**
 * Счет в нашем банке
 * суммы храним в копейках long - будет достаточно
 * можно менять остаток либо сам остаток либо его изменения
 * в дополнительноем поле
 */
public class TAccount {
    private static int accountKol = 0;
    private final int numberAccount;
    private long rest;
    private long deltaRest = 0;

    /**
     * Создать новый счет с заданным остатком
     * @param rest -остаток
     */
    public TAccount(long rest) {
        this.numberAccount = accountKol++;
        this.rest = rest;
    }

    /**
     * Получить остаток, без изменений
     * @return -остаток
     */
    public long getRest()
    {
        return rest;
    }

    /**
     * Получить остаток счета с изменениями
     * @return -остаток
     */
    public long getFullRest() {
        return rest + deltaRest;
    }

    /**
     *  Добавить денег на счет
     *
     * @param deltaRest  - сумма добавления
     * @param useDelta   - спользовать поле для изменений
     */
    public void AddRest(long deltaRest, boolean useDelta) {
        if (useDelta) this.deltaRest = this.deltaRest + deltaRest;
        else rest = rest + deltaRest;
    }

    /**
     * Забрать денег со счета
     * @param deltaRest -сумма для забирания
     * @param useDelta  -спользовать поле для изменений
     * @return - результат забирания, получилось или нет
     */
    public boolean SubRest(long deltaRest, boolean useDelta) {
        if (getFullRest() >= deltaRest) {
            if (useDelta) this.deltaRest = this.deltaRest - deltaRest;
            else rest = rest - deltaRest;
            return true;
        }
        else return false;
    }

    /**
     * Применить накопленные изменения
     */
    public void CommitDeltaRest()
    {
        rest = rest + deltaRest;
        deltaRest = 0;
    }

    /**
     * счет в виде строки
     * @return - строка с инфо о счете
     */
    @Override
    public String toString() {
        return  "" + numberAccount + "(" + getFullRest() + ")";
    }
}
