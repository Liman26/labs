package troitskiy;

/**
 * Клиент может переводить деньги с одного счёта на другой
 * Постоянно  выбирает случайным образом два счета и сумму
 * и пытается перевести деньги с одного счета на другой,
 * если не получилось не беда
 */
public class TClient extends Thread {
    private final TBank bank;

    /**
     * Конструктор клиента для заданного банка
     */
    public TClient(TBank bank) {
        this.bank = bank;
    }

    /**
     * Перевод денег со счета на счет
     * @param firstAccount   -откуда
     * @param secondAccount  -куда
     * @param sum            -сумма
     * @return               -результат, получилось перевести или нет
     */
    private boolean Pervod(TAccount firstAccount, TAccount secondAccount, long sum)
    {
        boolean useDelta = bank.useDelta;
        if (firstAccount.SubRest(sum, useDelta)) {
            secondAccount.AddRest(sum, useDelta);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Основное занятие клиента, занимается исключительно переводом
     * денег со счета на счет
     */
    @Override
    public void run() {
       while (!bank.isFail) {
           int firstAccount  = (int) (Math.random() * bank.kolvoAccounts);
           int secondAccount = (int) (Math.random() * bank.kolvoAccounts);
           if (firstAccount != secondAccount) {
               long sum = (long) (Math.random() * bank.balans / bank.kolvoAccounts * 2);

               if (firstAccount < secondAccount) {
                   synchronized (bank.accounts[firstAccount]) {
                       synchronized (bank.accounts[secondAccount]) {
                           Pervod(bank.accounts[firstAccount], bank.accounts[secondAccount], sum);
                       }
                   }
               } else {
                   synchronized (bank.accounts[secondAccount]) {
                       synchronized (bank.accounts[firstAccount]) {
                           Pervod(bank.accounts[firstAccount], bank.accounts[secondAccount], sum);
                       }
                   }
               }
           }
       }
    }
}
