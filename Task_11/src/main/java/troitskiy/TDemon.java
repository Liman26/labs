package troitskiy;

/**
 * демон наблюдатель, который будет следить, что всё ок - что денежная масса в системе постоянна -- нет утечек).
 */
public class TDemon extends Thread{
    private final TBank bank;

    /**
     * Конструктор
     * @param bank -банк в котором работает демон
     */
    public TDemon(TBank bank) {
        this.bank = bank;
    }

    /**
     * Процесс, который будет следить, что всё ок - что денежная масса в системе постоянна -- нет утечек).
     */
    @Override
    public void run() {
        while(!bank.isFail) {

            bank.useDelta = false;
            for (int i = 0; i < bank.accounts.length; i++) {
                synchronized (bank.accounts[i]) {
                    bank.accounts[i].CommitDeltaRest();
                }
            }

            bank.useDelta = true;
            for (int i = 0; i < bank.accounts.length; i++) {
                synchronized (bank.accounts[i]) {
                }
            }

            long testSum = 0;
            for (int i = 0; i < bank.accounts.length; i++) {
                synchronized (bank.accounts[i]) {
                    testSum = testSum + bank.accounts[i].getRest();
                }
            }

            if (testSum != bank.balans){
                bank.isFail = true;
                System.err.println("Balans Fail");
            }
            else System.out.println("Balans is OK");
        }

    }
}
