package troitskiy;

/**
 Есть банк. В нем N счетов и M клиентов.
 Клиент может переводить деньги с одного счёта на другой.

 Необходимо создать демона наблюдателя, который будет следить, что всё ок - что денежная масса в системе постоянна -- нет утечек). Денежный пул создаётся при запуске программы, он фиксирован и указывается через проперти файл

 В программе должно работать одновременно >= 30 потоков. Количество M и N также задаются через проперти.

 Подсказки:

 Не используйте для синхронизации один общий глобальный объект (в таком слкчае когда демон будет сверять счета то банк остановится)


 Кроме того, проверьте что обработаны следующие ситуации:

 Проблема синхронизации:

 Клиент снял со счёта 1 сумму X, с целью положить его на счёт 2.
 Демон проверил сумму на счёте 1.
 Демон проверил сумму на счёте 2.
 Клиент положил сумму X на счёт 2.


 Таким образом демон недосчитался X. Эту ситуацию нужно обработать.

 Если клиент хочет сделать перевод межлу счетами, то он сначала должен убедиться что требуемая сумма действительно есть на счёте. Если нет - то клиент может либо попытаться перевести сумму поменьше попробовать в следующий раз.

 Демон должен чекать суммы, актуально лежащие на счетах. можно сделать и дополнительные внутренние проверки

 Тесты:
 1. Положительный сценарий:
 Запускаем N клиентов M счетов(не менее 70). Все ок.
 2. Негативный сценарий:
 Даем какому-нибудь клиенту слишком большую сумму (больше общего amount). запускаем N клиентов  М счетов (не менее 70). Ожидаем, что будет исключение от демона.
 3. Положительный сценарий.
 Запускаем N клиентов (не менее 70), которые работают только с двумя счетами. Все ок.
 *
 */
public class TBank {
    public int kolvoClients;
    public int kolvoAccounts;
    public long balans;
    public TAccount[] accounts;
    public TClient[] clients;
    public TDemon demon;
    volatile public boolean isFail = false;
    volatile public boolean useDelta = false;

    /**
     * Конструктор банка
     * @param kolvoClients   -сколько клиентов занимается переводами денег
     * @param kolvoAccounts  -сколько счетов в банке
     * @param balans         -сколько денег в банке
     */
    public TBank(int kolvoClients, int kolvoAccounts, long balans) {
        this.kolvoClients = kolvoClients;
        this.kolvoAccounts = kolvoAccounts;
        this.balans = balans;

        accounts = new TAccount[this.kolvoAccounts];
        long balansSredniy = balans / this.kolvoAccounts;
        long ostatok = balans;
        for (int i = 0; i < this.kolvoAccounts; i++) {
            if (i != (kolvoAccounts - 1)) {
                accounts[i] = new TAccount(balansSredniy);
                ostatok = ostatok - balansSredniy;
            }
            else
               accounts[i] = new TAccount(ostatok);
        }

        clients = new TClient[this.kolvoClients];
        for (int i = 0; i < this.kolvoClients; i++) {
               clients[i] = new TClient(this);
               clients[i].start();
        }

        demon = new TDemon(this);
        demon.start();
    }
}
