package zurnachyan;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Поток - наблюдатель, переллельно работе клиентов проверяет нет ли утечек или переполнений в системе банка.
 */
public class Daemon extends Thread{

    private Bank bank;
    private BigDecimal currentSumm;

    /**
     * Конструктор потока
     * @param bank экземпляяр банка, в котором работает данный наблюдатель
     */
    public Daemon(Bank bank){
        this.bank = bank;
    }

    /**
     * Переопределеннй метод, выполняется при запуске потока.
     * В бесконечном цикле проверяется общий счет банка на переполнение или утечку денег
     */
    @Override
    public void run() {
        while(true) {
            calculateCurrentSumm(bank.accountList);
            if (!currentSumm.equals(bank.amountOfMoney)) {
                bank.setLossOfMoney(true);
                throw new RuntimeException("There is loss or overflow of money. Current Summ is " + currentSumm);
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Вспомогательный метод для безопасного подсчета текущей суммы денег в банке
     * @param accountList список счетов, среди которых считается сумма
     */
    private void calculateCurrentSumm(List<Account> accountList){
        accountList.forEach(Account::toLock);
        currentSumm = accountList.stream().map(Account::getSumm).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        accountList.forEach(Account::toUnLock);
    }
}
