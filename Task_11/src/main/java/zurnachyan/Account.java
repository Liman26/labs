package zurnachyan;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Класс, реализующий счет клиента в банке
 */
public class Account{
    private BigDecimal summ;
    private Lock accLock;

    /**
     * Конструктор счета, задает баланс и создает блокировщик
     * @param summ баланс счета
     */
    public Account(BigDecimal summ){
        this.summ = summ;
        accLock = new ReentrantLock();
    }

    /**
     * Метод для блокировки счета для других потоков
     */
    public void toLock(){
        accLock.lock();
    }

    /**
     * Метод для разблокировки счета
     */
    public void toUnLock(){
        accLock.unlock();
    }

    /**
     * Метод, возвращающий баланс счета
     * @return баланс счета
     */
    public BigDecimal getSumm() {
        return summ;
    }

    /**
     * Метод для изменения баланса счета
     * @param summ новый баланс
     */
    public void setSumm(BigDecimal summ) {
        this.summ = summ;
    }

}
