package zurnachyan;

import java.math.BigDecimal;
import java.util.Random;

/**
 * Класс, реализующий клиента банка.
 */
public class Client extends Thread{
    private Bank bank;
    private Random random = new Random();

    /**
     * Конструктор клиента, устанааливает банк, которому принадлежит данный клиент
     * @param bank банк
     */
    public Client(Bank bank) {
        this.bank = bank;
    }

    /**
     * ереопределенный метод, выполняющийся при запуске потока, совершает транзакцию внатри банковской системы
     * с одного случайного счета на другой случайный счет на случайную сумму
     */
    @Override
    public void run() {
        int fromIndex = random.nextInt(bank.getNumberOfAccounts());
        int toIndex = random.nextInt(bank.getNumberOfAccounts());
        bank.makeTransaction(bank.accountList.get(fromIndex), bank.accountList.get(toIndex),
                BigDecimal.valueOf(random.nextInt(bank.amountOfMoney.intValue() / bank.getNumberOfAccounts())));
    }
}
