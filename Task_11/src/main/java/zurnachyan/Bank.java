package zurnachyan;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Класс Банк, который содержит в себе информацию о счетах и клиентах, совершает транзакции.
 */
public class Bank {

    private int numberOfAccounts;
    private int numberOfClients;

    private AtomicBoolean lossOfMoney = new AtomicBoolean();

    public final BigDecimal amountOfMoney;
    public final List<Account> accountList = new ArrayList<>();

    /**
     * Конструктор присваивает значения полям и создает счета.
     * @param amountOfMoney общее количество денег в банке
     * @param numberOfAccounts количество счетов
     * @param numberOfClients количество клиентов
     */
    public Bank(BigDecimal amountOfMoney, int numberOfAccounts, int numberOfClients){
        this.amountOfMoney = amountOfMoney;
        this.numberOfAccounts = numberOfAccounts;
        this.numberOfClients = numberOfClients;
        this.lossOfMoney.set(false);

        for(int i = 0; i < numberOfAccounts; i++){
            accountList.add(new Account(amountOfMoney.divide(BigDecimal.valueOf(numberOfAccounts))));
        }
    }

    /**
     * Метод для выполнения транзакций, задает порядок блокировки счетов, блокирует их и совершает перевод
     * @param from аккаунт, с которого переводят деньги
     * @param to аккаунт, на которые переводят деньги
     * @param summ сумма перевода
     */
    public void makeTransaction(Account from, Account to, BigDecimal summ){
        Account firstAccount;
        Account secondAccount;
        if(accountList.indexOf(from) < accountList.indexOf(to)){
            firstAccount = from;
            secondAccount = to;
        }
        else{
            firstAccount = to;
            secondAccount = from;
        }
        firstAccount.toLock();
        secondAccount.toLock();
        if (from.getSumm().compareTo(summ) >= 0) {
            from.setSumm(from.getSumm().subtract(summ));
            to.setSumm(to.getSumm().add(summ));
        }
        firstAccount.toUnLock();
        secondAccount.toUnLock();
    }

    /**
     * Метод возвращает булевое значение флага, отвечающего за потери и переполнения
     * @return значение флага
     */
    public boolean getLossOfMoney() {
        return lossOfMoney.get();
    }

    /**
     * Метод для пометки флага, отвечающего за потери и переполнения
     * @param lossOfMoney новое значение флага
     */
    public void setLossOfMoney(boolean lossOfMoney) {
        this.lossOfMoney.set(lossOfMoney);
    }

    /**
     * Метод, возвращающей количество счетов
     * @return количество счетов
     */
    public int getNumberOfAccounts() {
        return numberOfAccounts;
    }

    /**
     * Метод, изменяющий количество счетов
     */
    public void setNumberOfAccounts(int numberOfAccounts) {
        this.numberOfAccounts = numberOfAccounts;
    }

    /**
     * Метод, возвращающей количество клиентов
     * @return количество клиентов
     */
    public int getNumberOfClients() {
        return numberOfClients;
    }

    /**
     * Метод, изменяющий количество клиентов
     */
    public void setNumberOfClients(int numberOfClients) {
        this.numberOfClients = numberOfClients;
    }
}
