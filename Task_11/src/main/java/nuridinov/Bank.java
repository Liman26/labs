package nuridinov;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс Банк
 */
public class Bank {
    public final BigDecimal amountOfMoney;
    public static List<Account> accounts = new ArrayList<>();

    /**
     * Контруктор класса
     * @param amountOfMoney - общая сумма денег в банке
     */
    public Bank(BigDecimal amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    /**
     * сеттер для счетов
     * @param accounts - счета
     */
    public void setAccounts(List<Account> accounts) {
        Bank.accounts = accounts;
    }

    /**
     * Метод для выполнения транзакции
     * @param from счет с которого переводят деньги
     * @param to счет на который переводят деньги
     * @param sumOfTransaction сумма переведа
     */
    static void makeTransaction(Account from, Account to, BigDecimal sumOfTransaction) {
        if (sumOfTransaction.intValue() != 0) {
            System.out.println("Sum of transaction: " + sumOfTransaction
                    + System.lineSeparator() + "State of accounts before transaction:"
                    + System.lineSeparator() + "From: " + from.getSum()
                    + System.lineSeparator() + "To: " + to.getSum());
            if (sumOfTransaction.intValue() <= from.getSum().intValue()) {
                from.transactOUT(sumOfTransaction);
                to.transactIN(sumOfTransaction);
                System.out.println("State of accounts after transaction:"
                        + System.lineSeparator() + "From: " + from.getSum()
                        + System.lineSeparator() + "To: " + to.getSum());
            } else {
                System.out.println("There are not enough funds to make transaction!");
            }
        }
    }
}
