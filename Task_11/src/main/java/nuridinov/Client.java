package nuridinov;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Класс клиент банка
 */
public class Client extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < Bank.accounts.size(); i++) {
            Account from = Bank.accounts.get(random());
            Account to = Bank.accounts.get(random());
            Bank.makeTransaction(from, to, randomSumOfTransaction(from));
        }
    }

    /**
     * Метод для получения случайного номера счета
     * @return - случайный номер счета
     */
    public int random() {
        return ThreadLocalRandom.current().nextInt(0, Bank.accounts.size());
    }

    /**
     * Метод для получения случайной суммы перевода
     * @param from Счет с которого делают перевод, возвращаемая сумма не должна превышать баланс счета
     * @return случайная сумма перевода
     */
    public BigDecimal randomSumOfTransaction(Account from) {
        if (from.getSum().intValue() != 0) {
            return BigDecimal.valueOf(ThreadLocalRandom.current().nextInt(1, from.getSum().intValue() + 1));
        } else return BigDecimal.ZERO;

    }
}
