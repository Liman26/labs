package nuridinov;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс демон-наблюдатель
 */
public class Daemon extends Thread{

    private final Bank sber;

    /**
     * конструктор
     * @param bank - банк за которым нужно наблюдать
     *
     */
    public Daemon(Bank bank){
        sber = bank;

    }

    @Override
    public void run() {
        while (true){
            Bank.accounts.forEach(Account::lock);
            List<Account> accountList = clone(Bank.accounts);
            Bank.accounts.forEach(Account::unlock);
            int actualSum = accountList.stream().map(account -> account.getSum().intValue()).reduce(0, Integer::sum);
            if(actualSum != sber.amountOfMoney.intValue()){
                throw new RuntimeException("money leakage");
            }
            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * метод для клонирования списка счетов
     * @param accounts - список счетов
     * @return - клонированный список счетов
     */
    public List<Account> clone(List<Account> accounts){
        return accounts.stream().map(a -> new Account(a.getSum())).collect(Collectors.toList());
    }
}
