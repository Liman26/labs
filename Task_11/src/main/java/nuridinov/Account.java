package nuridinov;

import java.math.BigDecimal;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Класс счет в банке
 */
public class Account {
    private BigDecimal sum;

    ReentrantLock lock = new ReentrantLock();

    /**
     * конструктор который получает на вход сумму денег для установки баланса на счету
     *
     * @param sum - сумма денег на балансе
     */
    public Account(BigDecimal sum) {
        this.sum = sum;
    }

    /**
     * геттер для получение текущей суммы баланса на счету
     *
     * @return сумма денег на счету
     */
    public BigDecimal getSum() {
        return sum;
    }

    /**
     * Сеттер для текущей суммы денег на балансе
     *
     * @param sum - текущая сумма
     */
    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    /**
     * Метод входящей траназакции, увелечивет сумму денег на счету
     *
     * @param sum - сумма на которую нужно увелечить, баланс на счету
     */
    public void transactIN(BigDecimal sum) {
        lock();
        this.sum = this.sum.add(sum);
        unlock();
    }

    /**
     * Метод исходящей транзакции, уменьшает сумму денег на счету
     *
     * @param sum - сумма на которую нужно уменьшить, баланс на счету
     */
    public void transactOUT(BigDecimal sum) {
        lock();
        this.sum = this.sum.subtract(sum);
        unlock();
    }

    /**
     * Метод для блокировки счета
     */
    public void lock() {
        lock.lock();
    }

    /**
     * Метод для разблокировки счета
     */
    public void unlock() {
        lock.unlock();
    }
}
