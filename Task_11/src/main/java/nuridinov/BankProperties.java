package nuridinov;

import java.io.*;
import java.util.Properties;

/**
 * Класс для получение необходимых данных из проперти файла
 */
public class BankProperties {
    public static int AMOUNT_OF_MONEY;
    public static int NUMBER_OF_CLIENTS;
    public static int NUMBER_OF_ACCOUNTS;

    /**
     * Подгрузка данных их проперти файла
     * @param fileName - название проперти файла
     * @return параметры банка
     */
    public static Properties loadProperties(String fileName) {
        Properties properties = new Properties();
        try {
            try (InputStream io = BankProperties.class.getResourceAsStream(fileName)) {
                properties.load(io);

            }
        } catch (IOException ex) {
            throw new RuntimeException("File do not exist");
        }

        AMOUNT_OF_MONEY = Integer.parseInt(properties.getProperty("amount_of_money"));
        NUMBER_OF_ACCOUNTS = Integer.parseInt(properties.getProperty("number_of_accounts"));
        NUMBER_OF_CLIENTS = Integer.parseInt(properties.getProperty("number_of_clients"));

        return properties;
    }

}
