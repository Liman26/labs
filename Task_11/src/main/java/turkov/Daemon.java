package turkov;

import java.math.BigDecimal;

/**
 * Класс демона наблюдателя, который работает в фоне при запуске банка и осуществляет контроль общей денежной массы на
 * всех счетах банка.
 */
public class Daemon extends Thread {

    private final Bank bank;

    public Daemon(Bank bank) {
        this.bank = bank;
    }

    /**
     * Метод для запуска демона в банке
     */
    public static void startDaemon(Bank bank) {
        Thread daemon = new Daemon(bank);
        daemon.setName("Daemon of Bank");
        daemon.setDaemon(true);
        daemon.start();
    }

    /**
     * Метод, который периодически (интервал 10 мс) сравнивает текущую сумму всех счетов в банке с той, которая задана
     * в конфигурационном файле банка *.properties. В случае обнаружения расхождений с первоначально суммой демон
     * установит блокировку на все счета и выбросит исключение о некорректной сумме их балансов.
     */
    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            BigDecimal currentTotal = bank.calcTotalAccountsBank();
            if (currentTotal.compareTo(bank.getStartTotalAccounts()) != 0) {
                bank.getAccountList().forEach(Account::setLock);
                bank.setBankWork(false);
                throw new RuntimeException("Total of accounts incorrect: " + currentTotal);
            }
        }
    }
}