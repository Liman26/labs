package turkov;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Класс банка, в котором N счетов и M клиентов. Клиент может переводить деньги с одного счёта на другой.
 * Есть так же демон-наблюдателя, который будет следить, что денежная масса в системе постоянна, нет утечек.
 * Денежный пул создаётся при запуске программы, он фиксирован и указывается через *.properties.
 */
public class Bank {
    /**
     * Список счетов банка
     */
    private final List<Account> bankAccountList = new ArrayList<>();
    /**
     * Начальная сумма остатков всех счетов
     */
    private BigDecimal startTotalAccounts;
    /**
     * Число клиентов банка
     */
    private int numberOfBankClients;
    /**
     * Количество счетов в банке
     */
    private int numberOfBankAccounts;

    private boolean bankWorking = true;

    public BigDecimal getStartTotalAccounts() {
        return startTotalAccounts;
    }

    public int getNumberBankClients() {
        return numberOfBankClients;
    }

    public int getNumberBankAccounts() {
        return numberOfBankAccounts;
    }

    public List<Account> getAccountList() {
        return bankAccountList;
    }

    public boolean bankWorking() {
        return bankWorking;
    }

    public void setBankWork(boolean bankWork) {
        this.bankWorking = bankWork;
    }

    /**
     * Метод для запуска работы банка. Выполняется чтение конфигурации банка из файла *.properties, создаются  и
     * заполняются случайными значениями счета, запускаеются демон-наблюдатель и клиенты.
     *
     * @param propertiesFileName - имя файла с настройками (кол-во клиентов, счетов и общая сумма всех счетов).
     * @param bank               - объект банка, в котором будут работать клиенты.
     */
    void startBank(String propertiesFileName, Bank bank) {
        bank.readPropertiesBank(propertiesFileName);
        bank.initializeAccountsBank();
        Daemon.startDaemon(bank);
        Client.startClients(bank);
    }

    /**
     * Метод для проведения транзакции между счетами клиентов. Счет блокируется на время работы с ним.
     * Если сумма перевода окажется больше остатка счета, процедура будет завершена.
     *
     * @param numDebitAccount   - счет списания
     * @param numDepositAccount - счет зачисления
     * @param amount            - сумма перевода
     */
    void makeTransaction(int numDebitAccount, int numDepositAccount, BigDecimal amount) {
        Account debitAccount = bankAccountList.get(numDebitAccount);
        Account depositAccount = bankAccountList.get(numDepositAccount);
        debitAccount.setLock();
        if (debitAccount.getBalance().compareTo(amount) < 0) {
            debitAccount.setUnlock();
            return;
        }
        depositAccount.setLock();
        debitAccount.setBalance(debitAccount.getBalance().subtract(amount));
        depositAccount.setBalance(depositAccount.getBalance().add(amount));
        debitAccount.setUnlock();
        depositAccount.setUnlock();
    }

    /**
     * Метод, который читает конфигурацию банка (кол-во клиентов, счетов и общая сумма всех счетов).
     *
     * @param propertiesFileName - имя файла с настройками.
     */
    void readPropertiesBank(String propertiesFileName) {
        Properties properties = new Properties();
        try {
            properties.load(Bank.class.getResourceAsStream(propertiesFileName));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        numberOfBankClients = Integer.parseInt(properties.getProperty("number_of_bank_clients"));
        numberOfBankAccounts = Integer.parseInt(properties.getProperty("number_of_bank_accounts"));
        startTotalAccounts = BigDecimal.valueOf(Long.parseLong(properties.getProperty("total_of_all_bank_accounts")));
    }

    /**
     * Процедура инициализации счетов банка, на которые производится зачисление случайных сумм ( X руб. Y копеек).
     * При заполнении выполняется проверка общей суммы всех счетов банка startTotalAccounts.
     */
    void initializeAccountsBank() {
        BigDecimal currentLimit = startTotalAccounts;
        for (int i = 1; i < numberOfBankAccounts; i++) {
            BigDecimal amount = randomAmountBalanceOrTransaction();
            if (amount.compareTo(currentLimit) > 0) {
                amount = currentLimit;
                currentLimit = BigDecimal.ZERO;
            } else {
                currentLimit = currentLimit.subtract(amount);
            }
            bankAccountList.add(new Account(amount));
        }
        bankAccountList.add(new Account(currentLimit));
    }

    /**
     * Метод, который производит подсчет суммы всех счетов в банке на момент вызова.
     *
     * @return - сумма всех счетов.
     */
    public BigDecimal calcTotalAccountsBank() {
        BigDecimal currentTotalAccounts = getAccountList().stream()
                .peek(Account::setLock)
                .map(Account::getBalance)
                .reduce(new BigDecimal(0), BigDecimal::add);
        getAccountList().forEach(Account::setUnlock);
        return currentTotalAccounts;
    }

    /**
     * Метода добавляем на 0-й счет дополнительную сумму средств (сверх начальной суммы startTotalAccounts).
     *
     * @param failAmount - сумма средств ( X руб. Y копеек), которая будет зачислена счет.
     */
    public void emulateFail(double failAmount) {
        BigDecimal amount = BigDecimal.valueOf(failAmount);
        Account failAccount = bankAccountList.get(0);
        failAccount.setBalance(failAccount.getBalance().add(amount));
    }

    /**
     * Метод для генерации случайного значения суммы, который используется при иницализации счетов и
     * осуществления клиентами переводов между ними.
     *
     * @return - сумма средств ( X руб. Y копеек) в пределах 0 < amount <= maxAmount.
     */
    public BigDecimal randomAmountBalanceOrTransaction() {
        double maxAmount = startTotalAccounts.intValue() * 2 / (double) numberOfBankAccounts;
        return BigDecimal.valueOf(Math.random() * maxAmount).setScale(2, RoundingMode.CEILING);
    }
}