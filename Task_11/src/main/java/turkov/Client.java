package turkov;

import java.math.BigDecimal;

/**
 * Класс клиента в банке.
 */
public class Client extends Thread {

    private final Bank bank;

    public Client(Bank bank) {
        this.bank = bank;
    }

    /**
     * Метод осуществляет запуск всех клиентов банка в виде отдельных потоков.
     * Количество клиентов зависит от первоначальных настроек в конфигурационном файле банка *.properties.
     */
    public static void startClients(Bank bank) {
        for (int i = 0; i <= bank.getNumberBankClients(); i++) {
            Thread client = new Client(bank);
            client.start();
        }
    }

    /**
     * Метод, который имитирует перевод средств клиентом со одного случайно выбранного счета на другой. Номера счетов и
     * сумма генерируются случайным образом. Если номера счетов совпали, случайный выбор дебетового счета повторяется.
     * Сумма перевода amount ( X руб. Y копеек) будет подбираться до тех пор, пока не выполнится условие
     * 0 < amount <= maxAmount.
     *
     * @see Bank#randomAmountBalanceOrTransaction()
     */
    @Override
    public void run() {
        int numDebitAccount = randomAccount();
        int numDepositAccount = randomAccount();
        BigDecimal amount;
        if (numDebitAccount == numDepositAccount) {
            numDebitAccount = randomAccount();
        }
        do {
            amount = bank.randomAmountBalanceOrTransaction();
        } while (amount.compareTo(BigDecimal.ZERO) == 0);
        bank.makeTransaction(numDebitAccount, numDepositAccount, amount);
    }

    /**
     * Метод генерирует случайный номер счета в банке.
     *
     * @return - номер счета
     */
    private int randomAccount() {
        return (int) (Math.random() * bank.getNumberBankAccounts());
    }
}