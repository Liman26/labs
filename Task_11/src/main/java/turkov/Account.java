package turkov;

import java.math.BigDecimal;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Класс, который описывает счет в банке
 */
public class Account {
    /**
     * Блокировка счета, обесечивающая конкурентную работу потоков
     */
    public final Lock lock = new ReentrantLock();
    /**
     * Баланс счета
     */
    private BigDecimal balanceAccount;

    public Account(BigDecimal amount) {
        this.balanceAccount = amount;
    }

    public BigDecimal getBalance() {
        return balanceAccount;
    }

    public void setBalance(BigDecimal amount) {
        this.balanceAccount = amount;
    }

    /**
     * Метод установки блокировки на счет (на время проведения операций с ним)
     */
    public void setLock() {
        lock.lock();
    }

    /**
     * Метод снятия блокировки со счета
     */
    public void setUnlock() {
        lock.unlock();
    }
}