package emelyanov;

import java.math.BigDecimal;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Банковский счет
 */
public class Account {
    /**
     * Остаток на счета
     */
    private BigDecimal rest;

    /**
     * Блокировка счета на момент работы со счетом
     */
    private final ReentrantLock lock = new ReentrantLock();

    public Account(BigDecimal rest) {
        this.rest = rest;
    }

    public BigDecimal getRest() {
        return rest;
    }

    public void setRest(BigDecimal rest) {
        this.rest = rest;
    }

    /**
     * Заблокировать работу со счетом,
     * если он уже залочен, то будет остановка потока до разблокировки
     */
    public void lock() {
        lock.lock();
    }

    /**
     * Разблокировать работу со счетом
     */
    public void unlock() {
        lock.unlock();
    }
}
