package emelyanov;

import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static emelyanov.Util.randomInteger;

/**
 * Клиент, выполняющий операции в банке со случайными счетами
 */
public class Client extends Thread {
    /**
     * Банк с которым работает клиет
     */
    private final Bank bank;
    /**
     * Максимальная сумма операции со счетами
     */
    private final int maxSummaOperation;

    /**
     * Конструктор клиента банка bank, могущего перекинуть средства до maxAccountRest
     *
     * @param bank              - банк
     * @param maxSummaOperation - максимальная сумма операции
     */
    public Client(Bank bank, int maxSummaOperation) {
        this.bank = bank;
        this.maxSummaOperation = maxSummaOperation;
    }

    /**
     * Перевод случайной суммы со случаного счета на случайный счет
     */
    @Override
    public void run() {
        int idAccountFrom = randomInteger(bank.getCountAccounts());
        int idAccountTo = randomInteger(bank.getCountAccounts());
        while (idAccountFrom == idAccountTo) {
            idAccountTo = randomInteger(bank.getCountAccounts());
        }
        bank.makeTransaction(idAccountFrom, idAccountTo, new BigDecimal(randomInteger(maxSummaOperation)));
    }

    /**
     * Создать и запустить всех клиентов в Pool-е размером countTransaction,
     * пока в банке результат Аудитора не выставлен в ошибку
     * или не достигли заданного количества countClient
     *
     * @param maxSummaOperation - максимальная сумма перевода
     * @param countClient       - количество клиентов
     * @param countTransaction  - максимальное кличество потоков в пуле
     * @param bank              - банк, по которому идут операции
     */
    public static boolean startClients(int maxSummaOperation, int countClient, int countTransaction, Bank bank) {
        ExecutorService service = Executors.newFixedThreadPool(countTransaction);
        for (int i = 0; i < countClient; i++) {
            if (bank.getResultAudit().equals(AuditorResults.GOOD))
                service.execute(new Client(bank, maxSummaOperation));
        }
        service.shutdown();
        try {
            return service.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

}
