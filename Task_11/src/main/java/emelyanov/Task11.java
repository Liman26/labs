package emelyanov;

import static emelyanov.Auditor.startAuditor;
import static emelyanov.Client.startClients;

public class Task11 {
    public static void main(String[] args) {
        int limitRestAccount = 100;
        int countAccount = 2;
        int countClient = 1000;
        int countThreadInPool = 1000;

        Bank bank = new Bank();
        bank.generateAccounts(limitRestAccount, countAccount);
        System.out.println(bank.getLimitRestAccounts());
        System.out.println(bank.getCurrentRestAccounts());
        startAuditor(bank);
        startClients(limitRestAccount, countClient, countThreadInPool, bank);

        System.out.println("Прошло переводов:" + bank.getCounterTransaction());
        System.out.println(bank.getResultAudit().getDescription());
    }
}
