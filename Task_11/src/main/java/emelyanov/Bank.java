package emelyanov;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

import static emelyanov.Util.randomInteger;

/**
 * Банк содержащий счета и выполняющий операции с ними
 */
public class Bank {

    /**
     * Общий остаток всех счетов, на момент начала работы клиентов
     * Заполняется при добавлении(генерации) счетов в банк
     */
    private BigDecimal limitRestAccounts = new BigDecimal(0);

    /**
     * Результат проверки аудитора
     */
    private AuditorResults resultAudit = AuditorResults.GOOD;

    /**
     * Количество прошедших переводов
     */
    private final AtomicLong counterTransaction = new AtomicLong(0);

    /**
     * Список счетов банка
     */
    private final List<Account> accountList = new ArrayList<>();

    public AuditorResults getResultAudit() {
        return resultAudit;
    }

    public void setResultAudit(AuditorResults resultAudit) {
        this.resultAudit = resultAudit;
    }

    public BigDecimal getLimitRestAccounts() {
        return limitRestAccounts;
    }

    public long getCounterTransaction() {
        return counterTransaction.get();
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    /**
     * Добавить счет в банке с остатком и увеличить общий остаток limitRestAccounts
     *
     * @param rest - остаток на счете
     */
    public void openAccount(BigDecimal rest) {
        accountList.add(new Account(rest));
        this.limitRestAccounts = this.limitRestAccounts.add(rest);
    }

    /**
     * Получаем сумму актуальных остатков всех счетов в банке
     * На момент считываения остатка блокируем счет
     *
     * @return сумма остатков на текущий момент
     */
    public BigDecimal getCurrentRestAccounts() {
        BigDecimal result = new BigDecimal(0);
        for (Account account : accountList) {
            account.lock();
            try {
                result = result.add(account.getRest());
            } finally {
                account.unlock();
            }
        }
        return result;
    }

    /**
     * Получить количество счетов в банке
     *
     * @return количество
     */
    public int getCountAccounts() {
        return accountList.size();
    }

    /**
     * Перевод средств с одного счета на другой, если есть остаток, если нет, то на выход
     *
     * @param idAccountFrom идентификатор счета с которого переводим
     * @param idAccountTo   идентификатор счета на который переводим
     * @param summa         сумма перевода
     */
    public void makeTransaction(int idAccountFrom, int idAccountTo, BigDecimal summa) {
        Account accountFrom = accountList.get(idAccountFrom);
        Account accountTo = accountList.get(idAccountTo);
        counterTransaction.incrementAndGet();
        System.out.println("Transaction:" + counterTransaction + " " + Thread.currentThread().getName() + " from " + idAccountFrom + " to " + idAccountTo + " summa=" + summa.toString());
        try {
            accountFrom.lock();
            accountTo.lock();
            if (accountFrom.getRest().compareTo(summa) < 0) {
                return;
            }
            accountFrom.setRest(accountFrom.getRest().subtract(summa));
            accountTo.setRest(accountTo.getRest().add(summa));
        } finally {
            accountFrom.unlock();
            accountTo.unlock();
        }
    }

    /**
     * Открыть счета в банке со случайным остатком на общую сумму maxRestAccounts
     *
     * @param maxRestAccounts - общая сумма счетов
     * @param countAccount    - количество счетов
     */
    public void generateAccounts(int maxRestAccounts, int countAccount) {
        if (countAccount <= 0) return;
        int limitRestAccounts = maxRestAccounts;
        for (int i = 0; i < countAccount - 1; i++) {
            int currentRestAccount = randomInteger(limitRestAccounts);
            openAccount(new BigDecimal(currentRestAccount));
            limitRestAccounts -= currentRestAccount;
        }
        openAccount(new BigDecimal(limitRestAccounts));
    }
}
