package emelyanov;

/**
 * Возможные состояния проверки Аудитора
 */
public enum AuditorResults {
    GOOD("AUDIT GOOD"),
    ERROR_LIMIT("AUDIT ERROR: Summa All Accounts is BAD");

    private final String description;

    AuditorResults(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
