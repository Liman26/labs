package emelyanov;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;

public class Util {
    /**
     * Генератор случайных чиссел
     *
     * @param maxValue максимальное значение
     * @return случайное число типа int от 0 до maxValue
     */
    public static int randomInteger(int maxValue) {
        return (int) Math.round(Math.random() * (maxValue - 1));
    }

    /**
     * Получить настройки из файла
     *
     * @param fileName - имя файла с настройками
     * @return настройки
     */
    public static Properties getProperties(String fileName) {
        Properties properties = new Properties();
        try {
            try (FileReader fr = new FileReader(Objects.requireNonNull(Util.class.getResource(fileName)).getPath())) {
                properties.load(fr);
            }
        } catch (IOException ex) {
            ex.printStackTrace(System.out);
        }
        return properties;
    }
}
