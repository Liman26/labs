package emelyanov;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Аудитор проверяющий что остаток на счетах соответствует остатку на счетах при начале обработки клиентов
 */
public class Auditor extends Thread {

    /**
     * Банк по которому идёт Аудит
     */
    private final Bank bank;

    public Auditor(Bank bank) {
        this.bank = bank;
        System.out.println("Auditor create for bank with All Rest: " + bank.getLimitRestAccounts());
    }

    /**
     * Сверяет текущий остаток на всех счетах банка и сверяем с первоначальным.
     * Если не совпадает, то сообщаем банку об ошибке и заканчиваем дальнейшие проверки.
     */
    @Override
    public void run() {
        bank.setResultAudit(AuditorResults.GOOD);
        while (bank.getResultAudit().equals(AuditorResults.GOOD)){
            System.out.println("Audit run");
            if (!bank.getLimitRestAccounts().equals(bank.getCurrentRestAccounts())) {
                bank.setResultAudit(AuditorResults.ERROR_LIMIT);
            }
            System.out.println("Audit end");
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Запустить аудитор в банке
     *
     * @param bank - проверяемый банк
     * @return аудитор
     */
    public static Auditor startAuditor(Bank bank) {
        Auditor auditor = new Auditor(bank);
        auditor.setDaemon(true);
        auditor.start();
        return auditor;
    }
}
