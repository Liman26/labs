package bachishche;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.Callable;

import static java.lang.Math.abs;

/**
 * Набор вспомогательных методов для работы и тестирования
 * многопользовательского банковского приложения
 */
public class BankUtils {
    private static Random rand = new Random();

    /**
     * Вернуть слуайно сгенерированное положительно целое число
     * от 1 до max - 1
     *
     * @param max верхняя граница генерации числа
     * @return Слуайно сгенерированное положительное целое число
     */
    public static int nextPositiveInt(int max) {
        if (max < 1)
            throw new IllegalArgumentException();
        return abs(rand.nextInt(max));
    }

    /**
     * Вернуть слуайно сгенерированное положительно целое число
     * от 1 до max - 1, исключая заданный параметр
     *
     * @param max       верхняя граница генерации числа
     * @param notEquals число, которому заданное число не должно быть равно
     * @return Слуайно сгенерированное положительное целое число
     */
    public static int nextPositiveInt(int max, int notEquals) {
        if (max < 1)
            throw new IllegalArgumentException();
        int next = abs(rand.nextInt(max));
        while (next == notEquals)
            next = abs(rand.nextInt(max));
        return next;
    }

    /**
     * Генерация списка клиентов определенного банка
     *
     * @param clientNumber   количество клиентов
     * @param maxTransaction макисмально допустимая сумма для перевода
     * @param bank           банк, со счетами которого взаимодействует клиент
     * @return список клинетов
     */
    public static List<Callable<Object>> getClientList(int clientNumber, BigDecimal maxTransaction, Bank bank) {
        ArrayList<Callable<Object>> clients = new ArrayList<>();
        for (int i = 0; i < clientNumber; i++) {
            clients.add(new Client(bank, maxTransaction));
        }
        return clients;
    }
}
