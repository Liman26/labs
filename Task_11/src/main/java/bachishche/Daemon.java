package bachishche;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Демон, проверяющий, чтобы сумма на всех счетах в банке была равно изначально заданной
 * общей сумме счетов
 */
public class Daemon extends Thread {
    private final BigDecimal totalBalance;
    private final Bank bank;
    private boolean active = true;

    public Daemon(Bank bank, BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
        this.bank = bank;
    }

    /**
     * Метод, запускающийся при создании потока. Работает до
     * завершения родительского потока, так как является демоном.
     * Проверяет сумму всех средств на корректность через заданные промежутки времени.
     */
    public void run() {
        while (active) {
            try {
                sleep(100);
                checkTotalSum();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException(e.getMessage());
            }
        }
    }

    /**
     * Проверка исходной общей суммы средств и средств на всех счетах,
     * скопированных в блоке synchronized.
     */
    void checkTotalSum() throws InterruptedException {
        ArrayList<Account> accounts;
        synchronized (bank.accounts) {
            accounts = (ArrayList<Account>) bank.accounts.clone();
            System.out.println("Daemon work in " + Thread.currentThread().getName());
        }
        BigDecimal currentSum = accounts.stream().map(Account::getBalance).reduce(BigDecimal.ZERO, BigDecimal::add);
        if (!currentSum.equals(totalBalance))
            throw new InterruptedException("The sum of the current account balances (" + currentSum +
                    ") and the initial total balance (" + totalBalance + ") are not equal!");
    }

    /**
     * Указать потоку, что работу необходимо завершить
     */
    @Override
    public void interrupt() {
        active = false;
    }

    /**
     * Проверить, завершает ли поток своб работу
     * @return true - поток завершается, false - поток продолжает работу
     */
    @Override
    public boolean isInterrupted() {
        return !active;
    }
}
