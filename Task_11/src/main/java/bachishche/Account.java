package bachishche;

import java.math.BigDecimal;

/**
 * Класс, описывающий сущность аккаунта (счета) в банке
 */
public class Account extends Thread implements Cloneable {
    private BigDecimal balance;

    /**
     * @param sum начальный баланс счёта
     */
    public Account(BigDecimal sum) {
        if (sum.signum() < 0)
            throw new IllegalArgumentException("Invalid account balance!");
        balance = sum;
    }

    /**
     * Узнать текущий баланс счёта
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Снять со счета указанную сумму средств
     *
     * @param delta сумма средств, которую необходимо снять со счета
     */
    public void getMoney(BigDecimal delta) {
        if (balance.subtract(delta).signum() < 0)
            throw new IllegalArgumentException("The account has insufficient funds.");
        balance = balance.subtract(delta);
    }

    /**
     * Пополнить счет
     *
     * @param delta сумма средств, которую необходимо положить на счет
     */
    public void addMoney(BigDecimal delta) {
        balance = balance.add(delta);
    }

    /**
     * Перегрузка метода клонирования
     *
     * @return новый объект с теми же значениями полей,
     * что и исходный
     */
    @Override
    public Object clone() {
        return new Account(this.balance);
    }

}
