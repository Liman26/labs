package bachishche;

import java.math.BigDecimal;
import java.util.concurrent.Callable;

import static bachishche.BankUtils.nextPositiveInt;
import static java.lang.Math.*;
import static java.lang.Thread.sleep;

/**
 * Класс-клиент: совершает переводы между
 * двумя счетами банка в отдельном потоке
 */
public class Client implements Callable<Object> {

    private final Bank bank;
    private BigDecimal maxTransactionValue;

    public Client(Bank bank, BigDecimal maxTransactionValue) {
        this.bank = bank;
        this.maxTransactionValue = maxTransactionValue;
    }

    /**
     * Метод, вызывающийся при работе объекта в отдельном потоке.
     * Вызывает запрос в банк на перевод между счетами.
     */

    @Override
    public Object call() {
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        transferRequest();
        return null;
    }

    /**
     * Запрос в банк на перевод средств между двумя счетами.
     * Сумма перевода, намера счета источника и счета назначения генерируются случаным образом.
     * Для обеспечения безопасности операции в многопоточном приложении
     * сперва блокируется счет с меньшим номером, а затем с большим.
     */
    private void transferRequest() {
        int fromId = nextPositiveInt(bank.getAccountNumber());
        int toId = nextPositiveInt(bank.getAccountNumber(), fromId);
        BigDecimal sum = new BigDecimal(nextPositiveInt(maxTransactionValue.intValue()));

        final Account less = bank.accounts.get(min(fromId, toId));
        final Account large = bank.accounts.get(max(fromId, toId));
        String result;
        synchronized (less) {
            synchronized (large) {
                if (fromId < toId)
                    result = transferRequest(less, large, sum);
                else
                    result = transferRequest(large, less, sum);
            }
        }
        logTransferInfo(fromId, toId, sum, result);
    }

    /**
     * Запрос банку на перевод заданной суммы между двумя счетами
     *
     * @param from счет источник
     * @param to   счет назначения
     * @param sum  сумма перевода
     * @return строка, содержащая данные о
     * балансе счетов во время попытки перевода и результат выполнения операции
     */
    private String transferRequest(Account from, Account to, BigDecimal sum) {
        BigDecimal fromBalanceBefore = from.getBalance();
        BigDecimal toBalanceBefore = to.getBalance();
        boolean transferred = bank.transfer(from, to, sum);
        return logTransferResultInfo(fromBalanceBefore, toBalanceBefore, transferred);
    }

    /**
     * Формирование строки отладочной информации, содержащей данные о
     * всех параметрах операции и результат её выполнения
     *
     * @param fromId номер счета источника
     * @param toId   номер счета назначения
     * @param sum    сумма перевода
     * @param result строка отладочной информации с результатом выполнения операции,
     *               сформированаая в моент запроса на перевод в банк методом
     *               logTransferResultInfo
     */
    private void logTransferInfo(int fromId, int toId, BigDecimal sum, String result) {
        System.out.println(Thread.currentThread().getName() + " try to transfer SUM: " + sum + " from " +
                fromId + " to " + toId + " " + result);
    }

    /**
     * Формирование строки отладочной информации, содержащей данные о
     * балансе счетов при попытке перевода
     *
     * @param from        счетисточник
     * @param to          счет назначения
     * @param transferred true, если операция была проведена
     *                    false - если операция была отклонена банком
     */
    private String logTransferResultInfo(BigDecimal from, BigDecimal to, boolean transferred) {
        StringBuilder result = new StringBuilder("(" + from + "->" + to + ")");
        if (transferred)
            result.append(" accept");
        else result.append(" decline");
        return result.toString();
    }
}
