package bachishche;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Структура банка, имеющего определенное количество счетов и
 * позволяющего переводить средства между двумя счетами
 */
public class Bank {
    private final int accountNumber;
    private final BigDecimal totalBalance;
    final ArrayList<Account> accounts;

    /**
     * Конструктор объекта. Создает список счетов заданного размера,
     * случайным образом распределяя между ними заданную общую сумму счетов
     *
     * @param accountNumber число счетов в банке
     * @param totalBalance  общая сумма вкладов на счетах
     */
    public Bank(int accountNumber, BigDecimal totalBalance) {
        this.accountNumber = accountNumber;
        this.totalBalance = totalBalance;
        accounts = new ArrayList<>();

        int available = this.totalBalance.intValue();
        for (int i = 0; i < accountNumber - 1; i++) {
            int currentBalance = BankUtils.nextPositiveInt(available / accountNumber);
            available -= currentBalance;
            accounts.add(new Account(new BigDecimal(currentBalance)));
        }
        accounts.add(new Account(new BigDecimal(available)));
    }

    /**
     * Перевести средства между счетами. Если на счете-исчтонике нет
     * необходимой для перевода суммы - операция отклоняется
     *
     * @param from счет, откуда необходимо снять средства
     * @param to   счёт зачисления средств
     * @param sum  сумма средств, которую необходимо перевести
     */
    public boolean transfer(Account from, Account to, BigDecimal sum) {
        if (from.getBalance().subtract(sum).signum() < 0)
            return false;
        from.getMoney(sum);
        to.addMoney(sum);
        return true;
    }

    public int getAccountNumber() {
        return accountNumber;
    }
}
