package mescheryakov;

/**
 * Класс, реализующий передачу денег с одного счета на другой.
 * Для этого он выбирает 2 рандомных счета и сумму
 */
public class Client extends Thread {
    private final Bank bank;

    /**
     * Конструктор работы клиента
     *
     * @param bank банк, который работает со счетами
     */
    public Client(Bank bank) {
        this.bank = bank;
    }

    /**
     * Метод для перевода денег с одного счета на другой
     *
     * @param from счет, откуда переводятся деньги
     * @param to   счет, куда переводятся деньги
     * @param sum  сумма перевода
     */
    private void transfer(Account from, Account to, long sum) {
        boolean difference = bank.difference;
        if (from.subMoney(sum, difference)) {
            to.addMoney(sum, difference);
        }
    }

    /**
     * Переопределенный метод run(), который занимается переводом денег с одного счета на другой
     */
    @Override
    public void run() {
        while (!bank.isFail) {
            int from = (int) (Math.random() * bank.numberOfAccounts);
            int to = (int) (Math.random() * bank.numberOfAccounts);
            if (from != to) {
                long sum = (long) (Math.random() * bank.balance / bank.numberOfAccounts * 2);
                if (from < to) {
                    synchronized (bank.accounts[from]) {
                        synchronized (bank.accounts[to]) {
                            transfer(bank.accounts[from], bank.accounts[to], sum);
                        }
                    }
                } else {
                    synchronized (bank.accounts[to]) {
                        synchronized (bank.accounts[from]) {
                            transfer(bank.accounts[from], bank.accounts[to], sum);
                        }
                    }
                }
            }
        }
    }
}
