package mescheryakov;

/**
 * Класс, реализующий операции с клиентами и балансами в банке
 */
public class Bank {
    int numberOfClients;
    int numberOfAccounts;
    long balance;
    Account[] accounts;
    Client[] clients;
    Demon demon;
    boolean isFail = false;
    boolean difference = false;

    /**
     * Коструктор для работы банка
     *
     * @param numberOfClients  количество клиентов
     * @param numberOfAccounts количество счетов
     * @param balance          баланс банка
     */
    public Bank(int numberOfClients, int numberOfAccounts, long balance) {
        this.numberOfClients = numberOfClients;
        this.numberOfAccounts = numberOfAccounts;
        this.balance = balance;

        accounts = new Account[this.numberOfAccounts];
        long averageBalance = balance / this.numberOfAccounts;
        long rest = balance;
        for (int i = 0; i < this.numberOfAccounts; i++) {
            if (i != (numberOfAccounts - 1)) {
                accounts[i] = new Account(averageBalance);
                rest = rest - averageBalance;
            } else
                accounts[i] = new Account(rest);
        }
        clients = new Client[this.numberOfClients];
        for (int i = 0; i < this.numberOfClients; i++) {
            clients[i] = new Client(this);
            clients[i].start();
        }
        demon = new Demon(this);
        demon.start();
    }
}
