package mescheryakov;

/**
 * Класс, содержащий методы, реализующие управление счетом в банке
 */
public class Account {
    private static int listAccounts = 0;
    private final int numberAccount;
    private long balance;
    private long addition = 0;

    /**
     * Констуктор для создания нового счета
     *
     * @param balance - остаток на балансе счета
     */
    public Account(long balance) {
        this.numberAccount = listAccounts++;
        this.balance = balance;
    }

    /**
     * Геттер для баланса
     *
     * @return остаток на балансе счета
     */
    public long getBalance() {
        return balance;
    }

    /**
     * Метод, для получения нового баланса после изменений
     *
     * @return баланс с изменениями
     */
    public long newBalance() {
        return balance + addition;
    }

    /**
     * Метод, реализуюзий добавление денег на счет
     *
     * @param addition   сумма добавления
     * @param difference изменение в счете
     */
    public void addMoney(long addition, boolean difference) {
        if (difference) this.addition = this.addition + addition;
        else balance = balance + addition;
    }

    /**
     * Метод, реализующий вывод денег со счета
     *
     * @param addition   сумма вывода
     * @param difference изменнеие в счете
     * @return результат, получилось ли вывести деньги со счета
     */
    public boolean subMoney(long addition, boolean difference) {
        if (newBalance() >= addition) {
            if (difference) this.addition = this.addition - addition;
            else balance = balance - addition;
            return true;
        } else return false;
    }

    /**
     * Метод, реализующий применение изменений в счете
     */
    public void applyChanges() {
        balance = balance + addition;
        addition = 0;
    }

    @Override
    public String toString() {
        return "" + numberAccount + "(" + newBalance() + ")";
    }
}

