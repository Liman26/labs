package mescheryakov;

/**
 * Класс "демон", проверяющий, что все операции проходят верно (без утечки денежных средств)
 */
public class Demon extends Thread {
    private final Bank bank;

    /**
     * Конструктор работы "демона"
     *
     * @param bank банк, который работает со счетами
     */
    public Demon(Bank bank) {
        this.bank = bank;
    }

    /**
     * Переопределенный метод run(), осуществляющий работу "демона"
     */
    @Override
    public void run() {
        while (!bank.isFail) {
            bank.difference = false;
            for (int i = 0; i < bank.accounts.length; i++) {
                synchronized (bank.accounts[i]) {
                    bank.accounts[i].applyChanges();
                }
            }

            bank.difference = true;
            for (int i = 0; i < bank.accounts.length; i++) {
                synchronized (bank.accounts[i]) {
                }
            }
            long testSum = 0;
            for (int i = 0; i < bank.accounts.length; i++) {
                synchronized (bank.accounts[i]) {
                    testSum = testSum + bank.accounts[i].getBalance();
                }
            }
            if (testSum != bank.balance) {
                bank.isFail = true;
                System.err.println("Баланс недоступен");
            } else System.out.println("Баланс доступен");
        }
    }
}
