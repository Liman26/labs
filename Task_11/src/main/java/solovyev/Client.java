package solovyev;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Класс характеризующий клиента банка, который бесконечно переводит деньги со счета на счет
 * столько раз, сколько всего счетов в банке
 */
public class Client extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < Bank.accountList.size(); i++) {
            Account from = Bank.accountList.get(randomAccount());
            Account to = Bank.accountList.get(randomAccount());

            Bank.makeTransaction(from, to, getRandomSum(from));
        }
    }

    /**
     * Метод случайныйм образом генерирует индекс счета.
     *
     * @return индекс счета
     */
    public int randomAccount() {
        return ThreadLocalRandom.current().nextInt(0, Bank.accountList.size());
    }

    /**
     * Метод генерирует число, не превышающее количество средств на счете
     *
     * @param from счет с которого хотят перевести деньги
     * @return количество денег для перевода
     */
    public BigDecimal getRandomSum(Account from) {
        if (from.getSumma().intValue() > 1)
            return BigDecimal.valueOf(ThreadLocalRandom.current().nextInt(1, from.getSumma().intValue() + 1));
        else
            return BigDecimal.ZERO;
    }
}
