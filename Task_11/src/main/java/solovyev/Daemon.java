package solovyev;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Поток наблюдатель, следит за тем чтобы общая денежная масса в банке была постоянной
 */
public class Daemon extends Thread {

    private final int expectedBankAmount;
    private Bank bank;
    private boolean activityStatus = true;

    public Daemon(Bank bank, int expectedBankAmount) {
        this.expectedBankAmount = expectedBankAmount;
        this.bank = bank;
    }

    @Override
    public void run() {
        while (activityStatus) {

            lockAccounts(Bank.accountList);
            List<Account> accounts = clone(Bank.accountList);

            System.err.println("Daemon checking start");

            int summa = accounts.stream().map(a -> a.getSumma().intValue()).reduce(0, Integer::sum);

            if (summa != expectedBankAmount) {
                System.err.println("Money lost " + summa);

                activityStatus = false;
                throw new RuntimeException("MONEY LEAK");
            } else
                System.err.println("STATUS: OK");

            unlockAccounts(Bank.accountList);

            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Метод клонирующий список счетов
     *
     * @param accounts список
     * @return возврщает копию списка
     */
    public List<Account> clone(List<Account> accounts) {
        return accounts.stream()
                .map(acc -> new Account(acc.getSumma()))
                .collect(Collectors.toList());
    }

    /**
     * Метод блокирующий все счета
     *
     * @param accounts список счетов
     */
    private void lockAccounts(List<Account> accounts) {
        accounts.forEach(Account::lock);
    }

    /**
     * Метод разблокирующий все счета
     *
     * @param accounts список счетов
     */
    private void unlockAccounts(List<Account> accounts) {
        accounts.forEach(Account::unlock);
    }

}
