package solovyev;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Класс для получения настроек из проперти
 */
public class MyProperties {

    public static int BANK_AMOUNT;
    public static int QUANTITY_OF_CLIENTS;
    public static int QUANTITY_OF_ACCOUNTS;

    /**
     * Метод загружающий данные из проперти файла в статические переменные объекта
     *
     * @param fileName имя проперти файла
     * @return объект проперти
     */
    public static Properties loadProperties(String fileName) {
        Properties properties = new Properties();
        try {
            try (InputStream io = MyProperties.class.getResourceAsStream(fileName)) {
                properties.load(io);

            }
        } catch (IOException ex) {
            throw new RuntimeException("File do not exist");
        }

        BANK_AMOUNT = Integer.parseInt(properties.getProperty("bank_amount"));
        QUANTITY_OF_ACCOUNTS = Integer.parseInt(properties.getProperty("quantity_of_accounts"));
        QUANTITY_OF_CLIENTS = Integer.parseInt(properties.getProperty("quantity_of_clients"));

        return properties;
    }
}


