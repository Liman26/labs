package solovyev;

import java.math.BigDecimal;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Класс характеризуюий счет пользователя в банке
 */
public class Account {

    private BigDecimal summa;

    ReentrantLock lock = new ReentrantLock();

    /**
     * Конструктор для нового счета
     *
     * @param summa количество денег на счету
     */
    public Account(BigDecimal summa) {
        this.summa = summa;
    }

    public BigDecimal getSumma() {
        return summa;
    }

    /**
     * Метод используемый для списания средств со счета
     *
     * @param summa переводимая сумма
     * @return true, если на счете достаточно средств для перевода, иначе false
     */
    public boolean checkAndWriteOff(BigDecimal summa) {
        try {
            lock();
            if ((this.summa.intValue() - summa.intValue()) >= 0) {
                this.summa = this.summa.subtract(summa);
                return true;
            } else
                return false;
        } finally {
            unlock();
        }
    }

    /**
     * Метод используемый для пополнения счета
     *
     * @param summa
     */
    public void incoming(BigDecimal summa) {
        try {
            lock();
            this.summa = this.summa.add(summa);
        } finally {
            unlock();
        }
    }

    /**
     * Метод блокирующий счет
     */
    public void lock() {
        lock.lock();
    }

    /**
     * Метод разблокирующий счет
     */
    public void unlock() {
        lock.unlock();
    }
}
