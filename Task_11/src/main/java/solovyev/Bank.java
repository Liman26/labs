package solovyev;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс характериющий банк
 */
public class Bank {

    /**
     * Сумма всех средств
     */
    public final BigDecimal amount;

    /**
     * Список всех счетов
     */
    public static List<Account> accountList = new ArrayList<>();

    /**
     * Конструктор инициализирующий сумму всех средств в банке
     *
     * @param amount сумма всех средств
     */
    public Bank(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Геттер для списка счетов
     *
     * @return список счетов
     */
    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;

    }

    /**
     * Метод совершает транзакцию, переводя деньги с одного счета на другой
     * При попытке перевести 0, ничего не будет происходить
     *
     * @param from  счет отправителя
     * @param to    счет получателя
     * @param summa переводимая сумма
     */
    static void makeTransaction(Account from, Account to, BigDecimal summa) {
        if (summa.intValue() != 0) {
            System.out.println(from + " Before: from: " + from.getSumma() + " to: " + to.getSumma() + " :: " + summa);
            if (from.checkAndWriteOff(summa)) {
                to.incoming(summa);
                System.out.println(from + " After: from: " + from.getSumma() + " to: " + to.getSumma() + " :: " + summa);
            } else
                System.out.println(from + " Operation canceled");
        }
    }
}
