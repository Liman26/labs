package solovyev;

/**
 * Класс ошибок для моих аннотаций
 */
public class MyAnnotationException extends RuntimeException {
    public MyAnnotationException(String message) {
        super(message);
    }

}
