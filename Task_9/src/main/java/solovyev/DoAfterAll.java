package solovyev;

import java.lang.annotation.*;

/**
 * Аннотация DoAfterAll
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface DoAfterAll {
}
