package solovyev;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Класс используемый для обработки аннотаций.
 */
public class MyJUnitProcessor {
    /**
     * Статический метод для запуска тестов.
     * Пишет название запускаемого метода.
     *
     * @param someClass Класс в котором разыскиваются методы с аннотациями DoBeforeAll, DoTest, DoAfterAll
     */
    public static List<Method> startTests(Class<?> someClass) {

        List<Method> methods = collectOrderly(someClass);

        for (Method method : methods) {
            System.out.println(method);
            try {
                method.invoke(someClass.getDeclaredConstructor().newInstance());
            } catch (Exception ignored) {
            }
        }

        return methods;
    }

    /**
     * Метод упорядочивающий методы в указанном порядке.
     * Методы с аннотацией @DoBeforeAll   0 или 1
     * Методы с аннотацией @DoTest  >=1
     * Методы с аннотацией @DoAfterAll   0 или 1
     * Если какое то из условий нарушено то выбрасывается исключение.
     *
     * @param someClass Класс в котором ищем методы с аннотациями DoTest, DoBeforeAll, DoAfterAll
     * @return список методов, в порядке их дальнейшего запуска
     */
    private static List<Method> collectOrderly(Class<?> someClass) {

        List<Method> orderedList = new ArrayList<>();
        List<Method> DoTestList = new ArrayList<>();
        List<Method> DoBeforeAllList = new ArrayList<>();
        List<Method> DoAfterAllList = new ArrayList<>();

        Stream.of(someClass.getDeclaredMethods())
                .forEach(method -> {
                            if (method.isAnnotationPresent(DoBeforeAll.class)) {
                                DoBeforeAllList.add(method);
                                if (DoBeforeAllList.size() > 1)
                                    throw new MyAnnotationException("Аннотаций @DoBeforeAll не может быть больше 1.");
                            }

                            if (method.isAnnotationPresent(DoTest.class)) {
                                DoTestList.add(method);
                            }

                            if (method.isAnnotationPresent(DoAfterAll.class)) {
                                DoAfterAllList.add(method);
                                if (DoAfterAllList.size() > 1)
                                    throw new MyAnnotationException("Аннотаций @DoAfterAll не может быть больше 1.");
                            }
                        }
                );
        if (DoTestList.isEmpty())
            throw new MyAnnotationException("Тесты не объявлены.");

        if (DoBeforeAllList.size() > 0)
            orderedList.add(0, DoBeforeAllList.get(0));

        orderedList.addAll(sortTestsByOrder(DoTestList));

        if (DoAfterAllList.size() > 0)
            orderedList.add(orderedList.size(), DoAfterAllList.get(0));

        return orderedList;
    }

    /**
     * Метод используемый для сортировки списка методов DoTest в порядке их приоритетов
     * Если некоторым тестам приоритет задан, то они будут выполнены в перую очередь
     *
     * @param testsList список методов помеченных аннотацией @DoTest
     * @return отсортированный список методов
     */
    private static List<Method> sortTestsByOrder(List<Method> testsList) {
        List<Method> WithOrder;
        List<Method> WithoutOrder = new ArrayList<>();

        WithOrder = testsList.stream()
                .filter(method -> method.getAnnotation(DoTest.class).order() != 0)
                .sorted(Comparator.comparingInt(method -> {

                    int order = method.getAnnotation(DoTest.class).order();

                    if (order < 1)
                        throw new MyAnnotationException("Приоритет указывается только натуральными числами от 1 до N");

                    return order;
                }))
                .collect(Collectors.toList());

        testsList.stream()
                .filter(method -> method.getAnnotation(DoTest.class).order() == 0)
                .forEach(WithoutOrder::add);

        List<Method> end = new ArrayList<>();
        end.addAll(WithOrder);
        end.addAll(WithoutOrder);

        return end;
    }
}