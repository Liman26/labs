package solovyev;

import java.lang.annotation.*;

/**
 * Аннотация DoTest
 * К ней так же может быть указан приоритет выполнения.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface DoTest {
    int order() default 0;
}
