package solovyev;

import java.lang.annotation.*;

/**
 * Аннотация DoBeforeAll
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface DoBeforeAll {
}
