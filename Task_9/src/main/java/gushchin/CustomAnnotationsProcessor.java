package gushchin;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CustomAnnotationsProcessor {
    /**
     * Метод, выполнящий тесты над классом, у которого имеются нужные аннотации.
     * Порядок аннотаций и их количество:
     * Методов с аннотацией @DoBeforeAll - 1
     * Методов с аннотацией @DoTestAll - от 1 и более. Если у методов задано свойство order в аннотации
     * то будет учитываться их порядок при выполнении
     * Методов с аннотацией @DoAfterAll - 1
     * В случае, если количество методов с нужной аннотацией будет недостаточно,
     * будет создано исключение RuntimeException
     *
     * @param performingClass - класс с методами, помеченными аннотациями, для запуска тестов
     * @return Список результатов выполненных методов
     */
    public static List<Object> start(Class<?> performingClass) throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Method[] declaredMethods = performingClass.getDeclaredMethods();
        List<Method> doTestMethods = new ArrayList<>();
        List<Method> doBeforeAllMethods = new ArrayList<>();
        List<Method> doAfterAllMethods = new ArrayList<>();
        for (Method m : declaredMethods) {
            if (m.isAnnotationPresent(DoTest.class)) doTestMethods.add(m);
            if (m.isAnnotationPresent(DoAfterAll.class)) doAfterAllMethods.add(m);
            if (m.isAnnotationPresent(DoBeforeAll.class)) doBeforeAllMethods.add(m);
        }
        if (doTestMethods.size() < 1) {
            throw new RuntimeException("Количество методов с аннотацией @DoTest должно быть больше или равно 1");
        }
        if (doBeforeAllMethods.size() != 1) {
            throw new RuntimeException("Количество методов с аннотацией @DoBeforeAll должно быть равно 1");
        }
        if (doAfterAllMethods.size() != 1) {
            throw new RuntimeException("Количество методов с аннотацией @DoAfterAll должно быть равно 1");
        }
        ArrayList<Object> result = new ArrayList<>();
        result.addAll(run(doBeforeAllMethods, performingClass));
        result.addAll(run(doTestMethods
                .stream()
                .sorted(Comparator.comparingInt(prev -> prev.getAnnotation(DoTest.class).order()))
                .collect(Collectors.toList()), performingClass));
        result.addAll(run(doAfterAllMethods, performingClass));
        return result;
    }

    /**
     * Метод для запуска методов из коллекции
     *
     * @param methods         - коллекция выполняемых методов
     * @param performingClass - исполняемый класс
     * @return - список результатов выполнения методов
     */
    private static ArrayList<Object> run(Collection<Method> methods, Class<?> performingClass) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        ArrayList<Object> result = new ArrayList<>();
        for (Method m : methods) {
            m.setAccessible(true);
            result.add(m.invoke(performingClass.getDeclaredConstructor().newInstance()));
        }
        return result;
    }
}
