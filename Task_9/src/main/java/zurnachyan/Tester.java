package zurnachyan;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.lang.Class;
import java.util.stream.Collectors;

/**
 * Класс, реализующий обработчик аннотаций
 */
public class Tester {

    /**
     * Метод, обрабатывающий методы для тестов с аннотациями, выявляет правильный порядок
     * выполнения этих методов и выполняет их в этом порядке
     * @param className Имя класса, содержащего тестовые методы
     * @return Возвращает список объектов, необходимый для дальнейших тестов работы данного обработчика
     * @throws Exception может быть выброшено исключение, если класс с данным именем не найден,
     * либо класс содержит более одного метода @DoBeforeAll или @DoAfterAll
     */
    public static List<Object> start(String className) throws Exception {
        Object classForTest = Class.forName(className).getDeclaredConstructor().newInstance();
        Method[] methods = classForTest.getClass().getDeclaredMethods();

        Method before = null;
        Method after = null;
        List<Method> tests = new ArrayList<>();

        List<Object> results = new ArrayList<>();

        for (Method meth: methods) {
            if (meth.isAnnotationPresent(DoTest.class)) {
                tests.add(meth);
            }
            else if (meth.isAnnotationPresent(DoBeforeAll.class)) {
                if(before == null) {
                    before = meth;
                }
                else throw new TestException("More than one methods with @DoBeforeAll");
            }
            else if (meth.isAnnotationPresent(DoAfterAll.class)) {
                if(after == null) {
                    after = meth;
                }
                else throw new TestException("More than one methods with @DoAfterAll");
            }
        }

        tests = tests.stream().sorted(Comparator.comparingInt(x -> x.getAnnotation(DoTest.class)
                .order())).collect(Collectors.toList());

        if(before != null)
            results.add(before.invoke(classForTest));
        for (Method test: tests){
            results.add(test.invoke(classForTest));
        }
        if(after != null)
            results.add(after.invoke(classForTest));

        return results;
    }
}
