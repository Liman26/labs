package zurnachyan;

/**
 * Исключение, выбрасывается в случае, когда класс с тестами содержит более одного метода,
 * помеченного аннотацией @DoBeforeAll или @DoAfterAll
 */
public class TestException extends Exception{
    public TestException(String message) {
        super(message);
    }
}
