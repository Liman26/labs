package nuridinov;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Класс обработчик
 */
public class AnnotationProcessor {

    /**
     * Метод для тестирования
     *
     * @param testClass класс в котором есть наборы методов с аннотациями @DoTest/@DoBeforeAll/@DoAfterAll
     * @return список значений методов полученного на вход класса
     * @throws NoSuchMethodException исключение срабатывает когда не удается найти конкретный метод
     * @throws InvocationTargetException исключение метода к которому использовали .invoke
     * @throws InstantiationException исключенние срабатывает, когда указанный объект класса не может быть создан
     * @throws IllegalAccessException генерируется, когда приложение пытается рефлексивно вызвать метод, но текущий выполняющийся метод не имеет доступа к определению указанного класса, поля, метод или конструктора.
     */
    public static List<Object> runTest(Class<?> testClass) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        List<Method> methods = collector(testClass);
        List<Object> invokedMethods = new ArrayList<>();
        for (Method method : methods) {
            Object invoke = method.invoke(testClass.getDeclaredConstructor().newInstance());
            invokedMethods.add(invoke);
        }
        return invokedMethods;
    }

    /**
     * Метод который собирает упорядоченный список методов
     *
     * @param testClass класс методы которого нужно упорядочить согласно аннотациям
     * @return упорядоченный список методов
     */
    public static List<Method> collector(Class<?> testClass) {
        /*
          счетчики для проверки аннотаций @DoBeforeAll/@DoAfterAll, которые не могут встречатся более одного раза
         */
        int exceptionCounterBefore = 0;
        int exceptionCounterAfter = 0;
        List<Method> finalList = new ArrayList<>();
        List<Method> listOfDoTest;
        listOfDoTest = Stream.of(testClass.getDeclaredMethods()).filter(method -> method.isAnnotationPresent(DoTest.class)).collect(Collectors.toList());
        if (listOfDoTest.isEmpty())
            throw new MyException("Должен быть хотя бы один метод аннотацией @DoTest");
        List<Method> doTestOrdered = doTestOrderer(listOfDoTest);
        for (Method method : testClass.getDeclaredMethods()) {
            if (method.isAnnotationPresent(DoBeforeAll.class)) {
                finalList.add(0, method);
                exceptionCounterBefore++;
            }
            if (method.isAnnotationPresent(DoAfterAll.class)) {
                finalList.add(1, method);
                exceptionCounterAfter++;
            }
            if (exceptionCounterBefore > 1)
                throw new MyException("Методы с аннотациями @DoBeforeAll были использованы более одного раза!");
            if (exceptionCounterAfter > 1)
                throw new MyException("Методы с аннотациями @DoAfterAll были использованы более одного раза!");
        }
        finalList.addAll(1, doTestOrdered);
        return finalList;
    }

    /**
     * Упорядочитель списка методов с аннотациями @DoTest
     *
     * @param listOfDoTest список с аннотациями @DoTest
     * @return упорядоченный список с аннотациями @DoTest
     */
    public static List<Method> doTestOrderer(List<Method> listOfDoTest) {
        List<Method> orderedListOfDoTest = new ArrayList<>();
        listOfDoTest.stream().filter(method -> method.getAnnotation(DoTest.class).order() != 0)
                .sorted(Comparator.comparingInt(method -> {
                    int order = method.getAnnotation(DoTest.class).order();
                    if (order < 1)
                        throw new MyException("Приоритет должен быть не меньше 1!");
                    return order;
                })).forEach(orderedListOfDoTest::add);

        listOfDoTest.stream().filter(method -> method.getAnnotation(DoTest.class).order() == 0).forEach(orderedListOfDoTest::add);
        return orderedListOfDoTest;
    }
}
