package koleda;

import java.lang.annotation.*;

/**
 * Аннотация метода, который запускается последним
 */

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DoAfterAll {
}
