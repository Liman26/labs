package koleda;

import java.lang.annotation.*;

/**
 * Аннотация метода для тестов, которые запускаются по приоритету
 */

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DoTest {
    int order() default 0;
}
