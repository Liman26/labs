package koleda;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс, который выполняет тесты методов с аннотациями DoBeforeAll, DoAfterAll, DoTest, запуская их по приоритету
 */

public class Task9 {

    /**
     * Метод, который обрабатывает методы класса className с заданными аннотациями
     * @param className - название класса, содержащего тесты
     * @return результат тестирования
     * @throws ClassNotFoundException если класс не найден
     * @throws InvocationTargetException исключение, которое обертывает исключение при вызове метода
     * @throws IllegalAccessException есил вызванный метод не имеет необходимого уровня доступа
     */
    public static List<Object> start(String className) throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {
        Class<?> classTest = Class.forName(className);
        Method[] methods = classTest.getDeclaredMethods();


        List<Method> beforeMethod = annotationMethods(methods, DoBeforeAll.class),
                afterMethod = annotationMethods(methods, DoAfterAll.class),
                testsMethod = annotationMethods(methods, DoTest.class);

        exceptions(beforeMethod, afterMethod, testsMethod);

        ArrayList<Object> results = run(beforeMethod, classTest);
        results.addAll(run(testsMethod.stream().sorted((el1, el2) ->
                el2.getAnnotation(DoTest.class).order() - el1.getAnnotation(DoTest.class).order()
        ).collect(Collectors.toList()), classTest));
        results.addAll(run(afterMethod, classTest));
        return results;
    }

    /**
     * Получает список всех аннотированых методов
     * @param methods список всех методов класса
     * @param annotation класс аннотации
     * @return список методов, помеченных заданной аннотацией
     */
    private static List<Method> annotationMethods(Method[] methods, Class<? extends Annotation> annotation) {
        List<Method> list = new ArrayList<>();
        for (var method : methods)
            if (method.isAnnotationPresent(annotation)) {
                if (annotation == DoAfterAll.class)
                    list.add(method);
                else if (annotation == DoBeforeAll.class)
                    list.add(method);
                else if (annotation == DoTest.class)
                    list.add(method);
            }
        return list;
    }

    /**
     * Запускает методы класса classTest
     * @param methods выполненные методы
     * @param classTest вспомогательный класс
     * @return результат выполнения методов
     */
    public static ArrayList<Object> run(Collection<Method> methods, Class<?> classTest) {
        ArrayList<Object> results = new ArrayList<>();
        for (Method method : methods) {
            method.setAccessible(true);
            try {
                results.add(method.invoke(classTest.getDeclaredConstructor().newInstance()));
            } catch (IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return results;
    }

    /**
     * Реализация работы исключений
     * @param _doBeforeAll список методов с аннотацией doBeforeAll (не более одного)
     * @param _doAfterAll список методов с аннотацией doAfterAll (не более одного)
     * @param _doTest список методов с аннотацией doTest (не менее одного)
     */
    public static void exceptions(List<Method> _doBeforeAll, List<Method> _doAfterAll, List<Method> _doTest) {
        if (_doBeforeAll.size() > 1)
            throw new RuntimeException("Количество методов DoBeforeAll превышает 1");
        else if (_doAfterAll.size() > 1)
            throw new RuntimeException("Количество методов DoAfterAll превышает 1");
        else if (_doTest.size() < 1)
            throw new RuntimeException("Количество методов DoTest менее 1");
    }
}
