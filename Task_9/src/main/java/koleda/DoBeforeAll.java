package koleda;

import java.lang.annotation.*;

/**
 * Аннотация метода, который запускается первым
 */

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DoBeforeAll {
}
