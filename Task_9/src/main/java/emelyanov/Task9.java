package emelyanov;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class Task9 {
    /**
     * Метод анализирует Класс из параметра и запускает в начале метод с аннотацией @DoBeforeAll если такой имеется,
     * далее запускает методы с аннотациями @DoTest в порядке приоритете указанного в аннотации (чем больше тем раньше запустить),
     * а по завершению всех тестов – метод с аннотацией @DoAfterAll
     * Методов с аннотацией @DoTest  >=1
     * Методов с аннотацией @DoBeforeAll   0 или 1
     * Методов с аннотацией @DoAfterAll   0 или 1
     * Если что то не так то кидаем исключение
     *
     * @param className - имя класса
     * @return Список результатов выполненных методов
     */
    public static List<Object> start(String className) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<?> jobClass = Class.forName(className);
        Method[] declaredMethods = jobClass.getDeclaredMethods();
        List<Method> beforeAllMethods = new ArrayList<>();
        List<Method> afterAllMethods = new ArrayList<>();
        List<Method> doTestMethods = new ArrayList<>();
        Arrays.stream(declaredMethods).forEach(method -> {
            if (method.isAnnotationPresent(DoBeforeAll.class)) beforeAllMethods.add(method);
            if (method.isAnnotationPresent(DoAfterAll.class)) afterAllMethods.add(method);
            if (method.isAnnotationPresent(DoTest.class)) doTestMethods.add(method);
        });
        if (beforeAllMethods.size() > 1) {
            throw new RuntimeException("Количество методов BeforeAll не корректно");
        }
        if (afterAllMethods.size() > 1) {
            throw new RuntimeException("Количество методов AfterAll не корректно");
        }
        if (doTestMethods.size() < 1) {
            throw new RuntimeException("Количество методов DoTest не корректно");
        }
        ArrayList<Object> results = runMethod(beforeAllMethods, jobClass);
        results.addAll(runMethod(doTestMethods.stream().sorted((el1, el2) ->
                el2.getAnnotation(DoTest.class).order() - el1.getAnnotation(DoTest.class).order()
        ).collect(Collectors.toList()), jobClass));
        results.addAll(runMethod(afterAllMethods, jobClass));
        return results;
    }

    /**
     * Запускает методы methods у класса jobClass
     *
     * @param methods  - список выполняемых методов
     * @param jobClass - класс
     * @return список результатов выполнения методов
     */
    public static ArrayList<Object> runMethod(Collection<Method> methods, Class<?> jobClass) throws
            NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        ArrayList<Object> results = new ArrayList<>();
        for (Method method : methods) {
            method.setAccessible(true);
            results.add(method.invoke(jobClass.getDeclaredConstructor().newInstance()));
        }
        return results;
    }
}
