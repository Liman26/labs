package bachishche;

import java.lang.reflect.Method;
import java.util.*;

/**
 * Класс-тестировщик, проверяющий методы класса, отмеченные
 * аннотациями @DoBeforeAll, @DoAfterAll и @DoTest.
 */
class Tester {
    /**
     * Метод, тестирующий методы переданного класса, отмеченные
     * аннотациями @DoBeforeAll, @DoAfterAll и @DoTest.
     * Первым выполняется, если он есть, метод с аннотацией @DoBeforeAll.
     * Заетм выполняются все методы, отмеченные аннотацией @DoTest в порядке
     * возрастания параметра аннотации value(). В тестируемом методе
     * должен быть хотя бы один метод с аннотацией @DoTest.
     * Последним выполняется, если он есть, метод с аннотацией @DoAfterAll.
     *
     * @param testingClass тестируемый класс
     * @return результаты выполнения тестируемых методов
     */
    public static Object[] start(Class testingClass)  {
        ArrayList<Object> returnedValues = new ArrayList<>();
        HashMap<Class, List<Method>> methodsMap = separateMethods(Arrays.asList(testingClass.getDeclaredMethods()));
        try {
            var testingObject = testingClass.getConstructor().newInstance();
            Method singleMethod = checkSingleMethod(methodsMap.get(DoBeforeAll.class));

            if (singleMethod != null)
                returnedValues.add(singleMethod.invoke(testingObject));
            List<Method> testedMethod = methodsMap.get(DoTest.class);
            if (testedMethod == null)
                throw new IllegalArgumentException("Methods for testing are not specified!");
            else {
                for (Method method : testedMethod) {
                    method.setAccessible(true);
                    returnedValues.add(method.invoke(testingObject));
                }
            }
            singleMethod = checkSingleMethod(methodsMap.get(DoAfterAll.class));
            if (singleMethod != null)
                returnedValues.add(singleMethod.invoke(testingObject));

        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return returnedValues.toArray();
    }

    /**
     * Проверка списка на количество методов,
     * если методов больше, чем 1 - выдается исключение
     *
     * @param methods список методов класса
     * @return объект единственного метода в списке
     * или null, если список пуст
     * @throws IllegalArgumentException если список содержит больше одного метода
     */
    private static Method checkSingleMethod(List<Method> methods) {
        if (methods == null)
            return null;
        if (methods.size() > 1)
            throw new IllegalArgumentException("You can't use the \"DoBeforeAll\" and \"DoAfterAll\" " +
                    "annotations for more than one method");
        Method singleMethod = methods.get(0);
        singleMethod.setAccessible(true);
        return singleMethod;
    }

    /**
     * Преобразование списка методов в HashMap,
     * где ключ - это класс аннотации, значение - список методов,
     * которые используют эту аннотацию.
     * Методы с аннотацией DoTest в списке сортируются
     * по параметру аннотации value()
     *
     * @param allMethods список методов тестируемого класса
     * @return в HashMap, где ключ - это класс аннотации,
     * значение - список методов, которые используют эту аннотацию.
     */
    private static HashMap<Class, List<Method>> separateMethods(List<Method> allMethods) {
        HashMap<Class, List<Method>> methods = new HashMap<>();
        methods.put(DoTest.class, allMethods.stream()
                .filter(x -> x.isAnnotationPresent(DoTest.class))
                .sorted(Comparator.comparingInt(x -> x.getAnnotation(DoTest.class).value()))
                .toList());
        methods.put(DoAfterAll.class, allMethods.stream()
                .filter(x -> x.isAnnotationPresent(DoAfterAll.class))
                .toList());
        methods.put(DoBeforeAll.class, allMethods.stream()
                .filter(x -> x.isAnnotationPresent(DoBeforeAll.class))
                .toList());
        return methods;
    }
}