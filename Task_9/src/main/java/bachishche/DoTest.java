package bachishche;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация для метода, который необходимо протестировать.
 * Если методов с данной аннотацией несколько - они выполняются
 * в порядке возрастания поля value.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DoTest {
    int value();
}
