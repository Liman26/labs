package turkov;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Класс, который выполняет тестирование "класса-теста", выбирая его методы, имеющие аннотации DoBeforeAll, DoTest,
 * DoAfterAll и запуская их для тестирования в соответствии с порядком:
 * 1) метод с аннотацией DoBeforeAll (кол-во методов с данной аннотацией <= 1);
 * 2) методы с аннотациями DoTest в порядке их приоритета order. Если у нескольких аннотаций будет одинаковый приоритет,
 * то методы запустятся в алфавитном порядке своих имён (кол-во методов с данной аннотацией >= 1);
 * 3) метод с аннотацией DoAfterAll (кол-во методов с данной аннотацией <= 1);
 */
public class ProcessorOfAnnotations {

    /**
     * Метод, который для полученного класса создаёт список методов имеющих задекларированные аннотации,
     * сортирует методы с аннотацией DoTest, запускает все методы согласно логике.
     * @param testClass - класс, методы которого необходимо протестировать
     * @return - список строк в качестве результата работы тестируемых методов
     * @throws Exception - исключение бросается, если количество аннотаций DoTest = 0
     */
    public static ArrayList<String> start(Class<?> testClass) throws Exception {

        LinkedList<Method> tempMethodsClass = new LinkedList<>(Arrays.asList(null, null));
        LinkedList<Method> listMethodsClass = new LinkedList<>();
        ArrayList<String> resultMethodReturn = new ArrayList<>();

        for (Method method : testClass.getDeclaredMethods()) {
            listMethodsClass = getAnnotationOfMethod(method, tempMethodsClass);
            tempMethodsClass = listMethodsClass;
        }
        if (listMethodsClass.size() == 2)
            throw new Exception("The number of annotations DoTest = 0");

        listMethodsClass.subList(1, listMethodsClass.size() - 1)
                .sort(Comparator.comparingInt((Method method) ->
                        method.getAnnotation(DoTest.class).order()).thenComparing(Method::getName));
        listMethodsClass.stream().filter(Objects::nonNull).forEach(method -> {
            try {
                resultMethodReturn.add((String) method.invoke(null));
            } catch (InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        return resultMethodReturn;
    }

    /**
     * Процедура для обработки аннотаций метода тестируемого класса
     * @param method - проверяемый метод тестируемого класса
     * @param listMethods - список методов тестирумого класса, который был собран при прохождении по предыдущих методов
     * @return - список методов тестирумого класса расширенный после обработки текущего метода method
     * @throws Exception - исключение, которое бросается, если количество аннотаций DoBeforeAll, DoAfterAll > 1
     */
    public static LinkedList<Method> getAnnotationOfMethod(Method method, LinkedList<Method> listMethods) throws Exception {
        if (method.isAnnotationPresent(DoTest.class)) {
            listMethods.add(1, method);
        } else if (method.isAnnotationPresent(DoBeforeAll.class)) {
            if (listMethods.getFirst() == null) {
                listMethods.set(0, method);
            } else throw new Exception("The number of annotations DoBeforeAll > 1");
        } else if (method.isAnnotationPresent(DoAfterAll.class)) {
            if (listMethods.getLast() == null) {
                listMethods.set(listMethods.size() - 1, method);
            } else throw new Exception("The number of annotations DoAfterAll > 1");
        }
        return listMethods;
    }
}
