package mescheryakov;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс, выполнящий тестирование методов с аннотацией DoAfterAll, DoBeforeAll, DoTest, запуская их
 * для тестирования по приоритету.
 */
public class AnnotationsProcessor {

    private static final List<Method> doBeforeAll = new ArrayList<>();
    private static final List<Method> doAfterAll = new ArrayList<>();
    private static final List<Method> doTest = new ArrayList<>();

    /**
     * Из «класса-теста» вначале должен быть запущен метод с аннотацией @DoBeforeAll, если такой имеется,
     * далее запущены методы с аннотациями @DoTest, а по завершению всех тестов – метод с аннотацией @DoAfterAll.
     *
     * @param nameClass имя класса
     * @return возвращает результат выполненных методов
     */
    public static List<Object> start(String nameClass) throws ClassNotFoundException {
        Class<?> process = Class.forName(nameClass);
        Method[] declaredMethods = process.getDeclaredMethods();
        Arrays.stream(declaredMethods).forEach(method -> {
            if (!method.isAnnotationPresent(DoBeforeAll.class)) {
                if (method.isAnnotationPresent(DoAfterAll.class)) {
                    doAfterAll.add(method);
                } else if (method.isAnnotationPresent(DoTest.class)) doTest.add(method);
            } else {
                doBeforeAll.add(method);
            }
        });
        AnnotationsProcessor ex = new AnnotationsProcessor();
        ex.exceptions();
        ArrayList<Object> result = run(doBeforeAll, process);
        result.addAll(run(doTest.stream()
                .sorted(Comparator.comparingInt(n ->
                        n.getAnnotation(DoTest.class).order()))
                .collect(Collectors.toList()), process));
        result.addAll(run(doAfterAll, process));
        return result;

    }

    /**
     * Метод, который запускает методы класса process
     *
     * @param methods выполненные методы
     * @param process вспомогательный класс
     * @return возвращает результат выполненения методов
     */
    public static ArrayList<Object> run(Collection<Method> methods, Class<?> process) {
        ArrayList<Object> result = new ArrayList<>();
        for (Method method : methods) {
            method.setAccessible(true);
            try {
                result.add(method.invoke(process.getDeclaredConstructor().newInstance()));
            } catch (IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * Метод, реализующий работу исключений.
     * Условие: Методов с аннотацией @DoTest  >=1
     * Методов с аннотацией @DoBeforeAll   0 или 1
     * Методов с аннотацией @DoAfterAll   0 или 1
     * Если что-то не так, то кидаем исключение.
     */
    public void exceptions() {
        if (doBeforeAll.size() > 1) {
            try {
                throw new Exception("Методов DoBeforeAll более 1");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (doAfterAll.size() > 1) {
            try {
                throw new Exception("Методов DoAfterAll более 1");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (doTest.size() < 1) {
            try {
                throw new Exception("Методов DoTest менее 1");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
