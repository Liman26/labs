package troitskiy;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

/**
 * Вам нужно создать класс, который может выполнять тесты (по сути свой аналог junit библиотеки:)), в качестве тестов у нас также выступают классы с наборами методов над которыми есть аннотация @DoTest - но только теперь это должна быть ваша аннотация Test а не библиотечная).
 * Чтобы класс мог выполнять тесты у него должен быть статический метод start(), которому в качестве параметра передается или объект типа Class, или имя класса.
 *
 * «класса-теста» вначале должен быть запущен метод с аннотацией @DoBeforeAll (тоже ваша аннотация) если такой имеется, далее запущены методы с аннотациями @DoTest, а по завершению всех тестов – метод с аннотацией @DoAfterAll (тоже ваша аннотация).
 * Методов с аннотацией @DoTest  >=1
 * Методов с аннотацией @DoBeforeAll   0 или 1
 * Методов с аннотацией @DoAfterAll   0 или 1
 * Если что то не так то кидаем исключение

 * Сделать так, чтобы к каждому тесту можно было также добавить приоритеты (order) (int числа от 1 до N), в соответствии с которыми будет выбираться порядок их выполнения, если приоритет одинаковый то порядок не имеет значения. Методы с аннотациями @DoBeforeAll и @DoAfterAll должны присутствовать в единственном экземпляре, иначе необходимо бросить исключение при запуске «тестирования».
 *
 * Каким то способом нужно доказать что ваши аннотации работают правильно) через junit)
 *
 * об особенностях реализации
 * 1) Для того, чтобы метод был тестом, он должен быть:
 *           1) статическим
 *           2) без параметров
 *           3) возвращать boolean  -  успешно пройден этот тест или нет
 *           4) у него должна присутствовать ОДНА и только ОДНА из аннотаций @DoTest,@DoBeforeAll,@DoAfterAll
 * 2) В классе для теста должно выполняться
 *           1) Методов с аннотацией @DoTest  >=1
 *           2) Методов с аннотацией @DoBeforeAll   0 или 1
 *           3) Методов с аннотацией @DoAfterAll   0 или 1
 *
 *
 */
public class TTest
{
    /**
     * Служебный метод для запуска единичного теста
     * @param m - метод который необходимо запустить
     * @return - Результат выполнения (прошол тест или нет)
     */
    private static boolean RunOneTest(TTestMethod m)
    {
        Boolean f;
        String  result = "Test ";

        try {
             f = (Boolean) m.getTestMethod().invoke(null);
        } catch (IllegalAccessException | InvocationTargetException e) {
            f = false;
            result = result + " (Exception) ";
        }

        if (f) result = result + " OK    - ";
        else   result = result + " FAIL - ";

        System.out.println(result + m.toString());
        return f;
    }

    /**
     * Чтобы класс мог выполнять тесты у него должен быть статический метод start(),
     * которому в качестве параметра передается или объект типа Class, или имя класса.
     *
     * @param className -название класса для теста
     * @return - результат тестирования
     */
    public static boolean Start(String className) throws Exception {

        long doTestCount        = 0;
        long doBeforeAllCount   = 0;
        long doAfterAllCount    = 0;
        long countOk            = 0;
        long countError         = 0;

        TTestMethod[]  testMethods = Arrays.stream(Class.forName(className).getMethods()).map(TTestMethod::new)
                                                                                         .filter((m)-> (m.getTestMethod() != null))
                                                                                         .sorted()
                                                                                         .toArray(TTestMethod[]::new);


        for (TTestMethod m:testMethods)
            switch (m.getType()) {
                case (TTestMethod.DO_TEST)      : doTestCount++;        break;
                case (TTestMethod.DO_BEFORE_ALL): doBeforeAllCount++;   break;
                case (TTestMethod.DO_AFTER_ALL) : doAfterAllCount++;    break;
            }


        if (doTestCount >= 1 && doBeforeAllCount <= 1 && doAfterAllCount <= 1) {

            System.out.println("Run test");

            for (TTestMethod m:testMethods)
            {
                if (RunOneTest(m)) countOk++;
                else               countError++;
            }
        }
        else throw new Exception("Annotation error");

        System.out.println("Test result, OK = " + countOk + ", FAIL = " + countError);
        return countError == 0;
   }


    /**
     * Чтобы класс мог выполнять тесты у него должен быть статический метод start(),
     * которому в качестве параметра передается или объект типа Class, или имя класса.
     * @param c  - класс для тестирования
     * @return - результат тестирования
     */
    public static boolean Start(Class c) throws Exception {
        return Start(c.getName());
    }

}
