package troitskiy;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * Метод и его доп инфо, такая как приоритет выполнения и пр.
 * в процессе тестирования, если нет деклараций
 * для теста то место метода возвращает null
 */
public class TTestMethod implements Comparable<TTestMethod>
{
    public static final int DO_TEST       = 1;
    public static final int DO_BEFORE_ALL = 2;
    public static final int DO_AFTER_ALL  = 3;

    private final Method method;
    private int order;
    private boolean isTestMethod;
    private int type;

    /**
     * Конструктор
     * @param method -тестовый метод
     */
    public TTestMethod(Method method)
    {
        this.method       = method;
        this.isTestMethod = false;

        int kol = 0;

        Annotation[] annotations = method.getDeclaredAnnotations();
        for (Annotation a : annotations)
        {
            if (a instanceof DoTest)
            {
                this.order = ((DoTest) a).order();
                this.type  = DO_TEST;
                kol++;
            }

            if (a instanceof DoBeforeAll)
            {
                this.order = Integer.MIN_VALUE;
                this.type  = DO_BEFORE_ALL;
                kol++;
            }

            if (a instanceof DoAfterAll)
            {
                this.order = Integer.MAX_VALUE;
                this.type  = DO_AFTER_ALL;
                kol++;
            }

        }

        if (kol == 1) this.isTestMethod = true;
    }

    /**
     * Возвращает собственно метод, если он не
     * тестовый возвращает null
     * @return -метод
     */
    public Method getTestMethod()
    {
         if (isTestMethod) return method;
         else              return null;
    }

    /**
     * Возвращает тип метода
     * @return - DO_TEST = 1;
     *           DO_BEFORE_ALL = 2;
     *           DO_AFTER_ALL  = 3;
     */
    public int getType()
    {
        return type;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TTestMethod that = (TTestMethod) o;
        return order == that.order && method.equals(that.method);
    }

    @Override
    public int hashCode() {
        return Objects.hash(method, order);
    }

    @Override
    public int compareTo(TTestMethod o)
    {
        return Integer.compare(order, o.order);
    }

    @Override
    public String toString() {
        return "TTestMethod{" +
                "method=" + method +
                ", order=" + order +
                ", isTestMethod=" + isTestMethod +
                '}';
    }
}
