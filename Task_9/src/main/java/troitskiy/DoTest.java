package troitskiy;

import java.lang.annotation.*;

/**
 * в качестве тестов у нас также выступают классы
 * с наборами методов над которыми есть аннотация @DoTest
 * к каждому тесту можно было также добавить приоритеты
 * (order) (int числа от 1 до N), в соответствии с которыми
 * будет выбираться порядок их выполнения,
 * если приоритет одинаковый то порядок не имеет значения.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DoTest
{
    int order() default 0;
}

