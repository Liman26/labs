package troitskiy;

import java.lang.annotation.*;

/**
 * завершению всех тестов запускается
 * метод с аннотацией @DoAfterAll (тоже ваша аннотация).
 * Методов с аннотацией @DoAfterAll   0 или 1
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DoAfterAll
{
}
