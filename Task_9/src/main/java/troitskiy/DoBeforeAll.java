package troitskiy;

import java.lang.annotation.*;

/**
 * в качестве тестов у нас также выступают классы с наборами методов над которыми есть аннотация
 * вначале должен быть запущен метод с аннотацией @DoBeforeAll (тоже ваша аннотация) если такой имеется
 * Методов с аннотацией @DoBeforeAll   0 или 1
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DoBeforeAll
{
}
