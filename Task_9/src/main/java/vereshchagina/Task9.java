package vereshchagina;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Класс, реализующий обработку выполнения методов с аннотациями @DoAfterAll, @DoBeforeAll, @DoTest
 */
public class Task9 {
    /**
     * статический метод, которому в качестве параметра передается имя класса nameClass
     * метод проверяет класс на следуюие условия:
     * Методов с аннотацией @DoTest  >=1
     * Методов с аннотацией @DoBeforeAll   0 или 1
     * Методов с аннотацией @DoAfterAll   0 или 1
     * Если что то не так, кидает исключение, иначе формирует список объектов,
     * возвращаемых методами класса в порядке:
     * 1.сначала метод с аннотацией @DoBeforeAll, если такой имеется
     * 2.далее методы с аннотациями @DoTest в соответствии с приоритетами
     * 3.метод с аннотацией @DoAfterAll, если такой имеется.
     *
     * @param nameClass имя тестируемого класса
     * @return список объектов, возвращаемых методами тестируемого класса
     */
    public static List<Object> start(String nameClass) throws Exception {
        if (nameClass == null)
            throw new IllegalArgumentException();
        Class<?> aClass = Class.forName(nameClass);
        Method[] listMethods = aClass.getDeclaredMethods();
        List<Method> methDoBeforeAll = new ArrayList<>();
        List<Method> methDoTest = new ArrayList<>();
        List<Method> methDoAfterAll = new ArrayList<>();

        for (Method meth : listMethods) {
            if (meth.isAnnotationPresent(DoAfterAll.class))
                methDoAfterAll.add(meth);
            if (meth.isAnnotationPresent(DoBeforeAll.class))
                methDoBeforeAll.add(meth);
            if (meth.isAnnotationPresent(DoTest.class))
                methDoTest.add(meth);
        }

        if (methDoBeforeAll.size() > 1)
            throw new IllegalArgumentException("Error in the amount of methods with an annotation DoBeforeAll (>1)");
        if (methDoAfterAll.size() > 1)
            throw new IllegalArgumentException("Error in the amount of methods with an annotation DoAfterAll (>1)");
        if (methDoTest.size() < 1)
            throw new IllegalArgumentException("Error in the amount of methods with an annotation DoTest (<1)");

        List<Object> results = new ArrayList<>();

        if (methDoBeforeAll.size() == 1) {
            methDoBeforeAll.get(0).setAccessible(true);
            results.add(methDoBeforeAll.get(0).invoke(aClass.getDeclaredConstructor().newInstance()));
        }

        methDoTest.stream()
                .sorted(Comparator.comparingInt((meth) -> meth.getAnnotation(DoTest.class).order()))
                .forEach(meth -> {
                    meth.setAccessible(true);
                    try {
                        results.add(meth.invoke(aClass.getDeclaredConstructor().newInstance()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

        if (methDoAfterAll.size() == 1) {
            methDoAfterAll.get(0).setAccessible(true);
            results.add(methDoAfterAll.get(0).invoke(aClass.getDeclaredConstructor().newInstance()));
        }

        return results;
    }
}
